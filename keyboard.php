<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diana24</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="assets/css/diana.css">
    <link rel="stylesheet" href="assets/css/Profile-Card.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
<div class="profile-card height:500" style="height:700px;">
    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->
    <p class="profile-bio" style="/*padding:0px;*/color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;"><strong>Yun Controller Keyboard Setup</strong></p>
    <div class="row justify-content-center" id="rowKeyboard">
        <div class="col-auto col-xl-2" id="keyboardCol" style="background-color:white;border-radius:15px 0px 0px 15px;border:1px solid rgb(47,164,231);margin-right:1px; opacity:.9;">
            <div class="table-responsive" id="table" style="background-color:white; color:rgb(45,150,214);margin-top:150px;">
                <table class="table">
                    <tbody id="keyboardTable">
                    <tr>
                        <td>
                            <button class="btn btn-outline-info active btn-lg" type="button">0</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">1</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">2</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">3</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">4</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">5</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">6</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">7</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">8</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">9</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">A</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">B</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">C</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">D</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">E</button>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-lg" type="button">F</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8 col-xl-7" id="selecion" style="background-color:white;border-radius:0px 15px 15px 0px;opacity:.9;">
            <div class="row">
                <div class="col">
                    <h1 id="selectionKey" style="border:1px solid blue;border-radius:15px;margin:10px auto;background-color:rgb(160,192,217);width:500px;color:rgb(25,77,118);text-shadow:2px 2px white;">Selected Key &lt;0&gt;</h1>
                </div>
            </div>
            <div class="row" id="sceneRow">
                <div class="col" style="border:1px solid blue;border-radius:15px;margin:10px;background-color:rgb(31,103,155);"><label class="col-form-label" for="sceneSelector" style="color:white;">Activate scene</label><select name="scene" id="sceneSelector">
                        <option value="-1" selected="">none</option>
                        <option value="0">Scene n.0</option>
                        <option value="1">Scene n.1</option>
                        <option value="2">Scene n.2</option>
                        <option value="3">Scene n.3</option>
                        <option value="4">Scene n.4</option>
                        <option value="5">Scene n.5</option>
                        <option value="6">Scene n.6</option>
                        <option value="7">Scene n.7</option>
                        <option value="8">Scene n.8</option>
                        <option value="9">Scene n.9</option>
                        <option value="10">Scene n.10</option>
                        <option value="11">Scene n.11</option>
                        <option value="12">Scene n.12</option>
                        <option value="13">Scene n.13</option>
                        <option value="14">Scene n.14</option>
                        <option value="15">Scene n.15</option>
                    </select></div>
            </div>
            <div class="row" id="groupRow">
                <div class="col d-inline-block" style="border:1px solid blue; border-radius:15px; margin:10px;background-color:rgb(31,103,155);"><label class="col-form-label">Activate group</label><select id="sceneSelector">
                        <option value="-1" selected="">none</option>
                        <option value="0">Group n.0</option>
                        <option value="1">Group n.1</option>
                        <option value="2">Group n.2</option>
                        <option value="3">Group n.3</option>
                        <option value="4">Group n.4</option>
                        <option value="5">Group n.5</option>
                        <option value="6">Group n.6</option>
                        <option value="7">Group n.7</option>
                        <option value="8">Group n.8</option>
                        <option value="9">Group n.9</option>
                        <option value="10">Group n.10</option>
                        <option value="11">Group n.11</option>
                        <option value="12">Group n.12</option>
                        <option value="13">Group n.13</option>
                        <option value="14">Group n.14</option>
                        <option value="15">Group n.15</option>
                    </select></div>
            </div>
            <div class="row" id="daliCommandRow">
                <div class="col d-inline-block" style="border:1px solid blue; border-radius:15px; margin:10px;background-color:rgb(31,103,155);">
                    <div class="row">
                        <div class="col"><label class="col-form-label float-left" style="display:inline-block;">Dali Cmd</label><select class="form-control-sm float-left mx-auto" id="daliCommandSelector" style="display: inline-block; margin-top:2px;">
                                <option value="-1" selected="">none</option>
                                <option value="00">00 Extinguish the lamp without fading</option>
                                <option value="01">01 Dim up 200 ms using the selected fade rate</option>
                                <option value="02">02 Dim down 200 ms using the selected fade rate</option>
                                <option value="03">03 Set the actual arc power level one step higher without fading</option>
                                <option value="04">04 Set the actual arc power level one step lower without fading</option>
                                <option value="05">05 Set the actual arc power level to the maximum value</option>
                                <option value="06">06 Set the actual arc power level to the minimum value</option>
                                <option value="07">07 Set the actual arc power level one step lower without fading</option>
                                <option value="08">08 Set the actual arc power level one step higher without fading</option>
                                <option value="10">10+Scene Set the light level to the value stored for the selected scene</option>
                                <option value="20">20 Reset the parameters to default settings</option>
                                <option value="21">21 Store the current light level in the DTR</option>
                                <option value="2A">2A Store the value in the DTR as the maximum level</option>
                                <option value="2B">2B Store the value in the DTR as the minimum level</option>
                                <option value="2C">2C Store the value in the DTR as the system failure level</option>
                                <option value="2D">2D Store the value in the DTR as the power on level</option>
                                <option value="2E">2E Store the value in the DTR as the fade time</option>
                                <option value="2F">2F Store the value in the DTR as the fade rate</option>
                                <option value="40">40+Scene Store the value in the DTR as the selected scene</option>
                                <option value="50">50+Scene Remove the selected scene from the slave unit</option>
                                <option value="60">60+Group Add the slave unit to the selected group</option>
                                <option value="70">70+Group Remove the slave unit from the selected group</option>
                                <option value="80">80 Store the value in the DTR as a short address</option>
                                <option value="90">90 Returns the status of the slave as XX</option>
                                <option value="91">91 Check if the slave is working YES/NO</option>
                                <option value="92">92 Check if there is a lamp failure YES/NO</option>
                                <option value="93">93 Check if the lamp is operating YES/NO</option>
                                <option value="94">94 Check if the slave has received a level out of limit</option>
                                <option value="95">95 Check if the slave is in reset state YES/NO</option>
                                <option value="96">96 Check if the slave is missing a short address YES/NO</option>
                                <option value="97">97 Returns the version number as XX</option>
                                <option value="98">98 Returns the content of the DTR as XX</option>
                                <option value="99">99 Returns the device type as XX</option>
                                <option value="9A">9A Returns the physical minimum level as XX</option>
                                <option value="9B">9B Check if the slave is in power failure mode YES/NO</option>
                                <option value="A0">A0 Returns the current light level as XX</option>
                                <option value="A1">A1 Returns the maximum allowed light level as XX</option>
                                <option value="A2">A2 Returns the minimum allowed light level as XX</option>
                                <option value="A3">A3 Return the power up level as XX</option>
                                <option value="A4">A4 Returns the system failure level as XX</option>
                                <option value="A5">A5 Returns the fade time as X and the fade rate as Y</option>
                                <option value="B0">B0+Scene Returns the light level XX for the selected scene XX</option>
                                <option value="C0">C0 Returns a bit pattern XX indicating which group (0-7) the slave belongs</option>
                                <option value="C1">C1 Returns a bit pattern XX indicating which group (8-15) the slave belongs</option>
                                <option value="C2">C2 Returns the high bits of the random address as HH</option>
                                <option value="C3">C3 Return the middle bit of the random address as MM</option>
                                <option value="C4">C4 Returns the lower bits of the random address as LL</option>
                                <option value="SA1">SA1 00 All special mode processes shall be terminated</option>
                                <option value="SA3">SA3 XX Store value XX in the DTR</option>
                                <option value="SA5">SA5 XX Initialize addressing commands for slaves with address XX</option>
                                <option value="SA7">SA7 00 Generate a new random address</option>
                                <option value="SA9">SA9 00 Compare the random address with the search address</option>
                                <option value="SAB">SAB 00 Withdraw from the compare process</option>
                                <option value="SB1">SB1 HH Store value HH as the high bits of the search address</option>
                                <option value="SB3">SB3 MM Store value MM as the middle bits of the search address</option>
                                <option value="SB5">SB5 LL Store value LL as the lower bits of the search address</option>
                                <option value="SB7">SB7 XX Program the selected slave with short address XX</option>
                                <option value="SB9">SB9 XX Check if the selected slave has short address XX YES/NO</option>
                                <option value="SBB">SBB 00 The selected slave returns its short address XX</option>
                                <option value="SBD">SBD 00 Go into physical selection mode</option>
                                <option value="STMAX">STORE XX AS MAX LEVEL</option>
                                <option value="STMIN">STORE XX AS MIN LEVEL</option>
                                <option value="STFAIL">STORE XX AS SYSTEM FAILURE LEVEL</option>
                                <option value="STPOWON">STORE XX AS POWER ON LEVEL</option>
                                <option value="STFADET">STORE XX AS FADE TIME</option>
                                <option value="STFADER">STORE XX AS FADE RATE</option>
                            </select></div>
                    </div>
                    <div class="row">
                        <div class="col"><label class="col-form-label float-left">value</label><input class="float-left" type="number" name="daliCommandValue" id="daliCommandValue" style="width:80px;margin-left:28px;"></div>
                    </div>
                </div>
            </div>
            <div class="row" id="stepperRow">
                <div class="col d-inline-block" style="border:1px solid blue; border-radius:15px; margin:10px;background-color:rgb(31,103,155);"><label class="col-form-label">Stepper motor</label>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="stepperDirection" id="stepperDirection"><label class="form-check-label" for="formCheck-1" style="color:white;">Up</label></div>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="stepperDirection" id="stepperDirection"><label class="form-check-label" for="formCheck-1" style="color:white;">Down</label></div>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="stepperDirection" checked="" id="stepperDirection"><label class="form-check-label" for="formCheck-1" style="display:inline-block;color:white;">Idle</label></div>
                    <label class="col-form-label"
                           style="margin-left:50px;">Steps</label><input type="number"></div>
            </div>
            <div class="row" id="buzzerRow">
                <div class="col d-inline-block" style="border:1px solid blue; border-radius:15px; margin:10px;background-color:rgb(31,103,155);"><label class="col-form-label">Buzzer</label>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="buzzer" id="stepperDirection"><label class="form-check-label" for="formCheck-1" style="color:white;">On</label></div>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="buzzer" id="stepperDirection"><label class="form-check-label" for="formCheck-1" style="color:white;">Off</label></div>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="buzzer" checked="" id="stepperDirection"><label class="form-check-label" for="formCheck-1" style="color:white;">Unchanged</label></div>
                </div>
            </div>
            <div class="row" id="alertRow">
                <div class="col d-inline-block" style="border:1px solid blue; border-radius:15px; margin:10px;background-color:rgb(31,103,155);"><label class="col-form-label">Alert light</label>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="alertLight"><label class="form-check-label" for="formCheck-1" style="color:white;">On</label></div>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="alertLight" checked=""><label class="form-check-label" for="formCheck-1" style="color:white;">Off</label></div>
                    <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="alertLight"><label class="form-check-label" for="formCheck-1" style="color:white;">Flashing</label></div>
                </div>
            </div>
            <div class="btn-group" role="group">
                <button class="btn btn-primary active" type="submit" id="KeyboardBtnSave" style="width:80px;;">Save</button>
                <button class="btn btn-primary" type="reset" id="KeyboardBtnReset" style="width:80px;">Reset</button>
            </div>
        </div>
    </div>
</div>
<div></div>
<div id="divFooter"></div>
<nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
    <div class="container-fluid"><a class="navbar-brand bg-primary m-auto fixed-bottom" href="#" style="text-align:center;">Copyright Paolo Maruotti 2018</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
        <div class="collapse navbar-collapse"
             id="navcol-2"></div>
    </div>
</nav>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-checkbox.js"></script>
</body>

</html>