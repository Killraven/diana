<?php

	/**
	 * questa API richiede il numero del bottone premuto sulla tastiera arduino, ed esegue il comando configurato
	 */

	require_once __DIR__ . '/../class/arduino_yun_config.class.php';

	if (key_exists('btn', $_REQUEST)){
		// recuperiamo il comando
		$btn = strtolower($_REQUEST['btn']);

		if ($btn == 'a') $btn = 10;elseif ($btn == 'b') $btn = 11;
		elseif ($btn == 'c') $btn = 12;
		elseif ($btn == 'd') $btn = 13;
		elseif ($btn == '*') $btn = 14;
		elseif ($btn == '#') $btn = 15;

		$num_pulsante = $btn;
		$YUN = new ARDUINO_YUN_CONFIG();
		echo $YUN->esegui_comandi_pulsante($num_pulsante);
	}

?>