<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diana24</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="assets/css/diana.css">
    <link rel="stylesheet" href="assets/css/Profile-Card.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
<div class="profile-card height:500">
    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->
    <p class="profile-bio" style="/*padding:0px;*/color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;"><strong>Yun Controller Led Marix Setup</strong></p>
    <div></div>
    <div class="card" style="max-width:800px;margin:0 auto;">
        <div class="card-body">
            <h2 class="card-title" style="text-shadow:2px 2px black;">Assign colors</h2>
            <div class="row" id="idHeading" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8">
                    <h4>Device type</h4>
                </div>
                <div class="col-lg-1">
                    <h4>On</h4>
                </div>
                <div class="col-lg-1">
                    <h4>Off</h4>
                </div>
                <div class="col">
                    <h4>Failure</h4>
                </div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Fluoreshent lamp<br></span></div>
                <div class="col-lg-1"><input type="color" value="#33cc33"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Emercency lighting<br></span></div>
                <div class="col-lg-1"><input type="color" value="#ff9933"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#cc3300"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Discharge lamps<br></span></div>
                <div class="col-lg-1"><input type="color" value="#ffff00"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Supply voltage controller for incandescent lamps<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Conversion from digital signal into d.c. voltage<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">LED modules<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Switching function<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Colour control<br></span></div>
                <div class="col-lg-1"><input type="color" value="#cc66ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Sequencer<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Load referencing<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Thermal gear protecion<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#FF8040"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Dimming curve selection<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Centrally supplied DC Emergency Operation<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Load shedding<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Thermal lamp protection<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
            <div class="row" style="border:1px solid blue; margin:5px; border-radius:5px;">
                <div class="col-lg-8"><span style="font-size:110%;color:rgb(47,164,231);">Integrated light source<br></span></div>
                <div class="col-lg-1"><input type="color" value="#3366ff"></div>
                <div class="col-lg-1"><input type="color"></div>
                <div class="col"><input type="color" value="#ff0000"></div>
            </div>
        </div>
    </div>
</div>
<div id="divFooter"></div>
<nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
    <div class="container-fluid"><a class="navbar-brand bg-primary m-auto fixed-bottom" href="#" style="text-align:center;">Copyright Paolo Maruotti 2018</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
        <div class="collapse navbar-collapse"
             id="navcol-2"></div>
    </div>
</nav>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-checkbox.js"></script>
</body>

</html>