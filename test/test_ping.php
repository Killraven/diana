<?PHP

	function php_ping($host, $timeout = 1, $verbose = false){
		/**
		 * Return vždy vrací False ale pro příchod paketu ECHO_REPLAY
		 * jinak vrací čas odezvy PING. Tento script musí běžet
		 * pod ROOTem, jinak není možné přistupovat k RAW socketu!
		 *
		 * @param  string  hostname or IP address
		 * @param  int     timeout
		 * @param  bool    verbose output
		 *
		 * @return double
		 */

		# vytvoří síťový socket pro RAW zápis,
		# AF_INET => protokol IPV4
		# SOCK_RAW => poskytne možnost manuálně vytvořit libovolnou konstrukci paketu
		$socket = socket_create(AF_INET, SOCK_RAW, 1);

		# Nastaví Tmeout a připojí se k danému "hostu".
		socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $timeout, 'usec' => 0));
		socket_connect($socket, $host, null);

		# Hlavičk ECHO REQUERS v hex \x08
		$header = "\x08\x00\x7d\x4b\x00\x00\x00\x00PingHost";

		$start = microtime(true);
		socket_send($socket, $header, strLen($header), 0);

		if ($result = socket_read($socket, 255)){
			// Je odpoved Echo Replay? (ping odpovedel?)
			if (substr($result, 0, 1) == "E"){
				# Verbose? Zobraz víc info
				echo $verbose ? sprintf('%s bytes from %s time=%s ms', strLen($header), $host, ((100 * (microtime(true) - $start)))) : '';

				# Uzavře soket
				socket_close($socket);

				# Vrať čas
				return microtime(true) - $start;
			}else{
				//Odpoved přišla, ale né ECHO_REPLAY, tedy např. DEST UNRECHABLE atp.
				echo $verbose ? sprintf('ICMP Unrechable %s time=%s ms', $host, ((100 * (microtime(true) - $start)))) : '';

				socket_close($socket);
				return False;
			}
		}else{
			echo $verbose ? sprintf('ICMP Timeout %s time=%s ms', $host, ((100 * (microtime(true) - $start)))) : '';
			socket_close($socket);
			return False;
		}
	}

	php_ping('192.168.1.133', 1, true);

?>