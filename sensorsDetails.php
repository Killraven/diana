<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diana24</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="assets/css/diana.css">
    <link rel="stylesheet" href="assets/css/Profile-Card.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
<div class="profile-card" style="height:2400px;background-repeat:repeat;background-size:1128px 782px;">
    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->
    <p class="profile-bio" style="/*padding:0px;*/color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;"><strong>Yun Controller Sensor Setup</strong></p>
    <div class="row" id="idCommandBtn">
        <div class="col sceneRowClass" id="idSensorScenarioRow">
            <div style="background-color:white;border-radius:5px;display:inline-block;margin-top:2px;height:27px;"><label for="sceneSelector" id="idSceneSelLbl" class="blueTextClass" style="border-radius:15px; padding-left:20px;padding-right:10px;">Sensor scenario</label><span class="blueTextClass;"
                                                                                                                                                                                                                                                                                      style="background-color:orange;width:50px;display:inline-block;height:27px;font-weight:900;margin-right:20px;border-radius:9px;color:black;padding:0px;margin:auto;">&lt;0&gt;</span>
            </div>
            <button
                    class="btn btn-primary btn-sm" type="submit" id="idBtnLoadSensorScenario">Modify
            </button>
            <button class="btn btn-warning btn-sm" type="submit" id="idBtnLoadSensorScenario">Delete</button>
            <button class="btn btn-primary btn-sm" type="submit" disabled="disabled" id="idBtnLoadSensorScenario">Save</button>
            <button class="btn btn-secondary btn-sm"
                    type="reset" disabled="disabled" id="idBtnLoadSensorScenario">Reset
            </button>
        </div>
    </div>
    <hr>
    <div class="row rowGenClass" id="idSceneToBeActivated" style="margin:5px auto;max-width:1000px;border-radius:15px;border:1px solid rgb(45,150,214);">
        <div class="col sceneRowClass" style="margin:5px auto;max-width:1000px;border-radius:15px;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl">Sensor Scenario name</label><input type="text"></div>
    </div>
    <div class="row rowGenClass" id="idSceneToBeActivated" style="margin:5px auto;max-width:1000px;border-radius:15px;border:1px solid rgb(45,150,214);">
        <div class="col-lg-3 sceneRowClass" style="margin:5px auto;max-width:1000px;border-radius:15px;">
            <div class="form-check radioActive" style="padding-top:8px;"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
            <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
        </div>
        <div class="col sceneRowClass" style="margin:5px auto;max-width:1000px;border-radius:15px;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl">Scene to be activated</label><select name="scene" id="sceneSelector">
                <option value="-1" selected="">none</option>
                <option value="0">Scene n.0</option>
                <option value="1">Scene n.1</option>
                <option value="2">Scene n.2</option>
                <option value="3">Scene n.3</option>
                <option value="4">Scene n.4</option>
                <option value="5">Scene n.5</option>
                <option value="6">Scene n.6</option>
                <option value="7">Scene n.7</option>
                <option value="8">Scene n.8</option>
                <option value="9">Scene n.9</option>
                <option value="10">Scene n.10</option>
                <option value="11">Scene n.11</option>
                <option value="12">Scene n.12</option>
                <option value="13">Scene n.13</option>
                <option value="14">Scene n.14</option>
                <option value="15">Scene n.15</option>
            </select></div>
    </div>
    <div class="row rowGenClass" id="idSceneToBeActivated" style="margin:5px auto;max-width:1000px;border-radius:15px;border:1px solid rgb(45,150,214);">
        <div class="col sceneRowClass" style="margin:5px auto;max-width:1000px;border-radius:15px;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl">Dali Cmd</label><select class="form-control-sm mx-auto" id="daliCommandSelector" style="display: inline-block; margin-top:2px;">
                <option value="-1" selected="">none</option>
                <option value="00">00 Extinguish the lamp without fading</option>
                <option value="01">01 Dim up 200 ms using the selected fade rate</option>
                <option value="02">02 Dim down 200 ms using the selected fade rate</option>
                <option value="03">03 Set the actual arc power level one step higher without fading</option>
                <option value="04">04 Set the actual arc power level one step lower without fading</option>
                <option value="05">05 Set the actual arc power level to the maximum value</option>
                <option value="06">06 Set the actual arc power level to the minimum value</option>
                <option value="07">07 Set the actual arc power level one step lower without fading</option>
                <option value="08">08 Set the actual arc power level one step higher without fading</option>
                <option value="10">10+Scene Set the light level to the value stored for the selected scene</option>
                <option value="20">20 Reset the parameters to default settings</option>
                <option value="21">21 Store the current light level in the DTR</option>
                <option value="2A">2A Store the value in the DTR as the maximum level</option>
                <option value="2B">2B Store the value in the DTR as the minimum level</option>
                <option value="2C">2C Store the value in the DTR as the system failure level</option>
                <option value="2D">2D Store the value in the DTR as the power on level</option>
                <option value="2E">2E Store the value in the DTR as the fade time</option>
                <option value="2F">2F Store the value in the DTR as the fade rate</option>
                <option value="40">40+Scene Store the value in the DTR as the selected scene</option>
                <option value="50">50+Scene Remove the selected scene from the slave unit</option>
                <option value="60">60+Group Add the slave unit to the selected group</option>
                <option value="70">70+Group Remove the slave unit from the selected group</option>
                <option value="80">80 Store the value in the DTR as a short address</option>
                <option value="90">90 Returns the status of the slave as XX</option>
                <option value="91">91 Check if the slave is working YES/NO</option>
                <option value="92">92 Check if there is a lamp failure YES/NO</option>
                <option value="93">93 Check if the lamp is operating YES/NO</option>
                <option value="94">94 Check if the slave has received a level out of limit</option>
                <option value="95">95 Check if the slave is in reset state YES/NO</option>
                <option value="96">96 Check if the slave is missing a short address YES/NO</option>
                <option value="97">97 Returns the version number as XX</option>
                <option value="98">98 Returns the content of the DTR as XX</option>
                <option value="99">99 Returns the device type as XX</option>
                <option value="9A">9A Returns the physical minimum level as XX</option>
                <option value="9B">9B Check if the slave is in power failure mode YES/NO</option>
                <option value="A0">A0 Returns the current light level as XX</option>
                <option value="A1">A1 Returns the maximum allowed light level as XX</option>
                <option value="A2">A2 Returns the minimum allowed light level as XX</option>
                <option value="A3">A3 Return the power up level as XX</option>
                <option value="A4">A4 Returns the system failure level as XX</option>
                <option value="A5">A5 Returns the fade time as X and the fade rate as Y</option>
                <option value="B0">B0+Scene Returns the light level XX for the selected scene XX</option>
                <option value="C0">C0 Returns a bit pattern XX indicating which group (0-7) the slave belongs</option>
                <option value="C1">C1 Returns a bit pattern XX indicating which group (8-15) the slave belongs</option>
                <option value="C2">C2 Returns the high bits of the random address as HH</option>
                <option value="C3">C3 Return the middle bit of the random address as MM</option>
                <option value="C4">C4 Returns the lower bits of the random address as LL</option>
                <option value="SA1">SA1 00 All special mode processes shall be terminated</option>
                <option value="SA3">SA3 XX Store value XX in the DTR</option>
                <option value="SA5">SA5 XX Initialize addressing commands for slaves with address XX</option>
                <option value="SA7">SA7 00 Generate a new random address</option>
                <option value="SA9">SA9 00 Compare the random address with the search address</option>
                <option value="SAB">SAB 00 Withdraw from the compare process</option>
                <option value="SB1">SB1 HH Store value HH as the high bits of the search address</option>
                <option value="SB3">SB3 MM Store value MM as the middle bits of the search address</option>
                <option value="SB5">SB5 LL Store value LL as the lower bits of the search address</option>
                <option value="SB7">SB7 XX Program the selected slave with short address XX</option>
                <option value="SB9">SB9 XX Check if the selected slave has short address XX YES/NO</option>
                <option value="SBB">SBB 00 The selected slave returns its short address XX</option>
                <option value="SBD">SBD 00 Go into physical selection mode</option>
                <option value="STMAX">STORE XX AS MAX LEVEL</option>
                <option value="STMIN">STORE XX AS MIN LEVEL</option>
                <option value="STFAIL">STORE XX AS SYSTEM FAILURE LEVEL</option>
                <option value="STPOWON">STORE XX AS POWER ON LEVEL</option>
                <option value="STFADET">STORE XX AS FADE TIME</option>
                <option value="STFADER">STORE XX AS FADE RATE</option>
            </select>
            <label
                    class="col-form-label" for="sceneSelector" id="idSceneSelLbl">&nbsp; value</label><input type="number"></div>
    </div>
    <div class="row" id="IdRowTimeControl" style="border:1px solid rgb(45,150,214); margin: 5px auto; max-width:1000px;border-radius:15px;">
        <div class="col-lg-3 rowGenClass" style="border-radius: 15px 0px 0px 15px;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl">Time controls</label>
            <div class="timeControlClass">
                <div class="form-group">
                    <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                    <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-lg-6 rowGenClass" style="padding-top:20px;"><label class="col-form-label rowGenClass" style="color: rgb(45,150,214);padding-right: 5px;">From</label><input type="time" style="width:80px;"><label class="col-form-label rowGenClass"
                                                                                                                                                                                                                                    style="color: rgb(45,150,214);padding-right: 5px;margin-left:35px;">To</label>
            <input
                    type="time" style="width:80px;"></div>
        <div class="col-lg-3 rowGenClass" style="border-radius: 0px 15px 15px 0px;"><input type="checkbox"><label class="col-form-label rowGenClass" style="color: rgb(45,150,214);padding-right: 5px;">Mo</label><input type="checkbox"><label class="col-form-label rowGenClass"
                                                                                                                                                                                                                                                style="color: rgb(45,150,214);padding-right: 5px;">Tu</label>
            <input
                    type="checkbox"><label class="col-form-label rowGenClass" style="color: rgb(45,150,214);padding-right: 5px;">We</label><input type="checkbox"><label class="col-form-label rowGenClass" style="color: rgb(45,150,214);padding-right: 5px;">Th</label><input type="checkbox">
            <label
                    class="col-form-label rowGenClass" style="color: rgb(45,150,214);padding-right: 5px;">Fr</label>
            <div></div>
            <input type="checkbox" style="margin-left:10px;"><label class="col-form-label rowGenClass" style="color: red;padding-right: 5px;">Sa</label><input type="checkbox"><label class="col-form-label rowGenClass" style="color: red">Su</label>
            <input
                    type="checkbox"><label class="col-form-label rowGenClass" style="color: rgb(45,150,214);font-weight:900;">Every day</label></div>
    </div>
    <div class="row" id="IdRowActuatorControl" style="border:1px solid rgb(45,150,214); margin: 5px auto; max-width:1000px;border-radius:15px;">
        <div class="col-lg-3 rowGenClass" style="border-radius: 15px 0px 0px 15px;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl">Actuator controls</label>
            <div class="timeControlClass">
                <div class="form-group">
                    <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                    <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-lg-6 rowGenClass"><label class="col-form-label" style="color:rgb(45,150,214);">Buzzer</label>
            <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="buzzer" id="stepperDirection"><label class="form-check-label" for="formCheck-1">On</label></div>
            <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="buzzer" id="stepperDirection"><label class="form-check-label" for="formCheck-1">Off</label></div>
            <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="buzzer" checked="" id="stepperDirection"><label class="form-check-label" for="formCheck-1">Unchanged</label></div>
            <label class="col-form-label" style="margin-left:30px;color:rgb(45,150,214);">Dur (s)</label>
            <input
                    type="number" name="numberOfSteps" value="0" placeholder="insert n. of steps" min="-50" max="50" step="1">
            <div></div>
            <label class="col-form-label" style="color:rgb(45,150,214);">Stepper</label>
            <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="stepperDirection" id="stepperDirection"><label class="form-check-label" for="formCheck-1">Up</label></div>
            <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="stepperDirection" id="stepperDirection"><label class="form-check-label" for="formCheck-1">Down</label></div>
            <div class="form-check d-inline-block"><input class="form-check-input" type="radio" name="stepperDirection" checked="" id="stepperDirection"><label class="form-check-label" for="formCheck-1">Idle</label></div>
            <label class="col-form-label" style="margin-left:30px;color:rgb(45,150,214);">Steps</label>
            <input
                    type="number" name="numberOfSteps" value="0" placeholder="insert n. of steps" min="-50" max="50" step="1"></div>
        <div class="col-lg-3 rowGenClass" style="border-radius: 0px 15px 15px 0px;"><input type="checkbox"><label class="col-form-label" style="color:rgb(45,150,214);">Red Led</label><input type="checkbox"><label class="col-form-label" style="color:rgb(45,150,214);">Green Led</label>
            <div></div>
            <input type="checkbox"><label class="col-form-label" style="margin-right:0px;padding-right:14px;color:rgb(45,150,214);">Yellow Led</label><input type="checkbox"><label class="col-form-label" style="color:rgb(45,150,214);">Blue Led</label></div>
    </div>
    <div class="row justify-content-center" id="idRowPirPhotoTemp">
        <div class="col">
            <div class="card-group">
                <div class="card" id="idPirCard"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/pir1.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Presence detector</h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Motion detected</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">No motion</label></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="idPhotoRCard"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/phoresistor1.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Light threshold</h4>
                        <div class="cardHSeparator"></div>
                        <h5 class="card-title">Photoresistor #1</h5>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPhotoR1Active"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                        <div
                                class="cardHSeparator"></div>
                        <h5 class="card-title">Photoresistor #2</h5>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPhotoR1Active"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card sensorCardClass" id="idTempCard"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/dht11.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Temp &amp; Humidity</h4>
                        <div class="cardHSeparator"></div>
                        <h5 class="card-title">Temperature</h5>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:75px;">T Min C°</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:75px;">T Max C°</label><input type="number" value="100" min="0"
                                                                                                                                                                                                                                                          max="100" class="minMaxClass"></div>
                        <div class="cardHSeparator"></div>
                        <h5 class="card-title">Humidity</h5>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:75px;">H Min %</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:75px;">H Max %</label><input type="number" value="100" min="0"
                                                                                                                                                                                                                                                        max="100" class="minMaxClass"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="idRowDistSoilPower">
        <div class="col">
            <div class="card-group">
                <div class="card" id="idPirCard"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/ultrasonic.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Distance monitor</h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:70px;;">Min cm</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:70px;;">Max cm</label><input type="number" value="100" min="0"
                                                                                                                                                                                                                                                        max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card" id="idPhotoRCard"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/soil.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Soil Humidity threshold</h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPhotoR1Active"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min %</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max %</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card sensorCardClass" id="idTempCard"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/power.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Power loss</h4>
                        <div class="cardHSeparator"></div>
                        <h5 class="card-title">Voltage</h5>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:75px;">Min V</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:75px;">Max V</label><input type="number" value="100" min="0"
                                                                                                                                                                                                                                                    max="100" class="minMaxClass"></div>
                        <div class="cardHSeparator"></div>
                        <h5 class="card-title">Ampere</h5>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:75px;">Min A</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:75px;">Max A</label><input type="number" value="100" min="0"
                                                                                                                                                                                                                                                    max="100" class="minMaxClass"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="rowGasSensor">
        <div class="col">
            <div class="card-group cardGasSensorClass">
                <div class="card" id="idPirCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/mq-2r.jpg">
                    <div class="card-body">
                        <h4 class="card-title">General combustile gas</h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card" id="idPhotoRCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/mq-3r.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Alcohol<br></h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPhotoR1Active"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card sensorCardClass" id="idTempCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/mq-4r.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Natural gas, Methane<br></h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:75px;">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:75px;">Max</label><input type="number" value="100" min="0" max="100"
                                                                                                                                                                                                                                                class="minMaxClass"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="rowGasSensor">
        <div class="col">
            <div class="card-group cardGasSensorClass">
                <div class="card" id="idPirCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/mq-7r.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Carbon Monoxide (CO)<br></h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card" id="idPhotoRCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/mq-9r.jpg">
                    <div class="card-body">
                        <h4 class="card-title" style="font-size:16px;">Carbon Monoxide, Coal Gas,&nbsp;<br>Liquefied Gas<br></h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPhotoR1Active"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card sensorCardClass" id="idTempCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/mq-135r.jpg">
                    <div class="card-body">
                        <h6 class="card-title">Air Quality Control (NH3, Benzene, Alcohol, smoke)<br></h6>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass" style="width:75px;">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass" style="width:75px;">Max</label><input type="number" value="100" min="0" max="100"
                                                                                                                                                                                                                                                class="minMaxClass"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="rowGasSensor">
        <div class="col">
            <div class="card-group cardGasSensorClass">
                <div class="card" id="idPirCard" style="height:270px;"><img class="img-fluid card-img-top w-100 d-block" src="assets/img/waterLevelr.jpg">
                    <div class="card-body">
                        <h4 class="card-title">Water level<br></h4>
                        <div class="cardHSeparator"></div>
                        <div class="divActiveSeparator">
                            <div class="form-group">
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Active</label></div>
                                <div class="form-check radioActive"><input class="form-check-input activeClass" type="radio" value="pirActive" id="idPIRActive"><label class="form-check-label activeClass" for="formCheck-1">Disabled</label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="minMaxClass">Min</label><input type="number" value="0" min="0" max="100" class="minMaxClass"><label class="minMaxClass">Max</label><input type="number" value="100" min="0" max="100" class="minMaxClass"></div>
                    </div>
                </div>
                <div class="card" id="idPhotoRCard" style="height:270px;">
                    <div class="card-body"></div>
                </div>
                <div class="card sensorCardClass" id="idTempCard" style="height:270px;">
                    <div class="card-body"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divFooter"></div>
<nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
    <div class="container-fluid"><a class="navbar-brand bg-primary m-auto fixed-bottom" href="#" style="text-align:center;">Copyright Paolo Maruotti 2018</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
        <div class="collapse navbar-collapse"
             id="navcol-2"></div>
    </div>
</nav>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-checkbox.js"></script>
</body>

</html>