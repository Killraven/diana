#include <Adafruit_BMP280.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include "Adafruit_MCP23017.h"

//KeypadMCP23017 --- START
#include <Keypad_MC17.h>
#include <Keypad.h>        // GDY120705 

//// debug output (enable/disable)
//#define DEBUG_MODE
//#ifdef DEBUG_MODE
//#define DEBUG_PRINT(x)  Serial.print(x)
//#define DEBUG_PRINTLN(x)  Serial.println(x)
//#else
//#define DEBUG_PRINT(x)
//#define DEBUG_PRINTLN(x)
//#endif

bool serrandaAperta = false;

#define HYGROMETER_PIN A2
int hygrometerValue = 0;

//INIZIO STEP -----------------------------------------------------------------
#define SER_Pin   7   //pin 14 on the 75HC595
#define RCLK_Pin  8   //pin 12 on the 75HC595
#define SRCLK_Pin 9   //pin 11 on the 75HC595
//How many of the shift registers - change this
#define number_of_74hc595s 1
//do not touch
#define numOfRegisterPins number_of_74hc595s * 8
boolean registers[numOfRegisterPins];

//declare variables for the motor pins
const int motorPin1 = 0;  // Blue   - 28BYJ48 pin 1
const int motorPin2 = 1;  // Pink   - 28BYJ48 pin 2
const int motorPin3 = 2;  // Yellow - 28BYJ48 pin 3
const int motorPin4 = 3;  // Orange - 28BYJ48 pin 4
// Red    - 28BYJ48 pin 5 (VCC)
const int motorSpeed = 1;     //variable to set stepper speed

//FINE   STEP -----------------------------------------------------------------

//---------------------------------
#include <FastLED.h>
#define LED_PIN     11 //13
#define NUM_LEDS    64
#define BRIGHTNESS  20
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
#define UPDATES_PER_SECOND 100
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;
#define FULLRED 0
#define FULLGREEN 60
#define WHITE     31
#define White100  255
#define White90   190
#define White80   145
#define White70   105
#define White60   90
#define White50   70
#define White40   55
#define White30   49
#define White20   45
#define White10   40
#define White0    FULLGREEN

void luciPerimetro(int lumen = White60, boolean fill = true) {
  //Serial.print("luciPerimetro (");Serial.print(lumen);Serial.print(",");Serial.print(fill);Serial.print(")");
  for (int row = 0; row < 8; row++) {
    for ( int col = 0; col < 8; col++) {
      int colDecoded = col;
      if ( row % 2 != 0 ) colDecoded = 7 - col;
      //Serial.print(row*8+col);Serial.print("["); Serial.print(row); Serial.print(","); Serial.print(col); Serial.print("]->"); Serial.print(row*8+colDecoded);Serial.print("["); Serial.print(row); Serial.print(","); Serial.print(colDecoded); Serial.print("]");
      if (fill) leds[row * 8 + colDecoded] = ColorFromPalette( myRedWhiteBluePalette_p, FULLGREEN, 80, LINEARBLEND);
      if ( (row == 0 || row == 7) || (col == 0 || col == 7) ) {
        leds[row * 8 + colDecoded] = ColorFromPalette( myRedWhiteBluePalette_p, WHITE, lumen, LINEARBLEND);
        //Serial.print("White50");
      }
      //Serial.println("");
    }
  }
  FastLED.show();
}

void luciAlternate(int lumen = White60, boolean fill = true) {
  //Serial.print("luciAlternate (");Serial.print(lumen);Serial.print(",");Serial.print(fill);Serial.print(")");
  for ( int i = 0; i < NUM_LEDS; i += 2) {
    leds[i] = ColorFromPalette( myRedWhiteBluePalette_p, WHITE, lumen, LINEARBLEND);
    if (fill) leds[i + 1] = ColorFromPalette( myRedWhiteBluePalette_p, FULLGREEN, 80, LINEARBLEND);
  }
  FastLED.show();
}

void luciPiene(int lumen = White60, boolean fill = true) {
  //Serial.print("luciPiene (");Serial.print(lumen);Serial.print(",");Serial.print(fill);Serial.print(")");
  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette( myRedWhiteBluePalette_p, WHITE, lumen, LINEARBLEND);
  }
  FastLED.show();
}

void luciSfumate(int lumen = 32, boolean reverse = false) {
  //Serial.print("luciSxSfumate (");Serial.print(lumen);Serial.print(",");Serial.print(fill);Serial.print(")");
  for (int row = 0; row < 8; row++) {
    for ( int col = 0; col < 8; col++) {
      int colDecoded = col;
      if ( row % 2 != 0 ) colDecoded = 7 - col;
      //Serial.print(row*8+col);Serial.print(">["); Serial.print(row); Serial.print(","); Serial.print(col); Serial.print("]->");
      //Serial.print(row*8+colDecoded);Serial.print("["); Serial.print(row); Serial.print(","); Serial.print(colDecoded); Serial.print("]=");
      //Serial.println(lumen*col);
        int luminosita=lumen*col;
        if (luminosita > 255) luminosita=255;
        if(reverse)luminosita = 255 -luminosita;  
        leds[row * 8 + colDecoded] = ColorFromPalette( myRedWhiteBluePalette_p, WHITE, luminosita, LINEARBLEND);
    }
  }
  FastLED.show();
}

//----------------------------------

#define I2C_UNO_ADDR_H 0x20
#define I2C_UNO_ADDR_I 0
#define I2C_DUE_ADDR_H 0x21
#define I2C_DUE_ADDR_I 1
Adafruit_MCP23017 mcp_UNO;
Adafruit_MCP23017 mcp_DUE;

#define LCD_UNO_ADDR  0x26
#define LCD_DUE_ADDR  0x27
#define LCD_TRE_ADDR  0x3F

const byte ROWS = 4; //four rows
const byte COLS = 4; //five columns
//define the cymbols on the buttons of the keypads
const char hexaKeys[ROWS][COLS] = {
  {'0', '1', '2', '3'},
  {'4', '5', '6', '7'},
  {'8', '9', 'A', 'B'},
  {'C', 'D', 'E', 'F'}
};
const byte rowPins[ROWS] = {8, 9, 10, 11}; //connect to the row pinouts of the keypad
const byte colPins[COLS] = {12, 13, 14, 15}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad_MC17 customKeypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS, I2C_UNO_ADDR_H);
//KeypadMCP23017 --- END

//RTC
#include <DS3231.h>
DS3231 clock;
RTCDateTime dt;

// DHT Sensore di temperatura e umidita'
#include <DHT.h>
#include <DHT_U.h>
#define PIN_INGRESSO_DHT  10             // Pin di ingresso del sensore

// Nelle seguenti define è possibile definire lo specifico tipo di sensore della serie DTH
#define TIPOSENSOREDHT           DHT11     // DHT 11 
//#define TIPOSENSOREDHT           DHT22     // DHT 22 (AM2302)
//#define TIPOSENSOREDHT           DHT21     // DHT 21 (AM2301)
DHT_Unified dht(PIN_INGRESSO_DHT, TIPOSENSOREDHT);

const uint32_t attesaInizialeMS = 2000; // sono 2000ms=2s
const uint32_t attesaTraLettureMS;  // tempo di attesa tra una lettura e la successiva, si

#define TRIGPIN 12
#define ECHOPIN 13

// utilizzerà il valore minimo dello specifico sensore

LiquidCrystal_I2C lcd(LCD_UNO_ADDR, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd2(LCD_DUE_ADDR, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd3(LCD_TRE_ADDR, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

Adafruit_BMP280 bmp;

#define BUZZERPIN 3 //pin sull'MCP23017 con indirizzo 0x21 I2C_DUE_ADDR_I

const int iPIRLedPinRED   = 1 ;//5;
const int iPIRLedPinGREEN = 0 ;//4;
const int iPIRInputPin    = 2 ;//6;
int iPIRState = LOW;
int iPIRCurrentState = 0;
int x = 0;
const int iPIRLEDBusAddr = I2C_UNO_ADDR_I; //indirizzo del bus I2C

int lightLevel;
int sensoreLuce1 = 0;
int sensoreLuce2 = 0;

int lastDistance = 0;

// This example shows how to set up a static color palette
// which is stored in PROGMEM (flash), which is almost always more
// plentiful than RAM.  A static PROGMEM palette like this
// takes up 64 bytes of flash.
const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
  CRGB::Red,
  CRGB::Black,
  CRGB::Blue, // 'white' is too bright compared to red and blue
  CRGB::Black,

  CRGB::Green,
  CRGB::Black,
  CRGB::Red,
  CRGB::Blue, // 'white' is too bright compared to red and blue

  CRGB::Black,
  CRGB::Green,
  CRGB::Red,
  CRGB::Blue, // 'white' is too bright compared to red and blue

  CRGB::Black,
  CRGB::Green,
  CRGB::Red,
  CRGB::Blue // 'white' is too bright compared to red and blue

};

//===========================================================================================================================================================================
//                                                             R E A D K E Y B O A R D
//===========================================================================================================================================================================
//#define KEYBOARD_ANALOG_PIN A5
//char* keypressed;
//int keyboardPin = 0;    // Analog input pin that the keypad is attached to
//int keyboardValue = 0;   // value read from the keyboard
//
//void readkeyboard(){
//   keyboardValue = analogRead((KEYBOARD_ANALOG_PIN)); // read the value (0-1023)
//   Serial.print("Pin value:");
//   Serial.println( keyboardValue);
//
//  if ((keyboardValue >270) && (keyboardValue < 274)){keypressed = "3";}
//  if ((keyboardValue >288) && (keyboardValue < 315)){keypressed = "1";}
//  if ((keyboardValue >316) && (keyboardValue < 319)){keypressed = "6";}
//  if ((keyboardValue >330) && (keyboardValue < 333)){keypressed = "9";}
//  if ((keyboardValue >344) && (keyboardValue < 347)){keypressed = "#";}
//  if ((keyboardValue >355) && (keyboardValue < 376)){keypressed = "4";}
//  if ((keyboardValue >379) && (keyboardValue < 394)){keypressed = "7";}
//  if ((keyboardValue >397) && (keyboardValue < 402)){keypressed = "2";}
//  if ((keyboardValue >405) && (keyboardValue < 425)){keypressed = "*";}
//  if ((keyboardValue >502) && (keyboardValue < 505)){keypressed = "A";}
//  if ((keyboardValue >510) && (keyboardValue < 513)){keypressed = "5";}
//  if ((keyboardValue >551) && (keyboardValue < 554)){keypressed = "8";}
//  if ((keyboardValue >592) && (keyboardValue < 596)){keypressed = "0";}
//  if ((keyboardValue >699) && (keyboardValue < 703)){keypressed = "B";}
//  if ((keyboardValue >778) && (keyboardValue < 783)){keypressed = "C";}
//  if ((keyboardValue >867) && (keyboardValue < 871)){keypressed = "D";}
//   if (keyboardValue >=871){keypressed = 0;}
////  //NOTE: the values used above are all halfway between the value obtained with each keypress in previous test sketch
//
//   while (keyboardValue > 25) {
//     keyboardValue = analogRead((KEYBOARD_ANALOG_PIN)); // read the value (0-1023)
//   }//wait until key no longer being pressed before continuing
//
//   Serial.println(keypressed);      // print the value back to the Serial view window on your PC
//   delay(50);                     // wait 1000 milliseconds before the next loop
//}

//===========================================================================================================================================================================
//                                                                 P R I N T _ T I M E
//===========================================================================================================================================================================
void print_time()
{
  /* Get the current time and date from the chip */
  dt = clock.getDateTime();
  lcd2.setCursor(0, 0);
  lcd2.print(clock.dateFormat("d-m-y", dt));
  lcd2.setCursor(0, 1);
  lcd2.print(clock.dateFormat("H:i:s", dt));

  lcd2.setCursor(10, 0);
  lcd2.print("L1:"); lcd2.print(sensoreLuce1); lcd2.print("   ");
  lcd2.setCursor(10, 1);
  lcd2.print("L2:"); lcd2.print(sensoreLuce2); lcd2.print("   ");
}

//===========================================================================================================================================================================
//                                                                               S E T U P
//===========================================================================================================================================================================
void setup() {
  //imposto la velocita' del collegamento seriale
  Serial.begin(9600);
  //inserisco un'attesa in modo che il collegamento sia correttamente inzializzato.
  // Senza questa attesa i primi comandi Serial.println potrebbero andare perduti
  delay(attesaInizialeMS); // power-up safety delay

  // INIZIO STEP ------------------------------------
  pinMode(SER_Pin, OUTPUT);
  pinMode(RCLK_Pin, OUTPUT);
  pinMode(SRCLK_Pin, OUTPUT);
  //reset all register pins
  clearRegisters();
  writeRegisters();
  // FINE  STEP ------------------------------------

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  for ( int i = 0; i < NUM_LEDS; i++) leds[0] = ColorFromPalette( myRedWhiteBluePalette_p, FULLGREEN, 0, LINEARBLEND);
  FastLED.setBrightness(  50 );
  FastLED.show();

  mcp_UNO.begin(I2C_UNO_ADDR_I);
  mcp_DUE.begin(I2C_DUE_ADDR_I);

  //KeypadMCP23017
  customKeypad.begin( );        // GDY120705

  //Serial.println("***Setup LCD");
  lcd.init();  //initialize the lcd
  lcd.backlight();  //open the backlight

  //Serial.println("***Setup BMP280");
  //controllo presenza sensore di pressione
  if (!bmp.begin()) {
    //Serial.println("Sensore BMP280 non trovato, controllare cablaggio!");
  }

  //Serial.println("***Setup PIR>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
  //INIZIO - sezione PIR -------------------------------------------------------------------------------------------------------------
  mcp_DUE.pinMode(iPIRLedPinRED, OUTPUT);
  mcp_DUE.pinMode(iPIRLedPinGREEN, OUTPUT);
  mcp_DUE.digitalWrite(iPIRLedPinRED, LOW);
  mcp_DUE.digitalWrite(iPIRLedPinGREEN, LOW);
  mcp_DUE.pinMode(iPIRInputPin, INPUT);
  //FINE - sezione PIR ---------------------------------------------------------------------------------------------------------------


  //INIZIO - sezione DHT -------------------------------------------------------------------------------------------------------------
  // Inizializzo il sensore
  //Serial.println("***Setup DHT");
  dht.begin();
  //stampo le caratteristiche tecniche
  sensor_t sensoreDHT;
  dht.temperature().getSensor(&sensoreDHT);
  dht.humidity().getSensor(&sensoreDHT);
  //FINE - sezione PIR -------------------------------------------------------------------------------------------------------------

  pinMode(TRIGPIN, OUTPUT);
  pinMode(ECHOPIN, INPUT);

  //RTC
  clock.begin();
  // Set sketch compiling time
  //clock.setDateTime(__DATE__, __TIME__);
  lcd2.init();  //initialize the lcd
  lcd2.backlight();  //open the backlight

  lcd3.init();  //initialize the lcd
  lcd3.backlight();  //open the backlight

  mcp_DUE.pinMode(BUZZERPIN, OUTPUT); //initialize the buzzer pin as an output
  mcp_DUE.digitalWrite(BUZZERPIN, HIGH);
  //Serial.println("***FINE Setup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
}

void apriSerranda(bool force = false, int passi=120);
void chiudiSerranda(bool force = false, int passi=120);

//===========================================================================================================================================================================
//                                                                               L O O P
//===========================================================================================================================================================================
void loop() {
  //  keyboardValue = analogRead(KEYBOARD_ANALOG_PIN);
  //  if (keyboardValue > 250)
  //    readkeyboard();
  //  else {keypressed = 0;}

  //KeypadMCP23017 --- START ------------------------------------------------------------------------------------------------------------------------------------------------
  char customKey = customKeypad.getKey();
  if (customKey != NO_KEY) {
    //Serial.print("Selezionata scena: ");
    //Serial.println(customKey);
    DEBUG_PRINT("Selezionata scena: ");
    DEBUG_PRINTLN(customKey);
    lcd3.setCursor(0, 1);
    lcd3.print("Sel. scena: ");
    lcd3.print(customKey);
    lcd3.print("    ");
  }
  switch (customKey) {
    case '0': scenario_0(); break;
    case '1': scenario_1(); break;
    case '2': scenario_2(); break;
    case '3': scenario_3(); break;
    case '4': scenario_4(); break;
    case '5': scenario_5(); break;
    case '6': scenario_6(); break;
    case '7': scenario_7(); break;
    case 'D': luciPiene(0); break;
    case 'E': apriSerranda(true,20); break;
    case 'F': chiudiSerranda(true,20); break;
  }
  
  //KeypadMCP23017 --- END   ------------------------------------------------------------------------------------------------------------------------------------------------

  //Foto restistenze --- START ----------------------------------------------------------------------------------------------------------------------------------------------
  //auto-adjust the minimum and maximum limits in real time
  sensoreLuce1 = analogRead(A0);
  sensoreLuce1 = map(sensoreLuce1, 0, 1023, 0, 100); //This infers the change in light values. The first range (0-1023) is remapped as a range of (0-255).
  //sensoreLuce1 = constrain(sensoreLuce1, 0, 100);
  delay(10);
  sensoreLuce2 = analogRead(A1);
  sensoreLuce2 = map(sensoreLuce2, 0, 1023, 0, 100);
  //sensoreLuce2 = constrain(sensoreLuce2, 0, 100);

  if ((sensoreLuce1 - sensoreLuce2) > 20) scenario_7();
  if ((sensoreLuce2 - sensoreLuce1) > 20) scenario_6();
  //Fotoresistenze --- END --------------------------------------------------------------------------------------------------------------------------------------------------

  //INZIO - sezione PIR ----------------------------------------------------------------------------------------------------------------------------------------------------
  iPIRCurrentState = mcp_DUE.digitalRead(iPIRInputPin);
  int pirSegnal = mcp_DUE.digitalRead( iPIRInputPin );
  int pirSegnal2 = (pirSegnal == HIGH) ? LOW : HIGH;
  mcp_DUE.digitalWrite( iPIRLedPinGREEN, pirSegnal );
  mcp_DUE.digitalWrite( iPIRLedPinRED, pirSegnal2 );

  lcd3.setCursor(0, 1);
  if (pirSegnal == LOW){
    //lcd3.print("nessun movimento  "); 
  }else{
    lcd3.print("rilevato movimento");
    luciAlternate(White60,false);
    luciPerimetro(White40,false);
    FastLED.show();
  }
  //FINE - sezione PIR -----------------------------------------------------------------------------------------------------------------------------------------------------

  //INIZIO - sezione Buzzer ---------------------------------------------------------------------------------------------------------------
  if (pirSegnal == HIGH) {
    //for(int i=0; i< 5; i++){
    mcp_DUE.digitalWrite(BUZZERPIN, HIGH);
    mcp_DUE.digitalWrite(BUZZERPIN, LOW);
    mcp_DUE.digitalWrite(BUZZERPIN, HIGH);
    //}
  }
  //FINE   - sezione Buzzer ---------------------------------------------------------------------------------------------------------------

  //INZIO - sezione DHT
  float fTemp, fHum;
  // recupera il valore della temperatura
  sensors_event_t eventoLetturaDHT;
  dht.temperature().getEvent(&eventoLetturaDHT);
  fTemp = eventoLetturaDHT.temperature;
  dht.humidity().getEvent(&eventoLetturaDHT);
  fHum = eventoLetturaDHT.relative_humidity;
  lcd.setCursor(0, 0);
  lcd.print("T:");
  lcd.print(fTemp, 1); //print the temperature on lcd
  lcd.print(char(223));//print the unit" ℃ "
  lcd.print("C");
  lcd.setCursor(0, 1);
  lcd.print("H:");
  lcd.print(fHum, 1); //print the humidity on lcd
  lcd.print(" %");
  //DHT --- END

  //BMP280 --- START
  //Serial.print(bmp.readTemperature());
  //Serial.println(" *C");

  //Serial.print("Pressure = ");
  //Serial.print(bmp.readPressure());
  //Serial.println(" Pa");

  lcd.setCursor(9, 1);
  lcd.print("P:");
  lcd.print(bmp.readPressure(), 1); //print the temperature on lcd

  float fPsl = 1035;

  // Calculate altitude assuming 'standard' barometric
  // pressure of 1013.25 millibar = 101325 Pascal
  //Serial.print("Altitude = ");
  //Serial.print(bmp.readAltitude(fPsl));
  //Serial.println(" meters");

  lcd.setCursor(9, 0);
  lcd.print("A:");
  lcd.print(bmp.readAltitude(fPsl), 1); //print the temperature on lcd
  lcd.print("m");

  // you can get a more precise measurement of altitude
  // if you know the current sea level pressure which will
  // vary with weather and such. If it is 1015 millibars
  // that is equal to 101500 Pascals.
  //Serial.print("Real altitude = ");
  //Serial.print(bmp.readAltitude(fPsl));
  //Serial.println(" meters");

  //Serial.println();

  //BMP280 --- END

  //HC-SR04 --- START
  long duration, distance;
  digitalWrite(TRIGPIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGPIN, HIGH);

  delayMicroseconds(10);
  digitalWrite(TRIGPIN, LOW);
  duration = pulseIn(ECHOPIN, HIGH);
  distance = (duration / 2) / 29.1;

  lcd3.setCursor(0, 0);
  lcd3.print("cm:");
  if (distance >= 200 || distance <= 0) {
    //Serial.println("Out of range");
    lcd.print("fuori portata (> 2m)");
  } else if (distance != lastDistance) {
    //Serial.print(distance);
    //Serial.println("cm");
    lcd3.print(distance, 1);
    lcd3.print("                    ");
  }
  lastDistance = distance;

  //IGROMETRO --- START ---------------------------------------------------------------------------
  hygrometerValue = analogRead(HYGROMETER_PIN);   //Read analog value
  hygrometerValue = constrain(hygrometerValue, 400, 1023); //Keep the ranges!
  hygrometerValue = map(hygrometerValue, 400, 1023, 100, 0); //Map value : 400 will be 100 and 1023 will be 0
  lcd3.setCursor(8, 0);
  lcd3.print("Igr.:");
  lcd3.print(hygrometerValue, 1);
  lcd3.print("%   ");
  //IGROMETRO --- END   ---------------------------------------------------------------------------

  /* print the current time */
  print_time();
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////
//set pins to ULN2003 high in sequence from 1 to 4
//delay "motorSpeed" between each pin setting (to determine speed)

void setMultiRegisterPin(int p1, int p2, int p3, int p4){
  setRegisterPin(motorPin1, p1);
  setRegisterPin(motorPin2, p2);
  setRegisterPin(motorPin3, p3);
  setRegisterPin(motorPin4, p4);
  writeRegisters();
  delay(motorSpeed);
}

void counterclockwise () {
  setMultiRegisterPin(HIGH,LOW,LOW,LOW);
  setMultiRegisterPin(HIGH,HIGH,LOW,LOW);
  setMultiRegisterPin(LOW,HIGH,LOW,LOW);
  setMultiRegisterPin(LOW,HIGH,HIGH,LOW);
  setMultiRegisterPin(LOW,LOW,HIGH,LOW);
  setMultiRegisterPin(LOW,LOW,HIGH,HIGH);
  setMultiRegisterPin(LOW,LOW,LOW,HIGH);
  setMultiRegisterPin(HIGH,LOW,LOW,HIGH);
}

//////////////////////////////////////////////////////////////////////////////
//set pins to ULN2003 high in sequence from 4 to 1
//delay "motorSpeed" between each pin setting (to determine speed)

void clockwise() {
  setMultiRegisterPin(LOW,LOW,LOW,HIGH);
  setMultiRegisterPin(LOW,LOW,HIGH,HIGH);
  setMultiRegisterPin(LOW,LOW,HIGH,LOW);
  setMultiRegisterPin(LOW,HIGH,HIGH,LOW);
  setMultiRegisterPin(LOW,HIGH,LOW,LOW);
  setMultiRegisterPin(HIGH,HIGH,LOW,LOW);
  setMultiRegisterPin(HIGH,LOW,LOW,LOW);
  setMultiRegisterPin(HIGH,LOW,LOW,HIGH);
}

//set all register pins to LOW
void clearRegisters() {
  for (int i = numOfRegisterPins - 1; i >=  0; i--) {
    registers[i] = LOW;
  }
}

//Set and display registers
//Only call AFTER all values are set how you would like (slow otherwise)
void writeRegisters() {

  digitalWrite(RCLK_Pin, LOW);

  for (int i = numOfRegisterPins - 1; i >=  0; i--) {
    digitalWrite(SRCLK_Pin, LOW);

    int val = registers[i];

    digitalWrite(SER_Pin, val);
    digitalWrite(SRCLK_Pin, HIGH);

  }
  digitalWrite(RCLK_Pin, HIGH);

}

//set an individual pin HIGH or LOW
void setRegisterPin(int index, int value) {
  registers[index] = value;
}

//===========================================================================================================================================================================
//                                                                               apriSerranda
//===========================================================================================================================================================================
void apriSerranda(bool force = false, int passi=120) {
  //Serial.println("apriSerranda");
  if (serrandaAperta == false || force == true) {
    for (int i = 0; i < passi; i++) clockwise();
    serrandaAperta = true;
  } else {
    //Serial.println("serranda gia' aperta");
  }
}
//===========================================================================================================================================================================
//                                                                               chiudiSerranda
//===========================================================================================================================================================================
void chiudiSerranda(bool force = false,int passi=120) {
  //Serial.println("chiudiSerranda");
  if (serrandaAperta == true || force == true) {
    for (int i = 0; i < passi; i++) counterclockwise();
    serrandaAperta = false;
  } else {
    //Serial.println("serranda gia' chiusa");
  }
}


//===========================================================================================================================================================================
//                                                                               scenario 1
//===========================================================================================================================================================================

void scenario_0() {
  //Serial.println("Scenario 0");
  luciAlternate(White70);
  FastLED.show();
  apriSerranda();
}
void scenario_1() {
  //Serial.println("Scenario 1");
  luciPerimetro(White50);
  FastLED.show();
  chiudiSerranda();
}

void scenario_2() {
  luciPiene();
  FastLED.show();
  apriSerranda();
}

void scenario_3() {
  //Serial.println("Scenario 3");
  //se l'igrometro dice che il terreno è bagnato ed è notte, allora
  // 1) accendi luci in giardino a bassa potenza
  // 2) abbassa serranda
  //      Serial.print("Igrometro:");
  //      Serial.println(hygrometerValue);
  //      Serial.print("sensoreLuce1:");
  //      Serial.println(sensoreLuce1);
  //      Serial.print("sensoreLuce2:");
  //      Serial.println(sensoreLuce2);

  if (hygrometerValue > 50 && (sensoreLuce1 > 50 && sensoreLuce2 > 50)) {
    for (int row = 0; row < 8; row++) {
      for ( int col = 0; col < 8; col++) {
        int colDecoded = col;
        if ( row % 2 != 0 ) colDecoded = 7 - col;
        leds[row * 8 + colDecoded] = ColorFromPalette( myRedWhiteBluePalette_p, FULLGREEN, 100, LINEARBLEND);
        if (col == 7)
          leds[row * 8 + colDecoded] = ColorFromPalette( myRedWhiteBluePalette_p, WHITE, White50, LINEARBLEND);
      }
    }
    FastLED.show();
    chiudiSerranda();
  } else {
    lcd3.setCursor(0, 1);
    lcd3.print("Scena 3:no cond. ");
    //Serial.println("Scenario 2: condizioni di attivazione non presenti");
  }
}

//scenario notturno
void scenario_4() {
  //Serial.println("Scenario 4");
  luciPerimetro(White30);
  FastLED.show();
  apriSerranda();
}

void scenario_5() {
  //Serial.println("Scenario 5");
  luciAlternate(White60,false);
  luciPerimetro(White40,false);
  FastLED.show();
  apriSerranda();
}

void scenario_6() {
  //Serial.println("Scenario 5");
  luciSfumate();
  FastLED.show();
}
void scenario_7() {
  //Serial.println("Scenario 5");
  luciSfumate(32,true);
  FastLED.show();
}
