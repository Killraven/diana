<?php
	error_reporting(E_ALL);
	header('Content-Type: text/html; charset=utf-8');

	include_once("class/dbEntity/bean.config.php");
	include_once("class/dbEntity/BeanUsers.php");
	include_once("class/dbEntity/BeanGroup.php");
	include_once("class/dbEntity/BeanSensorscenario.php");


	$stato_bin = str_repeat(chr(0), 8);
	$stato_bin[0] = 0;
	$stato_bin[1] = 0;
	$stato_bin[2] = 0;
	$stato_bin[3] = 0;
	$stato_bin[4] = 0;
	$stato_bin[5] = 1;
	$stato_bin[6] = 0;
	$stato_bin[7] = 0;
	echo "stato_bin:       $stato_bin<br>";
	$nodo = bindec($stato_bin);

	$nodochr = chr($nodo);

	$nodoord = ord($nodochr);

	echo "nodo:       $nodo<br>";
	$format = '%08b';
	$bin = sprintf($format, $nodoord);
	echo "binary:       $bin<br>";

	ini_set('precision', 20);  // For readability on 64 bit boxes.
	$dec = bindec($bin);
	echo 'bindec():     ' . $dec . "<br>";



	/*$bean = new BeanUsers(1);
	showBeanOperationResult("SELECT",$bean,$bean->affected_rows);

	$data = $bean->getAll();

	foreach ($data as $row){
		foreach ($row as $key => $value){
			echo $key."=".$value."<br>";
		}
	}

	$bean->close();*/


//	$bean = new Beangroup();
//
//
//	$group2 = $bean->getDdl();
//	echo "group2=".$group2."<br/><br/>";
//
//	$newGroup = str_replace("group2", "group", $group2);
//
//	echo "newGroup=".$newGroup."<br/><br/>";
//	echo "-----------".base64_encode($newGroup)."<br/><br/>";
//
//	echo "-----------".$bean->getUncodedDdl()."<br/><br/>";
//	echo "-----------".base64_encode($bean->getDdl())."<br/>";




//
//	$bean = new BeanUsers();
//	$bean->setUsername("paolotest33");
//	$bean->setPassword("pwd33");
//	$bean->setIsadmin(true);
//
//	$result = $bean->insert();
//	$lastInserted = $bean->getIdusers();
//
//	/** @var TYPE_NAME $bean */
//	showBeanOperationResult("INSERT", $bean, $bean->affected_rows, false);
//	echo $lastInserted . " last inserted<br/>";
//	$bean->close();
//
//// SELECT  previously inserted row and show result
//$bean = new BeanUsers($lastInserted);
//
//showBeanOperationResult("SELECT",$bean,$bean->affected_rows);
//$bean->close();
//
//// UPDATE previously inserted row and show result
//$bean = new BeanUsers($lastInserted);
//$bean->setUsername("Roberta33");
//$bean->setPassword("pwdRob33");
//$bean->setIsadmin(false);
//$result = $bean->updateCurrent();
//
//showBeanOperationResult("UPDATE",$bean,$result,true);
//$bean->close();
//
//// DELETE previously updated row and show result
//$bean = new BeanUsers($lastInserted);
//$result = $bean->delete($lastInserted);
//
//showBeanOperationResult("DELETE",$bean,$result);
//$bean->close();
//
//// Select after deletion and show result
//$bean = new BeanUsers($lastInserted);
//
//showBeanOperationResult("SELECT AFTER DELETION",$bean,$bean->affected_rows);
//$bean->close();

	/**
	 * Support functions to show result
	 */

	/**
	 * Show Sale bean information and the MySQLi result for the current object operation
	 *
	 * @param string $operation the class operation
	 * @param mixed  $bean      current object
	 * @param mysqli $result    the mysql result for the operation
	 * @param bool   $ddl       if true show the DDL
	 */
	function showBeanOperationResult($operation, BeanUsers $bean, $result, $ddl = false){
		echo "<br/><b>BEGIN</b> application operation: <b>$operation</b><br/>";
		showBeanUsers($bean, $operation, $ddl);
		showMySqlResult($result);
		echo "<b>END</b> application operation: <b>$operation</b><br/><br/><br/>";
	}


	function showBeanUsers(BeanUsers $bean = null, $operation, $showDdl = false){
		// If no errors
		if ($bean && !$bean->isSqlError()){
			echo "<br/>";
			echo "<b>Bean information:</b>:<hr>";
			echo "IdUsers          :   {$bean->getIdusers()}<br/>";
			echo "UserName          : {$bean->getUsername()}<br/>";
			echo "Passwrod     : {$bean->getPassword()}<br/>";
			echo "IsAdmin           : {$bean->getIsadmin()}<br/>";
			echo "<br/>";
			// Shows sql statements
			echo "<div style='background: lightgrey'>";
			echo "<sup>Executed SQL statemtent:</sup><br/>";
			echo $bean->lastSql() . "<br/>";
			echo "</div>";
		}

		// If errors
		if ($bean && $bean->isSqlError()){
			echo "<br/>";
			echo "<b>Error Unable to show bean object information:</b>";
			echo "<hr>";
			echo "<div style='background:indianred'>";
			echo "Error:" . $bean->lastSqlError();
			echo "<br/>";
			echo $bean->lastSql();
			echo "</div>";
		}

		// If DDL info requested and no error
		if ($bean && $showDdl){
			echo "<br/>";
			echo "<br/><sup>You requested to see DDL information:</sup><br/>";
			echo "<div style='background: yellowgreen'>";
			echo "<pre>";
			echo $bean->getDdl();
			echo "</pre>";
			echo "</div>";
		}

	}
	/**
	 * Shows some information about current sales agent object
	 *
	 * @param mixed  $bean
	 * @param string $operation the class operation
	 * @param bool   $showDdl
	 */
	function showBeanUser(BeanUsers $bean = null, $operation, $showDdl = false){
		// If no errors
		if ($bean && !$bean->isSqlError()){
			echo "<br/>";
			echo "<b>Bean information:</b>:<hr>";
			echo "IdUsers          :   {$bean->getIdusers()}<br/>";
			echo "UserName          : {$bean->getUsername()}<br/>";
			echo "Passwrod     : {$bean->getPassword()}<br/>";
			echo "IsAdmin           : {$bean->getIsadmin()}<br/>";
			echo "<br/>";
			// Shows sql statements
			echo "<div style='background: lightgrey'>";
			echo "<sup>Executed SQL statemtent:</sup><br/>";
			echo $bean->lastSql() . "<br/>";
			echo "</div>";
		}

		// If errors
		if ($bean && $bean->isSqlError()){
			echo "<br/>";
			echo "<b>Error Unable to show bean object information:</b>";
			echo "<hr>";
			echo "<div style='background:indianred'>";
			echo "Error:" . $bean->lastSqlError();
			echo "<br/>";
			echo $bean->lastSql();
			echo "</div>";
		}

		// If DDL info requested and no error
		if ($bean && $showDdl){
			echo "<br/>";
			echo "<br/><sup>You requested to see DDL information:</sup><br/>";
			echo "<div style='background: yellowgreen'>";
			echo "<pre>";
			echo $bean->getDdl();
			echo "</pre>";
			echo "</div>";
		}

	}

	/**
	 * Show MySql Result
	 *
	 * @param mixed MySql $result
	 */
	function showMySqlResult($result){
		echo "<br/>";
		echo "<div style='background: lightcyan'>";
		echo "<sup>MySQL result for operation:</sup>";
		var_dump($result);
		echo "</div>";
	}



