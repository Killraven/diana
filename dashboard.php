<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diana24</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="assets/css/diana.css">
    <link rel="stylesheet" href="assets/css/Profile-Card.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
<div class="profile-card height:500">
    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->
    </nav>
</div>
<img class="rounded-circle profile-pic" src="assets/img/Guillaume_seignac-diana_the_huntress_sm.jpg">
<h3 class="profile-name">Diana<br></h3>
<p class="profile-bio" style="color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;"><strong>&nbsp;Dali Intelligent ArduiNo Appliance</strong><br></p>
<h1 id="idEventLog">Event Log</h1>
<div class="row rowGenClass" id="sceneRowSensor" style=";max-width:800px; margin: 2px auto;border-radius:15px;">
    <div class="col sceneRowClass" style="margin:5px auto;max-width:800px;border-radius:15px;background-color:rgb(140,201,237);opacity:.9;color:white;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl" style="; opacity:1; color: black;background-color:rgb(140,201,237)">Level
            Filter</label><input type="checkbox"><label class="col-form-label" style="color: rgb(45,150,214);padding-right: 10px;; opacity:1; color: black;font-weight:800;background-color:rgb(140,201,237);">Critical</label>
        <input
                type="checkbox"><label class="col-form-label" style="color: rgb(45,150,214);padding-right: 10px;; opacity:1; color: black;font-weight:800;background-color:rgb(140,201,237);">Error</label><input type="checkbox"><label class="col-form-label"
                                                                                                                                                                                                                                         style="color: rgb(45,150,214);padding-right: 10px;; opacity:1; color: black;font-weight:800;background-color:rgb(140,201,237);">Warning</label>
        <input
                type="checkbox"><label class="col-form-label" style="color: rgb(45,150,214);padding-right: 10px;; opacity:1; color: black;font-weight:800;background-color:rgb(140,201,237);">Info</label><input type="checkbox"><label class="col-form-label"
                                                                                                                                                                                                                                        style="color: rgb(45,150,214);padding-right: 10px;font-weight:800;; opacity:1; color: black;background-color:rgb(140,201,237);">All</label>
    </div>
</div>
<ul class="social-list"></ul>
<div class="container" id="idEventContainer">
    <div class="table-responsive">
        <table class="table sensorTableClass">
            <thead>
            <tr id="idSensorTableHeader" class="tableHeader">
                <th>Level</th>
                <th>Id</th>
                <th>Date&amp;Time</th>
                <th>Event name</th>
                <th>Device Id</th>
                <th>Category</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Error</td>
                <td>10235</td>
                <td>2018-02-18 15:58</td>
                <td>Lamp failure</td>
                <td>2</td>
                <td>Malfunction</td>
            </tr>
            <tr>
                <td>Error</td>
                <td>10234</td>
                <td>2018-02-18 15:23<br></td>
                <td>lamp not operating</td>
                <td>4</td>
                <td>Malfunction<br></td>
            </tr>
            <tr>
                <td>Warning</td>
                <td>10233</td>
                <td>2018-02-18 14:58<br></td>
                <td>slave is in power failure mode</td>
                <td>7</td>
                <td>Power<br></td>
            </tr>
            <tr>
                <td>Info</td>
                <td>10232</td>
                <td>2018-02-18 14:51<br></td>
                <td>Scene.4 selected from keyboard</td>
                <td>n/a</td>
                <td>Manual</td>
            </tr>
            <tr>
                <td>Info</td>
                <td>10231</td>
                <td>2018-02-18 14:50<br></td>
                <td>Automatic Scene.1 selection</td>
                <td>n/a</td>
                <td>Auto</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
<div>
    <nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
        <div class="container-fluid"><a class="navbar-brand bg-primary m-auto fixed-bottom" href="#" style="text-align:center;">Copyright Paolo Maruotti 2018</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
            <div class="collapse navbar-collapse"
                 id="navcol-2"></div>
        </div>
    </nav>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-checkbox.js"></script>
</body>

</html>