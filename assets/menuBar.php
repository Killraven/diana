<!-- START Menu-->
<style>
    .nav-link, .nav-item, .dropdown-item {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 16px;
        font-weight: 400;
        height: 40px;
        }

    .dropdown {
        background-color: rgb(26, 39, 81);
        }
</style>

<div id="idHomeProfileBack" class="profile-back">
    <nav class="navbar navbar-light navbar-expand-md fixed-top">
        <div class="container"><a class="navbar-brand" href="#"><strong>DIANA</strong></a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" style="flex-basis: auto;display: flex !important;border-box*, *::before, *::after; color:rgb(134, 142, 150);" id="navcol-1">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" href="/diana/forms/EventList/eventlist.php">Dashboard</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Dali</a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" role="presentation" href="/diana/web/server_gui.php">Server</a>
                            <a class="dropdown-item" role="presentation" href="/diana/web/config_gui.php">Config</a>
                            <a class="dropdown-item" role="presentation" href="/diana/web/client_gui.php">Client</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Logs</a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" role="presentation" href="/diana/forms/MeasureList/measurelist.php">Measure log</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Configuration</a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" role="presentation" href="/diana/keyboard.php">Keyboard setup</a>
                            <a class="dropdown-item" role="presentation" href="/diana/sensors.php">Sensors</a>
                            <a class="dropdown-item" role="presentation" href="/diana/ledMatrix.php">Led matrix</a>
                            <a class="dropdown-item" href="/diana/forms/SensorList/sensorlist.php">Sensor list</a>
                            <a class="dropdown-item" href="/diana/forms/Userlist/userlist.php">User list</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Fault Config</a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" role="presentation" href="/diana/forms/DaliFault/dalifault.php">Dali fault</a>
                            <a class="dropdown-item" role="presentation" href="/diana/forms/SensorFault/sensorfault.php">Sensor fault</a>
                        </div>
                    </li>

                    <li class="nav-item" role="presentation"><a class="nav-link" href="#">Help</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- END Menu-->