<?php
	/*
	 * Copyright 2018 Vincenzo Suraci
	 */

	/**
	 * Questa classe implementa le parti comuni del
	 * CONVERTITORE DALI-ETHERNET HD67839-B2 della ADFweb
	 * https://www.adfweb.com/home/products/DALI_EtherNet.asp?frompg=nav7_24
	 */
	class DALI_ETHERNET{
		//---------------------------------------------------------------------------------------------
		// IDENTIFICATORI DEI COMANDI DALI
		//---------------------------------------------------------------------------------------------
		public static $COMANDO_DALI_NETWORK_SCANNING_REQUEST = 0;
		public static $COMANDO_DALI_NETWORK_SCANNING_RESPONSE = 1;

		public static $COMANDO_DALI_SCENE_SCANNING_REQUEST = 2;
		public static $COMANDO_DALI_SCENE_SCANNING_RESPONSE = 3;

		public static $COMANDO_ADV_SETTING_REQUEST = 4;

		public static $COMANDO_SCENE_SETTING_REQUEST = 5;

		public static $COMANDO_GENERIC_DALI_COMMAND_REQUEST = 6;
		public static $COMANDO_GENERIC_DALI_COMMAND_RESPONSE = 7;

		public static $COMANDO_SCENE_PROGRAMMING_REQUEST = 8;
		public static $COMANDO_SCENE_PROGRAMMING_REQUEST_1 = 9;
		public static $COMANDO_SCENE_PROGRAMMING_REQUEST_2 = 10;


		public static $COMANDO_GROUP_SETTING_REQUEST = 11;

		public static $COMANDO_NODE_ID_SETTING_REQUEST = 12;
		public static $COMANDO_NODE_ID_SETTING_REQUEST_1 = 13;
		public static $COMANDO_NODE_ID_SETTING_REQUEST_2 = 14;

		public static $COMANDO_SEARCH_ALL_DEVICES = 15;
		public static $COMANDO_SEARCH_NEW_DEVICES = 16;

		// steps/sec
		public static $FADE_RATE_TABLE = array(0 => 'not applicable', 1 => 357.796, 2 => 253.000, 3 => 178.898, 4 => 126.500, 5 => 89.449, 6 => 63.250, 7 => 44.725, 8 => 31.625, 9 => 22.362, 10 => 15.813, 11 => 11.181, 12 => 7.906, 13 => 5.591, 14 => 3.953, 15 => 2.795,);

		// sec
		public static $FADE_TIME_TABLE = array(0 => 0, 1 => 0.707, 2 => 1.000, 3 => 1.414, 4 => 2.000, 5 => 2.828, 6 => 4.000, 7 => 5.657, 8 => 8.000, 9 => 11.314, 10 => 16.000, 11 => 22.627, 12 => 32.000, 13 => 45.255, 14 => 64.000, 15 => 90.510,);

		public static $GENERIC_DALI_COMMAND_LIST = array(array(0, 0, 'Extinguish the lamp (without fading)', ''), array(1, 1, 'Dim up 200 ms using the selected fade rate', ''), array(2, 2, 'Dim down 200 ms using the selected fade rate', ''), array(3, 3, 'Set the actual arc power level one step higher without fading. If the lamp is off, it will be not ignited.', ''), array(4, 4, 'Set the actual arc power level one step lower without fading. If the lamp has already it\'s minimum value, it is not switched off.', ''), array(5, 5, 'Set the actual arc power level to the maximum value. If the lamp is off, it will be ignited.', ''), array(6, 6, 'Set the actual arc power level to the minimum value. If the lamp is off, it will be ignited.', ''), array(7, 7, 'Set the actual arc power level one step lower without fading. If the lamp has already it\'s minimum value, it is switched off.', ''), array(8, 8, 'Set the actual arc power level one step higher without fading. If the lamp is off, it will be ignited.', ''), array(9, 15, 'reserved', ''), array(16, 31, 'Set the light level to the value stored for the selected scene (n)', ''), array(32, 32, 'Reset the parameters to default settings', ''), array(33, 33, 'Store the current light level in the DTR (Data Transfer Register)', ''), array(34, 41, 'reserved', ''), array(42, 42, 'Store the value in the DTR as the maximum level', ''), array(43, 43, 'Store the value in the DTR as the minimum level', ''), array(44, 44, 'Store the value in the DTR as the system failure level', ''), array(45, 45, 'Store the value in the DTR as the power on level', ''), array(46, 46, 'Store the value in the DTR as the fade time', ''), array(47, 47, 'Store the value in the DTR as the fade rate', ''), array(48, 63, 'reserved', ''), array(64, 79, 'Store the value in the DTR as the selected scene (n)', ''), array(80, 95, 'Remove the selected scene (n) from the DALI slave', ''), array(96, 111, 'Add the DALI slave unit to the selected group (n)', ''), array(112, 127, 'Remove the DALI slave unit from the selected group (n)', ''), array(128, 128, 'Store the value in the DTR as a short address', ''), array(129, 143, 'reserved', ''), array(144, 144, 'Returns the status (XX) of the DALI slave', 'XX'), array(145, 145, 'Check if the DALI slave is working', 'yes/no'), array(146, 146, 'Check if there is a lamp failure', 'yes/no'), array(147, 147, 'Check if the lamp is operating', 'yes/no'), array(148, 148, 'Check if the slave has received a level out of limit', 'yes/no'), array(149, 149, 'Check if the DALI slave is in reset state', 'yes/no'), array(150, 150, 'Check if the DALI slave is missing a short address', 'XX'), array(151, 151, 'Returns the version number as XX', 'XX'), array(152, 152, 'Returns the content of the DTR as XX', 'XX'), array(153, 153, 'Returns the device type as XX', 'XX'), array(154, 154, 'Returns the physical minimum level as XX', 'XX'), array(155, 155, 'Check if the DALI slave is in power failure mode', 'yes/no'), array(156, 159, 'reserved', ''), array(160, 160, 'Returns the current light level as XX', 'XX'), array(161, 161, 'Returns the maximum allowed light level as XX', 'XX'), array(162, 162, 'Returns the minimum allowed light level as XX', 'XX'), array(163, 163, 'Return the power up level as XX', 'XX'), array(164, 164, 'Returns the system failure level as XX', 'XX'), array(165, 165, 'Returns the fade time as X and the fade rate as Y', 'XY'), array(166, 175, 'reserved', ''), array(176, 191, 'Returns the light level XX for the selected scene (n)', 'XX'), array(192, 192, 'Returns a bit pattern XX indicating which group (0-7) the DALI slave belongs to', 'XX'), array(193, 193, 'Returns a bit pattern XX indicating which group (8-15) the DALI slave belongs to', 'XX'), array(194, 194, 'Returns the high bits of the random address as HH', 'HH'), array(195, 195, 'Return the middle bit of the random address as MM', 'MM'), array(196, 196, 'Returns the lower bits of the random address as LL', 'LL'), array(197, 223, 'reserved', ''), array(224, 255, 'Returns application specific extension commands', ''),);

		//---------------------------------------------------------------------------------------------
		// BIT DELLO STATO DI UN NODO DALI
		//---------------------------------------------------------------------------------------------
		public static $BIT_STATO = array(7 => 'BALLAST KO', 6 => 'LAMP KO', 5 => 'LAMP ON', 4 => 'OUT OF LIMITS', 3 => 'CHANGE ON', 2 => 'RESET', 1 => 'NO ADDRESS', 0 => 'POWER FAILURE',);
		public static $COMANDO_CLOSE_SERVER_SOCKET = 255;
		public static $MAX_NUM_NODI_DALI = 64;
		public static $MAX_NUM_SCENE_DALI = 16;
		public static $MAX_NUM_GRUPPI_DALI = 16;
		public static $DALI_DEVICE_TYPES = array(0 => 'Fluorescent Lamps', 1 => 'Self-contained emergency lighting', 2 => 'discharge lamps (excluding fluorescent lamps)', 3 => 'Low voltage halogen lamps', 4 => 'Supply voltage controller for incandescent lamps', 5 => 'Conversion from digital signal into d. c. voltage', 6 => 'LED modules', 7 => 'Switching function', 8 => 'Colour control',);
		public static $MAX_DATAGRAM_SIZE = 2048;
		public static $ADDR_NULL = '___.___.___.___';
		public static $PORT_NULL = '___';

		//---------------------------------------------------------------------------------------------
		// IDENTIFICATORI DEI COMANDI DEL SERVER
		//---------------------------------------------------------------------------------------------
		public static $COUNTER_MIN = 0;

		//---------------------------------------------------------------------------------------------
		// MESSAGGI DELLE RICHIESTE DA INVIARE AL DALI-ETHERNET-CONVERTER
		//---------------------------------------------------------------------------------------------
		public static $COUNTER_INI = 1;
		public static $COUNTER_MAX = 63;
		protected static $BIT_STATO_BALLAST_KO = 7;
		protected static $BIT_STATO_LAMP_KO = 6;
		protected static $BIT_STATO_LAMP_ON = 5;

		//---------------------------------------------------------------------------------------------
		// COSTANTI DALI
		//---------------------------------------------------------------------------------------------
		protected static $BIT_STATO_OUT_OF_LIMITS = 4;
		protected static $BIT_STATO_CHANGE_ON = 3;
		protected static $BIT_STATO_RESET = 2;

		//---------------------------------------------------------------------------------------------
		// TIPOLOGIE DI DISPOSITIVI DALI
		//---------------------------------------------------------------------------------------------
		protected static $BIT_STATO_NO_ADDRESS = 1;

		//---------------------------------------------------------------------------------------------
		// CONFIGURAZIONE DEL SERVER
		//---------------------------------------------------------------------------------------------
		protected static $BIT_STATO_POWER_FAILURE = 0;
		protected static $REQUEST_DALI_NETWORK_SCANNING = 'DALI_INFO=';
		protected static $REQUEST_DALI_SCENE_SCANNING = 'DALI_SCEN=';
		protected static $REQUEST_CLOSE_SERVER_SOCKET = 'CLOSE_SRV=';
		protected static $RESPONSE_ANSWER_VAL = 'ANSWER=';
		protected static $RESPONSE_ANSWER_NOT_RECEIVED = 'ANSWER NOT RECEIVED';
		public $port = 10000;
		public $cmd_port = 10001;

		//---------------------------------------------------------------------------------------------
		// COUNTER
		//---------------------------------------------------------------------------------------------
		public $addr = '127.0.0.1';
		public $COUNTER;
		protected $send_socket;
		protected $listen_socket;

		function __construct($addr = null, $port = null, $cmd_port = null){
			// configuriamo la porta
			if ($port !== null) $this->port = $port;

			if ($cmd_port !== null) $this->cmd_port = $cmd_port;

			if ($addr !== null) $this->addr = $addr;else
				$this->addr = $this->recupera_ip_macchina();

			$this->COUNTER = self::$COUNTER_INI;
		}

		protected function recupera_ip_macchina(){
			$addr = array();

			if (false !== ($host = gethostname())) $addr[] = gethostbyname($host);

			$addr[] = $_SERVER['SERVER_ADDR'];

			$num_addr = count($addr);

			if ($num_addr == 0){
				$addr = $this->addr;
			}else{
				for ($i = 0; $i < $num_addr; $i++){
					if (filter_var($addr[$i], FILTER_VALIDATE_IP)){
						// abbiamo un indirizzo IP valido
						if ($addr[$i] != '127.0.1.1'){
							$addr = $addr[$i];
						}
					}
				}
			}

			return $addr;
		}

		function __destruct(){
			if ($this->send_socket !== null) @socket_close($this->send_socket);

			if ($this->listen_socket !== null) @socket_close($this->listen_socket);
		}

		function recupera_tmp_dir(){
			$dir = __DIR__ . '/../tmp';
			if (!is_dir($dir)) mkdir($dir);
			return $dir;
		}

		function recupera_html_successo($msg){
			return '
		<div class="alert alert-success" role="alert">
		  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
		  ' . $msg . '
		</div>';
		}

		function recupera_html_info($msg){
			return '
		<div class="alert alert-info" role="alert">
		  <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
		  ' . $msg . '
		</div>';
		}

		public function recupera_dati_risposta_COMANDO_DALI_SCENE_SCANNING(&$risposta){
			$risultato = 1;
			$html = '';
			$scene = array();

			// Gli ultimi 8 byte sono riservati
			$lunghezza_risposta_attesa = self::$MAX_NUM_SCENE_DALI * self::$MAX_NUM_NODI_DALI;
			$lunghezza_risposta = strlen($risposta);

			if ($lunghezza_risposta_attesa != $lunghezza_risposta){
				$html .= $this->recupera_html_errore('La risposta è lunga <b>' . $lunghezza_risposta . '</b> bytes, invece doveva essere lunga <b>' . $lunghezza_risposta_attesa . '</b> bytes.');
			}else{
				// Scorriamo i nodi DALI
				for ($num_nodo = 0; $num_nodo < self::$MAX_NUM_NODI_DALI; $num_nodo++){

					$chr_nodo = substr($risposta, self::$MAX_NUM_SCENE_DALI * $num_nodo, self::$MAX_NUM_SCENE_DALI);
					$scene[$num_nodo] = array();
					for ($num_scena = 0; $num_scena < self::$MAX_NUM_SCENE_DALI; $num_scena++) $scene[$num_nodo][$num_scena] = ord($chr_nodo[$num_scena]);
				}
			}

			return array('html' => $html, 'risultato' => $risultato, 'scene' => $scene,);
		}

		function recupera_html_errore($msg){
			return '
		<div class="alert alert-danger" role="alert">
		  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		  <span class="sr-only">Errore: </span>
		  ' . $msg . '
		</div>';
		}

		public function recupera_dati_risposta_COMANDO_DALI_NETWORK_SCANNING(&$risposta){
			$risultato = 1;
			$html = '';
			$nodi = array();

			// Gli ultimi 8 byte sono riservati
			$lunghezza_risposta_attesa = 8 * self::$MAX_NUM_NODI_DALI;
			$lunghezza_risposta = strlen($risposta);

			if ($lunghezza_risposta_attesa != $lunghezza_risposta){
				$risultato = 0;
				$html .= $this->recupera_html_errore('La risposta è lunga <b>' . $lunghezza_risposta . '</b> bytes, invece doveva essere lunga <b>' . $lunghezza_risposta_attesa . '</b> bytes.');
			}else{
				// Scorriamo i nodi DALI
				for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++){
					$nodo = array();

					$byte_min = 8 * $i;
					$bytes_nodo = substr($risposta, $byte_min, 8);

					$nodo['bytes'] = array();
					for ($j = 0; $j < 8; $j++) $nodo['bytes'][$j] = ord($bytes_nodo[$j]);

					$nodo['presente'] = $this->verifica_nodo_attivo($bytes_nodo) ? 1 : 0;
					$nodo['tipo'] = ord($bytes_nodo[3]) % 16;
					$nodo['versione'] = (ord($bytes_nodo[3]) - $nodo['tipo']) / 16;
					$nodo['stato'] = ord($bytes_nodo[0]);
					$nodo['ADV'] = ord($bytes_nodo[1]);
					$nodo['risposta'] = ord($bytes_nodo[2]);
					$nodo['ADV_min'] = ord($bytes_nodo[4]);
					$nodo['ADV_max'] = ord($bytes_nodo[5]);
					$nodo['gruppi'] = strrev(str_pad(decbin(ord($bytes_nodo[6])), 8, "0", STR_PAD_LEFT)) . strrev(str_pad(decbin(ord($bytes_nodo[7])), 8, "0", STR_PAD_LEFT));

					$nodi[$i] = $nodo;
				}
			}

			return array('html' => $html, 'risultato' => $risultato, 'nodi' => $nodi,);
		}

		/**
		 * Questa funzione verifica se un nodo è attivo, o meno
		 */
		protected function verifica_nodo_attivo(&$nodo_chr){
			return (substr($nodo_chr, 0, 8) != str_repeat(chr(0), 8));
		}

		public function recupera_stato_modulo($addr){
			$risultato = 0;
			$stato = 'Offline';
			$html = '';

			$r = $this->Ping($addr);

			$html .= $r['html'];
			if ($r['risultato'] == 1){
				$risultato = 1;
				$stato = 'Online';
			}

			return array('html' => $html, 'risultato' => $risultato, 'stato' => $stato,);
		}

		protected function Ping($host, $timeout = 1, $verbose = false){
			$risultato = 1;
			$html = '';

			/**
			 * Return vždy vrací False ale pro příchod paketu ECHO_REPLAY
			 * jinak vrací čas odezvy PING. Tento script musí běžet
			 * pod ROOTem, jinak není možné přistupovat k RAW socketu!
			 *
			 * @param  string  hostname or IP address
			 * @param  int     timeout
			 * @param  bool    verbose output
			 *
			 * @return double
			 */

			# vytvoří síťový socket pro RAW zápis,
			# AF_INET => protokol IPV4
			# SOCK_RAW => poskytne možnost manuálně vytvořit libovolnou konstrukci paketu
			$socket = socket_create(AF_INET, SOCK_RAW, 1);

			# Nastaví Tmeout a připojí se k danému "hostu".
			socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $timeout, 'usec' => 0));
			@socket_connect($socket, $host, null);

			# Hlavičk ECHO REQUERS v hex \x08
			$header = "\x08\x00\x7d\x4b\x00\x00\x00\x00PingHost";

			$start = microtime(true);
			@socket_send($socket, $header, strLen($header), 0);

			if ($result = @socket_read($socket, 255)){
				// Je odpoved Echo Replay? (ping odpovedel?)
				if (substr($result, 0, 1) == "E"){
					# Verbose? Zobraz víc info
					if ($verbose) $html .= (strlen($header) . ' bytes from ' . $host . ' time=' . ((100 * (microtime(true) - $start)))) . ' ms';

					# Uzavře soket
					@socket_close($socket);
				}else{
					//Odpoved přišla, ale né ECHO_REPLAY, tedy např. DEST UNRECHABLE atp.
					if ($verbose) $html .= ('ICMP Unrechable ' . $host . ' time=' . ((100 * (microtime(true) - $start)))) . ' ms';

					@socket_close($socket);
					$risultato = 0;
				}
			}else{
				if ($verbose) $html .= ('ICMP Timeout ' . $host . ' time=' . ((100 * (microtime(true) - $start)))) . ' ms';
				@socket_close($socket);
				$risultato = 0;
			}

			return array('risultato' => $risultato, 'html' => $html,);
		}

		protected function inverti_chr($chr){
			return chr(bindec(strrev($this->chr2bin($chr))));
		}

		protected function chr2bin($chr){
			return $this->dec2bin(ord($chr));
		}

		protected function dec2bin($dec){
			return str_pad(decbin($dec), 8, "0", STR_PAD_LEFT);
		}

		protected function recupera_html_stato_SCENE(&$scene){
			$html = '';
			$html_body = '';

			if (count($scene) > 0){
				// Scorriamo i nodi DALI
				foreach ($scene as $num_nodo => $chr_nodo){
					if ($this->verifica_scene_nodo_attive($chr_nodo)){
						$html_body .= '
					<tr>
					 <td>' . $num_nodo . '</td>
					';

						for ($j = 0; $j < strlen($chr_nodo); $j++){
							$chr = $chr_nodo[$j];
							$ADV = ord($chr);
							if ($ADV == 255) $html_body .= '<td><span class="glyphicon glyphicon-minus"></span></td>';else
								$html_body .= '<td>' . $this->recupera_html_ADV($ADV) . '</td>';
						}

						$html_body .= '
					</tr>
					';
					}
				}
			}

			if (strlen($html_body) > 0){
				$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th rowspan="2"># Nodo</th>
			   <th colspan="' . self::$MAX_NUM_SCENE_DALI . '">Scene</th>   
			  </tr>
			  <tr class="subtitle">
			  ';

				for ($i = 0; $i < self::$MAX_NUM_SCENE_DALI; $i++) $html .= '<th>' . $i . '</th>';

				$html .= '   
			  </tr>
			 </thead>
			 <tbody>
			' . $html_body . '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table> 
			';
			}else{
				$html .= $this->recupera_html_warning('Non sono presenti scene.');
			}

			return $html;
		}

		/**
		 * Questa funzione verifica se una scena è stata popolata, o meno
		 */
		protected function verifica_scene_nodo_attive(&$nodo_chr){
			return ($nodo_chr != str_repeat(chr(255), 16));
		}

		protected function recupera_html_ADV($adv){
			$adv_text = number_format(100 * ($adv / 254), 1) . '%';
			$color = '#' . ($adv > 127 ? '000' : 'fff');
			$bg_color = '#' . str_repeat(strtoupper(str_pad(dechex($adv), 2, "0", STR_PAD_LEFT)), 3);
			if ($adv == 255){
				$color = '#fff';
				$bg_color = 'rgb(217, 83, 79)';
				$adv_text = 'OFF';
			}
			return '<span 
		        title ="' . $adv . ', ' . $this->dec2hex($adv) . '"
				class="adv" 
				style="color:' . $color . '; 
				background-color: ' . $bg_color . ';">
				' . $adv_text . '
				</span>';
		}

		protected function dec2hex($dec, $NOX0 = false){
			return ($NOX0 ? '' : '0x') . str_pad(strtoupper(dechex($dec)), 2, "0", STR_PAD_LEFT);
		}

		function recupera_html_warning($msg){
			return '
		<div class="alert alert-warning" role="alert">
		  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		  <span class="sr-only">Attezione: </span>
		  ' . $msg . '
		</div>';
		}

		protected function recupera_html_stato_RETE(&$rete, $mostra_informazioni_estese = false){
			$html = '';
			$html_body = '';

			if (count($rete) > 0){
				foreach ($rete as $num_nodo => $chr_nodo){
					if ($this->verifica_nodo_attivo($chr_nodo)){
						$tipo = ord($chr_nodo[3]) % 16;
						$html_tipo = $this->recupera_html_TIPO($tipo);
						$html_versione = (ord($chr_nodo[3]) - $tipo) / 16;

						$html_body .= '
					<tr>
					 <td>' . $num_nodo . '</td>
					 <td>' . $this->recupera_html_stato_NODO($chr_nodo[0]) . '</td>
					 <td>' . $this->recupera_html_ADV(ord($chr_nodo[1])) . '</td>
					 <td><span class="badge">' . ord($chr_nodo[2]) . '</span></td>
					 <td>' . $html_tipo . ' - <span class="badge">' . $html_versione . '</span></td>
					 <td>' . $this->recupera_html_ADV(ord($chr_nodo[4])) . '</td>
					 <td>' . $this->recupera_html_ADV(ord($chr_nodo[5])) . '</td>
					 ';

						if ($mostra_informazioni_estese && strlen($chr_nodo) >= 20){
							$html_body .= '
						<td>' . $this->recupera_html_ADV(ord($chr_nodo[8])) . '</td>
						<td><span class="badge">' . ord($chr_nodo[9]) . '</span></td>
						<td>' . $this->recupera_html_FADE_RATE(ord($chr_nodo[10])) . '</td>
						<td>' . $this->recupera_html_FADE_TIME(ord($chr_nodo[11])) . '</td>
						<td><span class="badge">' . ord($chr_nodo[12]) . '</span></td>
						<td><span class="badge">' . $this->chr2hex($chr_nodo[13]) . $this->chr2hex($chr_nodo[14], true) . $this->chr2hex($chr_nodo[15], true) . '</span></td>
						<td><span class="badge">' . $this->chr2hex($chr_nodo[16]) . $this->chr2hex($chr_nodo[17], true) . $this->chr2hex($chr_nodo[18], true) . '</span></td>
						<td><span class="badge">' . ord($chr_nodo[19]) . '</span></td>
						';
						}

						$html_body .= $this->recupera_html_GROUP(ord($chr_nodo[6]), ord($chr_nodo[7])) . '
					</tr>';
					}
				}
			}

			if (strlen($html_body) > 0){
				$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th rowspan="2"># Nodo</th>
			   <th rowspan="2">Stato</th>
			   <th rowspan="2">ADV</th>
			   <th rowspan="2">Risposta</th>
			   <th rowspan="2">Tipo e versione</th>
			   <th rowspan="2">ADV<sub>min</sub></th>
			   <th rowspan="2">ADV<sub>MAX</sub></th>
			   ';

				if ($mostra_informazioni_estese){
					$html .= '
				<th rowspan="2" title="Power On Level, 254, no change, 1 – 254">POL</th>
				<th rowspan="2" title="System Failure Level, 254, no change, 0 – 255 (mask)">SFL</th>
				<th rowspan="2" title="Fade Rate, 7 (45 steps/sec), no change, 1 – 15">FR</th>
				<th rowspan="2" title="Fade Time, 0 (no fade), no change, 0 – 15">FT</th>
				<th rowspan="2" title="Short Address, 255 (no address), no change, 0 – 63, 255 (mask)">ShA</th>
				<th rowspan="2" title="Search address">SeA</th>
				<th rowspan="2" title="Random address">RnA</th>
				<th rowspan="2" title="Physical Min Level, Factory burn-in, Factory burn-in, 1 – 254">PML</th>
				';
				}

				$html .= '   
			   <th colspan="' . self::$MAX_NUM_GRUPPI_DALI . '">Gruppi</th>   
			  </tr>
			  <tr class="subtitle">
			  ';

				for ($i = 0; $i < self::$MAX_NUM_GRUPPI_DALI; $i++) $html .= '<th>' . $i . '</th>';

				$html .= '   
			  </tr>
			 </thead>
			 <tbody>
			' . $html_body . '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table> 
			';
			}else{
				$html .= $this->recupera_html_warning('Non sono presenti nodi DALI nella rete.');
			}

			return $html;
		}

		protected function recupera_html_TIPO($tipo){
			$html = '';

			$title = '';

			if (key_exists($tipo, self::$DALI_DEVICE_TYPES)) $title = ' title="' . self::$DALI_DEVICE_TYPES[$tipo] . '"';

			$html .= '<span class="badge"' . $title . '>' . $tipo . '</span>';

			return $html;
		}

		protected function recupera_html_stato_NODO($chr){
			$html = '';

			$dec = ord($chr);
			$bin = $this->dec2bin($dec);

			$title = '';
			for ($i = 0; $i < 8; $i++) if ($bin[$i] == 1) $title .= self::$BIT_STATO[$i] . PHP_EOL;

			$html .= '<span class="badge" title="' . $title . '">' . $dec . '</span>';

			return $html;
		}

		protected function recupera_html_FADE_RATE($fade_rate_dec){
			$html = '';

			$title = ' title="' . self::$FADE_RATE_TABLE[$fade_rate_dec] . ' steps/sec"';

			$html .= '<span class="badge"' . $title . '>' . $fade_rate_dec . '</span>';

			return $html;
		}

		protected function recupera_html_FADE_TIME($fade_time_dec){
			$html = '';

			$title = ' title="' . self::$FADE_TIME_TABLE[$fade_time_dec] . ' secs"';

			$html .= '<span class="badge"' . $title . '>' . $fade_time_dec . '</span>';

			return $html;
		}

		protected function chr2hex($chr, $NOX0 = false){
			return $this->dec2hex(ord($chr), $NOX0);
		}

		protected function recupera_html_GROUP($byte_6, $byte_7){
			$html = '';
			$str = str_pad(decbin($byte_6), 8, "0", STR_PAD_LEFT) . str_pad(decbin($byte_7), 8, "0", STR_PAD_LEFT);
			$bits = array();
			for ($i = 0; $i < strlen($str); $i++) $html .= '<td class="' . ($str[$i] == 0 ? 'ko' : 'ok') . '"><span class="glyphicon glyphicon-' . ($str[$i] == 0 ? 'minus' : 'ok') . '"></span></td>';
			return $html;
		}

		protected function addr_encode($addr){
			return str_replace(".", "_", $addr);
		}

		protected function addr_decode($encoded_addr){
			return str_replace("_", ".", $encoded_addr);
		}
	}

?>