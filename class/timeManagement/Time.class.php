<?php
/**
 * Copyright (c) 2018. Paolo Maruotti
 */

class Time{
	/**
	 * @var int
	 */
	protected $iHour;
	/**
	 * @var int
	 */
	protected $iMinute;
	/**
	 * @var int
	 */
	protected $iSecond;

	/**
	 * Time constructor.
	 *
	 * @param $h
	 * @param $m
	 * @param $s
	 */
	public function __construct($h, $m, $s){
			$this->iHour = $h;
			$this->iMinute = $m;
			$this->iSecond = $s;
		}
	}