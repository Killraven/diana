<?php

	/*
 * Copyright 2018 Vincenzo Suraci
 */

	require_once __DIR__ . '/dali_ethernet.class.php';
	require_once __DIR__ . '/select.class.php';

	class DALI_ETHERNET_CLIENT extends DALI_ETHERNET{
		protected $tmp_file = null;

		function __construct($addr = null, $port = null, $cmd_port = null){
			// chiamiamo il costruttore della classe padre
			parent::__construct($addr, $port, $cmd_port);

			// file temporaneo
			$this->tmp_file = $this->recupera_tmp_dir() . '/client_gui.json';

			// crichiamo il valore del counter
			$this->carica_COUNTER();
		}

		protected function carica_COUNTER(){
			$this->COUNTER = self::$COUNTER_INI;
			$key = $this->addr . ':' . $this->port;
			$stato = $this->carica_stato();
			if (key_exists($key, $stato)) $this->COUNTER = $stato[$key]['COUNTER'];else
				$this->salva_stato();
		}

		protected function carica_stato(){
			$stato = array();

			if (file_exists($this->tmp_file)) $stato = json_decode(file_get_contents($this->tmp_file), true);

			return $stato;
		}

		protected function salva_stato(){
			$stato = $this->carica_stato();
			$key = $this->addr . ':' . $this->port;

			if (key_exists($key, $stato)) $stato[$key]['COUNTER'] = $this->COUNTER;else
				$stato[$key] = array('COUNTER' => $this->COUNTER);

			$data = json_encode($stato);

			return file_put_contents($this->tmp_file, $data);
		}

		function __destruct(){
			parent::__destruct();
		}

		/**
		 * Questa funzione permette di inviare un comando al DALI-ETHERNET-CONVERTER
		 *
		 * @param $COMANDO : identificativo del comando da inviare
		 *
		 * @return un array composto da:
		 * 'risultato' => 0 se qualcosa è andato storto, 1 se tutto è andato a buon fine
		 * 'html' => HTML con le informazioni su quanto successo in fase di invio del comando e dell'attesa della risposta
		 * 'risposta' => byte con la risposta
		 */
		public function invia_comando($COMANDO, $PARAMETRI = array()){
			$risultato = 1;
			$html = '';
			$richiesta = '';
			$richiesta_html = '';
			$risposta = null;
			$risposta_html = '';
			$parametri_html = '';

			$wait_response = true;

			// Creiamo il messaggio di ricihiesta
			switch ($COMANDO){
				case self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST:
					/*
					 * This command is used to get the informations from the DALI nodes configured in the network.
					 *
					 * Note:
					 * This command must be sent to the UDP port 10000.
					 */
					$richiesta = self::$REQUEST_DALI_NETWORK_SCANNING;

					//$richiesta_html = $this->recupera_html_info('Il messaggio di richiesta contiene <b>' . strlen($richiesta) . '</b> bytes.') . $this->from_byte_to_table($richiesta, strlen($richiesta));
					$richiesta_html = $this->recupera_html_info('Request message lenght <b>' . strlen($richiesta) . '</b> bytes.') . $this->from_byte_to_table($richiesta, strlen($richiesta));

					//$html .= $this->recupera_html_info('Messaggio di richiesta creato.');
					$html .= $this->recupera_html_info('Request message created.');
					//$html .= $this->recupera_html_info('Mandando il messaggio e attendendo per una risposta...');
					$html .= $this->recupera_html_info('Sending request message, waiting for reply...');

					/*
					 * The converter sends back as response an array of data of 512 bytes. For each DALI node, 8 bytes are reserved. Here’s the data mapping of
					 * the data array sent:
					 * 0-7 Informations about DALI node 0,
					 * 8-15 Informations about DALI node 1,
					 * ... ,
					 * 504-511 Informations about DALI node 63
					 */
					$r = $this->send_message_and_get_response($this->addr, $this->port, $richiesta, $wait_response);
					$risposta = $r['risposta'];

					if ($r['risultato_trasmissione'] == 1 && $r['risultato_ricezione'] == 1){
						//$html .= $this->recupera_html_info('Messaggio inviato e risposta ricevuta.');
						$html .= $this->recupera_html_info('Message sent, received reply.');
					}else{
						if ($r['risultato_trasmissione'] == 0){
							//$html .= $this->recupera_html_info('Messaggio non inviato.') . $r['html'];
							$html .= $this->recupera_html_info('Message not sent.') . $r['html'];
							$risultato = 0;
						}

						if ($r['risultato_ricezione'] == 0){
							//$html .= $this->recupera_html_info('Risposta non ricevuta.') . $r['html'];
							$html .= $this->recupera_html_info('Replay not received.') . $r['html'];
							$risultato = 0;
						}
					}

					//if ($r['risultato_ricezione'] == 1) $risposta_html = $this->recupera_html_info('Il messaggio di risposta contiene <b>' . strlen($risposta) . '</b> bytes.') . $this->recupera_html_risposta_COMANDO_DALI_NETWORK_SCANNING($risposta);
					if ($r['risultato_ricezione'] == 1) $risposta_html = $this->recupera_html_info('Replay message lenght <b>' . strlen($risposta) . '</b> bytes.') . $this->recupera_html_risposta_COMANDO_DALI_NETWORK_SCANNING($risposta);

					break;
				case self::$COMANDO_DALI_SCENE_SCANNING_REQUEST;
					/*
					 * This command is used to get the scenes configured into the DALI nodes configured in the network.
					 *
					 * Note:
					 * This command must be sent to the UDP port 10000.
					 */
					$richiesta = self::$REQUEST_DALI_SCENE_SCANNING;

					//$richiesta_html = $this->recupera_html_info('Il messaggio di richiesta contiene <b>' . strlen($richiesta) . '</b> bytes.') . $this->from_byte_to_table($richiesta, strlen($richiesta));
					$richiesta_html = $this->recupera_html_info('Request message lenght <b>' . strlen($richiesta) . '</b> bytes.') . $this->from_byte_to_table($richiesta, strlen($richiesta));

					//$html .= $this->recupera_html_info('Messaggio di richiesta creato.');
					$html .= $this->recupera_html_info('Request message created.');
					//$html .= $this->recupera_html_info('Mandando il messaggio e attendendo per una risposta...');
					$html .= $this->recupera_html_info('Sending request message, waiting for reply...');

					/*
					 * The converter sends back as response an array of data of 1024 bytes. For each DALI node, 16 bytes are reserved, one for each possible
					 * scene configured. Here’s the data mapping of the data array sent:
					 * 0-15 Scenes configured on DALI node 0
					 * 16-31 Scenes configured on DALI node 1
					 * ...
					 * 1008-1023 Scenes configured on DALI node 63
					 *
					 * The bytes reserved for the scenes can have these values:
					 * 0-254 = Value set for the selected scene
					 * 255 = Scene not configures
					 * Note:
					 * In each group of 16 bytes, the first one is for the Scene 0 and the last one is for Scene 15.
					 */
					$r = $this->send_message_and_get_response($this->addr, $this->port, $richiesta, $wait_response);
					$risposta = $r['risposta'];

					if ($r['risultato_trasmissione'] == 1 && $r['risultato_ricezione'] == 1){
						//$html .= $this->recupera_html_info('Messaggio inviato e risposta ricevuta.');
						$html .= $this->recupera_html_info('Message sent, reply received.');
					}else{
						if ($r['risultato_trasmissione'] == 0){
							$risultato = 0;
							//$html .= $this->recupera_html_info('Messaggio non inviato.') . $r['html'];
							$html .= $this->recupera_html_info('Message not sent.') . $r['html'];
						}

						if ($r['risultato_ricezione'] == 0){
							$risultato = 0;
							//$html .= $this->recupera_html_info('Risposta non ricevuta.') . $r['html'];
							$html .= $this->recupera_html_info('Reply not received.') . $r['html'];
						}
					}

					//if ($r['risultato_ricezione'] == 1) $risposta_html = $this->recupera_html_info('Il messaggio di risposta contiene <b>' . strlen($risposta) . '</b> bytes.') . $this->recupera_html_risposta_COMANDO_DALI_SCENE_SCANNING($risposta);
					if ($r['risultato_ricezione'] == 1) $risposta_html = $this->recupera_html_info('Reply message lenght <b>' . strlen($risposta) . '</b> bytes.') . $this->recupera_html_risposta_COMANDO_DALI_SCENE_SCANNING($risposta);

					break;
				case self::$COMANDO_ADV_SETTING_REQUEST:
					/*
					 * This command is used to set the ADV for a specific DALI node, for a Group or for the entire DALI network.
					 */
					if (count($PARAMETRI) == 0){
						// Dobbiamo semplicemente restituire il pannello di gestione del ADV SETTING
						$parametri_html = $this->recupera_html_parametri_COMANDO_ADV_SETTING();
					}else{
						// recuperiamo i parametri
						$ADV = $PARAMETRI['ADV'];
						$COUNTER = $PARAMETRI['COUNTER'];
						$DALI_ID = $PARAMETRI['DALI_ID'];

						// creiamo la richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr($DALI_ID + 1);
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr($DALI_ID);
						$richiesta[9] = chr($ADV);
						$richiesta[10] = chr(hexdec('00'));

						//$html .= $this->recupera_html_info('Messaggio di richiesta creato.');
						$html .= $this->recupera_html_info('Request message created.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						$parametri_html = $this->recupera_html_parametri_COMANDO_ADV_SETTING($DALI_ID, $ADV);

						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						if ($r['risultato_trasmissione'] == 1){
							//$html .= $this->recupera_html_successo('Messaggio inviato.');
							$html .= $this->recupera_html_successo('Message sent.');
						}else{
							$html .= $this->recupera_html_errore('Message not sebnt.') . $r['html'];
							$risultato = 0;
						}

						$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);
					}
					break;
				case self::$COMANDO_SCENE_SETTING_REQUEST:
					/*
					 * This command is used to activate a programmed scene in a single DALI node, in a group or for the entire DALI network.
					 */
					if (count($PARAMETRI) == 0){
						// Dobbiamo semplicemente restituire il pannello di gestione del ADV SETTING
						$parametri_html = $this->recupera_html_parametri_COMANDO_SCENE_SETTING();
					}else{
						// recuperiamo i parametri
						$SCENE = $PARAMETRI['SCENE'];
						$COUNTER = $PARAMETRI['COUNTER'];
						$DALI_ID = $PARAMETRI['DALI_ID'];

						// creiamo la richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr($DALI_ID + hexdec('80'));
						$richiesta[9] = chr($SCENE + hexdec('10')); // numero compreso tra 16 e 31
						$richiesta[10] = chr(hexdec('00'));

						//$html .= $this->recupera_html_info('Messaggio di richiesta creato.');
						$html .= $this->recupera_html_info('Request message created.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						$parametri_html = $this->recupera_html_parametri_COMANDO_SCENE_SETTING($DALI_ID, $SCENE);

						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);

						if ($r['risultato_trasmissione'] == 1){
							//$html .= $this->recupera_html_successo('Messaggio inviato.');
							$html .= $this->recupera_html_successo('Message sent.');
						}else{
							//$html .= $this->recupera_html_errore('Messaggio non inviato.') . $r['html'];
							$html .= $this->recupera_html_errore('Message not sent.') . $r['html'];
							$risultato = 0;
						}

						$risposta = $r['risposta'];

						$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);
					}
					break;
				case self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST:
					/*
					 * This command is used to send a generic DALI command to a DALI node, to a group or to the entire network.
					 */
					if (count($PARAMETRI) == 0){
						// Dobbiamo semplicemente restituire il pannello di gestione del ADV SETTING
						$parametri_html = $this->recupera_html_parametri_COMANDO_GENERIC_DALI_COMMAND();
					}else{
						// recuperiamo i parametri
						$DALI_COMMAND = $PARAMETRI['DALI_COMMAND'];
						$COUNTER = $PARAMETRI['COUNTER'];
						$DALI_ID = $PARAMETRI['DALI_ID'];

						if (key_exists('DTR', $PARAMETRI)){
							// creiamo la PRIMA richiesta formata da 11 byte per settare il DTR
							$richiesta = str_repeat(chr(0), 11);
							$richiesta[0] = chr(hexdec('00'));
							$richiesta[1] = chr(hexdec('10'));
							$richiesta[2] = chr(hexdec('00'));
							$richiesta[3] = chr(hexdec('00'));
							$richiesta[4] = chr(hexdec('00'));
							$richiesta[5] = chr(hexdec('02'));
							$richiesta[6] = chr(hexdec('04'));
							$richiesta[7] = chr($COUNTER);
							$richiesta[8] = chr(hexdec('D1'));
							$richiesta[9] = chr($PARAMETRI['DTR']);
							$richiesta[10] = chr(hexdec('40')); // Probabile errore nella documentazione. Questo non è 0x00 ma 0x40

							//$html .= $this->recupera_html_info('Messaggio di richiesta 1 creato.');
							$html .= $this->recupera_html_info('First request message created.');

							// aumentiamo di uno il counter
							$this->incrementa_COUNTER();

							// mandiamo il messaggi senza aspettare risposta
							$wait_response = false;
							$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
							$risposta = $r['risposta'];

							if ($r['risultato_trasmissione'] == 1){
								//$html .= $this->recupera_html_successo('Messaggio 1 inviato.');
								$html .= $this->recupera_html_successo('First message sent.');
							}else{
								$html .= $this->recupera_html_errore('First message not sent.') . $r['html'];
								$risultato = 0;
							}

							// stampiamo la richiesta in HTML
							$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);
						}

						// creiamo la richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr($DALI_ID + hexdec('80'));
						$richiesta[9] = chr($DALI_COMMAND);
						$richiesta[10] = chr(hexdec('00'));

						//$html .= $this->recupera_html_info('Messaggio di richiesta creato.');
						$html .= $this->recupera_html_info('Request message created.');
						//$html .= $this->recupera_html_info('Mandando il messaggio e attendendo per una risposta...');
						$html .= $this->recupera_html_info('Sending message, waiting for reply...');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						$parametri_html = $this->recupera_html_parametri_COMANDO_GENERIC_DALI_COMMAND($DALI_ID, $DALI_COMMAND);

						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						if ($r['risultato_trasmissione'] == 1){
							$html .= $this->recupera_html_successo('Messaggio inviato.');
						}else{
							$html .= $this->recupera_html_errore('Messaggio non inviato.') . $r['html'];
							$risultato = 0;
						}

						$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);

						$risposta_html = $this->recupera_html_info('Il messaggio di risposta contiene <b>' . strlen($risposta) . '</b> bytes.') . $this->recupera_html_risposta_COMANDO_GENERIC_DALI_COMMAND($risposta);
					}
					break;
				case self::$COMANDO_SCENE_PROGRAMMING_REQUEST:
					/*
					 * This commands are used to program the scenes inside the DALI nodes. It is necessary to send two consecutive commands for the programming.
					 */
					if (count($PARAMETRI) == 0){
						// Dobbiamo semplicemente restituire il pannello di gestione del ADV SETTING
						$parametri_html = $this->recupera_html_parametri_COMANDO_SCENE_PROGRAMMING();
					}else{
						// recuperiamo i parametri
						$ADV = $PARAMETRI['ADV'];
						$SCENE = $PARAMETRI['SCENE'];
						$COUNTER = $PARAMETRI['COUNTER'];
						$DALI_ID = $PARAMETRI['DALI_ID'];

						// creiamo la PRIMA richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr(hexdec('D1'));
						$richiesta[9] = chr($ADV);
						$richiesta[10] = chr(hexdec('40')); // Probabile errore nella documentazione. Questo non è 0x00 ma 0x40

						$html .= $this->recupera_html_info('Messaggio di richiesta 1 creato.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						// carichiamo l'html con i parametri
						$parametri_html = $this->recupera_html_parametri_COMANDO_SCENE_PROGRAMMING($ADV, $DALI_ID, $SCENE);

						// mandiamo il messaggi senza aspettare risposta
						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						$html .= $this->recupera_html_info('Messaggio 1 inviato.');

						// stampiamo la richiesta in HTML
						$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);

						// creiamo la SECONDA richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr($DALI_ID + hexdec('80'));
						$richiesta[9] = chr($SCENE + hexdec('40'));
						$richiesta[10] = chr(hexdec('00'));

						$html .= $this->recupera_html_info('Messaggio di richiesta 2 creato.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						// mandiamo il messaggi senza aspettare risposta
						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						$html .= $this->recupera_html_info('Messaggio 2 inviato.');

						// stampiamo la richiesta in HTML
						$richiesta_html .= $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);
					}
					break;
				case self::$COMANDO_GROUP_SETTING_REQUEST:
					/*
					 * This command is used to enable/disable the membership of the DALI nodes inside the group.
					 */
					if (count($PARAMETRI) == 0){
						// Dobbiamo semplicemente restituire il pannello di gestione del ADV SETTING
						$parametri_html = $this->recupera_html_parametri_COMANDO_GROUP_SETTING();
					}else{
						// recuperiamo i parametri
						$GROUP = $PARAMETRI['GROUP'];
						$COUNTER = $PARAMETRI['COUNTER'];
						$DALI_ID = $PARAMETRI['DALI_ID'];
						$ENABLE = $PARAMETRI['AZIONE'];

						// creiamo la PRIMA richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr($DALI_ID + hexdec('80'));
						$richiesta[9] = ($ENABLE == 1 ? chr($GROUP + hexdec('60')) : chr($GROUP + hexdec('70')));
						$richiesta[10] = chr(hexdec('00'));

						$html .= $this->recupera_html_info('Messaggio di richiesta creato.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						// carichiamo l'html con i parametri
						$parametri_html = $this->recupera_html_parametri_COMANDO_GROUP_SETTING($DALI_ID, $GROUP, $ENABLE);

						// mandiamo il messaggi senza aspettare risposta
						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						$html .= $this->recupera_html_info('Messaggio inviato.');

						// stampiamo la richiesta in HTML
						$richiesta_html .= $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);
					}
					break;
				case self::$COMANDO_NODE_ID_SETTING_REQUEST:
					/*
					 * These commands are used to program the ID Address on the DALI nodes. It is necessary to send them consecutively.
					 *
					 * IMPORTANT: it is possible to connect a single DALI node to the HD67839 to program correctly the ID Node.
					 * IMPORTANT: for correct setting, it is necessary to send the “REQUEST 2” two times. This is a specification from DALI protocol.
					 */
					if (count($PARAMETRI) == 0){
						// Dobbiamo semplicemente restituire il pannello di gestione del ADV SETTING
						$parametri_html = $this->recupera_html_parametri_COMANDO_NODE_ID_SETTING();
					}else{
						// recuperiamo i parametri
						$COUNTER = $PARAMETRI['COUNTER'];
						$DALI_ID = $PARAMETRI['DALI_ID'];

						// creiamo la PRIMA richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr(hexdec('D1'));
						$richiesta[9] = chr($DALI_ID + 1);
						$richiesta[10] = chr(hexdec('00'));

						$html .= $this->recupera_html_info('Messaggio di richiesta 1 creato.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						// carichiamo l'html con i parametri
						$parametri_html = $this->recupera_html_parametri_COMANDO_NODE_ID_SETTING($DALI_ID);

						// mandiamo il messaggi senza aspettare risposta
						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						$html .= $this->recupera_html_info('Messaggio 1 inviato.');

						// stampiamo la richiesta in HTML
						$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);

						// creiamo la SECONDA richiesta formata da 11 byte
						$richiesta = str_repeat(chr(0), 11);
						$richiesta[0] = chr(hexdec('00'));
						$richiesta[1] = chr(hexdec('10'));
						$richiesta[2] = chr(hexdec('00'));
						$richiesta[3] = chr(hexdec('00'));
						$richiesta[4] = chr(hexdec('00'));
						$richiesta[5] = chr(hexdec('02'));
						$richiesta[6] = chr(hexdec('04'));
						$richiesta[7] = chr($COUNTER);
						$richiesta[8] = chr(hexdec('FF'));
						$richiesta[9] = chr(hexdec('80'));
						$richiesta[10] = chr(hexdec('00'));

						$html .= $this->recupera_html_info('Messaggio di richiesta 2 creato.');

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();

						// mandiamo il messaggi senza aspettare risposta
						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						$html .= $this->recupera_html_info('Messaggio 2 inviato una prima volta.');

						// stampiamo la richiesta in HTML
						$richiesta_html .= $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);

						// aumentiamo di uno il counter
						$this->incrementa_COUNTER();
						$richiesta[7] = chr($COUNTER + 1);

						// mandiamo il messaggi senza aspettare risposta
						$wait_response = false;
						$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
						$risposta = $r['risposta'];

						$html .= $this->recupera_html_info('Messaggio 2 inviato una seconda volta.');

						// stampiamo la richiesta in HTML
						$richiesta_html .= $this->from_byte_to_table($richiesta, strlen($richiesta), false, true, true);
					}
					break;
				case self::$COMANDO_CLOSE_SERVER_SOCKET:

					$richiesta = self::$REQUEST_CLOSE_SERVER_SOCKET;
					$html .= $this->recupera_html_info('Messaggio di richiesta creato.');
					$wait_response = false;

					$r = $this->send_message_and_get_response($this->addr, $this->port, $richiesta, $wait_response);
					$risposta = $r['risposta'];
					$html .= $this->recupera_html_info('Messaggio per il server principale inviato.');
					$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta));

					$r = $this->send_message_and_get_response($this->addr, $this->cmd_port, $richiesta, $wait_response);
					$risposta = $r['risposta'];
					$html .= $this->recupera_html_info('Messaggio per il server comandi inviato.');
					$richiesta_html = $this->from_byte_to_table($richiesta, strlen($richiesta));

					break;
				default:
					$risultato = 0;
					$html .= 'Comando <b>' . $COMANDO . '</b> sconosciuto.';
					break;
			}

			return array('risultato' => $risultato, 'html' => $html, 'richiesta' => $richiesta, 'richiesta_html' => $richiesta_html, 'risposta' => $risposta, 'risposta_html' => $risposta_html, 'parametri_html' => $parametri_html,);
		}

		protected function from_byte_to_table($str, $cols = 8, $print_chr = true, $print_dec = false, $print_hex = true){
			$html = '';

			$html .= '
		<table class="table">
		 <thead>
		  <tr class="title">
		   <th>Byte #</th>
		   ';

			for ($col = 1; $col <= $cols; $col++) $html .= '<th>' . $col . '</th>';

			$html .= '   
		  </tr>
		 </thead>
		 <tbody>
		';

			$tr_chr = '';
			$tr_DEC = '';
			$tr_HEX = '';

			$num_bytes = strlen($str);

			for ($num_byte = 0; $num_byte <= $num_bytes; $num_byte++){
				if (($num_byte % $cols) == 0){
					if ($num_byte > 0){
						$tr_chr .= '</tr>';
						$tr_DEC .= '</tr>';
						$tr_HEX .= '</tr>';
						if ($print_hex) $html .= $tr_HEX;
						if ($print_dec) $html .= $tr_DEC;
						if ($print_chr) $html .= $tr_chr;
						$tr_chr = '';
						$tr_DEC = '';
						$tr_HEX = '';
					}
					$tr_chr .= '<tr><th class="subtitle">Text</th>';
					$tr_DEC .= '<tr><th class="subtitle">Dec</th>';
					$tr_HEX .= '<tr><th class="subtitle">Hex</th>';
				}

				if ($num_byte < $num_bytes){
					$chr = substr($str, $num_byte, 1);
					$DEC = ord($chr);
					$HEX = bin2hex($chr);

					$tr_chr .= '<td>' . $chr . '</td>';
					$tr_DEC .= '<td>' . $DEC . '</td>';
					$tr_HEX .= '<td>0x' . strtoupper($HEX) . '</td>';
				}
			}

			$pad = $num_bytes % $cols;
			if ($pad > 0){
				$colspan = $cols - $pad;
				$tr_chr .= '<td colspan="' . $colspan . '"></td></tr>';
				$tr_DEC .= '<td colspan="' . $colspan . '"></td></tr>';
				$tr_HEX .= '<td colspan="' . $colspan . '"></td></tr>';
				if ($print_hex) $html .= $tr_HEX;
				if ($print_dec) $html .= $tr_DEC;
				if ($print_chr) $html .= $tr_chr;
			}

			$html .= '
		 </tbody>
		 <tfoot>
		 </tfoot>
		</table> 
		';

			return $html;
		}

		/**
		 * Alcuni messaggi vanno sulla porta 10.000, altri su una altra porta configurabile, ad es. 10.001
		 */
		protected function send_message_and_get_response($addr, $port, $msg, $wait_response = false){
			$risultato_trasmissione = 1;
			$risultato_ricezione = 1;
			$html = '';
			$risposta = false;

			$local_ip = $this->recupera_ip_macchina();

			// controlliamo che il numero di porta sia, appunto, un numero
			if (!is_numeric($port)){
				$risultato_trasmissione = 0;
				$html .= $this->recupera_html_errore('La porta <b>' . $port . '</b> deve essere un numero.');
			}else{
				// creiamo la socket UDP per inviare/ricevere messaggi
				if (false === ($this->send_socket = @socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))){
					$errorcode = socket_last_error();
					$errormsg = socket_strerror($errorcode);
					$risultato_trasmissione = 0;
					$html .= $this->recupera_html_errore('Errore in fase di creazione della socket UDP.');
					$html .= $this->recupera_html_errore($errormsg);
				}else{
					// agganciamo la socket UDP alla porta per inviare/ricevere messaggi
					if (false === @socket_bind($this->send_socket, $local_ip, $port)){
						// non riusciamo ad aggianciare la porta
						$errorcode = socket_last_error();
						$errormsg = socket_strerror($errorcode);

						// verifichiamo se client e server stanno sulla stessa macchina
						if ($local_ip == $addr){
							$html .= $this->recupera_html_warning('Impossibile agganciare la socket UDP <b>' . $local_ip . ':' . $port . '</b>.');
							$html .= $this->recupera_html_warning($errormsg);
							$html .= $this->recupera_html_warning('Server e client risiedono sulla stessa macchina, quindi si può ignorare il precedente errore.');
						}else{
							// client e server non sono nella stessa macchina: c'è un problema!
							$risultato_trasmissione = 0;
							$risultato_ricezione = 0;
							$html .= $this->recupera_html_errore('Impossibile agganciare la socket UDP <b>' . $local_ip . ':' . $port . '</b>.');
							$html .= $this->recupera_html_errore($errormsg);
						}
					}

					if ($risultato_trasmissione == 1){
						// calcoliamo la lunghezza del messaggio da inviare
						$len = strlen($msg);

						// inviamo il messaggio
						if (false === ($output = @socket_sendto($this->send_socket, $msg, $len, 0, $addr, $port))){
							$errorcode = socket_last_error();
							$errormsg = socket_strerror($errorcode);
							$risultato_trasmissione = 0;
							$html .= $this->recupera_html_errore('Errore in fase di trasmissione sulla socket UDP <b>' . $addr . ':' . $port . '</b>.');
							$html .= $this->recupera_html_errore($errormsg);
						}else{
							$html .= $this->recupera_html_successo('Messaggio inviato (<b>' . $len . '</b> bytes).');

							if ($wait_response){
								$html .= $this->recupera_html_info('In attesa di una risposta...');

								// attendiamo la risposta sulla stessa socket di invio - BLOCCANTE
								if (false === @socket_recvfrom($this->send_socket, $risposta, self::$MAX_DATAGRAM_SIZE, 0, $remote_ip, $remote_port)){
									$errorcode = socket_last_error();
									$errormsg = socket_strerror($errorcode);
									$risultato_ricezione = 0;
									$html .= $this->recupera_html_errore('Errore in fase di ricezione.');
									$html .= $this->recupera_html_errore($errormsg);
								}
							}
						}
					}
					@socket_close($this->send_socket);
				}
			}

			return array('risultato_trasmissione' => $risultato_trasmissione, 'risultato_ricezione' => $risultato_ricezione, 'html' => $html, 'risposta' => $risposta,);
		}

		protected function recupera_html_risposta_COMANDO_DALI_NETWORK_SCANNING(&$risposta){
			$html = '';

			// Gli ultimi 8 byte sono riservati
			$lunghezza_risposta_attesa = 8 * self::$MAX_NUM_NODI_DALI;
			$lunghezza_risposta = strlen($risposta);

			if ($lunghezza_risposta_attesa != $lunghezza_risposta){
				//$html .= $this->recupera_html_errore('La risposta è lunga <b>' . $lunghezza_risposta . '</b> bytes, invece doveva essere lunga <b>' . $lunghezza_risposta_attesa . '</b> bytes.');
				$html .= $this->recupera_html_errore('Reply is <b>' . $lunghezza_risposta . '</b> bytes long, it had to be <b>' . $lunghezza_risposta_attesa . '</b> bytes long.');
			}else{
				$rete = array();
				for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++){
					$byte_min = 8 * $i;
					$byte_MAX = 8 * ($i + 1) - 1;
					$rete[$i] = substr($risposta, $byte_min, 8);
					$rete[$i][6] = $this->inverti_chr($rete[$i][6]);
					$rete[$i][7] = $this->inverti_chr($rete[$i][7]);
				}
				$html .= $this->recupera_html_stato_RETE($rete);
			}

			return $html;
		}

		protected function recupera_html_risposta_COMANDO_DALI_SCENE_SCANNING(&$risposta){
			$html = '';

			// Gli ultimi 8 byte sono riservati
			$lunghezza_risposta_attesa = self::$MAX_NUM_SCENE_DALI * self::$MAX_NUM_NODI_DALI;
			$lunghezza_risposta = strlen($risposta);

			if ($lunghezza_risposta_attesa != $lunghezza_risposta){
				//$html .= $this->recupera_html_errore('La risposta è lunga <b>' . $lunghezza_risposta . '</b> bytes, invece doveva essere lunga <b>' . $lunghezza_risposta_attesa . '</b> bytes.');
				$html .= $this->recupera_html_errore('Reply is <b>' . $lunghezza_risposta . '</b> bytes long, it had to be <b>' . $lunghezza_risposta_attesa . '</b> bytes long.');
			}else{
				$scene = array();
				for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++){
					$byte_min = self::$MAX_NUM_SCENE_DALI * $i;
					$byte_MAX = self::$MAX_NUM_SCENE_DALI * ($i + 1) - 1;
					$scene[$i] = substr($risposta, $byte_min, self::$MAX_NUM_SCENE_DALI);
				}
				$html .= $this->recupera_html_stato_SCENE($scene);
			}

			return $html;
		}

		protected function recupera_html_parametri_COMANDO_ADV_SETTING($DALI_ID = '', $ADV = ''){
			$html = '';

			// DALI ID
			$html .= '
		<div class="block">
		<b>DALI ID</b>
		<br/>
		' . $this->recupera_html_select_DALI_ID($DALI_ID, false, 80) . '
		</div>
		';

			// ADV
			$html .= '
		<div class="block">
		<b>ADV</b>
		<br/>
		' . $this->recupera_html_select_ADV($ADV) . '
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<b>COUNTER</b>
		<br/>
		<input id="COUNTER" value="' . $this->COUNTER . '" type="text" class="form-control" placeholder="Indirizzo IP" aria-describedby="basic-addon2" disabled>
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<br/>
		<button type="button" class="btn btn-success" onclick="comando($(this),' . self::$COMANDO_ADV_SETTING_REQUEST . ');"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;Send</button>
		</div>
		';

			return $html;
		}

		protected function recupera_html_select_DALI_ID($DALI_ID = '', $solo_nodi = false, $broadcast_dec = 127){
			$elements = array();

			// aggiungiamo i nodi
			for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++) $elements[$i] = 'Nodo ' . $i;

			if (!$solo_nodi){
				for ($i = 0; $i < self::$MAX_NUM_GRUPPI_DALI; $i++) $elements[$i + self::$MAX_NUM_NODI_DALI] = 'Gruppo ' . $i;

				$elements[$broadcast_dec] = 'Broadcast';
			}

			$html = HtmlSelect::getHTMLselectFromArray($elements, $DALI_ID, 'DALI_ID', 'DALI_ID', '', '', false, true);

			$html = str_replace('<select', '<select data-size="10"', $html);

			return $html;
		}

		protected function recupera_html_select_ADV($ADV = ''){
			$elements = array();

			// aggiungiamo i nodi
			for ($i = 0; $i <= 255; $i++) $elements[$i] = '0x' . str_pad(strtoupper(dechex($i)), 2, "0", STR_PAD_LEFT);

			$html = HtmlSelect::getHTMLselectFromArray($elements, $ADV, 'ADV', 'ADV', '', '', false, true);

			for ($adv = 0; $adv < 255; $adv++) $html = str_replace('value="' . $adv . '"', 'value="' . $adv . '" style="background: #' . str_repeat(str_pad(strtoupper(dechex($adv)), 2, "0", STR_PAD_LEFT), 3) . '; color: #' . ($adv < 125 ? 'FFF' : '000') . ';"', $html);

			$html = str_replace('<select', '<select data-size="10"', $html);

			return $html;
		}

		protected function incrementa_COUNTER(){
			$this->COUNTER++;
			if ($this->COUNTER > self::$COUNTER_MAX) $this->COUNTER = self::$COUNTER_MIN;
			$this->salva_stato();
		}

		protected function recupera_html_parametri_COMANDO_SCENE_SETTING($DALI_ID = '', $SCENE = ''){
			$html = '';

			// DALI ID
			$html .= '
		<div class="block">
		<b>DALI ID</b>
		<br/>
		' . $this->recupera_html_select_DALI_ID($DALI_ID) . '
		</div>
		';

			// SCENE
			$html .= '
		<div class="block">
		<b>SCENE</b>
		<br/>
		' . $this->recupera_html_select_SCENE($SCENE) . '
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<b>COUNTER</b>
		<br/>
		<input id="COUNTER" value="' . $this->COUNTER . '" type="text" class="form-control" placeholder="Indirizzo IP" aria-describedby="basic-addon2" disabled>
		</div>
		';

			// INVIA
			$html .= '
		<div class="block">
		<br/>
		<button type="button" class="btn btn-success" onclick="comando($(this),' . self::$COMANDO_SCENE_SETTING_REQUEST . ');"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;Send</button>
		</div>
		';

			return $html;
		}

		protected function recupera_html_select_SCENE($SCENE = ''){
			$elements = array();

			// aggiungiamo i nodi
			for ($i = 0; $i < self::$MAX_NUM_SCENE_DALI; $i++) $elements[$i] = 'Scene ' . $i;

			$html = HtmlSelect::getHTMLselectFromArray($elements, $SCENE, 'SCENE', 'SCENE', '', '', false, true);

			$html = str_replace('<select', '<select data-size="10"', $html);

			return $html;
		}

		protected function recupera_html_parametri_COMANDO_GENERIC_DALI_COMMAND($DALI_ID = '', $DALI_COMMAND = ''){
			$html = '';

			// DALI ID
			$html .= '
		<div class="block">
		<b>DALI ID</b>
		<br/>
		' . $this->recupera_html_select_DALI_ID($DALI_ID) . '
		</div>
		';

			// SCENE
			$html .= '
		<div class="block">
		<b>DALI COMMAND</b>
		<br/>
		' . $this->recupera_html_select_DALI_COMMAND($DALI_COMMAND) . '
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<b>COUNTER</b>
		<br/>
		<input id="COUNTER" value="' . $this->COUNTER . '" type="text" class="form-control" placeholder="Indirizzo IP" aria-describedby="basic-addon2" disabled>
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<br/>
		<button type="button" class="btn btn-success" onclick="comando($(this),' . self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST . ');"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;Send</button>
		</div>
		';

			return $html;
		}

		protected function recupera_html_select_DALI_COMMAND($DALI_COMMAND = ''){
			$elements = array();

			// aggiungiamo i comandi
			foreach (self::$GENERIC_DALI_COMMAND_LIST as $comando){
				$testo_comando = $comando[2];
				if ($comando[0] == $comando[1]){
					$elements[$comando[0]] = $this->dec2hex($comando[0]) . ' - ' . $testo_comando;
				}elseif (false !== strpos($testo_comando, "(n)")){
					for ($i = $comando[0]; $i <= $comando[1]; $i++){
						$elements[$i] = $this->dec2hex($i) . ' - ' . str_replace('(n)', '(' . ($i - $comando[0]) . ')', $testo_comando);
					}
				}else{
					for ($i = $comando[0]; $i <= $comando[1]; $i++){
						$elements[$i] = $this->dec2hex($i) . ' - ' . $testo_comando;
					}
				}
			}

			$html = HtmlSelect::getHTMLselectFromArray($elements, $DALI_COMMAND, 'DALI_COMMAND', 'DALI_COMMAND', '', '', false, true);

			$html = str_replace('<select', '<select data-size="10"', $html);

			return $html;
		}

		protected function recupera_html_risposta_COMANDO_GENERIC_DALI_COMMAND(&$risposta){
			$html = '';

			if (strlen($risposta) == 10 || $risposta == self::$RESPONSE_ANSWER_NOT_RECEIVED) $html .= $this->from_byte_to_table($risposta, min(10, strlen($risposta)));else
				$html .= $this->recupera_html_errore('La risposta ottenuta è diversa da quella attesa.');

			return $html;
		}

		protected function recupera_html_parametri_COMANDO_SCENE_PROGRAMMING($ADV = '', $DALI_ID = '', $SCENE = ''){
			$html = '';

			// ADV
			$html .= '
		<div class="block">
		<b>ADV</b>
		<br/>
		' . $this->recupera_html_select_ADV($ADV) . '
		</div>
		';

			// DALI ID
			$html .= '
		<div class="block">
		<b>DALI ID</b>
		<br/>
		' . $this->recupera_html_select_DALI_ID($DALI_ID) . '
		</div>
		';

			// SCENE
			$html .= '
		<div class="block">
		<b>SCENE</b>
		<br/>
		' . $this->recupera_html_select_SCENE($SCENE) . '
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<b>COUNTER</b>
		<br/>
		<input id="COUNTER" value="' . $this->COUNTER . '" type="text" class="form-control" placeholder="Indirizzo IP" aria-describedby="basic-addon2" disabled>
		</div>
		';

			// INVIA
			$html .= '
		<div class="block">
		<br/>
		<button type="button" class="btn btn-success" onclick="comando($(this),' . self::$COMANDO_SCENE_PROGRAMMING_REQUEST . ');"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;Send</button>
		</div>
		';

			return $html;
		}

		protected function recupera_html_parametri_COMANDO_GROUP_SETTING($DALI_ID = '', $GROUP_NUMBER = '', $ENABLE = 1){
			$html = '';

			// DALI ID
			$html .= '
		<div class="block">
		<b>DALI ID</b>
		<br/>
		' . $this->recupera_html_select_DALI_ID($DALI_ID, true) . '
		</div>
		';

			// GROUP
			$html .= '
		<div class="block">
		<b>Group</b>
		<br/>
		' . $this->recupera_html_select_GROUP($GROUP_NUMBER) . '
		</div>
		';

			// STATUS
			$html .= '
		<div class="block"> 
		<b>Action</b>
		<br/>
		<select class="selectpicker" id="AZIONE">
		  <option class="enable" ' . ($ENABLE == 1 ? 'selected' : '') . ' value="1">Add</option>
		  <option class="disable" ' . ($ENABLE == 0 ? 'selected' : '') . ' value="0">Remove</option>
		</select>
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<b>COUNTER</b>
		<br/>
		<input id="COUNTER" value="' . $this->COUNTER . '" type="text" class="form-control" placeholder="Indirizzo IP" aria-describedby="basic-addon2" disabled>
		</div>
		';

			// INVIA
			$html .= '
		<div class="block">
		<br/>
		<button type="button" class="btn btn-success" onclick="comando($(this),' . self::$COMANDO_GROUP_SETTING_REQUEST . ');"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;Send</button>
		</div>
		';

			return $html;
		}

		protected function recupera_html_select_GROUP($GROUP = ''){
			$elements = array();

			// aggiungiamo i nodi
			for ($i = 0; $i < self::$MAX_NUM_GRUPPI_DALI; $i++) $elements[$i] = 'Gruppo ' . $i;

			$html = HtmlSelect::getHTMLselectFromArray($elements, $GROUP, 'GROUP', 'GROUP', '', '', false, true);

			$html = str_replace('<select', '<select data-size="10"', $html);

			return $html;
		}

		protected function recupera_html_parametri_COMANDO_NODE_ID_SETTING($DALI_ID = ''){
			$html = '';

			// DALI ID
			$html .= '
		<div class="block">
		<b>DALI ID</b>
		<br/>
		' . $this->recupera_html_select_DALI_ID($DALI_ID, true) . '
		</div>
		';

			// COUNTER
			$html .= '
		<div class="block">
		<b>COUNTER</b>
		<br/>
		<input id="COUNTER" value="' . $this->COUNTER . '" type="text" class="form-control" placeholder="IP Address" aria-describedby="basic-addon2" disabled>
		</div>
		';

			// INVIA
			$html .= '
		<div class="block">
		<br/>
		<button type="button" class="btn btn-success" onclick="comando($(this),' . self::$COMANDO_NODE_ID_SETTING_REQUEST . ');"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;Send</button>
		</div>
		';

			return $html;
		}

		protected function recupera_html_GROUP($byte_6, $byte_7){
			$html = '';
			$str = str_pad(decbin($byte_6), 8, "0", STR_PAD_LEFT) . str_pad(decbin($byte_7), 8, "0", STR_PAD_LEFT);
			$bits = array();
			for ($i = 0; $i < strlen($str); $i++) $html .= '<td class="' . ($str[$i] == 0 ? 'ko' : 'ok') . '"><span class="glyphicon glyphicon-' . ($str[$i] == 0 ? 'minus' : 'ok') . '"></span></td>';
			return $html;
		}
	}

?>