<?php
	/*
	 * Copyright 2018 Vincenzo Suraci
	 */

	require_once __DIR__ . '/dali_ethernet.class.php';
	include_once(__DIR__ ."/dbEntity/bean.config.php");
	include_once(__DIR__ ."/dbEntity/BeanDevice.php");

	class DALI_ETHERNET_SERVER extends DALI_ETHERNET{
		//---------------------------------------------------------------------------------------------
		// STATO DELLA RETE DI NODI DALI
		//---------------------------------------------------------------------------------------------

		protected $rete = null;
		protected $scene = null;

		//---------------------------------------------------------------------------------------------
		// LOG
		//---------------------------------------------------------------------------------------------

		protected $log_filename;
		protected $log_file;

		//---------------------------------------------------------------------------------------------
		// STATUS
		//---------------------------------------------------------------------------------------------

		protected $status_filename;
		protected $status_file;

		//---------------------------------------------------------------------------------------------
		// SESSIONE
		//---------------------------------------------------------------------------------------------

		protected $sessione;

		//---------------------------------------------------------------------------------------------
		// FLAG
		//---------------------------------------------------------------------------------------------

		// check if the server must continue listening
		protected $listen;

		// tiene conto se il server è stato avviato o meno
		protected $server_avviato;

		// registra i dati temporanei
		protected $DTR_chr;

		// registra l'ultima risposta inviata
		protected $ultima_risposta_inviata;

		function __construct($addr = null, $port = null, $cmd_port = null, $sessione = false, $command_server = false){
			// chiamiamo il costruttore della classe padre
			parent::__construct($addr, $port, $cmd_port);

			$this->server_avviato = false;

			// salviamo la sessione
			$this->sessione = $sessione;

			// avviamo il server
			if (false !== $sessione){
				// recuperiamo il file di log
				$this->log_file = $this->recupera_log_file($sessione);

				// avviamo il server
				$this->avvia_server($command_server);
			}
		}

		public function recupera_log_file($sessione){
			return $this->recupera_log_dir() . $this->recupera_log_filename($sessione);
		}

		protected function recupera_log_dir(){
			$dir = __DIR__ . '/../log/';
			if (!is_dir($dir)) mkdir($dir);
			return $dir;
		}

		public function recupera_log_filename($sessione){
			return 'log_' . $sessione . '.log';
		}

		/**
		 * Questa funzione avvia il server Ethernet
		 * del HD67839_B2_Y_DALI_ETHERNET_CONVERTER
		 * Costruisce quindi una rete DALI e rimane in attesa
		 * di comandi
		 */
		protected function avvia_server($command_server = false){
			// ripuliamo il file di log
			if (file_exists($this->log_file) && !$command_server) unlink($this->log_file);

			// Mettiamo in ascolto il server
			$this->listen_for_request_and_send_response($command_server);
		}

		public function listen_for_request_and_send_response($command_server = false){
			//Create a UDP socket
			if (!($this->listen_socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))){
				$errorcode = socket_last_error();
				$errormsg = socket_strerror($errorcode);

				$this->log('Couldn\'t create socket: [' . $errorcode . '] ' . $errormsg);
			}else{
				$this->log('Socket created');

				// Bind the source address
				$attempts = 0;
				$max_attempts = 100;

				$server_addr = $this->addr;
				$server_port = $this->port;

				if ($command_server) $server_port = $this->cmd_port;

				while ($attempts < $max_attempts && !socket_bind($this->listen_socket, $server_addr, $server_port)){
					$errorcode = socket_last_error();
					$errormsg = socket_strerror($errorcode);

					$this->log('Could not bind to <b>' . $server_addr . ':' . $server_port . '</b> : [' . $errorcode . '] ' . $errormsg);

					$server_port++;
					$attempts++;
				}

				if ($attempts < $max_attempts){
					set_time_limit(0);

					$this->log('Socket bind to <b>' . $server_addr . ':' . $server_port . '</b>');

					if ($command_server){
						$this->cmd_port = $server_port;
					}else{
						$this->port = $server_port;
						$this->cmd_port = self::$PORT_NULL;
					}

					$this->salva_sessione($this->sessione);

					// salviamo lo stato
					$this->status_filename = $this->recupera_filename_stato($this->addr, $this->port);
					$this->status_file = $this->recupera_file_stato();

					$stato = $this->recupera_stato();
					$salva_stato = false;

					if (false !== $stato && key_exists('rete', $stato)){
						$this->rete = $stato['rete'];
						//$this->log('Nodi e gruppi recuperati da file.');
						$this->log('Nodes and groups loaded from file.');
					}else{
						// Inizializziamo la rete di 64 nodi DALI
						$this->rete = $this->recupera_rete_DALI();
						//$this->log('Nodi e gruppi ricreati randomicamente.');
						$this->log('Nodes and groups randomly created.');
						$salva_stato = true;
					}

					if (false !== $stato && key_exists('scene', $stato)){
						$this->scene = $stato['scene'];
						//$this->log('Scene recuperate da file.');
						$this->log('Scenes loaded from file.');
					}else{
						// Inizializziamo la rete di 64 nodi DALI
						$this->scene = $this->recupera_scene_DALI();
						//$this->log('Scene ricreate randomicamente.');
						$this->log('Scenes randomly created.');
						$salva_stato = true;
					}

					if ($salva_stato) $this->salva_stato();

					$this->server_avviato = true;

					$this->listen = true;
					$bytes = 1;

					//Do some communication, this loop can handle multiple clients
					while ($this->listen){
						$remote_ip = '';
						$remote_port = 0;

						$this->log('Server listening ...<br/>');

						//Receive REQUEST
						$bytes = socket_recvfrom($this->listen_socket, $richiesta, self::$MAX_DATAGRAM_SIZE, 0, $remote_ip, $remote_port);

						if ($bytes > 0){
							//$this->log('Ricevuti <b>' . $bytes . '</b> bytes da <b>' . $remote_ip . ':' . $remote_port . '</b>');
							$this->log('<b>' . $bytes . '</b> bytes received from <b>' . $remote_ip . ':' . $remote_port . '</b>');

							// stampiamo la ricezione sotto forma di HEX
							if (strlen($richiesta) <= 11){
								$str = '';
								for ($i = 0; $i < strlen($richiesta); $i++) $str .= $this->chr2hex($richiesta[$i]) . ', ';
								$this->log($str);
							}

							// eseguiamo il comando richiesto e recuperiamo la eventuale risposta
							$risposta = $this->esegui_comando($richiesta, $remote_ip, $remote_port);

							// la risposta deve essere inviata sulla server_port
							if ($risposta != null){
								// stiamo attenti a chi inviamo il messaggio...
								$response_port = $server_port;

								if ($remote_ip == $server_addr){
									// abbiamo un problema ad inviare la risposta indietro:
									//$this->log('<b>ATTENZIONE</b>: hai ricevuto il messaggio dallo stesso indirizzo IP (<b>' . $remote_ip . '</b>) della macchina che fa da emulatore.');
									$this->log('<b>WARNING</b>: message received from the same IP address (<b>' . $remote_ip . '</b>) of the emulator.');
									$response_port = $remote_port;
								}

								if (is_array($risposta)){
									foreach ($risposta as $msg){
										// la risposta deve essere inviata sulla server_port
										//$this->log('Invio risposta da <b>' . strlen($msg) . '</b> bytes a <b>' . $remote_ip . ':' . $response_port . '</b>');
										$this->log('Sending response <b>' . strlen($msg) . '</b> bytes long to <b>' . $remote_ip . ':' . $response_port . '</b>');
										$len = strlen($msg);
										socket_sendto($this->listen_socket, $msg, $len, 0, $remote_ip, $response_port);

										// registriamo l'ultima risposta inviata
										$this->ultima_risposta_inviata = $msg;
									}
								}else{
									// la risposta deve essere inviata sulla server_port
									//$this->log('Invio risposta da <b>' . strlen($risposta) . '</b> bytes a <b>' . $remote_ip . ':' . $response_port . '</b>');
									$this->log('Sending response <b>' . strlen($risposta) . '</b> bytes long to <b>' . $remote_ip . ':' . $response_port . '</b>');
									$len = strlen($risposta);
									socket_sendto($this->listen_socket, $risposta, $len, 0, $remote_ip, $response_port);

									// registriamo l'ultima risposta inviata
									$this->ultima_risposta_inviata = $risposta;
								}
							}else{
								//$this->log('Nessuna risposta inviata a <b>' . $remote_ip . ':' . $remote_port . '</b>');
								$this->log('No response sent to <b>' . $remote_ip . ':' . $remote_port . '</b>');
							}

						}else{
							//$this->log('<b>Warning</b>: ricevuto un messaggio privo di byte da <b>' . $remote_ip . ':' . $remote_port . '</b>');
							$this->log('<b>Warning</b>: empty message received from <b>' . $remote_ip . ':' . $remote_port . '</b>');
						}
					}
				}
			}

			$this->log('Closing socket ...');

			// chiudiamo la socket
			socket_close($this->listen_socket);

			$this->log('Socket closed');
		}

		protected function log($str){
			file_put_contents($this->log_file, '[' . date('D, d M Y H:i:s') . '] ' . $str . '<br/>', FILE_APPEND);
		}

		protected function salva_sessione($sessione){
			$data = serialize(array('addr' => $this->addr, 'port' => $this->port, 'cmd_port' => $this->cmd_port,));
			$file = $this->recupera_tmp_dir() . '/' . $sessione . '.dat';
			file_put_contents($file, $data);

			//$this->log('Sessione salvata ' . $this->addr . ', ' . $this->port);
			$this->log('Session saved ' . $this->addr . ', ' . $this->port);
		}

		protected function recupera_filename_stato($addr, $port){
			return str_replace('.', '_', $addr) . '_' . $port . '.dat';
		}

		public function recupera_file_stato(){
			return $this->recupera_tmp_dir() . '/' . $this->status_filename;
		}

		public function recupera_stato(){
			$stato = false;
			if (file_exists($this->status_file)){
				$data = file_get_contents($this->status_file);
				$stato = unserialize($data);
			}
			return $stato;
		}

		/**
		 * Questa funzione crea una rete con 64 nodi DALI
		 */
		protected function recupera_rete_DALI($fromDB = false, $saveOnDb = false){
			$rete = array();
			for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++){
				if (!$fromDB){
					//creo il nodo in modo random
					$rete[$i] = $this->recupera_nodo_DALI((rand(0, 1) != 0));
					if(saveOnDb){
						//salvo il nodo appena creato nel database
						//1) costruisco il bean
						$beanDevice = new BeanDevice();
						$beanDevice->nodo2bean($rete[$i]);
						//2) lo salvo
						$result = $beanDevice->insert();
						$beanDevice->close();
					}
				}else{
					//carico il nodo dal db
					$beanDevice = new BeanDevice($i);
					$rete[$i] = $beanDevice->bean2nodo();
					$beanDevice->close();
				}
			}
			return $rete;
		}

		/*protected function recupera_rete_DALI(){
			$rete = array();
			for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++) $rete[$i] = $this->recupera_nodo_DALI((rand(0, 1) != 0));
			return $rete;
		}*/



		/**
		 * Questa funzione crea un nodo DALI
		 * Restituisce un array composto da 8 byte
		 * 0 => Status of DALI node
		 * 1 => ADV of DALI node
		 * 2 => Response received after command from DALI node
		 * 3 => Bit 0, 1, 2, 3 (least significant) = Type of DALI node, Bit 4, 5, 6, 7 (most significant) = Version of DALI node
		 * 4 => Min. settable value of DALI node
		 * 5 => Max. settable value of DALI node
		 * 6-7 => Byte 6, Bit 0 (less significant) = Group 0, Byte 6, Bit 7 (most significant) = Group 7, ..., Byte 7, Bit 7 (most significant) = Group 15
		 */
		protected function recupera_nodo_DALI($attivo = true){
			$nodo = str_repeat(chr(0), 20);

			if ($attivo){
				$nodo[0] = $this->recupera_stato_nodo_DALI();    // Lo stato dovrebbe essere composto da 8 bit del tipo: 0?100???
				$nodo[1] = chr(254);                            // 0 = spento, 1 = acceso al minimo, 254 = acceso al massimo, 255 valore non valido
				$nodo[2] = chr(1);                                // Non si capisce cosa voglia dire "Response received after command from DALI node" forse è il DTR (Data Trasnfer Register)
				$nodo[3] = $this->recupera_tipo_nodo_DALI();
				$nodo[4] = chr(0);                                // minimo valore = 0
				$nodo[5] = chr(254);                            // massimo valore = 254
				$nodo[6] = chr(rand(0, 255));                    // viene generato un numero random
				$nodo[7] = chr(rand(0, 255));                    // viene generato un numero random
				// FUNZIONALITA' ESTESE
				$nodo[8] = chr(254);                            // Power On Level, 254, no change, 1 – 254 [ description, default value, reset value, range ]
				$nodo[9] = chr(254);                            // System Failure Level, 254, no change, 0 – 255 (mask)
				$nodo[10] = chr(7);                                // Fade Rate, 7 (45 steps/sec), no change, 1 – 15
				$nodo[11] = chr(0);                                // Fade Time, 0 (no fade), no change, 0 – 15
				$nodo[12] = chr(255);                            // Short Address, 255 (no address), no change, 0 – 63, 255 (mask)
				$nodo[13] = chr(255);                            // Search Address Byte 1, 0xFF, 0xFF, 0 – 0xFF
				$nodo[14] = chr(255);                            // Search Address Byte 2, 0xFF, 0xFF, 0 – 0xFF
				$nodo[15] = chr(255);                            // Search Address Byte 3, 0xFF, 0xFF, 0 – 0xFF
				$nodo[16] = chr(255);                            // Random Address Byte 1, 0xFF, no change, 0 – 0xFF
				$nodo[17] = chr(255);                            // Random Address Byte 2, 0xFF, no change, 0 – 0xFF
				$nodo[18] = chr(255);                            // Random Address Byte 3, 0xFF, no change, 0 – 0xFF
				$nodo[19] = chr(0);                                // Physical Min Level, Factory burn-in, Factory burn-in, 1 – 254
			}

			return $nodo;
		}

		/**
		 *  0    Power supply fault (0 = No)
		 *    1    Missing short address (0 = No)
		 *    2    Reset status (0 = No)
		 *    3    Fading done    (0 = Fading has been completed, 1 = Fading in progress)
		 *    4    Limit value error (0 = The most recently requested lamp power value was between the MIN and MAX values, or was OFF)
		 *    5    Lamp power    (0 = OFF, 1 = ON)
		 *    6    Lamp failure (0 = OK)
		 *    7    Status of the DALI device (0 = OK)
		 */
		protected function recupera_stato_nodo_DALI(){
			$stato_bin = str_repeat(chr(0), 8);
			$stato_bin[0] = 0;
			$stato_bin[1] = 0;
			$stato_bin[2] = 0;
			$stato_bin[3] = 0;
			$stato_bin[4] = 0;
			$stato_bin[5] = 1;
			$stato_bin[6] = 0;
			$stato_bin[7] = 0;
			return chr(bindec($stato_bin));
		}

		/**
		 *  0:    Standard device (FLUORESCENT LAMP)
		 *    1:    Device for emergency lighting (EMERGENCY LAMP)
		 *    2:    Device for HID (High Intensity Discharge) lamps (HIGH VOLTAGE DISCHARGE)
		 *    3:    Device for dimming bulbs (ALOGEN LAMP)
		 *    5 to 255 are reserved for future device types.
		 */
		protected function recupera_tipo_nodo_DALI(){
			return chr(rand(0, 8));
		}

		/**
		 * Questa funzione crea 16 scene per ognuno dei 64 nodi DALI
		 * 0-254 = Value set for the selected scene
		 * 255 = Scene not configures
		 */
		protected function recupera_scene_DALI(){
			$scene = array();
			$prob = 2; //(numero da 0 a 254, 0 significa sempre, X significa una probabilità su (X+1) di essere settato)
			for ($i = 0; $i < self::$MAX_NUM_NODI_DALI; $i++){
				// creiamo un array di 16 byte posti a 255 (scena non configurata)
				$scene[$i] = str_repeat(chr(255), self::$MAX_NUM_SCENE_DALI);

				for ($j = 0; $j < self::$MAX_NUM_SCENE_DALI; $j++) if (rand(0, $prob) == 0) $scene[$i][$j] = chr(rand(0, 254));
			}
			return $scene;
		}

		protected function salva_stato(){
			if (count($this->rete) > 0){
				if (count($this->scene) > 0){
					$arr = array('rete' => $this->rete, 'scene' => $this->scene,);
					$data = serialize($arr);
					if (false !== file_put_contents($this->status_file, $data)) $this->log('Stato salvato');else
						//$this->log('Errore: impossibile salvare lo stato nel file ' . $this->status_file);
						$this->log('Error: cannot save status into ' . $this->status_file);
				}else{
					//$this->log('Stato NON salvato, <b>non ci sono scene</b>. Ricreate randomicamente.');
					$this->log('Status NOT saved, <b>no scene availabe</b>. Random creation of scenes.');
					$this->scene = $this->recupera_scene_DALI();
				}
			}else{
				//$this->log('Stato NON salvato, <b>non ci sono nodi</b>. Ricreati randomicamente.');
				$this->log('Status NOT saved, <b>no nodes available</b>. Random creation of nodes.');
				$this->rete = $this->recupera_rete_DALI();
			}
		}

		protected function esegui_comando($data, $remote_addr, $remote_port){
			$risposta = null;

			$stato = $this->recupera_stato();
			$this->rete = $stato['rete'];
			$this->scene = $stato['scene'];

			// Identifica il comando
			if (false !== ($COMANDO = $this->identifica_comando($data))){
				// Eseguiamo il comando e prepariamo la risposta
				// Le stringhe contengono un byte in più, bisogna ragionare byte per byte.
				switch ($COMANDO){
					case self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST:
						$this->log('Comando DALI NETWORK SCANNING ricevuto');
						$risposta = '';
						foreach ($this->rete as $nodo){
							$nodo = substr($nodo, 0, 8);
							$nodo[6] = chr(bindec(strrev($this->chr2bin($nodo[6]))));
							$nodo[7] = chr(bindec(strrev($this->chr2bin($nodo[7]))));
							$risposta .= $nodo;
						}
						break;
					case self::$COMANDO_DALI_SCENE_SCANNING_REQUEST;
						$this->log('Comando DALI SCENE SCANNING ricevuto');
						$risposta = str_repeat(chr(0), self::$MAX_NUM_NODI_DALI * self::$MAX_NUM_SCENE_DALI);
						$j = 0;
						foreach ($this->scene as $nodo => $scena) for ($i = 0; $i < strlen($scena); $i++) $risposta[$j++] = $scena[$i];
						$this->log('Risposta con ' . $j . ' bytes');
						break;
					case self::$COMANDO_ADV_SETTING_REQUEST:
						$this->log('Comando ADV SETTING ricevuto');
						$DALI_ID = ord($data[8]);
						if ($DALI_ID < self::$MAX_NUM_NODI_DALI){
							// Biosgna settare l'ADV di un nodo
							$this->set_node_ADV($DALI_ID, $data[9]);
							//$this->log('Comando ADV SETTING eseguito sul nodo <b>' . $DALI_ID . '</b> con ADV = <b>' . ord($data[9]) . '</b>');
							$this->log('ADV SETTING command executed on node <b>' . $DALI_ID . '</b> with parameter ADV = <b>' . ord($data[9]) . '</b>');
						}elseif ($DALI_ID < (self::$MAX_NUM_NODI_DALI + self::$MAX_NUM_GRUPPI_DALI)){
							$GROUP_ID = $DALI_ID - self::$MAX_NUM_NODI_DALI;

							// Biosgna settare l'ADV di un gruppo
							foreach (array_keys($this->rete) as $i) $this->set_node_ADV_GROUP($i, $GROUP_ID, $data[9]);
							//$this->log('Comando ADV SETTING eseguito sul gruppo <b>' . $GROUP_ID . '</b> con ADV = <b>' . ord($data[9]) . '</b>');
							$this->log('ADV SETTING command executed on group <b>' . $GROUP_ID . '</b> with parameter ADV = <b>' . ord($data[9]) . '</b>');
						}elseif ($DALI_ID == (self::$MAX_NUM_NODI_DALI + self::$MAX_NUM_GRUPPI_DALI)){
							// Bisogna settare l'ADV di tutti i nodi
							foreach (array_keys($this->rete) as $i) $this->set_node_ADV($i, $data[9]);
							//$this->log('Comando ADV SETTING eseguito su <b>tutti</b> i nodi con ADV = <b>' . ord($data[9]) . '</b>');
							$this->log('ADV SETTING command executed on <b>all</b> nodes with parameter ADV = <b>' . ord($data[9]) . '</b>');
						}else{
							//$this->log('Errore: il valore del DALI ID <b>' . $DALI_ID . '</b> non è valido:
							$this->log('Error: invalid DALI ID value <b>' . $DALI_ID . '</b> :
						0 to 63 (0x00 to 0x3F) to manage a single DALI node;
						64 to 79 (0x40 to 0x4F) to manage the Groups;
						80 (0x50) for broadcast.');
						}
						break;
					case self::$COMANDO_SCENE_SETTING_REQUEST:
						//$this->log('Comando SCENE SETTING ricevuto');
						$this->log('SCENE SETTING command received');
						$DALI_ID = ord($data[8]) - hexdec('80');
						$scena = ord($data[9]) - hexdec('10');
						if ($DALI_ID < self::$MAX_NUM_NODI_DALI){
							// Biosgna settare la scena di un nodo
							$this->set_scene_node($DALI_ID, $scena);
							//$this->log('Comando SCENE SETTING eseguito sul nodo <b>' . $DALI_ID . '</b> con SCENA = <b>' . $scena . '</b>');
							$this->log('SCENE SETTING command executed on <b>' . $DALI_ID . '</b> node with SCENE = <b>' . $scena . '</b>');
						}elseif ($DALI_ID < (self::$MAX_NUM_NODI_DALI + self::$MAX_NUM_GRUPPI_DALI)){
							$GROUP_ID = $DALI_ID - self::$MAX_NUM_NODI_DALI;

							// Biosgna settare la scena di un gruppo di nodi
							foreach (array_keys($this->rete) as $i) $this->program_scene_group($i, $GROUP_ID, $scena);
							//$this->log('Comando SCENE SETTING eseguito sul gruppo <b>' . $GROUP_ID . '</b> con SCENA = <b>' . $scena . '</b>');
							$this->log('SCENE SETTING command executed on <b>' . $GROUP_ID . '</b> group with SCENE = <b>' . $scena . '</b>');
						}elseif ($DALI_ID == 127){
							// Bisogna settare la scena di tutti i nodi
							foreach (array_keys($this->rete) as $i) $this->set_scene_node($i, $scena);
							//$this->log('Comando SCENE SETTING eseguito su <b>tutti</b> i nodi con SCENA = <b>' . $scena . '</b>');
							$this->log('SCENE SETTING command executed on <b>all</b> nodes with SCENE = <b>' . $scena . '</b>');
						}else{
							//$this->log('Errore: il valore del DALI ID <b>' . $DALI_ID . '</b> non è valido:
							$this->log('Error: DALI ID value <b>' . $DALI_ID . '</b> invalid:
						0 to 63 (0x00 to 0x3F) to manage a single DALI node;
						64 to 79 (0x40 to 0x4F) to manage the Groups;
						127 (0x7F) for broadcast.');
						}
						break;
					case self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST:
						$DALI_ID = ord($data[8]) - hexdec('80');
						$DALI_COMMAND = ord($data[9]);
						if ($DALI_COMMAND == 0){
							// Extinguish the lamp (without fading)
							$this->extinguish_the_lamp_without_fading($DALI_ID);
							if ($DALI_ID < self::$MAX_NUM_NODI_DALI){
								//$this->log('Comando <b>Extinguish the lamp (without fading)</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
								$this->log('<b>Extinguish the lamp (without fading)</b> command executed on <b>' . $DALI_ID . '</b> node.');
							}else{
								//$this->log('Comando <b>Extinguish the lamp (without fading)</b> eseguito su tutti i nodi (DALI ID = <b>' . $DALI_ID . '</b>).');
								$this->log('<b>Extinguish the lamp (without fading)</b> command executed on all nodes (DALI ID = <b>' . $DALI_ID . '</b>).');

								// rispondiamo sempre ANSWER NOT RECEIVED
								$risposta = self::$RESPONSE_ANSWER_NOT_RECEIVED;
							}
						}elseif ($DALI_COMMAND == 28){
							// Store the value in the DTR as the power on level
							//$this->log('Comando <b>Store the value in the DTR as the power on level</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the power on level</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][8] = $this->DTR_chr;
						}elseif ($DALI_COMMAND == 42){
							// Store the value in the DTR as the maximum level
							//$this->log('Comando <b>Store the value in the DTR as the maximum level</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the maximum level</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][5] = $this->DTR_chr;

						}elseif ($DALI_COMMAND == 43){
							// Store the value in the DTR as the minimum level
							//$this->log('Comando <b>Store the value in the DTR as the minimum level</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the minimum level</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][4] = $this->DTR_chr;
						}elseif ($DALI_COMMAND == 44){
							// Store the value in the DTR as the system failure level
							//$this->log('Comando <b>Store the value in the DTR as the system failure level</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the system failure level</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][9] = $this->DTR_chr;
						}elseif ($DALI_COMMAND == 45){
							// Store the value in the DTR as the power on level
							//$this->log('Comando <b>Store the value in the DTR as the power on level</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the power on level</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][8] = $this->DTR_chr;
						}elseif ($DALI_COMMAND == 46){
							// Store the value in the DTR as the fade time
							//$this->log('Comando <b>Store the value in the DTR as the fade time</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the fade time</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][11] = $this->DTR_chr;
						}elseif ($DALI_COMMAND == 47){
							// Store the value in the DTR as the fade rate
							//$this->log('Comando <b>Store the value in the DTR as the fade rate</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as the fade rate</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$this->rete[$DALI_ID][10] = $this->DTR_chr;
						}elseif ($DALI_COMMAND >= 80 && $DALI_COMMAND <= 95){
							// Remove the selected scene (n) from the DALI slave
							$scena = $DALI_COMMAND - 80;
							$this->program_scene_node($DALI_ID, $scena, chr(255));
							//$this->log('Comando <b>Remove the selected scene (n) from the DALI slave</b>, con n = <b>' . $scena . '</b>, eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Remove the selected scene (n) from the DALI slave</b> command, with n = <b>' . $scena . '</b>, executed on <b>' . $DALI_ID . '</b> node.');
							// rispondiamo sempre ANSWER=VAL
							// $risposta = self::$RESPONSE_ANSWER_VAL;
						}elseif ($DALI_COMMAND == 128) // 0x80 hex
						{
							// Store the value in the DTR as a short address
							//$this->log('Comando <b>Store the value in the DTR as a short address</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Store the value in the DTR as a short address</b> command executed on <b>' . $DALI_ID . '</b> node.');
							// il DTR va tolto 1 e dimezzato
							$DTR_dec = ord($this->DTR_chr);
							$this->rete[$DALI_ID][12] = chr(($DTR_dec - 1) / 2);
						}elseif ($DALI_COMMAND == 163){
							// Return the power up level as XX
							//$this->log('Comando <b>Return the power up level as XX</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Return the power up level as XX</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$hex = $this->return_the_power_up_level_as_XX($DALI_ID);
							$risposta = self::$RESPONSE_ANSWER_VAL . '1' . $hex;
						}elseif ($DALI_COMMAND == 164){
							// Returns the system failure level as XX
							//$this->log('Comando <b>Returns the system failure level as XX</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Returns the system failure level as XX</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$hex = $this->returns_the_system_failure_level_as_XX($DALI_ID);
							$risposta = self::$RESPONSE_ANSWER_VAL . '1' . $hex;
						}elseif ($DALI_COMMAND == 165){
							// Returns the fade time as X and the fade rate as Y
							//$this->log('Comando <b>Returns the fade time as X and the fade rate as Y</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Returns the fade time as X and the fade rate as Y</b> command executed on <b>' . $DALI_ID . '</b> node.');
							$hex = $this->returns_the_fade_time_as_X_and_the_fade_rate_as_Y($DALI_ID);
							$risposta = self::$RESPONSE_ANSWER_VAL . '1' . $hex;
						}elseif ($DALI_COMMAND == 255){
							// Return the power up level as XX
							//$this->log('Comando <b>Returns application specific extension commands</b> eseguito sul nodo <b>' . $DALI_ID . '</b>.');
							$this->log('<b>Returns application specific extension commands</b> command executed on <b>' . $DALI_ID . '</b> node.');
							// rispondiamo sempre ANSWER=VAL
							// $risposta = self::$RESPONSE_ANSWER_NOT_RECEIVED;
						}
						break;
					case self::$COMANDO_SCENE_PROGRAMMING_REQUEST_1:
					case self::$COMANDO_NODE_ID_SETTING_REQUEST_1:
						//$this->log('Comando SCENE PROGRAMMING REQUEST 1 o COMANDO_NODE_ID_SETTING_REQUEST_1 ricevuto');
						$this->log('SCENE PROGRAMMING REQUEST 1 or COMANDO_NODE_ID_SETTING_REQUEST_1 command received');
						$chr = $data[9];
						$dec = ord($chr);
						//$this->log('Salvato nel Data Transfer Register (DTR) il valore <b>' . $dec . ' (' . $this->dec2hex($dec) . ')</b>.');
						$this->log('Value <b>' . $dec . ' (' . $this->dec2hex($dec) . ')</b> saved into Data Transfer Register (DTR).');
						$this->DTR_chr = $chr;
						break;
					case self::$COMANDO_SCENE_PROGRAMMING_REQUEST_2:
						//$this->log('Comando SCENE PROGRAMMING REQUEST 2 ricevuto');
						$this->log('SCENE PROGRAMMING REQUEST 2 command received');
						$ADV_chr = $this->DTR_chr;
						$DALI_ID = ord($data[8]) - hexdec('80');
						$scena = ord($data[9]) - hexdec('40');
						if ($DALI_ID < self::$MAX_NUM_NODI_DALI){
							// Biosgna settare la scena di un nodo
							$this->program_scene_node($DALI_ID, $scena, $ADV_chr);
							//$this->log('Comando SCENE PROGRAMMING eseguito sul nodo <b>' . $DALI_ID . '</b> con SCENA = <b>' . $scena . '</b> e ADV = <b>' . ord($ADV_chr) . '</b>');
							$this->log('SCENE PROGRAMMING command executed on <b>' . $DALI_ID . '</b> node with SCENE = <b>' . $scena . '</b> and ADV = <b>' . ord($ADV_chr) . '</b>');
						}elseif ($DALI_ID < (self::$MAX_NUM_NODI_DALI + self::$MAX_NUM_GRUPPI_DALI)){
							$GROUP_ID = $DALI_ID - self::$MAX_NUM_NODI_DALI;

							// Biosgna settare la scena di un gruppo di nodi
							foreach (array_keys($this->rete) as $i) $this->set_scene_group($i, $GROUP_ID, $scena, $ADV_chr);
							//$this->log('Comando SCENE PROGRAMMING eseguito sul gruppo <b>' . $GROUP_ID . '</b> con SCENA = <b>' . $scena . '</b> e ADV = <b>' . ord($ADV_chr) . '</b>');
							$this->log('SCENE PROGRAMMING command executed on <b>' . $GROUP_ID . '</b> group with SCENE = <b>' . $scena . '</b> and ADV = <b>' . ord($ADV_chr) . '</b>');
						}elseif ($DALI_ID == 127){
							// Bisogna settare la scena di tutti i nodi
							foreach (array_keys($this->rete) as $i) $this->program_scene_node($i, $scena, $ADV_chr);
							//$this->log('Comando SCENE PROGRAMMING eseguito su <b>tutti</b> i nodi con SCENA = <b>' . $scena . '</b> e ADV = <b>' . ord($ADV_chr) . '</b>');
							$this->log('SCENE PROGRAMMING command executed on <b>all</b> nodes with SCENE = <b>' . $scena . '</b> and ADV = <b>' . ord($ADV_chr) . '</b>');
						}else{
							//$this->log('Errore: il valore del DALI ID <b>' . $DALI_ID . '</b> non è valido:
							$this->log('Error: invalid DALI ID value <b>' . $DALI_ID . '</b>:
						0 to 63 (0x00 to 0x3F) to manage a single DALI node;
						64 to 79 (0x40 to 0x4F) to manage the Groups;
						127 (0x7F) for broadcast.');
						}
						break;
					case self::$COMANDO_GROUP_SETTING_REQUEST:
						//$this->log('Comando GROUP SETTING ricevuto');
						$this->log('GROUP SETTING command received');
						$DALI_ID = ord($data[8]) - hexdec('80');
						$GROUP = ord($data[9]) - hexdec('60');
						$ENABLE = 1;
						if ($GROUP > 15){
							$GROUP -= self::$MAX_NUM_GRUPPI_DALI;
							$ENABLE = 0;
						}
						if ($DALI_ID < self::$MAX_NUM_NODI_DALI){
							// Biosgna settare la scena di un nodo
							$this->set_group_node($DALI_ID, $GROUP, $ENABLE);
							//$this->log('Comando GROUP_SETTING_REQUEST eseguito sul nodo <b>' . $DALI_ID . '</b> con GRUPPO = <b>' . $GROUP . '</b> <b>' . ($ENABLE == 1 ? 'AGGIUNTO' : 'ELIMINATO') . '</b>');
							$this->log('GROUP_SETTING_REQUEST command executed on <b>' . $DALI_ID . '</b> node with GROUP = <b>' . $GROUP . '</b> <b>' . ($ENABLE == 1 ? 'ADDED' : 'REMOVED') . '</b>');
						}else{
							//$this->log('Errore: il valore del DALI ID <b>' . $DALI_ID . '</b> supera il valore massimo di <b>' . (self::$MAX_NUM_NODI_DALI + self::$MAX_NUM_GRUPPI_DALI) . '</b>');
							$this->log('Error: DALI ID <b>' . $DALI_ID . '</b> value exceeds maximum value <b>' . (self::$MAX_NUM_NODI_DALI + self::$MAX_NUM_GRUPPI_DALI) . '</b>');
						}
						break;
					case self::$COMANDO_NODE_ID_SETTING_REQUEST_2:
						//$this->log('Comando NODE ID SETTING REQUEST 2 ricevuto');
						$this->log('NODE ID SETTING REQUEST 2 command received');
						$DALI_ID = ord($this->DTR_chr) - 1;
						//$this->log('Comando NODE_ID_SETTING eseguito sul nodo <b>' . $DALI_ID . '</b>');
						$this->log('NODE_ID_SETTING command executed on <b>' . $DALI_ID . '</b> node.');
						break;
					case self::$COMANDO_CLOSE_SERVER_SOCKET:
						//$this->log('Comando CLOSE SERVER SOCKET ricevuto');
						$this->log('CLOSE SERVER SOCKET command received');
						$this->listen = false;
						break;
					case self::$COMANDO_SEARCH_ALL_DEVICES:
						//$this->log('Comando SEARCH ALL DEVICES eseguito');
						$this->log('SEARCH ALL DEVICES command executed');
						break;
					case self::$COMANDO_SEARCH_NEW_DEVICES:
						//$this->log('Comando SEARCH NEW DEVICES eseguito');
						$this->log('SEARCH NEW DEVICES command executed');
						break;
					default:
						//$this->log('Comando <b>' . $COMANDO . '</b> sconosciuto.');
						$this->log('Command <b>' . $COMANDO . '</b> unknown.');
						break;
				}

				// salviamo lo stato
				$this->salva_stato();
			}else{
				if (strlen($data) <= 11){
					$comando = '';
					for ($i = 0; $i < strlen($data); $i++) $comando .= $this->chr2hex($data[$i]) . ', ';
					//$this->log('Comando ' . $comando . ' <b>sconosciuto</b>.');
					$this->log('Command ' . $comando . ' <b> unknown</b>.');
				}else{
					// Abbiamo ricevuto un comando più lungo di 11 byte
					//$this->log('Comando <b>sconosciuto</b>.');
					$this->log('<b>Unknown</b> command.');

					// Verifichiamo se questo comando coincide con l'ultima risposta inviata
					if (strlen($data) == strlen($this->ultima_risposta_inviata)){
						// hanno la stessa lunghezza, verifichiamone l'uguaglianza
						if ($data == $this->ultima_risposta_inviata){
							//$this->log('Il comando ricevuto è uguale all\'ultima risposta inviata (echo).');
							$this->log('Received command is the same as the last reply sent (echo).');

							// Sembra che quando un comando non sia interpretato dalla console, venga rimandato indietro

							// proviamo a ripeterlo...
							//$this->log('<b>SPERIMENTALE</b>: proviamo ad inviare una sequenza di 1.');
							//$risposta_sperimentale = chr(255);
							//$len = strlen($risposta_sperimentale);
							//socket_sendto($this->listen_socket, $risposta_sperimentale , $len, 0 , $remote_addr , $remote_port);
						}else{
							//$this->log('Il comando ricevuto è ha la stessa lunghezza dell\'ultima risposta inviata, ma il contenuto è diverso.');
							$this->log('Received command is the same length as the last reply sent, but has different content.');
						}
					}
				}
			}

			// restituiamo la risposta
			return $risposta;
		}

		protected function identifica_comando($data){
			$COMANDO = false;
			$COUNTER = false;

			if (strlen($data) == 11 && ord($data[1]) == hexdec('10') && ord($data[2]) == hexdec('00') && ord($data[3]) == hexdec('00') && ord($data[4]) == hexdec('00') && ord($data[5]) == hexdec('02') && ord($data[6]) == hexdec('04') && ord($data[10]) == hexdec('00') && ord($data[8]) == (ord($data[0]) - 1)){
				//$this->log('Comando <b>COMANDO_ADV_SETTING_REQUEST</b> identificato.');
				$this->log('<b>COMANDO_ADV_SETTING_REQUEST</b> command identified.');
				$COMANDO = self::$COMANDO_ADV_SETTING_REQUEST;
				$COUNTER = ord($data[7]);
			}elseif (strlen($data) == 7 && ord($data[0]) == hexdec('41') && ord($data[1]) == hexdec('4E') && ord($data[2]) == hexdec('53') && ord($data[3]) == hexdec('57') && ord($data[4]) == hexdec('45') && ord($data[5]) == hexdec('52') && ord($data[6]) == hexdec('3D')){
				//$this->log('Comando <b>COMANDO_SEARCH_ALL_DEVICES</b> identificato.');
				$this->log('<b>COMANDO_SEARCH_ALL_DEVICES</b> command identified.');
				$COMANDO = self::$COMANDO_SEARCH_ALL_DEVICES;
			}elseif (strlen($data) == 11 && ord($data[0]) == hexdec('00') && ord($data[1]) == hexdec('10') && ord($data[2]) == hexdec('00') && ord($data[3]) == hexdec('00') && ord($data[4]) == hexdec('00') && ord($data[5]) == hexdec('02') && ord($data[6]) == hexdec('04') && ord($data[8]) == hexdec('D1') && ord($data[10]) == hexdec('00')){
				/**
				 * Sembra esserci un errore nella documentazione sulla REQUEST 1 dello SCENE PROGRAMMING
				 * L'ultimo byte dovrebbe essere 0x40 e non 0x00, questo perché così lo imposta il software
				 * DALI CONSOLE
				 */
				$COMANDO = self::$COMANDO_NODE_ID_SETTING_REQUEST_1;
				//$this->log('Comando <b>COMANDO_NODE_ID_SETTING_REQUEST_1</b> identificato.');
				$this->log('<b>COMANDO_NODE_ID_SETTING_REQUEST_1</b> command identified.');
				$COUNTER = ord($data[7]);
			}elseif (strlen($data) == 11 && ord($data[0]) == hexdec('00') && ord($data[1]) == hexdec('10') && ord($data[2]) == hexdec('00') && ord($data[3]) == hexdec('00') && ord($data[4]) == hexdec('00') && ord($data[5]) == hexdec('02') && ord($data[6]) == hexdec('04') && ord($data[8]) == hexdec('D1') && ord($data[10]) == hexdec('40')){
				$COMANDO = self::$COMANDO_SCENE_PROGRAMMING_REQUEST_1;
				//$this->log('Comando <b>COMANDO_SCENE_PROGRAMMING_REQUEST_1</b> identificato.');
				$this->log('<b>COMANDO_SCENE_PROGRAMMING_REQUEST_1</b> command identified.');
				$COUNTER = ord($data[7]);
			}elseif (strlen($data) == 11 && ord($data[0]) == hexdec('00') && ord($data[1]) == hexdec('10') && ord($data[2]) == hexdec('00') && ord($data[3]) == hexdec('00') && ord($data[4]) == hexdec('00') && ord($data[5]) == hexdec('02') && ord($data[6]) == hexdec('04') && ord($data[10]) == hexdec('00') && ord($data[8]) >= hexdec('80')){
				if (ord($data[9]) >= hexdec('10') && ord($data[9]) < (hexdec('10') + self::$MAX_NUM_SCENE_DALI)){
					$COMANDO = self::$COMANDO_SCENE_SETTING_REQUEST;
					//$this->log('Comando <b>COMANDO_SCENE_SETTING_REQUEST</b> identificato.');
					$this->log('<b>COMANDO_SCENE_SETTING_REQUEST</b> command identified.');
				}elseif (ord($data[9]) >= hexdec('40') && ord($data[9]) < (hexdec('40') + self::$MAX_NUM_SCENE_DALI)){
					$COMANDO = self::$COMANDO_SCENE_PROGRAMMING_REQUEST_2;
					//$this->log('Comando <b>COMANDO_SCENE_PROGRAMMING_REQUEST_2</b> identificato.');
					$this->log('<b>COMANDO_SCENE_PROGRAMMING_REQUEST_2</b> command identified.');
				}elseif (ord($data[9]) >= hexdec('60') && ord($data[9]) < (hexdec('70') + self::$MAX_NUM_GRUPPI_DALI)){
					$COMANDO = self::$COMANDO_GROUP_SETTING_REQUEST;
					//$this->log('Comando <b>COMANDO_GROUP_SETTING_REQUEST</b> identificato.');
					$this->log('<b>COMANDO_GROUP_SETTING_REQUEST</b> command identified.');
				}else{
					$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
					//$this->log('Comando <b>COMANDO_GENERIC_DALI_COMMAND_REQUEST</b> identificato.');
					$this->log('<b>COMANDO_GENERIC_DALI_COMMAND_REQUEST</b> command identified.');
					$comando_dec = ord($data[9]);
					$TESTO_COMANDO = $this->identifica_GENERIC_DALI_COMMAND($comando_dec);
					if (false !== $TESTO_COMANDO) $this->log('Comando ' . $comando_dec . ' (' . $this->dec2hex($comando_dec) . ') <b>' . $TESTO_COMANDO . '</b> identificato.');else
						//$this->log('Comando ' . $comando_dec . ' (' . $this->dec2hex($comando_dec) . ') <b>non identificato</b>.');
						$this->log($comando_dec . ' command (' . $this->dec2hex($comando_dec) . ') <b> unknown</b>.');
				}
				$COUNTER = ord($data[7]);
			}else{
				// Questi comandi non richiedono interazione con il contatore.
				switch ($data){
					case self::$REQUEST_DALI_NETWORK_SCANNING:
						$COMANDO = self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST;
						//$this->log('Comando <b>COMANDO_DALI_NETWORK_SCANNING_REQUEST</b> identificato.');
						$this->log('<b>COMANDO_DALI_NETWORK_SCANNING_REQUEST</b> command identified.');
						break;
					case self::$REQUEST_DALI_SCENE_SCANNING:
						$COMANDO = self::$COMANDO_DALI_SCENE_SCANNING_REQUEST;
						//$this->log('Comando <b>COMANDO_DALI_SCENE_SCANNING_REQUEST</b> identificato.');
						$this->log('<b>COMANDO_DALI_SCENE_SCANNING_REQUEST</b> command identified.');
						break;
					case self::$REQUEST_CLOSE_SERVER_SOCKET:
						$COMANDO = self::$COMANDO_CLOSE_SERVER_SOCKET;
						//$this->log('Comando <b>COMANDO_CLOSE_SERVER_SOCKET</b> identificato.');
						$this->log('<b>COMANDO_CLOSE_SERVER_SOCKET</b> command identified.');
						break;
					default:
						$COMANDO = false;
						break;
				}
			}

			//if ($COMANDO === false)
			// $this->log('Comando <b>'.$data.'</b> non identificato.');
			//else
			// $this->log('Comando <b>'.$COMANDO.'</b> identificato.');

			if ($COUNTER !== false) $this->log('COUNTER = <b>' . $COUNTER . ' (' . $this->chr2hex($data[7]) . ')</b>.');

			return $COMANDO;
		}

		protected function identifica_GENERIC_DALI_COMMAND($command_dec){
			$command_text = false;
			foreach (self::$GENERIC_DALI_COMMAND_LIST as $command) if ($command_dec >= $command[0] && $command_dec <= $command[1]) $command_text = $command[2];
			return $command_text;
		}

		protected function set_node_ADV($node_id, $adv_chr){
			// aggiorniamo il bit di valore oltre i limiti min e MAX
			$min_adv_chr = $this->rete[$node_id][4];
			$max_adv_chr = $this->rete[$node_id][5];
			if ($adv_chr < $min_adv_chr || $adv_chr > $max_adv_chr) $this->set_node_STATUS($node_id, self::$BIT_STATO_OUT_OF_LIMITS, 1);else
				$this->set_node_STATUS($node_id, self::$BIT_STATO_OUT_OF_LIMITS, 0);

			// aggiorniamo il bit di lampada accesa
			$adv_dec = ord($adv_chr);
			if ($adv_dec > 0) $this->set_node_STATUS($node_id, self::$BIT_STATO_LAMP_ON, 1);else
				$this->set_node_STATUS($node_id, self::$BIT_STATO_LAMP_ON, 0);

			// salviamo il valore del ADV
			$this->rete[$node_id][1] = $adv_chr;
		}

		protected function set_node_STATUS($node_id, $bit, $value){
			$this->rete[$node_id][$bit] = $value;
		}

		protected function set_node_ADV_GROUP($node_id, $group_id, $adv_chr){
			// verifichiamo se il nodo appartiene al gruppo
			$bits = $this->chr2bin($this->rete[$node_id][6]) . $this->chr2bin($this->rete[$node_id][7]);
			if ($bits[$group_id] == 1) $this->set_node_ADV($node_id, $adv_chr);
		}

		protected function set_scene_node($node_id, $scena_dec){
			$adv_chr = $this->scene[$node_id][$scena_dec];
			if ($adv_chr != chr(255)) $this->set_node_ADV($node_id, $adv_chr);
		}

		protected function set_scene_group($node_id, $group_id, $scena_dec, $adv_chr){
			// verifichiamo se il nodo appartiene al gruppo
			$bits = $this->chr2bin($this->rete[$node_id][6]) . $this->chr2bin($this->rete[$node_id][7]);
			if ($bits[$group_id] == 1) $this->program_scene_node($node_id, $scena_dec, $adv_chr);
		}

		protected function program_scene_node($node_id, $scena_dec, $adv_chr){
			$this->scene[$node_id][$scena_dec] = $adv_chr;
		}

		/**
		 * Spegniamo tutte le lampade senza fading
		 */
		protected function extinguish_the_lamp_without_fading($node_id){
			if ($node_id < self::$MAX_NUM_NODI_DALI) $this->set_node_ADV($node_id, chr(0));elseif ($node_id == 82 || $node_id == 83) // questi due valori li ho sniffati dal software CONSOLE DALI
				for ($node_id = 0; $node_id < self::$MAX_NUM_NODI_DALI; $node_id++) $this->set_node_ADV($node_id, chr(0));
		}

		protected function return_the_power_up_level_as_XX($node_id){
			$chr = $this->rete[$node_id][8];
			return $this->chr2hex($chr, true);
		}

		protected function returns_the_system_failure_level_as_XX($node_id){
			$chr = $this->rete[$node_id][9];
			return $this->chr2hex($chr, true);
		}

		protected function returns_the_fade_time_as_X_and_the_fade_rate_as_Y($node_id){
			$chrX = $this->rete[$node_id][11];
			$chrY = $this->rete[$node_id][10];
			return substr($this->chr2hex($chrX, true), 1) . substr($this->chr2hex($chrY, true), 1);
		}

		protected function set_group_node($node_id, $group_id, $ENABLE){
			$bits = $this->chr2bin($this->rete[$node_id][6]) . $this->chr2bin($this->rete[$node_id][7]);
			$bits[$group_id] = $ENABLE;
			$this->rete[$node_id][6] = chr(bindec(substr($bits, 0, 8)));
			$this->rete[$node_id][7] = chr(bindec(substr($bits, 8)));
		}

		public function recupera_log_relative_path(){
			return '/log/' . $this->log_filename;
		}

		public function recupera_sessione($sessione){
			$file = $this->recupera_tmp_dir() . '/' . $sessione . '.dat';
			if (file_exists($file)){
				$data = file_get_contents($file);
				return unserialize($data);
			}
			return false;
		}

		public function recupera_log($offset = 0, $sessione){
			$file = $this->recupera_log_file($sessione);
			if (file_exists($file)) return file_get_contents($file, false, null, $offset);
			return '';
		}

		public function recupera_html_stato($addr, $port){
			$html = '';

			$this->status_filename = $this->recupera_filename_stato($addr, $port);
			$this->status_file = $this->recupera_file_stato();
			$stato = $this->recupera_stato();

			$html .= $this->recupera_html_stato_RETE($stato['rete'], true);

			$html .= $this->recupera_html_stato_SCENE($stato['scene']);

			return $html;
		}

		function __destruct(){
			//if ( file_exists($this->status_file) && $this->server_avviato )
			// unlink($this->status_file);

			if ($this->sessione !== false) $this->elimina_sessione();

			parent::__destruct();
		}

		public function elimina_sessione(){
			$file = $this->recupera_tmp_dir() . '/' . $this->sessione . '.dat';
			unlink($file);
		}
	}

?>