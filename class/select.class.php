<?php
	/*
	 * Copyright 2018 Vincenzo Suraci
	 */

	class HtmlSelect{
		/**
		 * $elements,           array di elementi
		 * $id_field_name,      nome del campo id della tabella (serve ad indicizzare la select)
		 * $name_field_name,    nome del campo name della tabella (serve a popolare la lista della select)
		 * $selected_id,        valore del campo id che seleziona uno degli elementi nella lista
		 * $select_id,          valore del campo "id" della select
		 * $select_class,       valore del campo "class" della select
		 * $select_onchange    valore del campo "onchange" della select
		 */
		public static function getHTMLselectFromArray($elements, $selected_id, $select_name, $select_id, $select_class, $select_onchange, $select_disabled = false, $no_empty_elem = false, $preferred = array(), $no_void_selection = false, $min_num_elem_to_activate_search = 10, $data_icons = array(), $multiple = false){

			// preferenze
			$num_preferences = count($preferred);
			if ($num_preferences > 0){
				$unsorted_elements = $elements;
				$elements = array();
				foreach ($preferred as $element_id => $num_clicks){
					if (key_exists($element_id, $unsorted_elements)){
						$elements[$element_id] = $unsorted_elements[$element_id];
						unset($unsorted_elements[$element_id]);
					}
				}
				foreach ($unsorted_elements as $element_id => $element_value) $elements[$element_id] = $element_value;
			}

			// se il numero di entry è superiore a 10 si attiva il campo di ricerca
			$data_live_search = '';
			if (count($elements) > $min_num_elem_to_activate_search) $data_live_search = ' data-live-search="true"';

			$elements_id = array_keys($elements);
			$html = '<select ';
			if ($multiple) $html .= 'multiple ';
			$html .= 'data-width="auto"' . $data_live_search;
			if ($select_disabled) $html .= ' disabled';
			$html .= ' id="' . $select_id . '" name="' . $select_name . '" class="selectpicker ' . $select_class . '" onchange="' . $select_onchange . '">';
			if (!key_exists('', $elements) && !$no_empty_elem) $html .= '<option value="">---</option>';
			if ($num_preferences > 0) $html .= '<optgroup label="">';
			for ($i = 0; $i < count($elements_id); $i++){
				$selected = '';
				if (!is_array($selected_id)){
					if ('' . $elements_id[$i] == '' . $selected_id) $selected = 'selected ';elseif ($selected_id === '' && $no_void_selection && '' . $elements_id[$i] !== ''){
						$selected_id = '' . $elements_id[$i];
						$selected = 'selected ';
					}
				}else{
					if (in_array($elements_id[$i], $selected_id)){
						$selected = 'selected ';
					}elseif (count($selected_id) == 0 && $no_void_selection && '' . $elements_id[$i] !== ''){
						$selected_id[] = '' . $elements_id[$i];
						$selected = 'selected ';
					}
				}
				$data_icon = '';
				if (key_exists($elements_id[$i], $data_icons)) $data_icon = 'data-icon="' . $data_icons[$elements_id[$i]] . '" ';
				$html .= '<option ' . $selected . $data_icon . 'value="' . $elements_id[$i] . '">' . $elements[$elements_id[$i]] . '</option>';
				if ($i == ($num_preferences - 1)) $html .= '</optgroup><optgroup label="">';
			}
			if ($num_preferences > 0) $html .= '</optgroup>';
			$html .= '</select>';
			return $html;
		}

	}

?>