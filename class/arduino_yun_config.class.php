<?php


	/*
	 * Copyright 2018 Vincenzo Suraci
	 */

	require_once __DIR__ . '/dali_ethernet_client.class.php';

	class ARDUINO_YUN_CONFIG extends DALI_ETHERNET{
		public static $MAX_NUM_PULSANTI = 32;
		protected $config_file;

		// pulsante -> lista_comandi
		// comando -> codice parametri
		protected $config_url;
		protected $configurazione;
		protected $DEC = null;

		function __construct(){
			$filename = 'config_yun.json';

			// file di configurazione
			$this->config_file = $this->recupera_config_dir() . $filename;
			$this->config_url = $this->recupera_config_url() . $filename;

			// recuperiamo la configurazione
			$this->configurazione = $this->recupera_configurazione();
		}

		protected function recupera_config_dir(){
			$dir = __DIR__ . '/../config/';
			if (!is_dir($dir)) mkdir($dir);
			return $dir;
		}

		protected function recupera_config_url(){
			return './../config/';
		}

		/**
		 * Restituisce un array del tipo:
		 * $num_pulsante => $array_pulsante
		 * Dove:
		 * $array_pulsante = 'descrizione', 'comandi' => array_lista_comandi
		 */
		protected function recupera_configurazione(){
			$r = array_fill(0, self::$MAX_NUM_PULSANTI, array('descrizione' => '', 'comandi' => array()));

			if (file_exists($this->config_file)){
				$s = json_decode(file_get_contents($this->config_file), true);
				foreach ($s as $i => $j) $r[$i] = $j;
			}

			return $r;
		}

		public function recupera_html_button_download_configurazione(){
			$html = '';

			$html .= '<button class="btn btn-xs btn-success" onclick="window.open(\'' . $this->config_url . '\',\'_blank\');"><span class="glyphicon glyphicon-download"></span> Scarica</button>';

			return $html;
		}

		public function esegui_comandi_pulsante($num_pulsante, $num_cols = 20){
			$output = '';

			if (key_exists($num_pulsante, $this->configurazione)){
				$controller_attivo_addr = array();
				$DECS = array();
				foreach ($this->configurazione[$num_pulsante]['comandi'] as $num_comando => $comando){
					$addr = $comando['addr'];

					if (!key_exists($addr, $controller_attivo_addr)){
						$controller_attivo_addr[$addr] = $this->recupera_stato_modulo($addr);
					}

					if ($controller_attivo_addr[$addr]['risultato'] == 1){
						$port = $comando['port'];
						$PARAMETRI = array();

						$key = $this->calcola_key_ethernet_client_da_addr_port($addr, $port);
						if (!key_exists($key, $DECS)){
							$DECS[$key] = new DALI_ETHERNET_CLIENT($addr, $port);

							/*
							 * Aggiornamento del 21-06-2017
							 * ----------------------------
							 *
							 * Pensavo fosse necessario mandare dei comandi di tipo DALI_INFO e DALI_SCEN
							 * utili a "sbloccare" il converter al suo avvio.
							 *
							 * In realtà sempbra che il problema dei comandi non eseguiti, sia da ricercarsi
							 * nel tipo di comando.
							 *
							 * Ad esempio, il comando ADV_SETTING mandato in Broadcast (0x80) a tutte le lampade, con
							 * ADV = 0, viene letteralmente ignorato dal converter.
							 *
							 * Invece, il comando SCENE_SETTING, mandato in Broadcast (mi pare, in questo caso sia 127)
							 * a tutte le lampade, con SCENE = XX (scena con tutti i valori spenti) FUNZIONI!!!
							 *
							 * Anche il contatore, all'inizio è posto pari a 1, poi cresce fino a 63 e poi
							 * è reinizializzato a 0.
							 *
							 * Sembra che il valore del contatore sia attualmente ignorato dal converter
							 */

							//$DECS[$key]->invia_comando(self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST);
							//$DECS[$key]->invia_comando(self::$COMANDO_DALI_SCENE_SCANNING_REQUEST);
							//$DECS[$key]->COUNTER = self::$COUNTER_MIN;
						}

						$COMANDO = $comando['COMANDO'];
						$PARAMETRI = $comando['PARAMETRI'];
						$PARAMETRI['COUNTER'] = $DECS[$key]->COUNTER;

						$r = $DECS[$key]->invia_comando($COMANDO, $PARAMETRI);
						if ($r['risultato'] == 0){
							// se qualcosa è andato storto, stampiamo l'output
							$output .= $r['html'];
						}
					}else{
						// se il converter è irraggiungibile, scriviamolo
						$output .= $addr . ' KO. ';
					}

				}

				if (key_exists('descrizione', $this->configurazione[$num_pulsante])) // stampiamo la descrizione del pulsante premuto
					$output .= $this->configurazione[$num_pulsante]['descrizione'];else
					// stampiamo il numero dello "scenario" attivato
					$output .= 'Scenario ' . $num_pulsante;
			}else{
				$output .= 'Comando non riconosciuto';
			}

			return $output;
		}

		protected function calcola_key_ethernet_client_da_addr_port($addr, $port){
			return md5($addr . '_' . $port);
		}

		public function recupera_lista_pulsanti(){
			return $this->configurazione;
		}

		public function recupera_html_pannelli(){
			$html = '';

			$html .= $this->recupera_html_pannello_configurazione_pulsanti();

			return $html;
		}

		protected function recupera_html_pannello_configurazione_pulsanti(){
			$html = '';

			$html .= '
			<div class="panel panel-primary" id="panel_pulsanti">
			 <div class="panel-heading">
			  <span class="glyphicon glyphicon-cog"></span>&nbsp;Configurazione pulsanti Arduino YUN <b><span id="span_pulsanti"></span></b>
			  <div style="float:right;"><button type="button" class="btn btn-xs btn-info" onclick="aggiorna_pulsanti();"><span class="glyphicon glyphicon-refresh"></span> Refresh</button></div>
			 </div>
			 <div class="panel-body" id="panel_body_pulsanti">
			 </div>
			</div>
			';

			return $html;
		}

		public function salva_comando($num_pulsante, $num_comando, $converter, $tipo, $elemento, $valore){
			if (!key_exists($num_pulsante, $this->configurazione)) $this->configurazione[$num_pulsante] = array('descrizione' => '', 'comandi' => array(),);

			// recuperiamo indirippo ip e porta
			$arr = explode(':', $converter);
			$addr = $arr[0];
			$port = $arr[1];

			switch ($tipo){
				case 'ADV':
					$this->configurazione[$num_pulsante]['comandi'][$num_comando] = array('addr' => $addr, 'port' => $port, 'COMANDO' => DALI_ETHERNET::$COMANDO_ADV_SETTING_REQUEST, 'PARAMETRI' => array('DALI_ID' => $elemento, 'ADV' => $valore),);
					return $this->salva_configurazione();
					break;
				case 'SCENA':
					$this->configurazione[$num_pulsante]['comandi'][$num_comando] = array('addr' => $addr, 'port' => $port, 'COMANDO' => DALI_ETHERNET::$COMANDO_SCENE_SETTING_REQUEST, 'PARAMETRI' => array('DALI_ID' => $elemento, 'SCENE' => $valore),);
					return $this->salva_configurazione();
					break;
			}

			return false;
		}

		protected function salva_configurazione(){
			return file_put_contents($this->config_file, json_encode($this->configurazione));
		}

		public function elimina_comando($num_pulsante, $num_comando){
			if (key_exists($num_pulsante, $this->configurazione)){
				$comandi = $this->configurazione[$num_pulsante]['comandi'];
				$this->configurazione[$num_pulsante]['comandi'] = array();
				$num_comando_da_salvare = 0;
				foreach ($comandi as $num_comando_da_verificare => $comando) if ($num_comando_da_verificare != $num_comando) $this->configurazione[$num_pulsante]['comandi'][$num_comando_da_salvare++] = $comando;
			}
			$this->salva_configurazione();
		}

		public function elimina_pulsante($num_pulsante_da_eliminare){
			$configurazione = array();
			foreach ($this->configurazione as $num_pulsante => $pulsante) if ($num_pulsante < $num_pulsante_da_eliminare) $configurazione[$num_pulsante] = $pulsante;elseif ($num_pulsante > $num_pulsante_da_eliminare) $configurazione[$num_pulsante - 1] = $pulsante;
			$this->configurazione = $configurazione;
			$this->salva_configurazione();
		}

		public function aumenta_pulsante($num_pulsante){
			$configurazione = array();
			if (key_exists($num_pulsante, $this->configurazione)){
				$num_pulsante_da_scalare = $num_pulsante - 1;
				while (!key_exists($num_pulsante_da_scalare, $this->configurazione) && $num_pulsante_da_scalare >= 0) $num_pulsante_da_scalare--;
				if (key_exists($num_pulsante_da_scalare, $this->configurazione)){
					$pulsante = $this->configurazione[$num_pulsante_da_scalare];
					$this->configurazione[$num_pulsante_da_scalare] = $this->configurazione[$num_pulsante];
					$this->configurazione[$num_pulsante] = $pulsante;
				}
			}
			$this->salva_configurazione();
		}

		public function diminuisci_pulsante($num_pulsante){
			$configurazione = array();
			$max_num_pulsante = max(array_keys($this->configurazione));
			if (key_exists($num_pulsante, $this->configurazione)){
				$num_pulsante_da_scalare = $num_pulsante + 1;
				while (!key_exists($num_pulsante_da_scalare, $this->configurazione) && $num_pulsante_da_scalare <= $max_num_pulsante) $num_pulsante_da_scalare++;
				if (key_exists($num_pulsante_da_scalare, $this->configurazione)){
					$pulsante = $this->configurazione[$num_pulsante_da_scalare];
					$this->configurazione[$num_pulsante_da_scalare] = $this->configurazione[$num_pulsante];
					$this->configurazione[$num_pulsante] = $pulsante;
				}
			}
			$this->salva_configurazione();
		}

		public function clona_pulsante($num_pulsante_da_clonare){
			$configurazione = array();
			if (key_exists($num_pulsante_da_clonare, $this->configurazione)){
				foreach ($this->configurazione as $num_pulsante => $pulsante) if ($num_pulsante < $num_pulsante_da_clonare){
					$configurazione[$num_pulsante] = $pulsante;
				}elseif ($num_pulsante == $num_pulsante_da_clonare){
					$configurazione[$num_pulsante] = $pulsante;
					$pulsante['descrizione'] = 'copia di ' . $pulsante['descrizione'];
					$configurazione[$num_pulsante + 1] = $pulsante;
				}else{
					$configurazione[$num_pulsante + 1] = $pulsante;
				}
			}
			$this->configurazione = $configurazione;
			$this->salva_configurazione();
		}

		public function aumenta_comando($num_pulsante, $num_comando){
			if (key_exists($num_pulsante, $this->configurazione)){
				$comando_prec = $this->configurazione[$num_pulsante]['comandi'][$num_comando - 1];
				$this->configurazione[$num_pulsante]['comandi'][$num_comando - 1] = $this->configurazione[$num_pulsante]['comandi'][$num_comando];
				$this->configurazione[$num_pulsante]['comandi'][$num_comando] = $comando_prec;
			}
			$this->salva_configurazione();
		}

		public function diminuisci_comando($num_pulsante, $num_comando){
			if (key_exists($num_pulsante, $this->configurazione)){
				$comando_succ = $this->configurazione[$num_pulsante]['comandi'][$num_comando + 1];
				$this->configurazione[$num_pulsante]['comandi'][$num_comando + 1] = $this->configurazione[$num_pulsante]['comandi'][$num_comando];
				$this->configurazione[$num_pulsante]['comandi'][$num_comando] = $comando_succ;
			}
			$this->salva_configurazione();
		}

		public function recupera_html_panel_body_pulsanti(){
			$html = '';

			$html = '';

			$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th colspan="5">
			    <span class="glyphicon glyphicon-hand-up"></span> 
			    Pulsanti
			    <div style="float:right;">
			     <button type="button" class="btn btn-sm btn-default" onclick="apri_o_chiudi(this);">
			      <span class="glyphicon glyphicon-chevron-up"></span>
			      </button>
			    </div>
			   </th>
			  </tr>
			 </thead>
			 <tbody id="matrice_nodi_scene">
			';

			$html .= $this->recupera_html_tbody_pulsanti();

			$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';

			return $html;
		}

		public function recupera_html_tbody_pulsanti(){
			$html = '';

			if (count($this->configurazione) > 0){
				$html .= '
			  <tr class="subtitle">
			   <th>#</th>
			   <th><span class="glyphicon glyphicon-info-sign"></span> Identificativo</th>
			   <th><span class="glyphicon glyphicon-tasks"></span> Lista comandi</th>
			   <th><span class="glyphicon glyphicon-cog"></span> Azioni</th>
			  </tr>
			';

				$max_num_pulsante = max(array_keys($this->configurazione));

				foreach ($this->configurazione as $num_pulsante => $pulsante){
					$html_id_pulsante = $this->recupera_html_identificativo_pulsante($num_pulsante, $pulsante['descrizione']);

					$num_comandi = count($this->configurazione[$num_pulsante]['comandi']);

					$html .= '
				<tr>
				 
				 <td style="vertical-align:top;">
				  ' . $num_pulsante . '
				 </td>
				 
				 <td class="highlight">
				 ' . $html_id_pulsante . '
				 </td>
				 
				 <td id="td_lista_comandi_pulsante_' . $num_pulsante . '">
				 ' . $this->recupera_html_lista_comandi_pulsante($num_pulsante) . '
				 </td>
				 
				 <td>
				  <button onclick="premi_pulsante(' . $num_pulsante . ');" type="button" class="btn btn-success">
				   <span class="glyphicon glyphicon-ok"></span> Prova
				  </button>
				  <button ' . ($num_pulsante == 0 ? 'disabled ' : '') . 'id="btn_aumenta_' . $num_pulsante . '" type="button" class="btn btn-info" onclick="aumenta_pulsante(' . $num_pulsante . ');">
			 	   <span class="glyphicon glyphicon-chevron-up"></span>&nbsp;
			      </button>
				  <button ' . ($num_pulsante == $max_num_pulsante ? 'disabled ' : '') . 'id="btn_diminuisci_' . $num_pulsante . '" type="button" class="btn btn-info" onclick="diminuisci_pulsante(' . $num_pulsante . ');">
			 	   <span class="glyphicon glyphicon-chevron-down"></span>&nbsp;
			      </button>
			      <button onclick="clona_pulsante(' . $num_pulsante . ');" type="button" class="btn btn-primary">
				   <span class="glyphicon glyphicon-link"></span> Clona
				  </button>
			      <button onclick="elimina_pulsante(' . $num_pulsante . ');" type="button" class="btn btn-danger">
				   <span class="glyphicon glyphicon-trash"></span> Elimina
				  </button>
				 </td>
				 
				 ';

					$html .= '
				</tr>';
				}
			}else{
				$html .= '<tr><th colspan="3">' . $this->recupera_html_warning('Nessun comando trovato.') . '</th></tr>';
			}

			return $html;
		}

		public function recupera_html_identificativo_pulsante($num, $descr, $modifica = false, $salva = false){
			$html = '';

			if ($salva){
				$this->configurazione[$num]['descrizione'] = $descr;
				$this->salva_configurazione();
			}

			if ($modifica) $html .= '
		 <br/>
		 <input style="display:none;" value="' . $descr . '" id="input_old_id_pulsante_' . $num . '">
		 <div class="block">
		  <input style="width:200px;" type="text" class="form-control" placeholder="Descrizione pulsante" value="' . $descr . '" id="input_id_pulsante_' . $num . '">
		 </div> 
		 <div class="block"> 
		  <button onclick="salva_id(this, ' . $num . ');" type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
		 </div> 
		 <div class="block"> 
		  <button onclick="annulla_salva_id(this, ' . $num . ');" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
		 </div>
		 ';elseif (strlen($descr) > 0) $html .= '<br/><span onclick="click_id_pulsante(this, ' . $num . ',\'' . $descr . '\');" class="label label-warning clickable">' . $descr . '</span>';
			else
				$html .= '<br/><span onclick="click_id_pulsante(this, ' . $num . ',\'\');" class="label label-default clickable">Pulsante #' . $num . '</span>';

			return $html;
		}

		public function recupera_html_lista_comandi_pulsante($num_pulsante, $action = '', $data = ''){
			$html = '';

			// carichiamo l'oggetto di configurazione
			if ($this->DEC == null) $this->DEC = new DALI_ETHERNET_CONFIG();

			$configurazione_pulsante = $this->configurazione[$num_pulsante];
			$num_comandi = count($configurazione_pulsante['comandi']);

			if ($num_comandi > 0 || $action == 'aggiungi_comando'){
				$html .= '
			<table class="table">
			 <thead>
			 </thead>
			 <tbody>
			';

				$aggiungi_comando = ($action == 'aggiungi_comando');

				// Stampiamo i comandi
				$num_comando = 0;
				foreach (array_keys($configurazione_pulsante['comandi']) as $num_comando){
					$modifica_comando = ($action == 'modifica_comando' && $num_comando == $data);
					$azioni = $modifica_comando || ($action != 'modifica_comando' && !$aggiungi_comando);
					$class = ' class="highlightable"';
					if ($modifica_comando) $class = ' class="highlight"';elseif ($aggiungi_comando || $action == 'modifica_comando') $class = '';
					$html .= '<tr' . $class . '>' . $this->recupera_html_tr_comando_pulsante($num_pulsante, $num_comando, false, $modifica_comando, $azioni) . '</tr>';
				}

				if ($aggiungi_comando) $html .= '<tr class="highlight">' . $this->recupera_html_tr_comando_pulsante($num_pulsante, $num_comandi, true, false, true) . '</tr>';

				$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';
			}

			$html .= $this->recupera_html_div_btn_aggiungi_comando($num_pulsante, $num_comandi, $action != 'aggiungi_comando' && $action != 'modifica_comando');

			return $html;
		}

		protected function recupera_html_tr_comando_pulsante($num_pulsante, $num_comando, $aggiungi = false, $modifica = false, $azioni = true){
			$html = '';

			$configurazione_converter;
			$addr;
			$port;
			$cmd_port;
			$tipo;
			$COMANDO;
			$PARAMETRI;

			$html .= '
		 <td style="vertical-align:top;">' . ($num_comando + 1) . ')&nbsp;</td>
		 <td id="' . $num_pulsante . '_' . $num_comando . '">';

			//-------------------------------------------------------------------------------------
			// SCELTA DEL CONVERTITORE

			$html_convertitore = '';

			if (!$aggiungi || $modifica){
				$comando = $this->configurazione[$num_pulsante]['comandi'][$num_comando];

				$addr = $comando['addr'];
				$port = $comando['port'];

				$configurazione_converter = $this->DEC->recupera_configurazione_da_addr_port($addr, $port);
				$cmd_port = $configurazione_converter['cmd_port'];

				$COMANDO = $comando['COMANDO'];
				$PARAMETRI = $comando['PARAMETRI'];
			}

			$html_convertitore = '';
			if ($aggiungi || $modifica){
				$selected_converter_value = ($modifica ? $addr . ':' . $port : '');
				$html_convertitore = $this->recupera_html_select_converter($num_pulsante, $num_comando, $selected_converter_value);
			}else{
				$html_convertitore = $this->recupera_html_div_modulo_HD678DA_B2($addr, $port, $cmd_port);
			}

			$html .= '
		<div class="block">
		 <b>Converter</b>
		 <br/>
		 <div class="block">
		  <img src="../img/HD678DA-B2.png" width="50px">
		 </div>
		 <div class="block">
		 ' . $html_convertitore . '
		 </div>
		</div>
		';

			//-------------------------------------------------------------------------------------
			// SCELTA DEL COMANDO (ADV/SCENA)

			if ($aggiungi || $modifica){
				$ADV_selected = ($modifica && $COMANDO == DALI_ETHERNET::$COMANDO_ADV_SETTING_REQUEST ? ' selected' : '');
				$SCENA_selected = ($modifica && $COMANDO == DALI_ETHERNET::$COMANDO_SCENE_SETTING_REQUEST ? ' selected' : '');

				$html .= '
			<div class="block">
			 <b>Comando</b>
			 <br/>
			 <div class="block">
			  <select
			    multiple
			    id="tipo_' . $num_pulsante . '_' . $num_comando . '"
			    data-max-options="1"
			    onchange="comando_scelto(this,' . $num_pulsante . ',' . $num_comando . ');"
			  	class="selectpicker"
			  	>
			   <option value="ADV"' . $ADV_selected . '>ADV</option>
			   <option value="SCENA"' . $SCENA_selected . '>SCENA</option>
			  </select>
			 </div>
			</div>
			';
			}

			//-------------------------------------------------------------------------------------
			// COMANDO

			if ($aggiungi || $modifica){
				$html .= '
			<div class="block" id="div_comando_' . $num_pulsante . '_' . $num_comando . '">
			';

				if ($modifica){
					$elemento = $PARAMETRI['DALI_ID'];
					if ($COMANDO == DALI_ETHERNET::$COMANDO_ADV_SETTING_REQUEST){
						$tipo = 'ADV';
						$valore = $PARAMETRI['ADV'];
					}elseif ($COMANDO == DALI_ETHERNET::$COMANDO_SCENE_SETTING_REQUEST){
						$tipo = 'SCENA';
						$valore = $PARAMETRI['SCENE'];
					}

					$html .= $this->recupera_html_parametri_comando($num_pulsante, $num_comando, $addr, $port, $tipo, $elemento, $valore);
				}

				$html .= '
			</div>
			';
			}else{
				$html .= '
			<div class="block"> 
			';

				switch ($COMANDO){
					case DALI_ETHERNET::$COMANDO_ADV_SETTING_REQUEST:
						$html .= '
					<div class="block">
					 <b>ELEMENTO</b>
					 <br/>
					 ' . $this->DEC->recupera_html_div_elemento($PARAMETRI['DALI_ID'], $configurazione_converter, false) . '
					</div>
					<div class="block">
					 <b>ADV</b>
					 <br/>
					 ' . $this->recupera_html_ADV($PARAMETRI['ADV']) . '
					</div>
					';
						break;
					case DALI_ETHERNET::$COMANDO_SCENE_SETTING_REQUEST:
						$html .= '
					<div class="block">
					 <b>ELEMENTO</b>
					 <br/>
					 ' . $this->DEC->recupera_html_div_elemento($PARAMETRI['DALI_ID'], $configurazione_converter, false) . '
					</div>
					<div class="block">
					 <b>SCENA</b>
					 <br/>
					 ' . $this->DEC->recupera_html_div_scena($PARAMETRI['SCENE'], $configurazione_converter['scene'][$PARAMETRI['SCENE']], false) . '
					</div>
					';
						break;
				}

				$html .= '
			</div>
			';
			}

			//-------------------------------------------------------------------------------------
			// AZIONI

			if ($azioni) $html .= $this->recupera_html_button_comando($num_pulsante, $num_comando, $aggiungi, $modifica);

			$html .= '
		 </td>
		';


			$html .= '</td>';

			return $html;
		}

		protected function recupera_html_select_converter($num_pulsante, $num_comando, $selected_converter_value = ''){
			$elements = array();

			$html = '
		<select  
		    multiple
		    data-max-options="1"
		    onchange="converter_scelto(this,' . $num_pulsante . ',' . $num_comando . ');"
			id = "converter_' . $num_pulsante . '_' . $num_comando . '"
			name = "select_converter" 
			class="selectpicker" 
			data-width="100%" 
			data-size="10" 
			>
		';

			$lista_converter = $this->DEC->recupera_lista_moduli();

			// aggiungiamo i converter
			foreach ($lista_converter as $converter){
				$addr = $converter['addr'];
				$port = $converter['port'];
				$cmd_port = $converter['cmd_port'];
				$value = $addr . ':' . $port;

				$html .= '<option value="' . $value . '"';

				if ($selected_converter_value != '') if ($selected_converter_value == $value) $html .= ' selected';

				$data_content_html = $this->recupera_html_div_modulo_HD678DA_B2($addr, $port, $cmd_port);
				$data_content = str_replace('"', "'", $data_content_html);

				$html .= ' data-content="' . $data_content . '"';

				$html .= '>';

				$html .= '</option>';
			}

			$html .= '
		</select>
		';

			return $html;
		}

		public function recupera_html_div_modulo_HD678DA_B2($addr, $port, $cmd_port){
			$html = '';

			$escaped_addr = $this->addr_encode($addr);

			$html .= '
		<div class="modulo" id="modulo_' . $escaped_addr . '_' . $port . '">
		  <div>
		   IP <b>' . $addr . '</b>
		  </div>
		  <div>
		   Ctrl <b>' . $port . '</b>
		  </div>
		  <div>
		   Cmd <b>' . $cmd_port . '</b>
		  </div>
		  <div>
		   <h6><span class="label label-default stato_modulo" id="span_' . $escaped_addr . '_' . $port . '"></span></h6>
		  </div>
		</div>
		';

			return $html;
		}

		public function recupera_html_parametri_comando($num_pulsante, $num_comando, $addr, $port, $tipo, $elemento = '', $valore = ''){
			$html = '';

			if ($this->DEC == null) $this->DEC = new DALI_ETHERNET_CONFIG();

			$configurazione = $this->DEC->recupera_configurazione_da_addr_port($addr, $port);

			if ($tipo == 'ADV'){
				$html = $this->DEC->recupera_html_div_controllo_ADV($configurazione, $elemento, $valore);
				$html = str_replace('id = "elemento_da_controllare"', 'id="select_elemento_' . $num_pulsante . '_' . $num_comando . '"', $html);
				$html = str_replace('id="controllo_ADV"', 'id="select_valore_' . $num_pulsante . '_' . $num_comando . '"', $html);
			}elseif ($tipo == 'SCENA'){
				$html = $this->DEC->recupera_html_div_controllo_scene($configurazione, $elemento, $valore);
				$html = str_replace('id = "elemento_scena"', 'id="select_elemento_' . $num_pulsante . '_' . $num_comando . '"', $html);
				$html = str_replace('id = "select_scena"', 'id="select_valore_' . $num_pulsante . '_' . $num_comando . '"', $html);
			}

			//$html .= $this->recupera_html_info('Elemento = '.$elemento);

			return $html;
		}

		public function recupera_html_button_comando($num_pulsante, $num_comando, $aggiungi, $modifica){
			$html = '';
			if ($aggiungi || $modifica){
				$html .= '
			<div class="rblock">
			 <br/>
			 
			 <button 
			 	id="btn_salva_comando_' . $num_pulsante . '_' . $num_comando . '" 
			 	type="button" 
			 	class="btn btn-success" 
			 	onclick="salva_comando(this,' . $num_pulsante . ',' . $num_comando . ');"' . ($aggiungi ? ' style="display:none;"' : '') . '>
			 	<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Salva
			 </button>
			 
			 <button 
			 	id="btn_annulla_comando_' . $num_pulsante . '_' . $num_comando . '" 
			 	type="button" 
			 	class="btn btn-danger" 
			 	onclick="annulla_comando(this,' . $num_pulsante . ',' . $num_comando . ');">
			 	<span class="glyphicon glyphicon-remove"></span>&nbsp;Annulla
			 </button>
			 
			</div> 
			';
			}else{
				$html .= '
			<div class="rblock">
			 <br/>
			 
			 <button 
			 	id="btn_modifica_comando_' . $num_pulsante . '_' . $num_comando . '" 
			 	type="button" 
			 	class="btn btn-primary" 
			 	onclick="modifica_comando(this,' . $num_pulsante . ',' . $num_comando . ');">
			 	<span class="glyphicon glyphicon-pencil"></span>&nbsp;Modifica
			 </button>
			 
			 <button 
			 	id="btn_aumenta_' . $num_pulsante . '_' . $num_comando . '" 
			 	' . ($num_comando > 0 ? '' : ' disabled') . '
			 	type="button" 
			 	class="btn btn-info" 
			 	onclick="aumenta_comando(this,' . $num_pulsante . ',' . $num_comando . ');">
			 	<span class="glyphicon glyphicon-chevron-up"></span>&nbsp;
			 </button>
			 
			 <button 
			 	id="btn_diminuisci_' . $num_pulsante . '_' . $num_comando . '" 
			 	' . ($num_comando < (count($this->configurazione[$num_pulsante]['comandi']) - 1) ? '' : ' disabled') . '
			 	type="button" 
			 	class="btn btn-info" 
			 	onclick="diminuisci_comando(this,' . $num_pulsante . ',' . $num_comando . ');">
			 	<span class="glyphicon glyphicon-chevron-down"></span>&nbsp;
			 </button>
			 
			 <button 
			 	id="btn_elimina_comando_' . $num_pulsante . '_' . $num_comando . '" 
			 	type="button" 
			 	class="btn btn-danger" 
			 	onclick="elimina_comando(this,' . $num_pulsante . ',' . $num_comando . ');">
			 	<span class="glyphicon glyphicon-trash"></span>&nbsp;Elimina
			 </button>
			 
			</div> 
			';
			}

			return $html;
		}

		protected function recupera_html_div_btn_aggiungi_comando($num_pulsante, $num_comando, $visibile = true){
			$style = '';
			if (!$visibile) $style = ' style="display:none;"';

			return '
		<div class="comando">
		 <button' . $style . ' 
		 	id="btn_aggiungi_comando_' . $num_pulsante . '" 
		 	onclick="aggiungi_comando(this,' . $num_pulsante . ',' . $num_comando . ');" 
		 	type="button" 
		 	class="btn btn-xs btn-primary">
		 	 <span class="glyphicon glyphicon-plus"></span> Aggiungi comando
		 </button>
		</div>
		';
		}

		function __destruct(){
			// salviamo la configurazione
			$this->salva_configurazione();

			parent::__destruct();
		}
	}
	