<?php
	/*
	 * Copyright 2018 Vincenzo Suraci
	 */

	class HTML_Dali{
		public static function recupera_html_barra_navigazione($active = ''){
			$html = '';

			$items = array('client' => array('<span class="glyphicon glyphicon-console"></span>&nbsp;Client', 'client_gui.php'), 'config' => array('<span class="glyphicon glyphicon-cog"></span>&nbsp;Config', 'config_gui.php'), 'yun' => array('<span class="glyphicon glyphicon-th"></span>&nbsp;Yun', 'yun_gui.php'), 'mobile' => array('<span class="glyphicon glyphicon-phone"></span>&nbsp;Mobile', 'mobile_gui.php'), 'server' => array('<span class="glyphicon glyphicon-tasks"></span>&nbsp;Server', 'server_gui.php'),);

			require_once __DIR__ . '/../class/dali_ethernet_client.class.php';

			$DC = new DALI_ETHERNET_CLIENT();

			$html .= '
		<nav class="navbar navbar-light bg-faded">
			  
		  <div class="container-fluid">
		    
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		     
			  <a class="navbar-brand" href="#">
			   <img src="../img/dali.png" height="30" alt="Dali">
		      </a> 
			  
		    </div>
		
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		';

			foreach ($items as $item_id => $item_arr){
				$html .= '
			<li' . ($active == $item_id ? ' class="active"' : '') . '><a target="_blank" href="' . $item_arr[1] . '">' . $item_arr[0] . ($active == $item_id ? ' <span class="sr-only">(current)</span>' : '') . '</a></li>
			';
			}

			$html .= '        
		      </ul>
		      
			  <span class="navbar-text">
		       <span class="glyphicon glyphicon-globe"></span>&nbsp;' . $DC->addr . '
		      </span>
		    </div>
		    <!-- /.navbar-collapse -->
		  </div>
		  <!-- /.container-fluid -->
		</nav>
		';

			return $html;
		}
	}

?>