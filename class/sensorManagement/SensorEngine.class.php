<?php
	/**
	 * Copyright (c) 2018. Paolo Maruotti
	 */

	include_once("class/dbEntity/bean.config.php");
	include_once("class/dbEntity/BeanSensorscenario.php");

	class SensorEngine{

		private $aoSensorScenearios = array();

		// this function checks whether the data stored by the sensors correspond to a sensor scenario
		public function checkSensorScenario(){
			// load scenarios into memory
			$aoSensorSceneario = loadSensorScenarios();
			// check if sensor data allow to activate one or more scenarios
			foreach ($aoSensorSceneario as $x){
				if (true == checkSingleSensorScenario($x)){
					//activation of sensor scenario
					activateSensorScenario($x);
				}
			}
		}

		// load all sensor scenarios
		protected function loadSensorScenarios(){
			//get sensor scenarios from database
			$bean = new BeanSensorscenario();
			showBeanOperationResult("SELECT",$bean,$bean->affected_rows);
            $bean->close();


		}

		/**
		 * @param array $aoSensorScenearios
		 *
		 * @return SensorEngine
		 */
		public function setSensorScenearios($aoSensorScenearios){
			$this->aoSensorScenearios = $aoSensorScenearios;
			return $this;
		}

		/**
		 * @return array
		 */
		public function getSensorScenearios(){
			return $this->aoSensorScenearios;
		}



		//

	}