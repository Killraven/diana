<?php
	/**
	 * Copyright (c) 2018. Paolo Maruotti
	 */

	class SensorScenario{
		protected $sName;           //scenario name

		protected $bSceneIsActive;  //scene is active?
		protected $iScenelinked;    //scene to be activated

		protected $bDaliCmdIsActive;//Dali cmd is active?
		protected $sDaliCmd;        //Dali cmd
		protected $sDaliArg;        //Dali cmd argument

		protected $bTimeControlsActive; //time controls active?
		protected $oTimeControls;       //time controls


	}