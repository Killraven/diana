<?php
	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */

	class Sensor{
		protected $iId;
		protected $sDesc;
		protected $sLocation;
		protected $oSensorType;
		protected $oUnit1;
		protected $oUnit2;

		/**
		 * @return integer
		 */
		public function getIId(){
			return $this->iId;
		}

		/**
		 * @param integer $iId
		 */
		public function setIId($iId){
			$this->iId = $iId;
		}

		/**
		 * @return string
		 */
		public function getSDesc(){
			return $this->sDesc;
		}

		/**
		 * @param string $sDesc
		 */
		public function setSDesc($sDesc){
			$this->sDesc = $sDesc;
		}

		/**
		 * @return string
		 */
		public function getSLocation(){
			return $this->sLocation;
		}

		/**
		 * @param string $sLocation
		 */
		public function setSLocation($sLocation){
			$this->sLocation = $sLocation;
		}

		/**
		 * @return
		 */
		public function getOSensorType(){
			return $this->oSensorType;
		}

		/**
		 * @param SensorType $oSensorType
		 */
		public function setOSensorType( SensorType $oSensorType){
			$this->oSensorType = $oSensorType;
		}

		/**
		 * @return mixed
		 */
		public function getOUnit1(){
			return $this->oUnit1;
		}

		/**
		 * @param mixed $oUnit1
		 */
		public function setOUnit1($oUnit1){
			$this->oUnit1 = $oUnit1;
		}

		/**
		 * @return mixed
		 */
		public function getOUnit2(){
			return $this->oUnit2;
		}

		/**
		 * @param mixed $oUnit2
		 */
		public function setOUnit2($oUnit2){
			$this->oUnit2 = $oUnit2;
		}


	}