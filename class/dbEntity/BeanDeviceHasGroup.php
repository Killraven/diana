<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanDeviceHasGroup extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping table field Device_idDevice
		 * Comment for field Device_idDevice: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $deviceIddevice
		 */
		private $deviceIddevice;

		/**
		 * Class attribute for mapping table field Group_idGroup
		 * Comment for field Group_idGroup: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $groupIdgroup
		 */
		private $groupIdgroup;

		/**
		 * Class attribute for storing the SQL DDL of table device_has_group
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBkZXZpY2VfaGFzX2dyb3VwYCAoCiAgYERldmljZV9pZERldmljZWAgaW50KDExKSBOT1QgTlVMTCwKICBgR3JvdXBfaWRHcm91cGAgaW50KDExKSBOT1QgTlVMTCwKICBQUklNQVJZIEtFWSAoYERldmljZV9pZERldmljZWAsYEdyb3VwX2lkR3JvdXBgKSwKICBLRVkgYGZrX0RldmljZV9oYXNfR3JvdXBfR3JvdXAxX2lkeGAgKGBHcm91cF9pZEdyb3VwYCksCiAgS0VZIGBma19EZXZpY2VfaGFzX0dyb3VwX0RldmljZTFfaWR4YCAoYERldmljZV9pZERldmljZWApLAogIENPTlNUUkFJTlQgYGZrX0RldmljZV9oYXNfR3JvdXBfRGV2aWNlMWAgRk9SRUlHTiBLRVkgKGBEZXZpY2VfaWREZXZpY2VgKSBSRUZFUkVOQ0VTIGBkZXZpY2VgIChgaWREZXZpY2VgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04sCiAgQ09OU1RSQUlOVCBgZmtfRGV2aWNlX2hhc19Hcm91cF9Hcm91cDFgIEZPUkVJR04gS0VZIChgR3JvdXBfaWRHcm91cGApIFJFRkVSRU5DRVMgYGdyb3VwYCAoYGlkR3JvdXBgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04KKSBFTkdJTkU9SW5ub0RCIERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setDeviceIddevice Sets the class attribute deviceIddevice with a given value
		 * The attribute deviceIddevice maps the field Device_idDevice defined as int(11).<br>
		 * Comment for field Device_idDevice: Not specified.<br>
		 *
		 * @param int $deviceIddevice
		 *
		 * @category Modifier
		 */
		public function setDeviceIddevice($deviceIddevice){
			$this->deviceIddevice = (int)$deviceIddevice;
		}

		/**
		 * setGroupIdgroup Sets the class attribute groupIdgroup with a given value
		 * The attribute groupIdgroup maps the field Group_idGroup defined as int(11).<br>
		 * Comment for field Group_idGroup: Not specified.<br>
		 *
		 * @param int $groupIdgroup
		 *
		 * @category Modifier
		 */
		public function setGroupIdgroup($groupIdgroup){
			$this->groupIdgroup = (int)$groupIdgroup;
		}

		/**
		 * getDeviceIddevice gets the class attribute deviceIddevice value
		 * The attribute deviceIddevice maps the field Device_idDevice defined as int(11).<br>
		 * Comment for field Device_idDevice: Not specified.
		 * @return int $deviceIddevice
		 * @category Accessor of $deviceIddevice
		 */
		public function getDeviceIddevice(){
			return $this->deviceIddevice;
		}

		/**
		 * getGroupIdgroup gets the class attribute groupIdgroup value
		 * The attribute groupIdgroup maps the field Group_idGroup defined as int(11).<br>
		 * Comment for field Group_idGroup: Not specified.
		 * @return int $groupIdgroup
		 * @category Accessor of $groupIdgroup
		 */
		public function getGroupIdgroup(){
			return $this->groupIdgroup;
		}

		/**
		 * Gets DDL SQL code of the table device_has_group
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "device_has_group";
		}

		/**
		 * The BeanDeviceHasGroup constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none ${ClassPkAttributeName} is given.
		 *  - with a fetched data row from the table {TableName} having {TablePkName}=${ClassPkAttributeName}
		 *
		 * @param int $deviceIddevice
		 * @param int $groupIdgroup
		 *
		 * @return BeanDeviceHasGroup Object
		 */
		public function __construct($deviceIddevice = NULL, $groupIdgroup = NULL){
			parent::__construct();
			if (!empty($deviceIddevice) && !empty($groupIdgroup)){
				$this->select($deviceIddevice, $groupIdgroup);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of device_has_group into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $deviceIddevice
		 * @param int $groupIdgroup
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($deviceIddevice, $groupIdgroup){
			$sql = "SELECT * FROM device_has_group WHERE Device_idDevice={$this->parseValue($deviceIddevice,'int')} AND Group_idGroup={$this->parseValue($groupIdgroup,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->deviceIddevice = (integer)$rowObject->Device_idDevice;
				@$this->groupIdgroup = (integer)$rowObject->Group_idGroup;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table device_has_group
		 *
		 * @param int $deviceIddevice
		 * @param int $groupIdgroup
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($deviceIddevice, $groupIdgroup){
			$sql = "DELETE FROM device_has_group WHERE Device_idDevice={$this->parseValue($deviceIddevice,'int')} AND Group_idGroup={$this->parseValue($groupIdgroup,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of device_has_group
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			// $constants = get_defined_constants();
			$sql = <<< SQL
        INSERT INTO device_has_group
        (Device_idDevice,Group_idGroup)
        VALUES({$this->parseValue($this->deviceIddevice)},
			{$this->parseValue($this->groupIdgroup)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table device_has_group with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $deviceIddevice
		 * @param int $groupIdgroup
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($deviceIddevice, $groupIdgroup){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                device_has_group
                SET 
            WHERE
                Device_idDevice={$this->parseValue($deviceIddevice, 'int')} AND Group_idGroup={$this->parseValue($groupIdgroup, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($deviceIddevice, $groupIdgroup);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of device_has_group previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if (!empty($this->deviceIddevice) && !empty($this->groupIdgroup)){
				return $this->update($this->deviceIddevice, $this->groupIdgroup);
			}else{
				return false;
			}
		}

	}

?>
