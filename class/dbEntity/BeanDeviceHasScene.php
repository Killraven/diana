<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanDeviceHasScene extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping table field Device_idDevice
		 * Comment for field Device_idDevice: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $deviceIddevice
		 */
		private $deviceIddevice;

		/**
		 * Class attribute for mapping table field Scene_idScene
		 * Comment for field Scene_idScene: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $sceneIdscene
		 */
		private $sceneIdscene;

		/**
		 * Class attribute for mapping table field advValue
		 * Comment for field advValue: Not specified.<br>
		 * Field information:
		 *  - Data type: smallint(6)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $advvalue
		 */
		private $advvalue;

		/**
		 * Class attribute for storing the SQL DDL of table device_has_scene
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBkZXZpY2VfaGFzX3NjZW5lYCAoCiAgYERldmljZV9pZERldmljZWAgaW50KDExKSBOT1QgTlVMTCwKICBgU2NlbmVfaWRTY2VuZWAgaW50KDExKSBOT1QgTlVMTCwKICBgYWR2VmFsdWVgIHNtYWxsaW50KDYpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYERldmljZV9pZERldmljZWAsYFNjZW5lX2lkU2NlbmVgKSwKICBLRVkgYGZrX0RldmljZV9oYXNfU2NlbmVfU2NlbmUxX2lkeGAgKGBTY2VuZV9pZFNjZW5lYCksCiAgS0VZIGBma19EZXZpY2VfaGFzX1NjZW5lX0RldmljZV9pZHhgIChgRGV2aWNlX2lkRGV2aWNlYCksCiAgQ09OU1RSQUlOVCBgZmtfRGV2aWNlX2hhc19TY2VuZV9EZXZpY2VgIEZPUkVJR04gS0VZIChgRGV2aWNlX2lkRGV2aWNlYCkgUkVGRVJFTkNFUyBgZGV2aWNlYCAoYGlkRGV2aWNlYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OLAogIENPTlNUUkFJTlQgYGZrX0RldmljZV9oYXNfU2NlbmVfU2NlbmUxYCBGT1JFSUdOIEtFWSAoYFNjZW5lX2lkU2NlbmVgKSBSRUZFUkVOQ0VTIGBzY2VuZWAgKGBpZFNjZW5lYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OCikgRU5HSU5FPUlubm9EQiBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setDeviceIddevice Sets the class attribute deviceIddevice with a given value
		 * The attribute deviceIddevice maps the field Device_idDevice defined as int(11).<br>
		 * Comment for field Device_idDevice: Not specified.<br>
		 *
		 * @param int $deviceIddevice
		 *
		 * @category Modifier
		 */
		public function setDeviceIddevice($deviceIddevice){
			$this->deviceIddevice = (int)$deviceIddevice;
		}

		/**
		 * setSceneIdscene Sets the class attribute sceneIdscene with a given value
		 * The attribute sceneIdscene maps the field Scene_idScene defined as int(11).<br>
		 * Comment for field Scene_idScene: Not specified.<br>
		 *
		 * @param int $sceneIdscene
		 *
		 * @category Modifier
		 */
		public function setSceneIdscene($sceneIdscene){
			$this->sceneIdscene = (int)$sceneIdscene;
		}

		/**
		 * setAdvvalue Sets the class attribute advvalue with a given value
		 * The attribute advvalue maps the field advValue defined as smallint(6).<br>
		 * Comment for field advValue: Not specified.<br>
		 *
		 * @param int $advvalue
		 *
		 * @category Modifier
		 */
		public function setAdvvalue($advvalue){
			$this->advvalue = (int)$advvalue;
		}

		/**
		 * getDeviceIddevice gets the class attribute deviceIddevice value
		 * The attribute deviceIddevice maps the field Device_idDevice defined as int(11).<br>
		 * Comment for field Device_idDevice: Not specified.
		 * @return int $deviceIddevice
		 * @category Accessor of $deviceIddevice
		 */
		public function getDeviceIddevice(){
			return $this->deviceIddevice;
		}

		/**
		 * getSceneIdscene gets the class attribute sceneIdscene value
		 * The attribute sceneIdscene maps the field Scene_idScene defined as int(11).<br>
		 * Comment for field Scene_idScene: Not specified.
		 * @return int $sceneIdscene
		 * @category Accessor of $sceneIdscene
		 */
		public function getSceneIdscene(){
			return $this->sceneIdscene;
		}

		/**
		 * getAdvvalue gets the class attribute advvalue value
		 * The attribute advvalue maps the field advValue defined as smallint(6).<br>
		 * Comment for field advValue: Not specified.
		 * @return int $advvalue
		 * @category Accessor of $advvalue
		 */
		public function getAdvvalue(){
			return $this->advvalue;
		}

		/**
		 * Gets DDL SQL code of the table device_has_scene
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "device_has_scene";
		}

		/**
		 * The BeanDeviceHasScene constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none ${ClassPkAttributeName} is given.
		 *  - with a fetched data row from the table {TableName} having {TablePkName}=${ClassPkAttributeName}
		 *
		 * @param int $deviceIddevice
		 * @param int $sceneIdscene
		 *
		 * @return BeanDeviceHasScene Object
		 */
		public function __construct($deviceIddevice = NULL, $sceneIdscene = NULL){
			parent::__construct();
			if (!empty($deviceIddevice) && !empty($sceneIdscene)){
				$this->select($deviceIddevice, $sceneIdscene);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of device_has_scene into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $deviceIddevice
		 * @param int $sceneIdscene
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($deviceIddevice, $sceneIdscene){
			$sql = "SELECT * FROM device_has_scene WHERE Device_idDevice={$this->parseValue($deviceIddevice,'int')} AND Scene_idScene={$this->parseValue($sceneIdscene,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->deviceIddevice = (integer)$rowObject->Device_idDevice;
				@$this->sceneIdscene = (integer)$rowObject->Scene_idScene;
				@$this->advvalue = (integer)$rowObject->advValue;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table device_has_scene
		 *
		 * @param int $deviceIddevice
		 * @param int $sceneIdscene
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($deviceIddevice, $sceneIdscene){
			$sql = "DELETE FROM device_has_scene WHERE Device_idDevice={$this->parseValue($deviceIddevice,'int')} AND Scene_idScene={$this->parseValue($sceneIdscene,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of device_has_scene
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			// $constants = get_defined_constants();
			$sql = <<< SQL
        INSERT INTO device_has_scene
        (Device_idDevice,Scene_idScene,advValue)
        VALUES({$this->parseValue($this->deviceIddevice)},
			{$this->parseValue($this->sceneIdscene)},
			{$this->parseValue($this->advvalue)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table device_has_scene with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $deviceIddevice
		 * @param int $sceneIdscene
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($deviceIddevice, $sceneIdscene){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                device_has_scene
                SET 
				advValue={$this->parseValue($this->advvalue)}
            WHERE
                Device_idDevice={$this->parseValue($deviceIddevice, 'int')} AND Scene_idScene={$this->parseValue($sceneIdscene, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($deviceIddevice, $sceneIdscene);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of device_has_scene previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if (!empty($this->deviceIddevice) && !empty($this->sceneIdscene)){
				return $this->update($this->deviceIddevice, $this->sceneIdscene);
			}else{
				return false;
			}
		}

	}

?>
