<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanDevicetype extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idDeviceType of table devicetype
		 * Comment for field idDeviceType: Not specified<br>
		 * @var int $iddevicetype
		 */
		private $iddevicetype;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field DeviceTypeDesc
		 * Comment for field DeviceTypeDesc: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $devicetypedesc
		 */
		private $devicetypedesc;

		/**
		 * Class attribute for storing the SQL DDL of table devicetype
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBkZXZpY2V0eXBlYCAoCiAgYGlkRGV2aWNlVHlwZWAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgRGV2aWNlVHlwZURlc2NgIHZhcmNoYXIoNDUpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkRGV2aWNlVHlwZWApCikgRU5HSU5FPUlubm9EQiBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIddevicetype Sets the class attribute iddevicetype with a given value
		 * The attribute iddevicetype maps the field idDeviceType defined as int(11).<br>
		 * Comment for field idDeviceType: Not specified.<br>
		 *
		 * @param int $iddevicetype
		 *
		 * @category Modifier
		 */
		public function setIddevicetype($iddevicetype){
			$this->iddevicetype = (int)$iddevicetype;
		}

		/**
		 * setDevicetypedesc Sets the class attribute devicetypedesc with a given value
		 * The attribute devicetypedesc maps the field DeviceTypeDesc defined as varchar(45).<br>
		 * Comment for field DeviceTypeDesc: Not specified.<br>
		 *
		 * @param string $devicetypedesc
		 *
		 * @category Modifier
		 */
		public function setDevicetypedesc($devicetypedesc){
			$this->devicetypedesc = (string)$devicetypedesc;
		}

		/**
		 * getIddevicetype gets the class attribute iddevicetype value
		 * The attribute iddevicetype maps the field idDeviceType defined as int(11).<br>
		 * Comment for field idDeviceType: Not specified.
		 * @return int $iddevicetype
		 * @category Accessor of $iddevicetype
		 */
		public function getIddevicetype(){
			return $this->iddevicetype;
		}

		/**
		 * getDevicetypedesc gets the class attribute devicetypedesc value
		 * The attribute devicetypedesc maps the field DeviceTypeDesc defined as varchar(45).<br>
		 * Comment for field DeviceTypeDesc: Not specified.
		 * @return string $devicetypedesc
		 * @category Accessor of $devicetypedesc
		 */
		public function getDevicetypedesc(){
			return $this->devicetypedesc;
		}

		/**
		 * Gets DDL SQL code of the table devicetype
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "devicetype";
		}

		/**
		 * The BeanDevicetype constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $iddevicetype is given.
		 *  - with a fetched data row from the table devicetype having idDeviceType=$iddevicetype
		 *
		 * @param int $iddevicetype . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanDevicetype Object
		 */
		public function __construct($iddevicetype = null){
			parent::__construct();
			if (!empty($iddevicetype)){
				$this->select($iddevicetype);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of devicetype into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $iddevicetype the primary key idDeviceType value of table devicetype which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($iddevicetype){
			$sql = "SELECT * FROM devicetype WHERE idDeviceType={$this->parseValue($iddevicetype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->iddevicetype = (integer)$rowObject->idDeviceType;
				@$this->devicetypedesc = $this->replaceAposBackSlash($rowObject->DeviceTypeDesc);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table devicetype
		 *
		 * @param int $iddevicetype the primary key idDeviceType value of table devicetype which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($iddevicetype){
			$sql = "DELETE FROM devicetype WHERE idDeviceType={$this->parseValue($iddevicetype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of devicetype
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->iddevicetype = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO devicetype
            (DeviceTypeDesc)
            VALUES(
			{$this->parseValue($this->devicetypedesc, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->iddevicetype = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table devicetype with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $iddevicetype the primary key idDeviceType value of table devicetype which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($iddevicetype){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                devicetype
            SET 
				DeviceTypeDesc={$this->parseValue($this->devicetypedesc, 'notNumber')}
            WHERE
                idDeviceType={$this->parseValue($iddevicetype, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($iddevicetype);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of devicetype previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->iddevicetype != ""){
				return $this->update($this->iddevicetype);
			}else{
				return false;
			}
		}

	}

?>
