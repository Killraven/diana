<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanSensorscenarioHasSensor extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping table field sensorscenario_idSensorScenario
		 * Comment for field sensorscenario_idSensorScenario: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $sensorscenarioIdsensorscenario
		 */
		private $sensorscenarioIdsensorscenario;

		/**
		 * Class attribute for mapping table field sensor_idsensor
		 * Comment for field sensor_idsensor: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $sensorIdsensor
		 */
		private $sensorIdsensor;

		/**
		 * Class attribute for mapping table field isActive
		 * Comment for field isActive: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default: 0
		 *  - Extra:
		 * @var int $isactive
		 */
		private $isactive;

		/**
		 * Class attribute for mapping table field flag1
		 * Comment for field flag1: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $flag1
		 */
		private $flag1;

		/**
		 * Class attribute for mapping table field flag2
		 * Comment for field flag2: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $flag2
		 */
		private $flag2;

		/**
		 * Class attribute for mapping table field min1
		 * Comment for field min1: Not specified.<br>
		 * Field information:
		 *  - Data type: double
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $min1
		 */
		private $min1;

		/**
		 * Class attribute for mapping table field max1
		 * Comment for field max1: Not specified.<br>
		 * Field information:
		 *  - Data type: double
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $max1
		 */
		private $max1;

		/**
		 * Class attribute for mapping table field min2
		 * Comment for field min2: Not specified.<br>
		 * Field information:
		 *  - Data type: double
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $min2
		 */
		private $min2;

		/**
		 * Class attribute for mapping table field max2
		 * Comment for field max2: Not specified.<br>
		 * Field information:
		 *  - Data type: double
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $max2
		 */
		private $max2;

		/**
		 * Class attribute for storing the SQL DDL of table sensorscenario_has_sensor
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBzZW5zb3JzY2VuYXJpb19oYXNfc2Vuc29yYCAoCiAgYHNlbnNvcnNjZW5hcmlvX2lkU2Vuc29yU2NlbmFyaW9gIGludCgxMSkgTk9UIE5VTEwsCiAgYHNlbnNvcl9pZHNlbnNvcmAgaW50KDExKSBOT1QgTlVMTCwKICBgaXNBY3RpdmVgIHRpbnlpbnQoNCkgTk9UIE5VTEwgREVGQVVMVCAnMCcsCiAgYGZsYWcxYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgZmxhZzJgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGBtaW4xYCBkb3VibGUgREVGQVVMVCBOVUxMLAogIGBtYXgxYCBkb3VibGUgREVGQVVMVCBOVUxMLAogIGBtaW4yYCBkb3VibGUgREVGQVVMVCBOVUxMLAogIGBtYXgyYCBkb3VibGUgREVGQVVMVCBOVUxMLAogIFBSSU1BUlkgS0VZIChgc2Vuc29yc2NlbmFyaW9faWRTZW5zb3JTY2VuYXJpb2AsYHNlbnNvcl9pZHNlbnNvcmApLAogIEtFWSBgZmtfc2Vuc29yc2NlbmFyaW9faGFzX3NlbnNvcl9zZW5zb3IxX2lkeGAgKGBzZW5zb3JfaWRzZW5zb3JgKSwKICBLRVkgYGZrX3NlbnNvcnNjZW5hcmlvX2hhc19zZW5zb3Jfc2Vuc29yc2NlbmFyaW8xX2lkeGAgKGBzZW5zb3JzY2VuYXJpb19pZFNlbnNvclNjZW5hcmlvYCksCiAgQ09OU1RSQUlOVCBgZmtfc2Vuc29yc2NlbmFyaW9faGFzX3NlbnNvcl9zZW5zb3IxYCBGT1JFSUdOIEtFWSAoYHNlbnNvcl9pZHNlbnNvcmApIFJFRkVSRU5DRVMgYHNlbnNvcmAgKGBpZHNlbnNvcmApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19zZW5zb3JzY2VuYXJpb19oYXNfc2Vuc29yX3NlbnNvcnNjZW5hcmlvMWAgRk9SRUlHTiBLRVkgKGBzZW5zb3JzY2VuYXJpb19pZFNlbnNvclNjZW5hcmlvYCkgUkVGRVJFTkNFUyBgc2Vuc29yc2NlbmFyaW9gIChgaWRTZW5zb3JTY2VuYXJpb2ApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTgopIEVOR0lORT1Jbm5vREIgREVGQVVMVCBDSEFSU0VUPXV0Zjg=";

		/**
		 * setSensorscenarioIdsensorscenario Sets the class attribute sensorscenarioIdsensorscenario with a given value
		 * The attribute sensorscenarioIdsensorscenario maps the field sensorscenario_idSensorScenario defined as int(11).<br>
		 * Comment for field sensorscenario_idSensorScenario: Not specified.<br>
		 *
		 * @param int $sensorscenarioIdsensorscenario
		 *
		 * @category Modifier
		 */
		public function setSensorscenarioIdsensorscenario($sensorscenarioIdsensorscenario){
			$this->sensorscenarioIdsensorscenario = (int)$sensorscenarioIdsensorscenario;
		}

		/**
		 * setSensorIdsensor Sets the class attribute sensorIdsensor with a given value
		 * The attribute sensorIdsensor maps the field sensor_idsensor defined as int(11).<br>
		 * Comment for field sensor_idsensor: Not specified.<br>
		 *
		 * @param int $sensorIdsensor
		 *
		 * @category Modifier
		 */
		public function setSensorIdsensor($sensorIdsensor){
			$this->sensorIdsensor = (int)$sensorIdsensor;
		}

		/**
		 * setIsactive Sets the class attribute isactive with a given value
		 * The attribute isactive maps the field isActive defined as tinyint(4).<br>
		 * Comment for field isActive: Not specified.<br>
		 *
		 * @param int $isactive
		 *
		 * @category Modifier
		 */
		public function setIsactive($isactive){
			$this->isactive = (int)$isactive;
		}

		/**
		 * setFlag1 Sets the class attribute flag1 with a given value
		 * The attribute flag1 maps the field flag1 defined as tinyint(4).<br>
		 * Comment for field flag1: Not specified.<br>
		 *
		 * @param int $flag1
		 *
		 * @category Modifier
		 */
		public function setFlag1($flag1){
			$this->flag1 = (int)$flag1;
		}

		/**
		 * setFlag2 Sets the class attribute flag2 with a given value
		 * The attribute flag2 maps the field flag2 defined as tinyint(4).<br>
		 * Comment for field flag2: Not specified.<br>
		 *
		 * @param int $flag2
		 *
		 * @category Modifier
		 */
		public function setFlag2($flag2){
			$this->flag2 = (int)$flag2;
		}

		/**
		 * setMin1 Sets the class attribute min1 with a given value
		 * The attribute min1 maps the field min1 defined as double.<br>
		 * Comment for field min1: Not specified.<br>
		 *
		 * @param float $min1
		 *
		 * @category Modifier
		 */
		public function setMin1($min1){
			$this->min1 = (double)$min1;
		}

		/**
		 * setMax1 Sets the class attribute max1 with a given value
		 * The attribute max1 maps the field max1 defined as double.<br>
		 * Comment for field max1: Not specified.<br>
		 *
		 * @param float $max1
		 *
		 * @category Modifier
		 */
		public function setMax1($max1){
			$this->max1 = (double)$max1;
		}

		/**
		 * setMin2 Sets the class attribute min2 with a given value
		 * The attribute min2 maps the field min2 defined as double.<br>
		 * Comment for field min2: Not specified.<br>
		 *
		 * @param float $min2
		 *
		 * @category Modifier
		 */
		public function setMin2($min2){
			$this->min2 = (double)$min2;
		}

		/**
		 * setMax2 Sets the class attribute max2 with a given value
		 * The attribute max2 maps the field max2 defined as double.<br>
		 * Comment for field max2: Not specified.<br>
		 *
		 * @param float $max2
		 *
		 * @category Modifier
		 */
		public function setMax2($max2){
			$this->max2 = (double)$max2;
		}

		/**
		 * getSensorscenarioIdsensorscenario gets the class attribute sensorscenarioIdsensorscenario value
		 * The attribute sensorscenarioIdsensorscenario maps the field sensorscenario_idSensorScenario defined as int(11).<br>
		 * Comment for field sensorscenario_idSensorScenario: Not specified.
		 * @return int $sensorscenarioIdsensorscenario
		 * @category Accessor of $sensorscenarioIdsensorscenario
		 */
		public function getSensorscenarioIdsensorscenario(){
			return $this->sensorscenarioIdsensorscenario;
		}

		/**
		 * getSensorIdsensor gets the class attribute sensorIdsensor value
		 * The attribute sensorIdsensor maps the field sensor_idsensor defined as int(11).<br>
		 * Comment for field sensor_idsensor: Not specified.
		 * @return int $sensorIdsensor
		 * @category Accessor of $sensorIdsensor
		 */
		public function getSensorIdsensor(){
			return $this->sensorIdsensor;
		}

		/**
		 * getIsactive gets the class attribute isactive value
		 * The attribute isactive maps the field isActive defined as tinyint(4).<br>
		 * Comment for field isActive: Not specified.
		 * @return int $isactive
		 * @category Accessor of $isactive
		 */
		public function getIsactive(){
			return $this->isactive;
		}

		/**
		 * getFlag1 gets the class attribute flag1 value
		 * The attribute flag1 maps the field flag1 defined as tinyint(4).<br>
		 * Comment for field flag1: Not specified.
		 * @return int $flag1
		 * @category Accessor of $flag1
		 */
		public function getFlag1(){
			return $this->flag1;
		}

		/**
		 * getFlag2 gets the class attribute flag2 value
		 * The attribute flag2 maps the field flag2 defined as tinyint(4).<br>
		 * Comment for field flag2: Not specified.
		 * @return int $flag2
		 * @category Accessor of $flag2
		 */
		public function getFlag2(){
			return $this->flag2;
		}

		/**
		 * getMin1 gets the class attribute min1 value
		 * The attribute min1 maps the field min1 defined as double.<br>
		 * Comment for field min1: Not specified.
		 * @return float $min1
		 * @category Accessor of $min1
		 */
		public function getMin1(){
			return $this->min1;
		}

		/**
		 * getMax1 gets the class attribute max1 value
		 * The attribute max1 maps the field max1 defined as double.<br>
		 * Comment for field max1: Not specified.
		 * @return float $max1
		 * @category Accessor of $max1
		 */
		public function getMax1(){
			return $this->max1;
		}

		/**
		 * getMin2 gets the class attribute min2 value
		 * The attribute min2 maps the field min2 defined as double.<br>
		 * Comment for field min2: Not specified.
		 * @return float $min2
		 * @category Accessor of $min2
		 */
		public function getMin2(){
			return $this->min2;
		}

		/**
		 * getMax2 gets the class attribute max2 value
		 * The attribute max2 maps the field max2 defined as double.<br>
		 * Comment for field max2: Not specified.
		 * @return float $max2
		 * @category Accessor of $max2
		 */
		public function getMax2(){
			return $this->max2;
		}

		/**
		 * Gets DDL SQL code of the table sensorscenario_has_sensor
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "sensorscenario_has_sensor";
		}

		/**
		 * The BeanSensorscenarioHasSensor constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none ${ClassPkAttributeName} is given.
		 *  - with a fetched data row from the table {TableName} having {TablePkName}=${ClassPkAttributeName}
		 *
		 * @param int $sensorscenarioIdsensorscenario
		 * @param int $sensorIdsensor
		 *
		 * @return BeanSensorscenarioHasSensor Object
		 */
		public function __construct($sensorscenarioIdsensorscenario = NULL, $sensorIdsensor = NULL){
			parent::__construct();
			if (!empty($sensorscenarioIdsensorscenario) && !empty($sensorIdsensor)){
				$this->select($sensorscenarioIdsensorscenario, $sensorIdsensor);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of sensorscenario_has_sensor into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $sensorscenarioIdsensorscenario
		 * @param int $sensorIdsensor
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($sensorscenarioIdsensorscenario, $sensorIdsensor){
			$sql = "SELECT * FROM sensorscenario_has_sensor WHERE sensorscenario_idSensorScenario={$this->parseValue($sensorscenarioIdsensorscenario,'int')} AND sensor_idsensor={$this->parseValue($sensorIdsensor,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->sensorscenarioIdsensorscenario = (integer)$rowObject->sensorscenario_idSensorScenario;
				@$this->sensorIdsensor = (integer)$rowObject->sensor_idsensor;
				@$this->isactive = (integer)$rowObject->isActive;
				@$this->flag1 = (integer)$rowObject->flag1;
				@$this->flag2 = (integer)$rowObject->flag2;
				@$this->min1 = (double)$rowObject->min1;
				@$this->max1 = (double)$rowObject->max1;
				@$this->min2 = (double)$rowObject->min2;
				@$this->max2 = (double)$rowObject->max2;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table sensorscenario_has_sensor
		 *
		 * @param int $sensorscenarioIdsensorscenario
		 * @param int $sensorIdsensor
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($sensorscenarioIdsensorscenario, $sensorIdsensor){
			$sql = "DELETE FROM sensorscenario_has_sensor WHERE sensorscenario_idSensorScenario={$this->parseValue($sensorscenarioIdsensorscenario,'int')} AND sensor_idsensor={$this->parseValue($sensorIdsensor,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of sensorscenario_has_sensor
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			// $constants = get_defined_constants();
			$sql = <<< SQL
        INSERT INTO sensorscenario_has_sensor
        (sensorscenario_idSensorScenario,sensor_idsensor,isActive,flag1,flag2,min1,max1,min2,max2)
        VALUES({$this->parseValue($this->sensorscenarioIdsensorscenario)},
			{$this->parseValue($this->sensorIdsensor)},
			{$this->parseValue($this->isactive)},
			{$this->parseValue($this->flag1)},
			{$this->parseValue($this->flag2)},
			{$this->parseValue($this->min1)},
			{$this->parseValue($this->max1)},
			{$this->parseValue($this->min2)},
			{$this->parseValue($this->max2)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table sensorscenario_has_sensor with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $sensorscenarioIdsensorscenario
		 * @param int $sensorIdsensor
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($sensorscenarioIdsensorscenario, $sensorIdsensor){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                sensorscenario_has_sensor
                SET 
				isActive={$this->parseValue($this->isactive)},
				flag1={$this->parseValue($this->flag1)},
				flag2={$this->parseValue($this->flag2)},
				min1={$this->parseValue($this->min1)},
				max1={$this->parseValue($this->max1)},
				min2={$this->parseValue($this->min2)},
				max2={$this->parseValue($this->max2)}
            WHERE
                sensorscenario_idSensorScenario={$this->parseValue($sensorscenarioIdsensorscenario, 'int')} AND sensor_idsensor={$this->parseValue($sensorIdsensor, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($sensorscenarioIdsensorscenario, $sensorIdsensor);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of sensorscenario_has_sensor previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if (!empty($this->sensorscenarioIdsensorscenario) && !empty($this->sensorIdsensor)){
				return $this->update($this->sensorscenarioIdsensorscenario, $this->sensorIdsensor);
			}else{
				return false;
			}
		}

	}

?>
