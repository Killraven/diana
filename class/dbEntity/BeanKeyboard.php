<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanKeyboard extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idKeyboard of table keyboard
		 * Comment for field idKeyboard: Not specified<br>
		 * @var int $idkeyboard
		 */
		private $idkeyboard;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field daliCmdValue
		 * Comment for field daliCmdValue: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $dalicmdvalue
		 */
		private $dalicmdvalue;

		/**
		 * Class attribute for mapping table field stepperDirection
		 * Comment for field stepperDirection: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $stepperdirection
		 */
		private $stepperdirection;

		/**
		 * Class attribute for mapping table field stepperSteps
		 * Comment for field stepperSteps: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $steppersteps
		 */
		private $steppersteps;

		/**
		 * Class attribute for mapping table field buzzerValue
		 * Comment for field buzzerValue: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $buzzervalue
		 */
		private $buzzervalue;

		/**
		 * Class attribute for mapping table field lightValue
		 * Comment for field lightValue: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $lightvalue
		 */
		private $lightvalue;

		/**
		 * Class attribute for mapping table field Group_idGroup
		 * Comment for field Group_idGroup: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $groupIdgroup
		 */
		private $groupIdgroup;

		/**
		 * Class attribute for mapping table field Scene_idScene
		 * Comment for field Scene_idScene: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $sceneIdscene
		 */
		private $sceneIdscene;

		/**
		 * Class attribute for mapping table field Command_idCommand
		 * Comment for field Command_idCommand: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $commandIdcommand
		 */
		private $commandIdcommand;

		/**
		 * Class attribute for storing the SQL DDL of table keyboard
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBrZXlib2FyZGAgKAogIGBpZEtleWJvYXJkYCBpbnQoMTEpIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULAogIGBkYWxpQ21kVmFsdWVgIGludCgxMSkgREVGQVVMVCBOVUxMLAogIGBzdGVwcGVyRGlyZWN0aW9uYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgc3RlcHBlclN0ZXBzYCBpbnQoMTEpIERFRkFVTFQgTlVMTCwKICBgYnV6emVyVmFsdWVgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGBsaWdodFZhbHVlYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgR3JvdXBfaWRHcm91cGAgaW50KDExKSBOT1QgTlVMTCwKICBgU2NlbmVfaWRTY2VuZWAgaW50KDExKSBOT1QgTlVMTCwKICBgQ29tbWFuZF9pZENvbW1hbmRgIGludCgxMSkgTk9UIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZEtleWJvYXJkYCksCiAgS0VZIGBma19LZXlib2FyZF9TY2VuZTFfaWR4YCAoYFNjZW5lX2lkU2NlbmVgKSwKICBLRVkgYGZrX0tleWJvYXJkX0NvbW1hbmQxX2lkeGAgKGBDb21tYW5kX2lkQ29tbWFuZGApLAogIEtFWSBgZmtfS2V5Ym9hcmRfR3JvdXAxX2lkeGAgKGBHcm91cF9pZEdyb3VwYCksCiAgQ09OU1RSQUlOVCBgZmtfS2V5Ym9hcmRfQ29tbWFuZDFgIEZPUkVJR04gS0VZIChgQ29tbWFuZF9pZENvbW1hbmRgKSBSRUZFUkVOQ0VTIGBjb21tYW5kYCAoYGlkQ29tbWFuZGApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19LZXlib2FyZF9Hcm91cDFgIEZPUkVJR04gS0VZIChgR3JvdXBfaWRHcm91cGApIFJFRkVSRU5DRVMgYGdyb3VwYCAoYGlkR3JvdXBgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04sCiAgQ09OU1RSQUlOVCBgZmtfS2V5Ym9hcmRfU2NlbmUxYCBGT1JFSUdOIEtFWSAoYFNjZW5lX2lkU2NlbmVgKSBSRUZFUkVOQ0VTIGBzY2VuZWAgKGBpZFNjZW5lYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OCikgRU5HSU5FPUlubm9EQiBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIdkeyboard Sets the class attribute idkeyboard with a given value
		 * The attribute idkeyboard maps the field idKeyboard defined as int(11).<br>
		 * Comment for field idKeyboard: Not specified.<br>
		 *
		 * @param int $idkeyboard
		 *
		 * @category Modifier
		 */
		public function setIdkeyboard($idkeyboard){
			$this->idkeyboard = (int)$idkeyboard;
		}

		/**
		 * setDalicmdvalue Sets the class attribute dalicmdvalue with a given value
		 * The attribute dalicmdvalue maps the field daliCmdValue defined as int(11).<br>
		 * Comment for field daliCmdValue: Not specified.<br>
		 *
		 * @param int $dalicmdvalue
		 *
		 * @category Modifier
		 */
		public function setDalicmdvalue($dalicmdvalue){
			$this->dalicmdvalue = (int)$dalicmdvalue;
		}

		/**
		 * setStepperdirection Sets the class attribute stepperdirection with a given value
		 * The attribute stepperdirection maps the field stepperDirection defined as tinyint(4).<br>
		 * Comment for field stepperDirection: Not specified.<br>
		 *
		 * @param int $stepperdirection
		 *
		 * @category Modifier
		 */
		public function setStepperdirection($stepperdirection){
			$this->stepperdirection = (int)$stepperdirection;
		}

		/**
		 * setSteppersteps Sets the class attribute steppersteps with a given value
		 * The attribute steppersteps maps the field stepperSteps defined as int(11).<br>
		 * Comment for field stepperSteps: Not specified.<br>
		 *
		 * @param int $steppersteps
		 *
		 * @category Modifier
		 */
		public function setSteppersteps($steppersteps){
			$this->steppersteps = (int)$steppersteps;
		}

		/**
		 * setBuzzervalue Sets the class attribute buzzervalue with a given value
		 * The attribute buzzervalue maps the field buzzerValue defined as tinyint(4).<br>
		 * Comment for field buzzerValue: Not specified.<br>
		 *
		 * @param int $buzzervalue
		 *
		 * @category Modifier
		 */
		public function setBuzzervalue($buzzervalue){
			$this->buzzervalue = (int)$buzzervalue;
		}

		/**
		 * setLightvalue Sets the class attribute lightvalue with a given value
		 * The attribute lightvalue maps the field lightValue defined as tinyint(4).<br>
		 * Comment for field lightValue: Not specified.<br>
		 *
		 * @param int $lightvalue
		 *
		 * @category Modifier
		 */
		public function setLightvalue($lightvalue){
			$this->lightvalue = (int)$lightvalue;
		}

		/**
		 * setGroupIdgroup Sets the class attribute groupIdgroup with a given value
		 * The attribute groupIdgroup maps the field Group_idGroup defined as int(11).<br>
		 * Comment for field Group_idGroup: Not specified.<br>
		 *
		 * @param int $groupIdgroup
		 *
		 * @category Modifier
		 */
		public function setGroupIdgroup($groupIdgroup){
			$this->groupIdgroup = (int)$groupIdgroup;
		}

		/**
		 * setSceneIdscene Sets the class attribute sceneIdscene with a given value
		 * The attribute sceneIdscene maps the field Scene_idScene defined as int(11).<br>
		 * Comment for field Scene_idScene: Not specified.<br>
		 *
		 * @param int $sceneIdscene
		 *
		 * @category Modifier
		 */
		public function setSceneIdscene($sceneIdscene){
			$this->sceneIdscene = (int)$sceneIdscene;
		}

		/**
		 * setCommandIdcommand Sets the class attribute commandIdcommand with a given value
		 * The attribute commandIdcommand maps the field Command_idCommand defined as int(11).<br>
		 * Comment for field Command_idCommand: Not specified.<br>
		 *
		 * @param int $commandIdcommand
		 *
		 * @category Modifier
		 */
		public function setCommandIdcommand($commandIdcommand){
			$this->commandIdcommand = (int)$commandIdcommand;
		}

		/**
		 * getIdkeyboard gets the class attribute idkeyboard value
		 * The attribute idkeyboard maps the field idKeyboard defined as int(11).<br>
		 * Comment for field idKeyboard: Not specified.
		 * @return int $idkeyboard
		 * @category Accessor of $idkeyboard
		 */
		public function getIdkeyboard(){
			return $this->idkeyboard;
		}

		/**
		 * getDalicmdvalue gets the class attribute dalicmdvalue value
		 * The attribute dalicmdvalue maps the field daliCmdValue defined as int(11).<br>
		 * Comment for field daliCmdValue: Not specified.
		 * @return int $dalicmdvalue
		 * @category Accessor of $dalicmdvalue
		 */
		public function getDalicmdvalue(){
			return $this->dalicmdvalue;
		}

		/**
		 * getStepperdirection gets the class attribute stepperdirection value
		 * The attribute stepperdirection maps the field stepperDirection defined as tinyint(4).<br>
		 * Comment for field stepperDirection: Not specified.
		 * @return int $stepperdirection
		 * @category Accessor of $stepperdirection
		 */
		public function getStepperdirection(){
			return $this->stepperdirection;
		}

		/**
		 * getSteppersteps gets the class attribute steppersteps value
		 * The attribute steppersteps maps the field stepperSteps defined as int(11).<br>
		 * Comment for field stepperSteps: Not specified.
		 * @return int $steppersteps
		 * @category Accessor of $steppersteps
		 */
		public function getSteppersteps(){
			return $this->steppersteps;
		}

		/**
		 * getBuzzervalue gets the class attribute buzzervalue value
		 * The attribute buzzervalue maps the field buzzerValue defined as tinyint(4).<br>
		 * Comment for field buzzerValue: Not specified.
		 * @return int $buzzervalue
		 * @category Accessor of $buzzervalue
		 */
		public function getBuzzervalue(){
			return $this->buzzervalue;
		}

		/**
		 * getLightvalue gets the class attribute lightvalue value
		 * The attribute lightvalue maps the field lightValue defined as tinyint(4).<br>
		 * Comment for field lightValue: Not specified.
		 * @return int $lightvalue
		 * @category Accessor of $lightvalue
		 */
		public function getLightvalue(){
			return $this->lightvalue;
		}

		/**
		 * getGroupIdgroup gets the class attribute groupIdgroup value
		 * The attribute groupIdgroup maps the field Group_idGroup defined as int(11).<br>
		 * Comment for field Group_idGroup: Not specified.
		 * @return int $groupIdgroup
		 * @category Accessor of $groupIdgroup
		 */
		public function getGroupIdgroup(){
			return $this->groupIdgroup;
		}

		/**
		 * getSceneIdscene gets the class attribute sceneIdscene value
		 * The attribute sceneIdscene maps the field Scene_idScene defined as int(11).<br>
		 * Comment for field Scene_idScene: Not specified.
		 * @return int $sceneIdscene
		 * @category Accessor of $sceneIdscene
		 */
		public function getSceneIdscene(){
			return $this->sceneIdscene;
		}

		/**
		 * getCommandIdcommand gets the class attribute commandIdcommand value
		 * The attribute commandIdcommand maps the field Command_idCommand defined as int(11).<br>
		 * Comment for field Command_idCommand: Not specified.
		 * @return int $commandIdcommand
		 * @category Accessor of $commandIdcommand
		 */
		public function getCommandIdcommand(){
			return $this->commandIdcommand;
		}

		/**
		 * Gets DDL SQL code of the table keyboard
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "keyboard";
		}

		/**
		 * The BeanKeyboard constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idkeyboard is given.
		 *  - with a fetched data row from the table keyboard having idKeyboard=$idkeyboard
		 *
		 * @param int $idkeyboard . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanKeyboard Object
		 */
		public function __construct($idkeyboard = null){
			parent::__construct();
			if (!empty($idkeyboard)){
				$this->select($idkeyboard);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of keyboard into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idkeyboard the primary key idKeyboard value of table keyboard which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idkeyboard){
			$sql = "SELECT * FROM keyboard WHERE idKeyboard={$this->parseValue($idkeyboard,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idkeyboard = (integer)$rowObject->idKeyboard;
				@$this->dalicmdvalue = (integer)$rowObject->daliCmdValue;
				@$this->stepperdirection = (integer)$rowObject->stepperDirection;
				@$this->steppersteps = (integer)$rowObject->stepperSteps;
				@$this->buzzervalue = (integer)$rowObject->buzzerValue;
				@$this->lightvalue = (integer)$rowObject->lightValue;
				@$this->groupIdgroup = (integer)$rowObject->Group_idGroup;
				@$this->sceneIdscene = (integer)$rowObject->Scene_idScene;
				@$this->commandIdcommand = (integer)$rowObject->Command_idCommand;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table keyboard
		 *
		 * @param int $idkeyboard the primary key idKeyboard value of table keyboard which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idkeyboard){
			$sql = "DELETE FROM keyboard WHERE idKeyboard={$this->parseValue($idkeyboard,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of keyboard
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idkeyboard = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO keyboard
            (daliCmdValue,stepperDirection,stepperSteps,buzzerValue,lightValue,Group_idGroup,Scene_idScene,Command_idCommand)
            VALUES(
			{$this->parseValue($this->dalicmdvalue)},
			{$this->parseValue($this->stepperdirection)},
			{$this->parseValue($this->steppersteps)},
			{$this->parseValue($this->buzzervalue)},
			{$this->parseValue($this->lightvalue)},
			{$this->parseValue($this->groupIdgroup)},
			{$this->parseValue($this->sceneIdscene)},
			{$this->parseValue($this->commandIdcommand)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idkeyboard = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table keyboard with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idkeyboard the primary key idKeyboard value of table keyboard which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idkeyboard){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                keyboard
            SET 
				daliCmdValue={$this->parseValue($this->dalicmdvalue)},
				stepperDirection={$this->parseValue($this->stepperdirection)},
				stepperSteps={$this->parseValue($this->steppersteps)},
				buzzerValue={$this->parseValue($this->buzzervalue)},
				lightValue={$this->parseValue($this->lightvalue)},
				Group_idGroup={$this->parseValue($this->groupIdgroup)},
				Scene_idScene={$this->parseValue($this->sceneIdscene)},
				Command_idCommand={$this->parseValue($this->commandIdcommand)}
            WHERE
                idKeyboard={$this->parseValue($idkeyboard, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idkeyboard);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of keyboard previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idkeyboard != ""){
				return $this->update($this->idkeyboard);
			}else{
				return false;
			}
		}

	}

?>
