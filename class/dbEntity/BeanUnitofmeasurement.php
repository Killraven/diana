<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanUnitofmeasurement extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idunitOfMeasurement of table unitofmeasurement
		 * Comment for field idunitOfMeasurement: Not specified<br>
		 * @var int $idunitofmeasurement
		 */
		private $idunitofmeasurement;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field symbol
		 * Comment for field symbol: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(10)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $symbol
		 */
		private $symbol;

		/**
		 * Class attribute for mapping table field name
		 * Comment for field name: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $name
		 */
		private $name;

		/**
		 * Class attribute for mapping table field quantity
		 * Comment for field quantity: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $quantity
		 */
		private $quantity;

		/**
		 * Class attribute for mapping table field siUnit
		 * Comment for field siUnit: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $siunit
		 */
		private $siunit;

		/**
		 * Class attribute for mapping table field otherSiUnit
		 * Comment for field otherSiUnit: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $othersiunit
		 */
		private $othersiunit;

		/**
		 * Class attribute for storing the SQL DDL of table unitofmeasurement
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGB1bml0b2ZtZWFzdXJlbWVudGAgKAogIGBpZHVuaXRPZk1lYXN1cmVtZW50YCBpbnQoMTEpIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULAogIGBzeW1ib2xgIHZhcmNoYXIoMTApIE5PVCBOVUxMLAogIGBuYW1lYCB2YXJjaGFyKDQ1KSBOT1QgTlVMTCwKICBgcXVhbnRpdHlgIHZhcmNoYXIoNDUpIE5PVCBOVUxMLAogIGBzaVVuaXRgIHZhcmNoYXIoNDUpIERFRkFVTFQgTlVMTCwKICBgb3RoZXJTaVVuaXRgIHZhcmNoYXIoNDUpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkdW5pdE9mTWVhc3VyZW1lbnRgKQopIEVOR0lORT1Jbm5vREIgQVVUT19JTkNSRU1FTlQ9MzMgREVGQVVMVCBDSEFSU0VUPXV0Zjg=";

		/**
		 * setIdunitofmeasurement Sets the class attribute idunitofmeasurement with a given value
		 * The attribute idunitofmeasurement maps the field idunitOfMeasurement defined as int(11).<br>
		 * Comment for field idunitOfMeasurement: Not specified.<br>
		 *
		 * @param int $idunitofmeasurement
		 *
		 * @category Modifier
		 */
		public function setIdunitofmeasurement($idunitofmeasurement){
			$this->idunitofmeasurement = (int)$idunitofmeasurement;
		}

		/**
		 * setSymbol Sets the class attribute symbol with a given value
		 * The attribute symbol maps the field symbol defined as varchar(10).<br>
		 * Comment for field symbol: Not specified.<br>
		 *
		 * @param string $symbol
		 *
		 * @category Modifier
		 */
		public function setSymbol($symbol){
			$this->symbol = (string)$symbol;
		}

		/**
		 * setName Sets the class attribute name with a given value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.<br>
		 *
		 * @param string $name
		 *
		 * @category Modifier
		 */
		public function setName($name){
			$this->name = (string)$name;
		}

		/**
		 * setQuantity Sets the class attribute quantity with a given value
		 * The attribute quantity maps the field quantity defined as varchar(45).<br>
		 * Comment for field quantity: Not specified.<br>
		 *
		 * @param string $quantity
		 *
		 * @category Modifier
		 */
		public function setQuantity($quantity){
			$this->quantity = (string)$quantity;
		}

		/**
		 * setSiunit Sets the class attribute siunit with a given value
		 * The attribute siunit maps the field siUnit defined as varchar(45).<br>
		 * Comment for field siUnit: Not specified.<br>
		 *
		 * @param string $siunit
		 *
		 * @category Modifier
		 */
		public function setSiunit($siunit){
			$this->siunit = (string)$siunit;
		}

		/**
		 * setOthersiunit Sets the class attribute othersiunit with a given value
		 * The attribute othersiunit maps the field otherSiUnit defined as varchar(45).<br>
		 * Comment for field otherSiUnit: Not specified.<br>
		 *
		 * @param string $othersiunit
		 *
		 * @category Modifier
		 */
		public function setOthersiunit($othersiunit){
			$this->othersiunit = (string)$othersiunit;
		}

		/**
		 * getIdunitofmeasurement gets the class attribute idunitofmeasurement value
		 * The attribute idunitofmeasurement maps the field idunitOfMeasurement defined as int(11).<br>
		 * Comment for field idunitOfMeasurement: Not specified.
		 * @return int $idunitofmeasurement
		 * @category Accessor of $idunitofmeasurement
		 */
		public function getIdunitofmeasurement(){
			return $this->idunitofmeasurement;
		}

		/**
		 * getSymbol gets the class attribute symbol value
		 * The attribute symbol maps the field symbol defined as varchar(10).<br>
		 * Comment for field symbol: Not specified.
		 * @return string $symbol
		 * @category Accessor of $symbol
		 */
		public function getSymbol(){
			return $this->symbol;
		}

		/**
		 * getName gets the class attribute name value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.
		 * @return string $name
		 * @category Accessor of $name
		 */
		public function getName(){
			return $this->name;
		}

		/**
		 * getQuantity gets the class attribute quantity value
		 * The attribute quantity maps the field quantity defined as varchar(45).<br>
		 * Comment for field quantity: Not specified.
		 * @return string $quantity
		 * @category Accessor of $quantity
		 */
		public function getQuantity(){
			return $this->quantity;
		}

		/**
		 * getSiunit gets the class attribute siunit value
		 * The attribute siunit maps the field siUnit defined as varchar(45).<br>
		 * Comment for field siUnit: Not specified.
		 * @return string $siunit
		 * @category Accessor of $siunit
		 */
		public function getSiunit(){
			return $this->siunit;
		}

		/**
		 * getOthersiunit gets the class attribute othersiunit value
		 * The attribute othersiunit maps the field otherSiUnit defined as varchar(45).<br>
		 * Comment for field otherSiUnit: Not specified.
		 * @return string $othersiunit
		 * @category Accessor of $othersiunit
		 */
		public function getOthersiunit(){
			return $this->othersiunit;
		}

		/**
		 * Gets DDL SQL code of the table unitofmeasurement
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "unitofmeasurement";
		}

		/**
		 * The BeanUnitofmeasurement constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idunitofmeasurement is given.
		 *  - with a fetched data row from the table unitofmeasurement having idunitOfMeasurement=$idunitofmeasurement
		 *
		 * @param int $idunitofmeasurement . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanUnitofmeasurement Object
		 */
		public function __construct($idunitofmeasurement = null){
			parent::__construct();
			if (!empty($idunitofmeasurement)){
				$this->select($idunitofmeasurement);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of unitofmeasurement into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idunitofmeasurement the primary key idunitOfMeasurement value of table unitofmeasurement which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idunitofmeasurement){
			$sql = "SELECT * FROM unitofmeasurement WHERE idunitOfMeasurement={$this->parseValue($idunitofmeasurement,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idunitofmeasurement = (integer)$rowObject->idunitOfMeasurement;
				@$this->symbol = $this->replaceAposBackSlash($rowObject->symbol);
				@$this->name = $this->replaceAposBackSlash($rowObject->name);
				@$this->quantity = $this->replaceAposBackSlash($rowObject->quantity);
				@$this->siunit = $this->replaceAposBackSlash($rowObject->siUnit);
				@$this->othersiunit = $this->replaceAposBackSlash($rowObject->otherSiUnit);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table unitofmeasurement
		 *
		 * @param int $idunitofmeasurement the primary key idunitOfMeasurement value of table unitofmeasurement which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idunitofmeasurement){
			$sql = "DELETE FROM unitofmeasurement WHERE idunitOfMeasurement={$this->parseValue($idunitofmeasurement,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of unitofmeasurement
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idunitofmeasurement = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO unitofmeasurement
            (symbol,name,quantity,siUnit,otherSiUnit)
            VALUES(
			{$this->parseValue($this->symbol, 'notNumber')},
			{$this->parseValue($this->name, 'notNumber')},
			{$this->parseValue($this->quantity, 'notNumber')},
			{$this->parseValue($this->siunit, 'notNumber')},
			{$this->parseValue($this->othersiunit, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idunitofmeasurement = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table unitofmeasurement with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idunitofmeasurement the primary key idunitOfMeasurement value of table unitofmeasurement which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idunitofmeasurement){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                unitofmeasurement
            SET 
				symbol={$this->parseValue($this->symbol, 'notNumber')},
				name={$this->parseValue($this->name, 'notNumber')},
				quantity={$this->parseValue($this->quantity, 'notNumber')},
				siUnit={$this->parseValue($this->siunit, 'notNumber')},
				otherSiUnit={$this->parseValue($this->othersiunit, 'notNumber')}
            WHERE
                idunitOfMeasurement={$this->parseValue($idunitofmeasurement, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idunitofmeasurement);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of unitofmeasurement previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idunitofmeasurement != ""){
				return $this->update($this->idunitofmeasurement);
			}else{
				return false;
			}
		}

	}

?>
