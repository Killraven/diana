<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanEvent extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key id of table event
		 * Comment for field id: Not specified<br>
		 * @var int $id
		 */
		private $id;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field datetime
		 * Comment for field datetime: Not specified.<br>
		 * Field information:
		 *  - Data type: datetime
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default: CURRENT_TIMESTAMP
		 *  - Extra:
		 * @var string $datetime
		 */
		private $datetime;

		/**
		 * Class attribute for mapping table field eventLevel_id
		 * Comment for field eventLevel_id: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $eventlevelId
		 */
		private $eventlevelId;

		/**
		 * Class attribute for mapping table field eventCategory_id
		 * Comment for field eventCategory_id: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $eventcategoryId
		 */
		private $eventcategoryId;

		/**
		 * Class attribute for mapping table field eventType_ideventType
		 * Comment for field eventType_ideventType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $eventtypeIdeventtype
		 */
		private $eventtypeIdeventtype;

		/**
		 * Class attribute for mapping table field Device_idDevice
		 * Comment for field Device_idDevice: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $deviceIddevice
		 */
		private $deviceIddevice;

		/**
		 * Class attribute for mapping table field Device_DeviceStatus_idDeviceStatus
		 * Comment for field Device_DeviceStatus_idDeviceStatus: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $deviceDevicestatusIddevicestatus
		 */
		private $deviceDevicestatusIddevicestatus;

		/**
		 * Class attribute for mapping table field Device_DeviceType_idDeviceType
		 * Comment for field Device_DeviceType_idDeviceType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $deviceDevicetypeIddevicetype
		 */
		private $deviceDevicetypeIddevicetype;

		/**
		 * Class attribute for mapping table field users_idusers
		 * Comment for field users_idusers: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $usersIdusers
		 */
		private $usersIdusers;

		/**
		 * Class attribute for storing the SQL DDL of table event
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBldmVudGAgKAogIGBpZGAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgZGF0ZXRpbWVgIGRhdGV0aW1lIERFRkFVTFQgQ1VSUkVOVF9USU1FU1RBTVAsCiAgYGV2ZW50TGV2ZWxfaWRgIGludCgxMSkgTk9UIE5VTEwsCiAgYGV2ZW50Q2F0ZWdvcnlfaWRgIGludCgxMSkgTk9UIE5VTEwsCiAgYGV2ZW50VHlwZV9pZGV2ZW50VHlwZWAgaW50KDExKSBOT1QgTlVMTCwKICBgRGV2aWNlX2lkRGV2aWNlYCBpbnQoMTEpIE5PVCBOVUxMLAogIGBEZXZpY2VfRGV2aWNlU3RhdHVzX2lkRGV2aWNlU3RhdHVzYCBpbnQoMTEpIERFRkFVTFQgTlVMTCwKICBgRGV2aWNlX0RldmljZVR5cGVfaWREZXZpY2VUeXBlYCBpbnQoMTEpIERFRkFVTFQgTlVMTCwKICBgdXNlcnNfaWR1c2Vyc2AgaW50KDExKSBOT1QgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkYCksCiAgS0VZIGBma19ldmVudF9ldmVudExldmVsMV9pZHhgIChgZXZlbnRMZXZlbF9pZGApLAogIEtFWSBgZmtfZXZlbnRfZXZlbnRDYXRlZ29yeTFfaWR4YCAoYGV2ZW50Q2F0ZWdvcnlfaWRgKSwKICBLRVkgYGZrX2V2ZW50X2V2ZW50VHlwZTFfaWR4YCAoYGV2ZW50VHlwZV9pZGV2ZW50VHlwZWApLAogIEtFWSBgZmtfZXZlbnRfRGV2aWNlMV9pZHhgIChgRGV2aWNlX2lkRGV2aWNlYCksCiAgS0VZIGBma19ldmVudF91c2VyczFfaWR4YCAoYHVzZXJzX2lkdXNlcnNgKSwKICBDT05TVFJBSU5UIGBma19ldmVudF9EZXZpY2UxYCBGT1JFSUdOIEtFWSAoYERldmljZV9pZERldmljZWApIFJFRkVSRU5DRVMgYGRldmljZWAgKGBpZERldmljZWApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19ldmVudF9ldmVudENhdGVnb3J5MWAgRk9SRUlHTiBLRVkgKGBldmVudENhdGVnb3J5X2lkYCkgUkVGRVJFTkNFUyBgZXZlbnRjYXRlZ29yeWAgKGBpZGApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19ldmVudF9ldmVudExldmVsMWAgRk9SRUlHTiBLRVkgKGBldmVudExldmVsX2lkYCkgUkVGRVJFTkNFUyBgZXZlbnRsZXZlbGAgKGBpZGApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19ldmVudF9ldmVudFR5cGUxYCBGT1JFSUdOIEtFWSAoYGV2ZW50VHlwZV9pZGV2ZW50VHlwZWApIFJFRkVSRU5DRVMgYGV2ZW50dHlwZWAgKGBpZGV2ZW50VHlwZWApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19ldmVudF91c2VyczFgIEZPUkVJR04gS0VZIChgdXNlcnNfaWR1c2Vyc2ApIFJFRkVSRU5DRVMgYHVzZXJzYCAoYGlkdXNlcnNgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04KKSBFTkdJTkU9SW5ub0RCIEFVVE9fSU5DUkVNRU5UPTE1IERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setId Sets the class attribute id with a given value
		 * The attribute id maps the field id defined as int(11).<br>
		 * Comment for field id: Not specified.<br>
		 *
		 * @param int $id
		 *
		 * @category Modifier
		 */
		public function setId($id){
			$this->id = (int)$id;
		}

		/**
		 * setDatetime Sets the class attribute datetime with a given value
		 * The attribute datetime maps the field datetime defined as datetime.<br>
		 * Comment for field datetime: Not specified.<br>
		 *
		 * @param string $datetime
		 *
		 * @category Modifier
		 */
		public function setDatetime($datetime){
			$this->datetime = (string)$datetime;
		}

		/**
		 * setEventlevelId Sets the class attribute eventlevelId with a given value
		 * The attribute eventlevelId maps the field eventLevel_id defined as int(11).<br>
		 * Comment for field eventLevel_id: Not specified.<br>
		 *
		 * @param int $eventlevelId
		 *
		 * @category Modifier
		 */
		public function setEventlevelId($eventlevelId){
			$this->eventlevelId = (int)$eventlevelId;
		}

		/**
		 * setEventcategoryId Sets the class attribute eventcategoryId with a given value
		 * The attribute eventcategoryId maps the field eventCategory_id defined as int(11).<br>
		 * Comment for field eventCategory_id: Not specified.<br>
		 *
		 * @param int $eventcategoryId
		 *
		 * @category Modifier
		 */
		public function setEventcategoryId($eventcategoryId){
			$this->eventcategoryId = (int)$eventcategoryId;
		}

		/**
		 * setEventtypeIdeventtype Sets the class attribute eventtypeIdeventtype with a given value
		 * The attribute eventtypeIdeventtype maps the field eventType_ideventType defined as int(11).<br>
		 * Comment for field eventType_ideventType: Not specified.<br>
		 *
		 * @param int $eventtypeIdeventtype
		 *
		 * @category Modifier
		 */
		public function setEventtypeIdeventtype($eventtypeIdeventtype){
			$this->eventtypeIdeventtype = (int)$eventtypeIdeventtype;
		}

		/**
		 * setDeviceIddevice Sets the class attribute deviceIddevice with a given value
		 * The attribute deviceIddevice maps the field Device_idDevice defined as int(11).<br>
		 * Comment for field Device_idDevice: Not specified.<br>
		 *
		 * @param int $deviceIddevice
		 *
		 * @category Modifier
		 */
		public function setDeviceIddevice($deviceIddevice){
			$this->deviceIddevice = (int)$deviceIddevice;
		}

		/**
		 * setDeviceDevicestatusIddevicestatus Sets the class attribute deviceDevicestatusIddevicestatus with a given value
		 * The attribute deviceDevicestatusIddevicestatus maps the field Device_DeviceStatus_idDeviceStatus defined as int(11).<br>
		 * Comment for field Device_DeviceStatus_idDeviceStatus: Not specified.<br>
		 *
		 * @param int $deviceDevicestatusIddevicestatus
		 *
		 * @category Modifier
		 */
		public function setDeviceDevicestatusIddevicestatus($deviceDevicestatusIddevicestatus){
			$this->deviceDevicestatusIddevicestatus = (int)$deviceDevicestatusIddevicestatus;
		}

		/**
		 * setDeviceDevicetypeIddevicetype Sets the class attribute deviceDevicetypeIddevicetype with a given value
		 * The attribute deviceDevicetypeIddevicetype maps the field Device_DeviceType_idDeviceType defined as int(11).<br>
		 * Comment for field Device_DeviceType_idDeviceType: Not specified.<br>
		 *
		 * @param int $deviceDevicetypeIddevicetype
		 *
		 * @category Modifier
		 */
		public function setDeviceDevicetypeIddevicetype($deviceDevicetypeIddevicetype){
			$this->deviceDevicetypeIddevicetype = (int)$deviceDevicetypeIddevicetype;
		}

		/**
		 * setUsersIdusers Sets the class attribute usersIdusers with a given value
		 * The attribute usersIdusers maps the field users_idusers defined as int(11).<br>
		 * Comment for field users_idusers: Not specified.<br>
		 *
		 * @param int $usersIdusers
		 *
		 * @category Modifier
		 */
		public function setUsersIdusers($usersIdusers){
			$this->usersIdusers = (int)$usersIdusers;
		}

		/**
		 * getId gets the class attribute id value
		 * The attribute id maps the field id defined as int(11).<br>
		 * Comment for field id: Not specified.
		 * @return int $id
		 * @category Accessor of $id
		 */
		public function getId(){
			return $this->id;
		}

		/**
		 * getDatetime gets the class attribute datetime value
		 * The attribute datetime maps the field datetime defined as datetime.<br>
		 * Comment for field datetime: Not specified.
		 * @return string $datetime
		 * @category Accessor of $datetime
		 */
		public function getDatetime(){
			return $this->datetime;
		}

		/**
		 * getEventlevelId gets the class attribute eventlevelId value
		 * The attribute eventlevelId maps the field eventLevel_id defined as int(11).<br>
		 * Comment for field eventLevel_id: Not specified.
		 * @return int $eventlevelId
		 * @category Accessor of $eventlevelId
		 */
		public function getEventlevelId(){
			return $this->eventlevelId;
		}

		/**
		 * getEventcategoryId gets the class attribute eventcategoryId value
		 * The attribute eventcategoryId maps the field eventCategory_id defined as int(11).<br>
		 * Comment for field eventCategory_id: Not specified.
		 * @return int $eventcategoryId
		 * @category Accessor of $eventcategoryId
		 */
		public function getEventcategoryId(){
			return $this->eventcategoryId;
		}

		/**
		 * getEventtypeIdeventtype gets the class attribute eventtypeIdeventtype value
		 * The attribute eventtypeIdeventtype maps the field eventType_ideventType defined as int(11).<br>
		 * Comment for field eventType_ideventType: Not specified.
		 * @return int $eventtypeIdeventtype
		 * @category Accessor of $eventtypeIdeventtype
		 */
		public function getEventtypeIdeventtype(){
			return $this->eventtypeIdeventtype;
		}

		/**
		 * getDeviceIddevice gets the class attribute deviceIddevice value
		 * The attribute deviceIddevice maps the field Device_idDevice defined as int(11).<br>
		 * Comment for field Device_idDevice: Not specified.
		 * @return int $deviceIddevice
		 * @category Accessor of $deviceIddevice
		 */
		public function getDeviceIddevice(){
			return $this->deviceIddevice;
		}

		/**
		 * getDeviceDevicestatusIddevicestatus gets the class attribute deviceDevicestatusIddevicestatus value
		 * The attribute deviceDevicestatusIddevicestatus maps the field Device_DeviceStatus_idDeviceStatus defined as int(11).<br>
		 * Comment for field Device_DeviceStatus_idDeviceStatus: Not specified.
		 * @return int $deviceDevicestatusIddevicestatus
		 * @category Accessor of $deviceDevicestatusIddevicestatus
		 */
		public function getDeviceDevicestatusIddevicestatus(){
			return $this->deviceDevicestatusIddevicestatus;
		}

		/**
		 * getDeviceDevicetypeIddevicetype gets the class attribute deviceDevicetypeIddevicetype value
		 * The attribute deviceDevicetypeIddevicetype maps the field Device_DeviceType_idDeviceType defined as int(11).<br>
		 * Comment for field Device_DeviceType_idDeviceType: Not specified.
		 * @return int $deviceDevicetypeIddevicetype
		 * @category Accessor of $deviceDevicetypeIddevicetype
		 */
		public function getDeviceDevicetypeIddevicetype(){
			return $this->deviceDevicetypeIddevicetype;
		}

		/**
		 * getUsersIdusers gets the class attribute usersIdusers value
		 * The attribute usersIdusers maps the field users_idusers defined as int(11).<br>
		 * Comment for field users_idusers: Not specified.
		 * @return int $usersIdusers
		 * @category Accessor of $usersIdusers
		 */
		public function getUsersIdusers(){
			return $this->usersIdusers;
		}

		/**
		 * Gets DDL SQL code of the table event
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "event";
		}

		/**
		 * The BeanEvent constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $id is given.
		 *  - with a fetched data row from the table event having id=$id
		 *
		 * @param int $id . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanEvent Object
		 */
		public function __construct($id = null){
			parent::__construct();
			if (!empty($id)){
				$this->select($id);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of event into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $id the primary key id value of table event which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($id){
			$sql = "SELECT * FROM event WHERE id={$this->parseValue($id,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->id = (integer)$rowObject->id;
				@$this->datetime = empty($rowObject->datetime) ? null : date(FETCHED_DATETIME_FORMAT, strtotime($rowObject->datetime));
				@$this->eventlevelId = (integer)$rowObject->eventLevel_id;
				@$this->eventcategoryId = (integer)$rowObject->eventCategory_id;
				@$this->eventtypeIdeventtype = (integer)$rowObject->eventType_ideventType;
				@$this->deviceIddevice = (integer)$rowObject->Device_idDevice;
				@$this->deviceDevicestatusIddevicestatus = (integer)$rowObject->Device_DeviceStatus_idDeviceStatus;
				@$this->deviceDevicetypeIddevicetype = (integer)$rowObject->Device_DeviceType_idDeviceType;
				@$this->usersIdusers = (integer)$rowObject->users_idusers;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table event
		 *
		 * @param int $id the primary key id value of table event which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($id){
			$sql = "DELETE FROM event WHERE id={$this->parseValue($id,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of event
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->id = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO event
            (datetime,eventLevel_id,eventCategory_id,eventType_ideventType,Device_idDevice,Device_DeviceStatus_idDeviceStatus,Device_DeviceType_idDeviceType,users_idusers)
            VALUES(
			{$this->parseValue($this->datetime, 'datetime')},
			{$this->parseValue($this->eventlevelId)},
			{$this->parseValue($this->eventcategoryId)},
			{$this->parseValue($this->eventtypeIdeventtype)},
			{$this->parseValue($this->deviceIddevice)},
			{$this->parseValue($this->deviceDevicestatusIddevicestatus)},
			{$this->parseValue($this->deviceDevicetypeIddevicetype)},
			{$this->parseValue($this->usersIdusers)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->id = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table event with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $id the primary key id value of table event which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($id){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                event
            SET 
				datetime={$this->parseValue($this->datetime, 'datetime')},
				eventLevel_id={$this->parseValue($this->eventlevelId)},
				eventCategory_id={$this->parseValue($this->eventcategoryId)},
				eventType_ideventType={$this->parseValue($this->eventtypeIdeventtype)},
				Device_idDevice={$this->parseValue($this->deviceIddevice)},
				Device_DeviceStatus_idDeviceStatus={$this->parseValue($this->deviceDevicestatusIddevicestatus)},
				Device_DeviceType_idDeviceType={$this->parseValue($this->deviceDevicetypeIddevicetype)},
				users_idusers={$this->parseValue($this->usersIdusers)}
            WHERE
                id={$this->parseValue($id, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($id);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of event previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->id != ""){
				return $this->update($this->id);
			}else{
				return false;
			}
		}

	}

?>
