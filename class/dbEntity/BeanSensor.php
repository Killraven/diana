<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanSensor extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idsensor of table sensor
		 * Comment for field idsensor: Not specified<br>
		 * @var int $idsensor
		 */
		private $idsensor;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field desc
		 * Comment for field desc: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(255)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $desc
		 */
		private $desc;

		/**
		 * Class attribute for mapping table field location
		 * Comment for field location: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(255)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $location
		 */
		private $location;

		/**
		 * Class attribute for mapping table field sensorType_idsensorType
		 * Comment for field sensorType_idsensorType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $sensortypeIdsensortype
		 */
		private $sensortypeIdsensortype;

		/**
		 * Class attribute for mapping table field unitofmeasurement_idunitOfMeasurement
		 * Comment for field unitofmeasurement_idunitOfMeasurement: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $unitofmeasurementIdunitofmeasurement
		 */
		private $unitofmeasurementIdunitofmeasurement;

		/**
		 * Class attribute for mapping table field unitofmeasurement_idunitOfMeasurement1
		 * Comment for field unitofmeasurement_idunitOfMeasurement1: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $unitofmeasurementIdunitofmeasurement1
		 */
		private $unitofmeasurementIdunitofmeasurement1;

		/**
		 * Class attribute for storing the SQL DDL of table sensor
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBzZW5zb3JgICgKICBgaWRzZW5zb3JgIGludCgxMSkgTk9UIE5VTEwgQVVUT19JTkNSRU1FTlQsCiAgYGRlc2NgIHZhcmNoYXIoMjU1KSBERUZBVUxUIE5VTEwsCiAgYGxvY2F0aW9uYCB2YXJjaGFyKDI1NSkgREVGQVVMVCBOVUxMLAogIGBzZW5zb3JUeXBlX2lkc2Vuc29yVHlwZWAgaW50KDExKSBOT1QgTlVMTCwKICBgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudGAgaW50KDExKSBOT1QgTlVMTCwKICBgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudDFgIGludCgxMSkgTk9UIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZHNlbnNvcmApLAogIEtFWSBgZmtfc2Vuc29yX3NlbnNvclR5cGUxX2lkeGAgKGBzZW5zb3JUeXBlX2lkc2Vuc29yVHlwZWApLAogIEtFWSBgZmtfc2Vuc29yX3VuaXRvZm1lYXN1cmVtZW50MV9pZHhgIChgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudGApLAogIEtFWSBgZmtfc2Vuc29yX3VuaXRvZm1lYXN1cmVtZW50Ml9pZHhgIChgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudDFgKSwKICBDT05TVFJBSU5UIGBma19zZW5zb3Jfc2Vuc29yVHlwZTFgIEZPUkVJR04gS0VZIChgc2Vuc29yVHlwZV9pZHNlbnNvclR5cGVgKSBSRUZFUkVOQ0VTIGBzZW5zb3J0eXBlYCAoYGlkc2Vuc29yVHlwZWApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19zZW5zb3JfdW5pdG9mbWVhc3VyZW1lbnQxYCBGT1JFSUdOIEtFWSAoYHVuaXRvZm1lYXN1cmVtZW50X2lkdW5pdE9mTWVhc3VyZW1lbnRgKSBSRUZFUkVOQ0VTIGB1bml0b2ZtZWFzdXJlbWVudGAgKGBpZHVuaXRPZk1lYXN1cmVtZW50YCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OLAogIENPTlNUUkFJTlQgYGZrX3NlbnNvcl91bml0b2ZtZWFzdXJlbWVudDJgIEZPUkVJR04gS0VZIChgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudDFgKSBSRUZFUkVOQ0VTIGB1bml0b2ZtZWFzdXJlbWVudGAgKGBpZHVuaXRPZk1lYXN1cmVtZW50YCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OCikgRU5HSU5FPUlubm9EQiBBVVRPX0lOQ1JFTUVOVD0yNCBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIdsensor Sets the class attribute idsensor with a given value
		 * The attribute idsensor maps the field idsensor defined as int(11).<br>
		 * Comment for field idsensor: Not specified.<br>
		 *
		 * @param int $idsensor
		 *
		 * @category Modifier
		 */
		public function setIdsensor($idsensor){
			$this->idsensor = (int)$idsensor;
		}

		/**
		 * setDesc Sets the class attribute desc with a given value
		 * The attribute desc maps the field desc defined as varchar(255).<br>
		 * Comment for field desc: Not specified.<br>
		 *
		 * @param string $desc
		 *
		 * @category Modifier
		 */
		public function setDesc($desc){
			$this->desc = (string)$desc;
		}

		/**
		 * setLocation Sets the class attribute location with a given value
		 * The attribute location maps the field location defined as varchar(255).<br>
		 * Comment for field location: Not specified.<br>
		 *
		 * @param string $location
		 *
		 * @category Modifier
		 */
		public function setLocation($location){
			$this->location = (string)$location;
		}

		/**
		 * setSensortypeIdsensortype Sets the class attribute sensortypeIdsensortype with a given value
		 * The attribute sensortypeIdsensortype maps the field sensorType_idsensorType defined as int(11).<br>
		 * Comment for field sensorType_idsensorType: Not specified.<br>
		 *
		 * @param int $sensortypeIdsensortype
		 *
		 * @category Modifier
		 */
		public function setSensortypeIdsensortype($sensortypeIdsensortype){
			$this->sensortypeIdsensortype = (int)$sensortypeIdsensortype;
		}

		/**
		 * setUnitofmeasurementIdunitofmeasurement Sets the class attribute unitofmeasurementIdunitofmeasurement with a given value
		 * The attribute unitofmeasurementIdunitofmeasurement maps the field unitofmeasurement_idunitOfMeasurement defined as int(11).<br>
		 * Comment for field unitofmeasurement_idunitOfMeasurement: Not specified.<br>
		 *
		 * @param int $unitofmeasurementIdunitofmeasurement
		 *
		 * @category Modifier
		 */
		public function setUnitofmeasurementIdunitofmeasurement($unitofmeasurementIdunitofmeasurement){
			$this->unitofmeasurementIdunitofmeasurement = (int)$unitofmeasurementIdunitofmeasurement;
		}

		/**
		 * setUnitofmeasurementIdunitofmeasurement1 Sets the class attribute unitofmeasurementIdunitofmeasurement1 with a given value
		 * The attribute unitofmeasurementIdunitofmeasurement1 maps the field unitofmeasurement_idunitOfMeasurement1 defined as int(11).<br>
		 * Comment for field unitofmeasurement_idunitOfMeasurement1: Not specified.<br>
		 *
		 * @param int $unitofmeasurementIdunitofmeasurement1
		 *
		 * @category Modifier
		 */
		public function setUnitofmeasurementIdunitofmeasurement1($unitofmeasurementIdunitofmeasurement1){
			$this->unitofmeasurementIdunitofmeasurement1 = (int)$unitofmeasurementIdunitofmeasurement1;
		}

		/**
		 * getIdsensor gets the class attribute idsensor value
		 * The attribute idsensor maps the field idsensor defined as int(11).<br>
		 * Comment for field idsensor: Not specified.
		 * @return int $idsensor
		 * @category Accessor of $idsensor
		 */
		public function getIdsensor(){
			return $this->idsensor;
		}

		/**
		 * getDesc gets the class attribute desc value
		 * The attribute desc maps the field desc defined as varchar(255).<br>
		 * Comment for field desc: Not specified.
		 * @return string $desc
		 * @category Accessor of $desc
		 */
		public function getDesc(){
			return $this->desc;
		}

		/**
		 * getLocation gets the class attribute location value
		 * The attribute location maps the field location defined as varchar(255).<br>
		 * Comment for field location: Not specified.
		 * @return string $location
		 * @category Accessor of $location
		 */
		public function getLocation(){
			return $this->location;
		}

		/**
		 * getSensortypeIdsensortype gets the class attribute sensortypeIdsensortype value
		 * The attribute sensortypeIdsensortype maps the field sensorType_idsensorType defined as int(11).<br>
		 * Comment for field sensorType_idsensorType: Not specified.
		 * @return int $sensortypeIdsensortype
		 * @category Accessor of $sensortypeIdsensortype
		 */
		public function getSensortypeIdsensortype(){
			return $this->sensortypeIdsensortype;
		}

		/**
		 * getUnitofmeasurementIdunitofmeasurement gets the class attribute unitofmeasurementIdunitofmeasurement value
		 * The attribute unitofmeasurementIdunitofmeasurement maps the field unitofmeasurement_idunitOfMeasurement defined as int(11).<br>
		 * Comment for field unitofmeasurement_idunitOfMeasurement: Not specified.
		 * @return int $unitofmeasurementIdunitofmeasurement
		 * @category Accessor of $unitofmeasurementIdunitofmeasurement
		 */
		public function getUnitofmeasurementIdunitofmeasurement(){
			return $this->unitofmeasurementIdunitofmeasurement;
		}

		/**
		 * getUnitofmeasurementIdunitofmeasurement1 gets the class attribute unitofmeasurementIdunitofmeasurement1 value
		 * The attribute unitofmeasurementIdunitofmeasurement1 maps the field unitofmeasurement_idunitOfMeasurement1 defined as int(11).<br>
		 * Comment for field unitofmeasurement_idunitOfMeasurement1: Not specified.
		 * @return int $unitofmeasurementIdunitofmeasurement1
		 * @category Accessor of $unitofmeasurementIdunitofmeasurement1
		 */
		public function getUnitofmeasurementIdunitofmeasurement1(){
			return $this->unitofmeasurementIdunitofmeasurement1;
		}

		/**
		 * Gets DDL SQL code of the table sensor
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "sensor";
		}

		/**
		 * The BeanSensor constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idsensor is given.
		 *  - with a fetched data row from the table sensor having idsensor=$idsensor
		 *
		 * @param int $idsensor . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanSensor Object
		 */
		public function __construct($idsensor = null){
			parent::__construct();
			if (!empty($idsensor)){
				$this->select($idsensor);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of sensor into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idsensor the primary key idsensor value of table sensor which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idsensor){
			$sql = "SELECT * FROM sensor WHERE idsensor={$this->parseValue($idsensor,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idsensor = (integer)$rowObject->idsensor;
				@$this->desc = $this->replaceAposBackSlash($rowObject->desc);
				@$this->location = $this->replaceAposBackSlash($rowObject->location);
				@$this->sensortypeIdsensortype = (integer)$rowObject->sensorType_idsensorType;
				@$this->unitofmeasurementIdunitofmeasurement = (integer)$rowObject->unitofmeasurement_idunitOfMeasurement;
				@$this->unitofmeasurementIdunitofmeasurement1 = (integer)$rowObject->unitofmeasurement_idunitOfMeasurement1;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table sensor
		 *
		 * @param int $idsensor the primary key idsensor value of table sensor which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idsensor){
			$sql = "DELETE FROM sensor WHERE idsensor={$this->parseValue($idsensor,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of sensor
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idsensor = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO sensor
            (desc,location,sensorType_idsensorType,unitofmeasurement_idunitOfMeasurement,unitofmeasurement_idunitOfMeasurement1)
            VALUES(
			{$this->parseValue($this->desc, 'notNumber')},
			{$this->parseValue($this->location, 'notNumber')},
			{$this->parseValue($this->sensortypeIdsensortype)},
			{$this->parseValue($this->unitofmeasurementIdunitofmeasurement)},
			{$this->parseValue($this->unitofmeasurementIdunitofmeasurement1)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idsensor = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table sensor with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idsensor the primary key idsensor value of table sensor which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idsensor){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                sensor
            SET 
				desc={$this->parseValue($this->desc, 'notNumber')},
				location={$this->parseValue($this->location, 'notNumber')},
				sensorType_idsensorType={$this->parseValue($this->sensortypeIdsensortype)},
				unitofmeasurement_idunitOfMeasurement={$this->parseValue($this->unitofmeasurementIdunitofmeasurement)},
				unitofmeasurement_idunitOfMeasurement1={$this->parseValue($this->unitofmeasurementIdunitofmeasurement1)}
            WHERE
                idsensor={$this->parseValue($idsensor, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idsensor);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of sensor previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idsensor != ""){
				return $this->update($this->idsensor);
			}else{
				return false;
			}
		}

	}

?>
