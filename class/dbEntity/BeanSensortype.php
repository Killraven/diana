<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanSensortype extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idsensorType of table sensortype
		 * Comment for field idsensorType: Not specified<br>
		 * @var int $idsensortype
		 */
		private $idsensortype;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field model
		 * Comment for field model: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $model
		 */
		private $model;

		/**
		 * Class attribute for mapping table field desc
		 * Comment for field desc: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(255)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $desc
		 */
		private $desc;

		/**
		 * Class attribute for storing the SQL DDL of table sensortype
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBzZW5zb3J0eXBlYCAoCiAgYGlkc2Vuc29yVHlwZWAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgbW9kZWxgIHZhcmNoYXIoNDUpIERFRkFVTFQgTlVMTCwKICBgZGVzY2AgdmFyY2hhcigyNTUpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkc2Vuc29yVHlwZWApCikgRU5HSU5FPUlubm9EQiBBVVRPX0lOQ1JFTUVOVD0yMyBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIdsensortype Sets the class attribute idsensortype with a given value
		 * The attribute idsensortype maps the field idsensorType defined as int(11).<br>
		 * Comment for field idsensorType: Not specified.<br>
		 *
		 * @param int $idsensortype
		 *
		 * @category Modifier
		 */
		public function setIdsensortype($idsensortype){
			$this->idsensortype = (int)$idsensortype;
		}

		/**
		 * setModel Sets the class attribute model with a given value
		 * The attribute model maps the field model defined as varchar(45).<br>
		 * Comment for field model: Not specified.<br>
		 *
		 * @param string $model
		 *
		 * @category Modifier
		 */
		public function setModel($model){
			$this->model = (string)$model;
		}

		/**
		 * setDesc Sets the class attribute desc with a given value
		 * The attribute desc maps the field desc defined as varchar(255).<br>
		 * Comment for field desc: Not specified.<br>
		 *
		 * @param string $desc
		 *
		 * @category Modifier
		 */
		public function setDesc($desc){
			$this->desc = (string)$desc;
		}

		/**
		 * getIdsensortype gets the class attribute idsensortype value
		 * The attribute idsensortype maps the field idsensorType defined as int(11).<br>
		 * Comment for field idsensorType: Not specified.
		 * @return int $idsensortype
		 * @category Accessor of $idsensortype
		 */
		public function getIdsensortype(){
			return $this->idsensortype;
		}

		/**
		 * getModel gets the class attribute model value
		 * The attribute model maps the field model defined as varchar(45).<br>
		 * Comment for field model: Not specified.
		 * @return string $model
		 * @category Accessor of $model
		 */
		public function getModel(){
			return $this->model;
		}

		/**
		 * getDesc gets the class attribute desc value
		 * The attribute desc maps the field desc defined as varchar(255).<br>
		 * Comment for field desc: Not specified.
		 * @return string $desc
		 * @category Accessor of $desc
		 */
		public function getDesc(){
			return $this->desc;
		}

		/**
		 * Gets DDL SQL code of the table sensortype
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "sensortype";
		}

		/**
		 * The BeanSensortype constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idsensortype is given.
		 *  - with a fetched data row from the table sensortype having idsensorType=$idsensortype
		 *
		 * @param int $idsensortype . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanSensortype Object
		 */
		public function __construct($idsensortype = null){
			parent::__construct();
			if (!empty($idsensortype)){
				$this->select($idsensortype);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of sensortype into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idsensortype the primary key idsensorType value of table sensortype which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idsensortype){
			$sql = "SELECT * FROM sensortype WHERE idsensorType={$this->parseValue($idsensortype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idsensortype = (integer)$rowObject->idsensorType;
				@$this->model = $this->replaceAposBackSlash($rowObject->model);
				@$this->desc = $this->replaceAposBackSlash($rowObject->desc);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table sensortype
		 *
		 * @param int $idsensortype the primary key idsensorType value of table sensortype which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idsensortype){
			$sql = "DELETE FROM sensortype WHERE idsensorType={$this->parseValue($idsensortype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of sensortype
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idsensortype = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO sensortype
            (model,desc)
            VALUES(
			{$this->parseValue($this->model, 'notNumber')},
			{$this->parseValue($this->desc, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idsensortype = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table sensortype with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idsensortype the primary key idsensorType value of table sensortype which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idsensortype){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                sensortype
            SET 
				model={$this->parseValue($this->model, 'notNumber')},
				desc={$this->parseValue($this->desc, 'notNumber')}
            WHERE
                idsensorType={$this->parseValue($idsensortype, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idsensortype);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of sensortype previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idsensortype != ""){
				return $this->update($this->idsensortype);
			}else{
				return false;
			}
		}

	}

?>
