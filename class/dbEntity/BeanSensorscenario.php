<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanSensorscenario extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idSensorScenario of table sensorscenario
		 * Comment for field idSensorScenario: Not specified<br>
		 * @var int $idsensorscenario
		 */
		private $idsensorscenario;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field name
		 * Comment for field name: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $name
		 */
		private $name;

		/**
		 * Class attribute for mapping table field isSceneActive
		 * Comment for field isSceneActive: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $issceneactive
		 */
		private $issceneactive;

		/**
		 * Class attribute for mapping table field Scene_idScene
		 * Comment for field Scene_idScene: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : YES
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $sceneIdscene
		 */
		private $sceneIdscene;

		/**
		 * Class attribute for mapping table field isCommandActive
		 * Comment for field isCommandActive: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $iscommandactive
		 */
		private $iscommandactive;

		/**
		 * Class attribute for mapping table field command_idCommand
		 * Comment for field command_idCommand: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : YES
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $commandIdcommand
		 */
		private $commandIdcommand;

		/**
		 * Class attribute for mapping table field isTimeControlActive
		 * Comment for field isTimeControlActive: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $istimecontrolactive
		 */
		private $istimecontrolactive;

		/**
		 * Class attribute for mapping table field timecontrols_idtimeControls
		 * Comment for field timecontrols_idtimeControls: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $timecontrolsIdtimecontrols
		 */
		private $timecontrolsIdtimecontrols;

		/**
		 * Class attribute for storing the SQL DDL of table sensorscenario
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBzZW5zb3JzY2VuYXJpb2AgKAogIGBpZFNlbnNvclNjZW5hcmlvYCBpbnQoMTEpIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULAogIGBuYW1lYCB2YXJjaGFyKDQ1KSBOT1QgTlVMTCwKICBgaXNTY2VuZUFjdGl2ZWAgdGlueWludCg0KSBERUZBVUxUIE5VTEwsCiAgYFNjZW5lX2lkU2NlbmVgIGludCgxMSkgREVGQVVMVCBOVUxMLAogIGBpc0NvbW1hbmRBY3RpdmVgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGBjb21tYW5kX2lkQ29tbWFuZGAgaW50KDExKSBERUZBVUxUIE5VTEwsCiAgYGlzVGltZUNvbnRyb2xBY3RpdmVgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGB0aW1lY29udHJvbHNfaWR0aW1lQ29udHJvbHNgIGludCgxMSkgTk9UIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZFNlbnNvclNjZW5hcmlvYCksCiAgS0VZIGBma19TZW5zb3JTY2VuYXJpb19TY2VuZTFfaWR4YCAoYFNjZW5lX2lkU2NlbmVgKSwKICBLRVkgYGZrX3NlbnNvcnNjZW5hcmlvX2NvbW1hbmQxX2lkeGAgKGBjb21tYW5kX2lkQ29tbWFuZGApLAogIEtFWSBgZmtfc2Vuc29yc2NlbmFyaW9fdGltZWNvbnRyb2xzMV9pZHhgIChgdGltZWNvbnRyb2xzX2lkdGltZUNvbnRyb2xzYCksCiAgQ09OU1RSQUlOVCBgZmtfU2Vuc29yU2NlbmFyaW9fU2NlbmUxYCBGT1JFSUdOIEtFWSAoYFNjZW5lX2lkU2NlbmVgKSBSRUZFUkVOQ0VTIGBzY2VuZWAgKGBpZFNjZW5lYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OLAogIENPTlNUUkFJTlQgYGZrX3NlbnNvcnNjZW5hcmlvX2NvbW1hbmQxYCBGT1JFSUdOIEtFWSAoYGNvbW1hbmRfaWRDb21tYW5kYCkgUkVGRVJFTkNFUyBgY29tbWFuZGAgKGBpZENvbW1hbmRgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04sCiAgQ09OU1RSQUlOVCBgZmtfc2Vuc29yc2NlbmFyaW9fdGltZWNvbnRyb2xzMWAgRk9SRUlHTiBLRVkgKGB0aW1lY29udHJvbHNfaWR0aW1lQ29udHJvbHNgKSBSRUZFUkVOQ0VTIGB0aW1lY29udHJvbHNgIChgaWR0aW1lQ29udHJvbHNgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04KKSBFTkdJTkU9SW5ub0RCIERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setIdsensorscenario Sets the class attribute idsensorscenario with a given value
		 * The attribute idsensorscenario maps the field idSensorScenario defined as int(11).<br>
		 * Comment for field idSensorScenario: Not specified.<br>
		 *
		 * @param int $idsensorscenario
		 *
		 * @category Modifier
		 */
		public function setIdsensorscenario($idsensorscenario){
			$this->idsensorscenario = (int)$idsensorscenario;
		}

		/**
		 * setName Sets the class attribute name with a given value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.<br>
		 *
		 * @param string $name
		 *
		 * @category Modifier
		 */
		public function setName($name){
			$this->name = (string)$name;
		}

		/**
		 * setIssceneactive Sets the class attribute issceneactive with a given value
		 * The attribute issceneactive maps the field isSceneActive defined as tinyint(4).<br>
		 * Comment for field isSceneActive: Not specified.<br>
		 *
		 * @param int $issceneactive
		 *
		 * @category Modifier
		 */
		public function setIssceneactive($issceneactive){
			$this->issceneactive = (int)$issceneactive;
		}

		/**
		 * setSceneIdscene Sets the class attribute sceneIdscene with a given value
		 * The attribute sceneIdscene maps the field Scene_idScene defined as int(11).<br>
		 * Comment for field Scene_idScene: Not specified.<br>
		 *
		 * @param int $sceneIdscene
		 *
		 * @category Modifier
		 */
		public function setSceneIdscene($sceneIdscene){
			$this->sceneIdscene = (int)$sceneIdscene;
		}

		/**
		 * setIscommandactive Sets the class attribute iscommandactive with a given value
		 * The attribute iscommandactive maps the field isCommandActive defined as tinyint(4).<br>
		 * Comment for field isCommandActive: Not specified.<br>
		 *
		 * @param int $iscommandactive
		 *
		 * @category Modifier
		 */
		public function setIscommandactive($iscommandactive){
			$this->iscommandactive = (int)$iscommandactive;
		}

		/**
		 * setCommandIdcommand Sets the class attribute commandIdcommand with a given value
		 * The attribute commandIdcommand maps the field command_idCommand defined as int(11).<br>
		 * Comment for field command_idCommand: Not specified.<br>
		 *
		 * @param int $commandIdcommand
		 *
		 * @category Modifier
		 */
		public function setCommandIdcommand($commandIdcommand){
			$this->commandIdcommand = (int)$commandIdcommand;
		}

		/**
		 * setIstimecontrolactive Sets the class attribute istimecontrolactive with a given value
		 * The attribute istimecontrolactive maps the field isTimeControlActive defined as tinyint(4).<br>
		 * Comment for field isTimeControlActive: Not specified.<br>
		 *
		 * @param int $istimecontrolactive
		 *
		 * @category Modifier
		 */
		public function setIstimecontrolactive($istimecontrolactive){
			$this->istimecontrolactive = (int)$istimecontrolactive;
		}

		/**
		 * setTimecontrolsIdtimecontrols Sets the class attribute timecontrolsIdtimecontrols with a given value
		 * The attribute timecontrolsIdtimecontrols maps the field timecontrols_idtimeControls defined as int(11).<br>
		 * Comment for field timecontrols_idtimeControls: Not specified.<br>
		 *
		 * @param int $timecontrolsIdtimecontrols
		 *
		 * @category Modifier
		 */
		public function setTimecontrolsIdtimecontrols($timecontrolsIdtimecontrols){
			$this->timecontrolsIdtimecontrols = (int)$timecontrolsIdtimecontrols;
		}

		/**
		 * getIdsensorscenario gets the class attribute idsensorscenario value
		 * The attribute idsensorscenario maps the field idSensorScenario defined as int(11).<br>
		 * Comment for field idSensorScenario: Not specified.
		 * @return int $idsensorscenario
		 * @category Accessor of $idsensorscenario
		 */
		public function getIdsensorscenario(){
			return $this->idsensorscenario;
		}

		/**
		 * getName gets the class attribute name value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.
		 * @return string $name
		 * @category Accessor of $name
		 */
		public function getName(){
			return $this->name;
		}

		/**
		 * getIssceneactive gets the class attribute issceneactive value
		 * The attribute issceneactive maps the field isSceneActive defined as tinyint(4).<br>
		 * Comment for field isSceneActive: Not specified.
		 * @return int $issceneactive
		 * @category Accessor of $issceneactive
		 */
		public function getIssceneactive(){
			return $this->issceneactive;
		}

		/**
		 * getSceneIdscene gets the class attribute sceneIdscene value
		 * The attribute sceneIdscene maps the field Scene_idScene defined as int(11).<br>
		 * Comment for field Scene_idScene: Not specified.
		 * @return int $sceneIdscene
		 * @category Accessor of $sceneIdscene
		 */
		public function getSceneIdscene(){
			return $this->sceneIdscene;
		}

		/**
		 * getIscommandactive gets the class attribute iscommandactive value
		 * The attribute iscommandactive maps the field isCommandActive defined as tinyint(4).<br>
		 * Comment for field isCommandActive: Not specified.
		 * @return int $iscommandactive
		 * @category Accessor of $iscommandactive
		 */
		public function getIscommandactive(){
			return $this->iscommandactive;
		}

		/**
		 * getCommandIdcommand gets the class attribute commandIdcommand value
		 * The attribute commandIdcommand maps the field command_idCommand defined as int(11).<br>
		 * Comment for field command_idCommand: Not specified.
		 * @return int $commandIdcommand
		 * @category Accessor of $commandIdcommand
		 */
		public function getCommandIdcommand(){
			return $this->commandIdcommand;
		}

		/**
		 * getIstimecontrolactive gets the class attribute istimecontrolactive value
		 * The attribute istimecontrolactive maps the field isTimeControlActive defined as tinyint(4).<br>
		 * Comment for field isTimeControlActive: Not specified.
		 * @return int $istimecontrolactive
		 * @category Accessor of $istimecontrolactive
		 */
		public function getIstimecontrolactive(){
			return $this->istimecontrolactive;
		}

		/**
		 * getTimecontrolsIdtimecontrols gets the class attribute timecontrolsIdtimecontrols value
		 * The attribute timecontrolsIdtimecontrols maps the field timecontrols_idtimeControls defined as int(11).<br>
		 * Comment for field timecontrols_idtimeControls: Not specified.
		 * @return int $timecontrolsIdtimecontrols
		 * @category Accessor of $timecontrolsIdtimecontrols
		 */
		public function getTimecontrolsIdtimecontrols(){
			return $this->timecontrolsIdtimecontrols;
		}

		/**
		 * Gets DDL SQL code of the table sensorscenario
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "sensorscenario";
		}

		/**
		 * The BeanSensorscenario constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idsensorscenario is given.
		 *  - with a fetched data row from the table sensorscenario having idSensorScenario=$idsensorscenario
		 *
		 * @param int $idsensorscenario . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanSensorscenario Object
		 */
		public function __construct($idsensorscenario = null){
			parent::__construct();
			if (!empty($idsensorscenario)){
				$this->select($idsensorscenario);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of sensorscenario into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idsensorscenario the primary key idSensorScenario value of table sensorscenario which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idsensorscenario){
			$sql = "SELECT * FROM sensorscenario WHERE idSensorScenario={$this->parseValue($idsensorscenario,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idsensorscenario = (integer)$rowObject->idSensorScenario;
				@$this->name = $this->replaceAposBackSlash($rowObject->name);
				@$this->issceneactive = (integer)$rowObject->isSceneActive;
				@$this->sceneIdscene = (integer)$rowObject->Scene_idScene;
				@$this->iscommandactive = (integer)$rowObject->isCommandActive;
				@$this->commandIdcommand = (integer)$rowObject->command_idCommand;
				@$this->istimecontrolactive = (integer)$rowObject->isTimeControlActive;
				@$this->timecontrolsIdtimecontrols = (integer)$rowObject->timecontrols_idtimeControls;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table sensorscenario
		 *
		 * @param int $idsensorscenario the primary key idSensorScenario value of table sensorscenario which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idsensorscenario){
			$sql = "DELETE FROM sensorscenario WHERE idSensorScenario={$this->parseValue($idsensorscenario,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of sensorscenario
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idsensorscenario = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO sensorscenario
            (name,isSceneActive,Scene_idScene,isCommandActive,command_idCommand,isTimeControlActive,timecontrols_idtimeControls)
            VALUES(
			{$this->parseValue($this->name, 'notNumber')},
			{$this->parseValue($this->issceneactive)},
			{$this->parseValue($this->sceneIdscene)},
			{$this->parseValue($this->iscommandactive)},
			{$this->parseValue($this->commandIdcommand)},
			{$this->parseValue($this->istimecontrolactive)},
			{$this->parseValue($this->timecontrolsIdtimecontrols)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idsensorscenario = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table sensorscenario with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idsensorscenario the primary key idSensorScenario value of table sensorscenario which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idsensorscenario){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                sensorscenario
            SET 
				name={$this->parseValue($this->name, 'notNumber')},
				isSceneActive={$this->parseValue($this->issceneactive)},
				Scene_idScene={$this->parseValue($this->sceneIdscene)},
				isCommandActive={$this->parseValue($this->iscommandactive)},
				command_idCommand={$this->parseValue($this->commandIdcommand)},
				isTimeControlActive={$this->parseValue($this->istimecontrolactive)},
				timecontrols_idtimeControls={$this->parseValue($this->timecontrolsIdtimecontrols)}
            WHERE
                idSensorScenario={$this->parseValue($idsensorscenario, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idsensorscenario);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of sensorscenario previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idsensorscenario != ""){
				return $this->update($this->idsensorscenario);
			}else{
				return false;
			}
		}

	}

?>
