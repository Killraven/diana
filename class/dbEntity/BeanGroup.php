<?php
	include_once("bean.config.php");

	/**
	 * Copyright (c) 2018. Paolo Maruotti
	 */
	// namespace beans;

	class BeanGroup extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idGroup of table group
		 * Comment for field idGroup: Not specified<br>
		 * @var int $idgroup
		 */
		private $idgroup;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field name
		 * Comment for field name: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $name
		 */
		private $name;

		/**
		 * Class attribute for storing the SQL DDL of table group
		 * @var string base64 encoded string for DDL
		 */
		//private $ddl = "Q1JFQVRFIFRBQkxFIGBncm91cDJgICgKICBgaWRHcm91cGAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgbmFtZWAgdmFyY2hhcig0NSkgTk9UIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZEdyb3VwYCkKKSBFTkdJTkU9TXlJU0FNIERFRkFVTFQgQ0hBUlNFVD11dGY4";
		private $ddl = "Q1JFQVRFIFRBQkxFIGBncm91cGAgKAogIGBpZEdyb3VwYCBpbnQoMTEpIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULAogIGBuYW1lYCB2YXJjaGFyKDQ1KSBOT1QgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkR3JvdXBgKQopIEVOR0lORT1NeUlTQU0gREVGQVVMVCBDSEFSU0VUPXV0Zjg=";


		/**
		 * setIdgroup Sets the class attribute idgroup with a given value
		 * The attribute idgroup maps the field idGroup defined as int(11).<br>
		 * Comment for field idGroup: Not specified.<br>
		 *
		 * @param int $idgroup
		 *
		 * @category Modifier
		 */
		public function setIdgroup($idgroup){
			$this->idgroup = (int)$idgroup;
		}

		/**
		 * setName Sets the class attribute name with a given value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.<br>
		 *
		 * @param string $name
		 *
		 * @category Modifier
		 */
		public function setName($name){
			$this->name = (string)$name;
		}

		/**
		 * getIdgroup gets the class attribute idgroup value
		 * The attribute idgroup maps the field idGroup defined as int(11).<br>
		 * Comment for field idGroup: Not specified.
		 * @return int $idgroup
		 * @category Accessor of $idgroup
		 */
		public function getIdgroup(){
			return $this->idgroup;
		}

		/**
		 * getName gets the class attribute name value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.
		 * @return string $name
		 * @category Accessor of $name
		 */
		public function getName(){
			return $this->name;
		}

		/**
		 * Gets DDL SQL code of the table group
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		public function getUncodedDdl(){
			return ($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "group";
		}

		/**
		 * The Beangroup constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idgroup is given.
		 *  - with a fetched data row from the table group having idGroup=$idgroup
		 *
		 * @param int $idgroup . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanGroup Object
		 */
		public function __construct($idgroup = null){
			parent::__construct();
			if (!empty($idgroup)){
				$this->select($idgroup);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of group into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idgroup the primary key idGroup value of table group which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idgroup){
			$sql = "SELECT * FROM group WHERE idGroup={$this->parseValue($idgroup,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idgroup = (integer)$rowObject->idGroup;
				@$this->name = $this->replaceAposBackSlash($rowObject->name);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table group
		 *
		 * @param int $idgroup the primary key idGroup value of table group which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idgroup){
			$sql = "DELETE FROM group WHERE idGroup={$this->parseValue($idgroup,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of group
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idgroup = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO group
            (name)
            VALUES(
			{$this->parseValue($this->name, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idgroup = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table group with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idgroup the primary key idGroup value of table group which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idgroup){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                group
            SET 
				name={$this->parseValue($this->name, 'notNumber')}
            WHERE
                idGroup={$this->parseValue($idgroup, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idgroup);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of group previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idgroup != ""){
				return $this->update($this->idgroup);
			}else{
				return false;
			}
		}

	}

?>
