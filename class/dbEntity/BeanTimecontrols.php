<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanTimecontrols extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idtimeControls of table timecontrols
		 * Comment for field idtimeControls: Not specified<br>
		 * @var int $idtimecontrols
		 */
		private $idtimecontrols;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field Mo
		 * Comment for field Mo: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $mo
		 */
		private $mo;

		/**
		 * Class attribute for mapping table field Tu
		 * Comment for field Tu: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $tu
		 */
		private $tu;

		/**
		 * Class attribute for mapping table field We
		 * Comment for field We: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $we
		 */
		private $we;

		/**
		 * Class attribute for mapping table field Th
		 * Comment for field Th: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $th
		 */
		private $th;

		/**
		 * Class attribute for mapping table field Fr
		 * Comment for field Fr: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $fr
		 */
		private $fr;

		/**
		 * Class attribute for mapping table field Sa
		 * Comment for field Sa: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $sa
		 */
		private $sa;

		/**
		 * Class attribute for mapping table field Su
		 * Comment for field Su: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $su
		 */
		private $su;

		/**
		 * Class attribute for mapping table field from
		 * Comment for field from: Not specified.<br>
		 * Field information:
		 *  - Data type: string|time
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $from
		 */
		private $from;

		/**
		 * Class attribute for mapping table field to
		 * Comment for field to: Not specified.<br>
		 * Field information:
		 *  - Data type: string|time
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $to
		 */
		private $to;

		/**
		 * Class attribute for storing the SQL DDL of table timecontrols
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGB0aW1lY29udHJvbHNgICgKICBgaWR0aW1lQ29udHJvbHNgIGludCgxMSkgTk9UIE5VTEwgQVVUT19JTkNSRU1FTlQsCiAgYE1vYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgVHVgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGBXZWAgdGlueWludCg0KSBERUZBVUxUIE5VTEwsCiAgYFRoYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgRnJgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGBTYWAgdGlueWludCg0KSBERUZBVUxUIE5VTEwsCiAgYFN1YCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgZnJvbWAgdGltZSBERUZBVUxUIE5VTEwsCiAgYHRvYCB0aW1lIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkdGltZUNvbnRyb2xzYCkKKSBFTkdJTkU9SW5ub0RCIERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setIdtimecontrols Sets the class attribute idtimecontrols with a given value
		 * The attribute idtimecontrols maps the field idtimeControls defined as int(11).<br>
		 * Comment for field idtimeControls: Not specified.<br>
		 *
		 * @param int $idtimecontrols
		 *
		 * @category Modifier
		 */
		public function setIdtimecontrols($idtimecontrols){
			$this->idtimecontrols = (int)$idtimecontrols;
		}

		/**
		 * setMo Sets the class attribute mo with a given value
		 * The attribute mo maps the field Mo defined as tinyint(4).<br>
		 * Comment for field Mo: Not specified.<br>
		 *
		 * @param int $mo
		 *
		 * @category Modifier
		 */
		public function setMo($mo){
			$this->mo = (int)$mo;
		}

		/**
		 * setTu Sets the class attribute tu with a given value
		 * The attribute tu maps the field Tu defined as tinyint(4).<br>
		 * Comment for field Tu: Not specified.<br>
		 *
		 * @param int $tu
		 *
		 * @category Modifier
		 */
		public function setTu($tu){
			$this->tu = (int)$tu;
		}

		/**
		 * setWe Sets the class attribute we with a given value
		 * The attribute we maps the field We defined as tinyint(4).<br>
		 * Comment for field We: Not specified.<br>
		 *
		 * @param int $we
		 *
		 * @category Modifier
		 */
		public function setWe($we){
			$this->we = (int)$we;
		}

		/**
		 * setTh Sets the class attribute th with a given value
		 * The attribute th maps the field Th defined as tinyint(4).<br>
		 * Comment for field Th: Not specified.<br>
		 *
		 * @param int $th
		 *
		 * @category Modifier
		 */
		public function setTh($th){
			$this->th = (int)$th;
		}

		/**
		 * setFr Sets the class attribute fr with a given value
		 * The attribute fr maps the field Fr defined as tinyint(4).<br>
		 * Comment for field Fr: Not specified.<br>
		 *
		 * @param int $fr
		 *
		 * @category Modifier
		 */
		public function setFr($fr){
			$this->fr = (int)$fr;
		}

		/**
		 * setSa Sets the class attribute sa with a given value
		 * The attribute sa maps the field Sa defined as tinyint(4).<br>
		 * Comment for field Sa: Not specified.<br>
		 *
		 * @param int $sa
		 *
		 * @category Modifier
		 */
		public function setSa($sa){
			$this->sa = (int)$sa;
		}

		/**
		 * setSu Sets the class attribute su with a given value
		 * The attribute su maps the field Su defined as tinyint(4).<br>
		 * Comment for field Su: Not specified.<br>
		 *
		 * @param int $su
		 *
		 * @category Modifier
		 */
		public function setSu($su){
			$this->su = (int)$su;
		}

		/**
		 * setFrom Sets the class attribute from with a given value
		 * The attribute from maps the field from defined as string|time.<br>
		 * Comment for field from: Not specified.<br>
		 *
		 * @param string $from
		 *
		 * @category Modifier
		 */
		public function setFrom($from){
			$this->from = (string)$from;
		}

		/**
		 * setTo Sets the class attribute to with a given value
		 * The attribute to maps the field to defined as string|time.<br>
		 * Comment for field to: Not specified.<br>
		 *
		 * @param string $to
		 *
		 * @category Modifier
		 */
		public function setTo($to){
			$this->to = (string)$to;
		}

		/**
		 * getIdtimecontrols gets the class attribute idtimecontrols value
		 * The attribute idtimecontrols maps the field idtimeControls defined as int(11).<br>
		 * Comment for field idtimeControls: Not specified.
		 * @return int $idtimecontrols
		 * @category Accessor of $idtimecontrols
		 */
		public function getIdtimecontrols(){
			return $this->idtimecontrols;
		}

		/**
		 * getMo gets the class attribute mo value
		 * The attribute mo maps the field Mo defined as tinyint(4).<br>
		 * Comment for field Mo: Not specified.
		 * @return int $mo
		 * @category Accessor of $mo
		 */
		public function getMo(){
			return $this->mo;
		}

		/**
		 * getTu gets the class attribute tu value
		 * The attribute tu maps the field Tu defined as tinyint(4).<br>
		 * Comment for field Tu: Not specified.
		 * @return int $tu
		 * @category Accessor of $tu
		 */
		public function getTu(){
			return $this->tu;
		}

		/**
		 * getWe gets the class attribute we value
		 * The attribute we maps the field We defined as tinyint(4).<br>
		 * Comment for field We: Not specified.
		 * @return int $we
		 * @category Accessor of $we
		 */
		public function getWe(){
			return $this->we;
		}

		/**
		 * getTh gets the class attribute th value
		 * The attribute th maps the field Th defined as tinyint(4).<br>
		 * Comment for field Th: Not specified.
		 * @return int $th
		 * @category Accessor of $th
		 */
		public function getTh(){
			return $this->th;
		}

		/**
		 * getFr gets the class attribute fr value
		 * The attribute fr maps the field Fr defined as tinyint(4).<br>
		 * Comment for field Fr: Not specified.
		 * @return int $fr
		 * @category Accessor of $fr
		 */
		public function getFr(){
			return $this->fr;
		}

		/**
		 * getSa gets the class attribute sa value
		 * The attribute sa maps the field Sa defined as tinyint(4).<br>
		 * Comment for field Sa: Not specified.
		 * @return int $sa
		 * @category Accessor of $sa
		 */
		public function getSa(){
			return $this->sa;
		}

		/**
		 * getSu gets the class attribute su value
		 * The attribute su maps the field Su defined as tinyint(4).<br>
		 * Comment for field Su: Not specified.
		 * @return int $su
		 * @category Accessor of $su
		 */
		public function getSu(){
			return $this->su;
		}

		/**
		 * getFrom gets the class attribute from value
		 * The attribute from maps the field from defined as string|time.<br>
		 * Comment for field from: Not specified.
		 * @return string $from
		 * @category Accessor of $from
		 */
		public function getFrom(){
			return $this->from;
		}

		/**
		 * getTo gets the class attribute to value
		 * The attribute to maps the field to defined as string|time.<br>
		 * Comment for field to: Not specified.
		 * @return string $to
		 * @category Accessor of $to
		 */
		public function getTo(){
			return $this->to;
		}

		/**
		 * Gets DDL SQL code of the table timecontrols
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "timecontrols";
		}

		/**
		 * The BeanTimecontrols constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idtimecontrols is given.
		 *  - with a fetched data row from the table timecontrols having idtimeControls=$idtimecontrols
		 *
		 * @param int $idtimecontrols . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanTimecontrols Object
		 */
		public function __construct($idtimecontrols = null){
			parent::__construct();
			if (!empty($idtimecontrols)){
				$this->select($idtimecontrols);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of timecontrols into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idtimecontrols the primary key idtimeControls value of table timecontrols which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idtimecontrols){
			$sql = "SELECT * FROM timecontrols WHERE idtimeControls={$this->parseValue($idtimecontrols,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idtimecontrols = (integer)$rowObject->idtimeControls;
				@$this->mo = (integer)$rowObject->Mo;
				@$this->tu = (integer)$rowObject->Tu;
				@$this->we = (integer)$rowObject->We;
				@$this->th = (integer)$rowObject->Th;
				@$this->fr = (integer)$rowObject->Fr;
				@$this->sa = (integer)$rowObject->Sa;
				@$this->su = (integer)$rowObject->Su;
				@$this->from = $rowObject->from;
				@$this->to = $rowObject->to;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table timecontrols
		 *
		 * @param int $idtimecontrols the primary key idtimeControls value of table timecontrols which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idtimecontrols){
			$sql = "DELETE FROM timecontrols WHERE idtimeControls={$this->parseValue($idtimecontrols,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of timecontrols
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idtimecontrols = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO timecontrols
            (Mo,Tu,We,Th,Fr,Sa,Su,from,to)
            VALUES(
			{$this->parseValue($this->mo)},
			{$this->parseValue($this->tu)},
			{$this->parseValue($this->we)},
			{$this->parseValue($this->th)},
			{$this->parseValue($this->fr)},
			{$this->parseValue($this->sa)},
			{$this->parseValue($this->su)},
			{$this->parseValue($this->from, 'notNumber')},
			{$this->parseValue($this->to, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idtimecontrols = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table timecontrols with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idtimecontrols the primary key idtimeControls value of table timecontrols which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idtimecontrols){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                timecontrols
            SET 
				Mo={$this->parseValue($this->mo)},
				Tu={$this->parseValue($this->tu)},
				We={$this->parseValue($this->we)},
				Th={$this->parseValue($this->th)},
				Fr={$this->parseValue($this->fr)},
				Sa={$this->parseValue($this->sa)},
				Su={$this->parseValue($this->su)},
				from={$this->parseValue($this->from, 'notNumber')},
				to={$this->parseValue($this->to, 'notNumber')}
            WHERE
                idtimeControls={$this->parseValue($idtimecontrols, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idtimecontrols);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of timecontrols previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idtimecontrols != ""){
				return $this->update($this->idtimecontrols);
			}else{
				return false;
			}
		}

	}

?>
