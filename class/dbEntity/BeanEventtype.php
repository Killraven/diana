<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanEventtype extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key ideventType of table eventtype
		 * Comment for field ideventType: Not specified<br>
		 * @var int $ideventtype
		 */
		private $ideventtype;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field name
		 * Comment for field name: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $name
		 */
		private $name;

		/**
		 * Class attribute for storing the SQL DDL of table eventtype
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBldmVudHR5cGVgICgKICBgaWRldmVudFR5cGVgIGludCgxMSkgTk9UIE5VTEwgQVVUT19JTkNSRU1FTlQsCiAgYG5hbWVgIHZhcmNoYXIoNDUpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkZXZlbnRUeXBlYCkKKSBFTkdJTkU9SW5ub0RCIEFVVE9fSU5DUkVNRU5UPTYgREVGQVVMVCBDSEFSU0VUPXV0Zjg=";

		/**
		 * setIdeventtype Sets the class attribute ideventtype with a given value
		 * The attribute ideventtype maps the field ideventType defined as int(11).<br>
		 * Comment for field ideventType: Not specified.<br>
		 *
		 * @param int $ideventtype
		 *
		 * @category Modifier
		 */
		public function setIdeventtype($ideventtype){
			$this->ideventtype = (int)$ideventtype;
		}

		/**
		 * setName Sets the class attribute name with a given value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.<br>
		 *
		 * @param string $name
		 *
		 * @category Modifier
		 */
		public function setName($name){
			$this->name = (string)$name;
		}

		/**
		 * getIdeventtype gets the class attribute ideventtype value
		 * The attribute ideventtype maps the field ideventType defined as int(11).<br>
		 * Comment for field ideventType: Not specified.
		 * @return int $ideventtype
		 * @category Accessor of $ideventtype
		 */
		public function getIdeventtype(){
			return $this->ideventtype;
		}

		/**
		 * getName gets the class attribute name value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.
		 * @return string $name
		 * @category Accessor of $name
		 */
		public function getName(){
			return $this->name;
		}

		/**
		 * Gets DDL SQL code of the table eventtype
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "eventtype";
		}

		/**
		 * The BeanEventtype constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $ideventtype is given.
		 *  - with a fetched data row from the table eventtype having ideventType=$ideventtype
		 *
		 * @param int $ideventtype . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanEventtype Object
		 */
		public function __construct($ideventtype = null){
			parent::__construct();
			if (!empty($ideventtype)){
				$this->select($ideventtype);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of eventtype into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $ideventtype the primary key ideventType value of table eventtype which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($ideventtype){
			$sql = "SELECT * FROM eventtype WHERE ideventType={$this->parseValue($ideventtype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->ideventtype = (integer)$rowObject->ideventType;
				@$this->name = $this->replaceAposBackSlash($rowObject->name);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table eventtype
		 *
		 * @param int $ideventtype the primary key ideventType value of table eventtype which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($ideventtype){
			$sql = "DELETE FROM eventtype WHERE ideventType={$this->parseValue($ideventtype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of eventtype
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->ideventtype = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO eventtype
            (name)
            VALUES(
			{$this->parseValue($this->name, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->ideventtype = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table eventtype with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $ideventtype the primary key ideventType value of table eventtype which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($ideventtype){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                eventtype
            SET 
				name={$this->parseValue($this->name, 'notNumber')}
            WHERE
                ideventType={$this->parseValue($ideventtype, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($ideventtype);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of eventtype previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->ideventtype != ""){
				return $this->update($this->ideventtype);
			}else{
				return false;
			}
		}

	}

?>
