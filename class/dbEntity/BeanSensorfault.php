<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanSensorfault extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idsensorFault of table sensorfault
		 * Comment for field idsensorFault: Not specified<br>
		 * @var int $idsensorfault
		 */
		private $idsensorfault;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = false;

		/**
		 * Class attribute for mapping table field sensor_idsensor
		 * Comment for field sensor_idsensor: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $sensorIdsensor
		 */
		private $sensorIdsensor;

		/**
		 * Class attribute for mapping table field fixed1
		 * Comment for field fixed1: Not specified.<br>
		 * Field information:
		 *  - Data type: float
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $fixed1
		 */
		private $fixed1;

		/**
		 * Class attribute for mapping table field fixed2
		 * Comment for field fixed2: Not specified.<br>
		 * Field information:
		 *  - Data type: float
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $fixed2
		 */
		private $fixed2;

		/**
		 * Class attribute for storing the SQL DDL of table sensorfault
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBzZW5zb3JmYXVsdGAgKAogIGBpZHNlbnNvckZhdWx0YCBpbnQoMTEpIE5PVCBOVUxMLAogIGBzZW5zb3JfaWRzZW5zb3JgIGludCgxMSkgTk9UIE5VTEwsCiAgYGZpeGVkMWAgZmxvYXQgREVGQVVMVCBOVUxMLAogIGBmaXhlZDJgIGZsb2F0IERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkc2Vuc29yRmF1bHRgKSwKICBLRVkgYGZrX3NlbnNvckZhdWx0X3NlbnNvcjFfaWR4YCAoYHNlbnNvcl9pZHNlbnNvcmApLAogIENPTlNUUkFJTlQgYGZrX3NlbnNvckZhdWx0X3NlbnNvcjFgIEZPUkVJR04gS0VZIChgc2Vuc29yX2lkc2Vuc29yYCkgUkVGRVJFTkNFUyBgc2Vuc29yYCAoYGlkc2Vuc29yYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OCikgRU5HSU5FPUlubm9EQiBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIdsensorfault Sets the class attribute idsensorfault with a given value
		 * The attribute idsensorfault maps the field idsensorFault defined as int(11).<br>
		 * Comment for field idsensorFault: Not specified.<br>
		 *
		 * @param int $idsensorfault
		 *
		 * @category Modifier
		 */
		public function setIdsensorfault($idsensorfault){
			$this->idsensorfault = (int)$idsensorfault;
		}

		/**
		 * setSensorIdsensor Sets the class attribute sensorIdsensor with a given value
		 * The attribute sensorIdsensor maps the field sensor_idsensor defined as int(11).<br>
		 * Comment for field sensor_idsensor: Not specified.<br>
		 *
		 * @param int $sensorIdsensor
		 *
		 * @category Modifier
		 */
		public function setSensorIdsensor($sensorIdsensor){
			$this->sensorIdsensor = (int)$sensorIdsensor;
		}

		/**
		 * setFixed1 Sets the class attribute fixed1 with a given value
		 * The attribute fixed1 maps the field fixed1 defined as float.<br>
		 * Comment for field fixed1: Not specified.<br>
		 *
		 * @param float $fixed1
		 *
		 * @category Modifier
		 */
		public function setFixed1($fixed1){
			$this->fixed1 = (float)$fixed1;
		}

		/**
		 * setFixed2 Sets the class attribute fixed2 with a given value
		 * The attribute fixed2 maps the field fixed2 defined as float.<br>
		 * Comment for field fixed2: Not specified.<br>
		 *
		 * @param float $fixed2
		 *
		 * @category Modifier
		 */
		public function setFixed2($fixed2){
			$this->fixed2 = (float)$fixed2;
		}

		/**
		 * getIdsensorfault gets the class attribute idsensorfault value
		 * The attribute idsensorfault maps the field idsensorFault defined as int(11).<br>
		 * Comment for field idsensorFault: Not specified.
		 * @return int $idsensorfault
		 * @category Accessor of $idsensorfault
		 */
		public function getIdsensorfault(){
			return $this->idsensorfault;
		}

		/**
		 * getSensorIdsensor gets the class attribute sensorIdsensor value
		 * The attribute sensorIdsensor maps the field sensor_idsensor defined as int(11).<br>
		 * Comment for field sensor_idsensor: Not specified.
		 * @return int $sensorIdsensor
		 * @category Accessor of $sensorIdsensor
		 */
		public function getSensorIdsensor(){
			return $this->sensorIdsensor;
		}

		/**
		 * getFixed1 gets the class attribute fixed1 value
		 * The attribute fixed1 maps the field fixed1 defined as float.<br>
		 * Comment for field fixed1: Not specified.
		 * @return float $fixed1
		 * @category Accessor of $fixed1
		 */
		public function getFixed1(){
			return $this->fixed1;
		}

		/**
		 * getFixed2 gets the class attribute fixed2 value
		 * The attribute fixed2 maps the field fixed2 defined as float.<br>
		 * Comment for field fixed2: Not specified.
		 * @return float $fixed2
		 * @category Accessor of $fixed2
		 */
		public function getFixed2(){
			return $this->fixed2;
		}

		/**
		 * Gets DDL SQL code of the table sensorfault
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "sensorfault";
		}

		/**
		 * The BeanSensorfault constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idsensorfault is given.
		 *  - with a fetched data row from the table sensorfault having idsensorFault=$idsensorfault
		 *
		 * @param int $idsensorfault . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanSensorfault Object
		 */
		public function __construct($idsensorfault = null){
			parent::__construct();
			if (!empty($idsensorfault)){
				$this->select($idsensorfault);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of sensorfault into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idsensorfault the primary key idsensorFault value of table sensorfault which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idsensorfault){
			$sql = "SELECT * FROM sensorfault WHERE idsensorFault={$this->parseValue($idsensorfault,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idsensorfault = (integer)$rowObject->idsensorFault;
				@$this->sensorIdsensor = (integer)$rowObject->sensor_idsensor;
				@$this->fixed1 = (float)$rowObject->fixed1;
				@$this->fixed2 = (float)$rowObject->fixed2;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table sensorfault
		 *
		 * @param int $idsensorfault the primary key idsensorFault value of table sensorfault which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idsensorfault){
			$sql = "DELETE FROM sensorfault WHERE idsensorFault={$this->parseValue($idsensorfault,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of sensorfault
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idsensorfault = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO sensorfault
            (idsensorFault,sensor_idsensor,fixed1,fixed2)
            VALUES({$this->parseValue($this->idsensorfault)},
			{$this->parseValue($this->sensorIdsensor)},
			{$this->parseValue($this->fixed1)},
			{$this->parseValue($this->fixed2)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idsensorfault = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table sensorfault with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idsensorfault the primary key idsensorFault value of table sensorfault which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idsensorfault){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                sensorfault
            SET 
				sensor_idsensor={$this->parseValue($this->sensorIdsensor)},
				fixed1={$this->parseValue($this->fixed1)},
				fixed2={$this->parseValue($this->fixed2)}
            WHERE
                idsensorFault={$this->parseValue($idsensorfault, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idsensorfault);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of sensorfault previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idsensorfault != ""){
				return $this->update($this->idsensorfault);
			}else{
				return false;
			}
		}

	}

?>
