<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanUsers extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idusers of table users
		 * Comment for field idusers: Not specified<br>
		 * @var int $idusers
		 */
		private $idusers;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field username
		 * Comment for field username: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $username
		 */
		private $username;

		/**
		 * Class attribute for mapping table field password
		 * Comment for field password: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $password
		 */
		private $password;

		/**
		 * Class attribute for mapping table field isAdmin
		 * Comment for field isAdmin: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(1)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $isadmin
		 */
		private $isadmin;

		/**
		 * Class attribute for storing the SQL DDL of table users
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGB1c2Vyc2AgKAogIGBpZHVzZXJzYCBpbnQoMTEpIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULAogIGB1c2VybmFtZWAgdmFyY2hhcig0NSkgREVGQVVMVCBOVUxMLAogIGBwYXNzd29yZGAgdmFyY2hhcig0NSkgREVGQVVMVCBOVUxMLAogIGBpc0FkbWluYCB0aW55aW50KDEpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkdXNlcnNgKSwKICBVTklRVUUgS0VZIGBpZHVzZXJzX1VOSVFVRWAgKGBpZHVzZXJzYCkKKSBFTkdJTkU9SW5ub0RCIEFVVE9fSU5DUkVNRU5UPTI3IERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setIdusers Sets the class attribute idusers with a given value
		 * The attribute idusers maps the field idusers defined as int(11).<br>
		 * Comment for field idusers: Not specified.<br>
		 *
		 * @param int $idusers
		 *
		 * @category Modifier
		 */
		public function setIdusers($idusers){
			$this->idusers = (int)$idusers;
		}

		/**
		 * setUsername Sets the class attribute username with a given value
		 * The attribute username maps the field username defined as varchar(45).<br>
		 * Comment for field username: Not specified.<br>
		 *
		 * @param string $username
		 *
		 * @category Modifier
		 */
		public function setUsername($username){
			$this->username = (string)$username;
		}

		/**
		 * setPassword Sets the class attribute password with a given value
		 * The attribute password maps the field password defined as varchar(45).<br>
		 * Comment for field password: Not specified.<br>
		 *
		 * @param string $password
		 *
		 * @category Modifier
		 */
		public function setPassword($password){
			$this->password = (string)$password;
		}

		/**
		 * setIsadmin Sets the class attribute isadmin with a given value
		 * The attribute isadmin maps the field isAdmin defined as tinyint(1).<br>
		 * Comment for field isAdmin: Not specified.<br>
		 *
		 * @param int $isadmin
		 *
		 * @category Modifier
		 */
		public function setIsadmin($isadmin){
			$this->isadmin = (int)$isadmin;
		}

		/**
		 * getIdusers gets the class attribute idusers value
		 * The attribute idusers maps the field idusers defined as int(11).<br>
		 * Comment for field idusers: Not specified.
		 * @return int $idusers
		 * @category Accessor of $idusers
		 */
		public function getIdusers(){
			return $this->idusers;
		}

		/**
		 * getUsername gets the class attribute username value
		 * The attribute username maps the field username defined as varchar(45).<br>
		 * Comment for field username: Not specified.
		 * @return string $username
		 * @category Accessor of $username
		 */
		public function getUsername(){
			return $this->username;
		}

		/**
		 * getPassword gets the class attribute password value
		 * The attribute password maps the field password defined as varchar(45).<br>
		 * Comment for field password: Not specified.
		 * @return string $password
		 * @category Accessor of $password
		 */
		public function getPassword(){
			return $this->password;
		}

		/**
		 * getIsadmin gets the class attribute isadmin value
		 * The attribute isadmin maps the field isAdmin defined as tinyint(1).<br>
		 * Comment for field isAdmin: Not specified.
		 * @return int $isadmin
		 * @category Accessor of $isadmin
		 */
		public function getIsadmin(){
			return $this->isadmin;
		}

		/**
		 * Gets DDL SQL code of the table users
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "users";
		}

		/**
		 * The BeanUsers constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idusers is given.
		 *  - with a fetched data row from the table users having idusers=$idusers
		 *
		 * @param int $idusers . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanUsers Object
		 */
		public function __construct($idusers = null){
			parent::__construct();
			if (!empty($idusers)){
				$this->select($idusers);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of users into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idusers the primary key idusers value of table users which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idusers){
			$sql = "SELECT * FROM users WHERE idusers={$this->parseValue($idusers,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idusers = (integer)$rowObject->idusers;
				@$this->username = $this->replaceAposBackSlash($rowObject->username);
				@$this->password = $this->replaceAposBackSlash($rowObject->password);
				@$this->isadmin = (integer)$rowObject->isAdmin;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}


		public function getAll(){
			$sql = "SELECT * FROM users";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$result->data_seek(0);

			$aoData = array();

			while($row = $result->fetch_assoc()){
				$aoData[] = $row;
			}

			return $aoData;
		}

		/**
		 * Deletes a specific row from the table users
		 *
		 * @param int $idusers the primary key idusers value of table users which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idusers){
			$sql = "DELETE FROM users WHERE idusers={$this->parseValue($idusers,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of users
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idusers = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO users
            (username,password,isAdmin)
            VALUES(
			{$this->parseValue($this->username, 'notNumber')},
			{$this->parseValue($this->password, 'notNumber')},
			{$this->parseValue($this->isadmin)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idusers = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table users with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idusers the primary key idusers value of table users which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idusers){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                users
            SET 
				username={$this->parseValue($this->username, 'notNumber')},
				password={$this->parseValue($this->password, 'notNumber')},
				isAdmin={$this->parseValue($this->isadmin)}
            WHERE
                idusers={$this->parseValue($idusers, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idusers);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of users previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idusers != ""){
				return $this->update($this->idusers);
			}else{
				return false;
			}
		}

	}

?>
