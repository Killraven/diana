<?php
	include_once("bean.config.php");
	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanCommand extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idCommand of table command
		 * Comment for field idCommand: Not specified<br>
		 * @var int $idcommand
		 */
		private $idcommand;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field commandText
		 * Comment for field commandText: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(255)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $commandtext
		 */
		private $commandtext;

		/**
		 * Class attribute for mapping table field arg
		 * Comment for field arg: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $arg
		 */
		private $arg;

		/**
		 * Class attribute for storing the SQL DDL of table command
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBjb21tYW5kYCAoCiAgYGlkQ29tbWFuZGAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgY29tbWFuZFRleHRgIHZhcmNoYXIoMjU1KSBERUZBVUxUIE5VTEwsCiAgYGFyZ2AgdmFyY2hhcig0NSkgREVGQVVMVCBOVUxMLAogIFBSSU1BUlkgS0VZIChgaWRDb21tYW5kYCkKKSBFTkdJTkU9SW5ub0RCIEFVVE9fSU5DUkVNRU5UPTI1NiBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIdcommand Sets the class attribute idcommand with a given value
		 * The attribute idcommand maps the field idCommand defined as int(11).<br>
		 * Comment for field idCommand: Not specified.<br>
		 *
		 * @param int $idcommand
		 *
		 * @category Modifier
		 */
		public function setIdcommand($idcommand){
			$this->idcommand = (int)$idcommand;
		}

		/**
		 * setCommandtext Sets the class attribute commandtext with a given value
		 * The attribute commandtext maps the field commandText defined as varchar(255).<br>
		 * Comment for field commandText: Not specified.<br>
		 *
		 * @param string $commandtext
		 *
		 * @category Modifier
		 */
		public function setCommandtext($commandtext){
			$this->commandtext = (string)$commandtext;
		}

		/**
		 * setArg Sets the class attribute arg with a given value
		 * The attribute arg maps the field arg defined as varchar(45).<br>
		 * Comment for field arg: Not specified.<br>
		 *
		 * @param string $arg
		 *
		 * @category Modifier
		 */
		public function setArg($arg){
			$this->arg = (string)$arg;
		}

		/**
		 * getIdcommand gets the class attribute idcommand value
		 * The attribute idcommand maps the field idCommand defined as int(11).<br>
		 * Comment for field idCommand: Not specified.
		 * @return int $idcommand
		 * @category Accessor of $idcommand
		 */
		public function getIdcommand(){
			return $this->idcommand;
		}

		/**
		 * getCommandtext gets the class attribute commandtext value
		 * The attribute commandtext maps the field commandText defined as varchar(255).<br>
		 * Comment for field commandText: Not specified.
		 * @return string $commandtext
		 * @category Accessor of $commandtext
		 */
		public function getCommandtext(){
			return $this->commandtext;
		}

		/**
		 * getArg gets the class attribute arg value
		 * The attribute arg maps the field arg defined as varchar(45).<br>
		 * Comment for field arg: Not specified.
		 * @return string $arg
		 * @category Accessor of $arg
		 */
		public function getArg(){
			return $this->arg;
		}

		/**
		 * Gets DDL SQL code of the table command
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "command";
		}

		/**
		 * The BeanCommand constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idcommand is given.
		 *  - with a fetched data row from the table command having idCommand=$idcommand
		 *
		 * @param int $idcommand . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanCommand Object
		 */
		public function __construct($idcommand = null){
			parent::__construct();
			if (!empty($idcommand)){
				$this->select($idcommand);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of command into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idcommand the primary key idCommand value of table command which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idcommand){
			$sql = "SELECT * FROM command WHERE idCommand={$this->parseValue($idcommand,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idcommand = (integer)$rowObject->idCommand;
				@$this->commandtext = $this->replaceAposBackSlash($rowObject->commandText);
				@$this->arg = $this->replaceAposBackSlash($rowObject->arg);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table command
		 *
		 * @param int $idcommand the primary key idCommand value of table command which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idcommand){
			$sql = "DELETE FROM command WHERE idCommand={$this->parseValue($idcommand,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of command
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idcommand = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO command
            (commandText,arg)
            VALUES(
			{$this->parseValue($this->commandtext, 'notNumber')},
			{$this->parseValue($this->arg, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idcommand = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table command with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idcommand the primary key idCommand value of table command which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idcommand){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                command
            SET 
				commandText={$this->parseValue($this->commandtext, 'notNumber')},
				arg={$this->parseValue($this->arg, 'notNumber')}
            WHERE
                idCommand={$this->parseValue($idcommand, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idcommand);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of command previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idcommand != ""){
				return $this->update($this->idcommand);
			}else{
				return false;
			}
		}

	}

?>
