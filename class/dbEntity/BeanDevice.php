<?php
	include_once("bean.config.php");
	include_once(__DIR__ ."/dbEntity/bean.config.php");
	include_once(__DIR__ ."/dbEntity/BeanDevice.php");
	include_once(__DIR__ ."/dbEntity/BeanDevicetype.php");
	include_once(__DIR__ ."/dbEntity/BeanDevicehasgroup.php");


	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	/**
	 * Class BeanDevice
	 */
	class BeanDevice extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idDevice of table device
		 * Comment for field idDevice: Not specified<br>
		 * @var int $iddevice
		 */
		private $iddevice;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field DeviceStatus_idDeviceStatus
		 * Comment for field DeviceStatus_idDeviceStatus: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $devicestatusIddevicestatus
		 */
		private $devicestatusIddevicestatus;

		/**
		 * Class attribute for mapping table field adv
		 * Comment for field adv: Not specified.<br>
		 * Field information:
		 *  - Data type: smallint(6)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $adv
		 */
		private $adv;

		/**
		 * Class attribute for mapping table field advMin
		 * Comment for field advMin: Not specified.<br>
		 * Field information:
		 *  - Data type: smallint(6)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $advmin
		 */
		private $advmin;

		/**
		 * Class attribute for mapping table field advMax
		 * Comment for field advMax: Not specified.<br>
		 * Field information:
		 *  - Data type: smallint(6)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $advmax
		 */
		private $advmax;

		/**
		 * Class attribute for mapping table field response
		 * Comment for field response: Not specified.<br>
		 * Field information:
		 *  - Data type: smallint(6)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $response
		 */
		private $response;

		/**
		 * Class attribute for mapping table field version
		 * Comment for field version: Not specified.<br>
		 * Field information:
		 *  - Data type: smallint(6)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $version
		 */
		private $version;

		/**
		 * Class attribute for mapping table field DeviceType_idDeviceType
		 * Comment for field DeviceType_idDeviceType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $devicetypeIddevicetype
		 */
		private $devicetypeIddevicetype;

		/**
		 * Class attribute for mapping table field statusBallastKO
		 * Comment for field statusBallastKO: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statusballastko
		 */
		private $statusballastko;

		/**
		 * Class attribute for mapping table field statusLampKO
		 * Comment for field statusLampKO: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statuslampko
		 */
		private $statuslampko;

		/**
		 * Class attribute for mapping table field statusLampON
		 * Comment for field statusLampON: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statuslampon
		 */
		private $statuslampon;

		/**
		 * Class attribute for mapping table field statusOutOfLimits
		 * Comment for field statusOutOfLimits: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statusoutoflimits
		 */
		private $statusoutoflimits;

		/**
		 * Class attribute for mapping table field statusChangeOn
		 * Comment for field statusChangeOn: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statuschangeon
		 */
		private $statuschangeon;

		/**
		 * Class attribute for mapping table field statusReset
		 * Comment for field statusReset: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statusreset
		 */
		private $statusreset;

		/**
		 * Class attribute for mapping table field statusNoAddress
		 * Comment for field statusNoAddress: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statusnoaddress
		 */
		private $statusnoaddress;

		/**
		 * Class attribute for mapping table field statusPowerFailure
		 * Comment for field statusPowerFailure: Not specified.<br>
		 * Field information:
		 *  - Data type: tinyint(4)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var int $statuspowerfailure
		 */
		private $statuspowerfailure;

		/**
		 * Class attribute for storing the SQL DDL of table device
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBkZXZpY2VgICgKICBgaWREZXZpY2VgIGludCgxMSkgTk9UIE5VTEwgQVVUT19JTkNSRU1FTlQsCiAgYERldmljZVN0YXR1c19pZERldmljZVN0YXR1c2AgaW50KDExKSBOT1QgTlVMTCwKICBgYWR2YCBzbWFsbGludCg2KSBERUZBVUxUIE5VTEwsCiAgYGFkdk1pbmAgc21hbGxpbnQoNikgREVGQVVMVCBOVUxMLAogIGBhZHZNYXhgIHNtYWxsaW50KDYpIERFRkFVTFQgTlVMTCwKICBgcmVzcG9uc2VgIHNtYWxsaW50KDYpIERFRkFVTFQgTlVMTCwKICBgdmVyc2lvbmAgc21hbGxpbnQoNikgREVGQVVMVCBOVUxMLAogIGBEZXZpY2VUeXBlX2lkRGV2aWNlVHlwZWAgaW50KDExKSBOT1QgTlVMTCwKICBgc3RhdHVzQmFsbGFzdEtPYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgc3RhdHVzTGFtcEtPYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgc3RhdHVzTGFtcE9OYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgc3RhdHVzT3V0T2ZMaW1pdHNgIHRpbnlpbnQoNCkgREVGQVVMVCBOVUxMLAogIGBzdGF0dXNDaGFuZ2VPbmAgdGlueWludCg0KSBERUZBVUxUIE5VTEwsCiAgYHN0YXR1c1Jlc2V0YCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgc3RhdHVzTm9BZGRyZXNzYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBgc3RhdHVzUG93ZXJGYWlsdXJlYCB0aW55aW50KDQpIERFRkFVTFQgTlVMTCwKICBQUklNQVJZIEtFWSAoYGlkRGV2aWNlYCksCiAgS0VZIGBma19EZXZpY2VfRGV2aWNlVHlwZTFfaWR4YCAoYERldmljZVR5cGVfaWREZXZpY2VUeXBlYCksCiAgQ09OU1RSQUlOVCBgZmtfRGV2aWNlX0RldmljZVR5cGUxYCBGT1JFSUdOIEtFWSAoYERldmljZVR5cGVfaWREZXZpY2VUeXBlYCkgUkVGRVJFTkNFUyBgZGV2aWNldHlwZWAgKGBpZERldmljZVR5cGVgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04KKSBFTkdJTkU9SW5ub0RCIERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setIddevice Sets the class attribute iddevice with a given value
		 * The attribute iddevice maps the field idDevice defined as int(11).<br>
		 * Comment for field idDevice: Not specified.<br>
		 *
		 * @param int $iddevice
		 *
		 * @category Modifier
		 */
		public function setIddevice($iddevice){
			$this->iddevice = (int)$iddevice;
		}

		/**
		 * setDevicestatusIddevicestatus Sets the class attribute devicestatusIddevicestatus with a given value
		 * The attribute devicestatusIddevicestatus maps the field DeviceStatus_idDeviceStatus defined as int(11).<br>
		 * Comment for field DeviceStatus_idDeviceStatus: Not specified.<br>
		 *
		 * @param int $devicestatusIddevicestatus
		 *
		 * @category Modifier
		 */
		public function setDevicestatusIddevicestatus($devicestatusIddevicestatus){
			$this->devicestatusIddevicestatus = (int)$devicestatusIddevicestatus;
		}

		/**
		 * setAdv Sets the class attribute adv with a given value
		 * The attribute adv maps the field adv defined as smallint(6).<br>
		 * Comment for field adv: Not specified.<br>
		 *
		 * @param int $adv
		 *
		 * @category Modifier
		 */
		public function setAdv($adv){
			$this->adv = (int)$adv;
		}

		/**
		 * setAdvmin Sets the class attribute advmin with a given value
		 * The attribute advmin maps the field advMin defined as smallint(6).<br>
		 * Comment for field advMin: Not specified.<br>
		 *
		 * @param int $advmin
		 *
		 * @category Modifier
		 */
		public function setAdvmin($advmin){
			$this->advmin = (int)$advmin;
		}

		/**
		 * setAdvmax Sets the class attribute advmax with a given value
		 * The attribute advmax maps the field advMax defined as smallint(6).<br>
		 * Comment for field advMax: Not specified.<br>
		 *
		 * @param int $advmax
		 *
		 * @category Modifier
		 */
		public function setAdvmax($advmax){
			$this->advmax = (int)$advmax;
		}

		/**
		 * setResponse Sets the class attribute response with a given value
		 * The attribute response maps the field response defined as smallint(6).<br>
		 * Comment for field response: Not specified.<br>
		 *
		 * @param int $response
		 *
		 * @category Modifier
		 */
		public function setResponse($response){
			$this->response = (int)$response;
		}

		/**
		 * setVersion Sets the class attribute version with a given value
		 * The attribute version maps the field version defined as smallint(6).<br>
		 * Comment for field version: Not specified.<br>
		 *
		 * @param int $version
		 *
		 * @category Modifier
		 */
		public function setVersion($version){
			$this->version = (int)$version;
		}

		/**
		 * setDevicetypeIddevicetype Sets the class attribute devicetypeIddevicetype with a given value
		 * The attribute devicetypeIddevicetype maps the field DeviceType_idDeviceType defined as int(11).<br>
		 * Comment for field DeviceType_idDeviceType: Not specified.<br>
		 *
		 * @param int $devicetypeIddevicetype
		 *
		 * @category Modifier
		 */
		public function setDevicetypeIddevicetype($devicetypeIddevicetype){
			$this->devicetypeIddevicetype = (int)$devicetypeIddevicetype;
		}

		/**
		 * setStatusballastko Sets the class attribute statusballastko with a given value
		 * The attribute statusballastko maps the field statusBallastKO defined as tinyint(4).<br>
		 * Comment for field statusBallastKO: Not specified.<br>
		 *
		 * @param int $statusballastko
		 *
		 * @category Modifier
		 */
		public function setStatusballastko($statusballastko){
			$this->statusballastko = (int)$statusballastko;
		}

		/**
		 * setStatuslampko Sets the class attribute statuslampko with a given value
		 * The attribute statuslampko maps the field statusLampKO defined as tinyint(4).<br>
		 * Comment for field statusLampKO: Not specified.<br>
		 *
		 * @param int $statuslampko
		 *
		 * @category Modifier
		 */
		public function setStatuslampko($statuslampko){
			$this->statuslampko = (int)$statuslampko;
		}

		/**
		 * setStatuslampon Sets the class attribute statuslampon with a given value
		 * The attribute statuslampon maps the field statusLampON defined as tinyint(4).<br>
		 * Comment for field statusLampON: Not specified.<br>
		 *
		 * @param int $statuslampon
		 *
		 * @category Modifier
		 */
		public function setStatuslampon($statuslampon){
			$this->statuslampon = (int)$statuslampon;
		}

		/**
		 * setStatusoutoflimits Sets the class attribute statusoutoflimits with a given value
		 * The attribute statusoutoflimits maps the field statusOutOfLimits defined as tinyint(4).<br>
		 * Comment for field statusOutOfLimits: Not specified.<br>
		 *
		 * @param int $statusoutoflimits
		 *
		 * @category Modifier
		 */
		public function setStatusoutoflimits($statusoutoflimits){
			$this->statusoutoflimits = (int)$statusoutoflimits;
		}

		/**
		 * setStatuschangeon Sets the class attribute statuschangeon with a given value
		 * The attribute statuschangeon maps the field statusChangeOn defined as tinyint(4).<br>
		 * Comment for field statusChangeOn: Not specified.<br>
		 *
		 * @param int $statuschangeon
		 *
		 * @category Modifier
		 */
		public function setStatuschangeon($statuschangeon){
			$this->statuschangeon = (int)$statuschangeon;
		}

		/**
		 * setStatusreset Sets the class attribute statusreset with a given value
		 * The attribute statusreset maps the field statusReset defined as tinyint(4).<br>
		 * Comment for field statusReset: Not specified.<br>
		 *
		 * @param int $statusreset
		 *
		 * @category Modifier
		 */
		public function setStatusreset($statusreset){
			$this->statusreset = (int)$statusreset;
		}

		/**
		 * setStatusnoaddress Sets the class attribute statusnoaddress with a given value
		 * The attribute statusnoaddress maps the field statusNoAddress defined as tinyint(4).<br>
		 * Comment for field statusNoAddress: Not specified.<br>
		 *
		 * @param int $statusnoaddress
		 *
		 * @category Modifier
		 */
		public function setStatusnoaddress($statusnoaddress){
			$this->statusnoaddress = (int)$statusnoaddress;
		}

		/**
		 * setStatuspowerfailure Sets the class attribute statuspowerfailure with a given value
		 * The attribute statuspowerfailure maps the field statusPowerFailure defined as tinyint(4).<br>
		 * Comment for field statusPowerFailure: Not specified.<br>
		 *
		 * @param int $statuspowerfailure
		 *
		 * @category Modifier
		 */
		public function setStatuspowerfailure($statuspowerfailure){
			$this->statuspowerfailure = (int)$statuspowerfailure;
		}

		/**
		 * getIddevice gets the class attribute iddevice value
		 * The attribute iddevice maps the field idDevice defined as int(11).<br>
		 * Comment for field idDevice: Not specified.
		 * @return int $iddevice
		 * @category Accessor of $iddevice
		 */
		public function getIddevice(){
			return $this->iddevice;
		}

		/**
		 * getDevicestatusIddevicestatus gets the class attribute devicestatusIddevicestatus value
		 * The attribute devicestatusIddevicestatus maps the field DeviceStatus_idDeviceStatus defined as int(11).<br>
		 * Comment for field DeviceStatus_idDeviceStatus: Not specified.
		 * @return int $devicestatusIddevicestatus
		 * @category Accessor of $devicestatusIddevicestatus
		 */
		public function getDevicestatusIddevicestatus(){
			return $this->devicestatusIddevicestatus;
		}

		/**
		 * getAdv gets the class attribute adv value
		 * The attribute adv maps the field adv defined as smallint(6).<br>
		 * Comment for field adv: Not specified.
		 * @return int $adv
		 * @category Accessor of $adv
		 */
		public function getAdv(){
			return $this->adv;
		}

		/**
		 * getAdvmin gets the class attribute advmin value
		 * The attribute advmin maps the field advMin defined as smallint(6).<br>
		 * Comment for field advMin: Not specified.
		 * @return int $advmin
		 * @category Accessor of $advmin
		 */
		public function getAdvmin(){
			return $this->advmin;
		}

		/**
		 * getAdvmax gets the class attribute advmax value
		 * The attribute advmax maps the field advMax defined as smallint(6).<br>
		 * Comment for field advMax: Not specified.
		 * @return int $advmax
		 * @category Accessor of $advmax
		 */
		public function getAdvmax(){
			return $this->advmax;
		}

		/**
		 * getResponse gets the class attribute response value
		 * The attribute response maps the field response defined as smallint(6).<br>
		 * Comment for field response: Not specified.
		 * @return int $response
		 * @category Accessor of $response
		 */
		public function getResponse(){
			return $this->response;
		}

		/**
		 * getVersion gets the class attribute version value
		 * The attribute version maps the field version defined as smallint(6).<br>
		 * Comment for field version: Not specified.
		 * @return int $version
		 * @category Accessor of $version
		 */
		public function getVersion(){
			return $this->version;
		}

		/**
		 * getDevicetypeIddevicetype gets the class attribute devicetypeIddevicetype value
		 * The attribute devicetypeIddevicetype maps the field DeviceType_idDeviceType defined as int(11).<br>
		 * Comment for field DeviceType_idDeviceType: Not specified.
		 * @return int $devicetypeIddevicetype
		 * @category Accessor of $devicetypeIddevicetype
		 */
		public function getDevicetypeIddevicetype(){
			return $this->devicetypeIddevicetype;
		}

		/**
		 * getStatusballastko gets the class attribute statusballastko value
		 * The attribute statusballastko maps the field statusBallastKO defined as tinyint(4).<br>
		 * Comment for field statusBallastKO: Not specified.
		 * @return int $statusballastko
		 * @category Accessor of $statusballastko
		 */
		public function getStatusballastko(){
			return $this->statusballastko;
		}

		/**
		 * getStatuslampko gets the class attribute statuslampko value
		 * The attribute statuslampko maps the field statusLampKO defined as tinyint(4).<br>
		 * Comment for field statusLampKO: Not specified.
		 * @return int $statuslampko
		 * @category Accessor of $statuslampko
		 */
		public function getStatuslampko(){
			return $this->statuslampko;
		}

		/**
		 * getStatuslampon gets the class attribute statuslampon value
		 * The attribute statuslampon maps the field statusLampON defined as tinyint(4).<br>
		 * Comment for field statusLampON: Not specified.
		 * @return int $statuslampon
		 * @category Accessor of $statuslampon
		 */
		public function getStatuslampon(){
			return $this->statuslampon;
		}

		/**
		 * getStatusoutoflimits gets the class attribute statusoutoflimits value
		 * The attribute statusoutoflimits maps the field statusOutOfLimits defined as tinyint(4).<br>
		 * Comment for field statusOutOfLimits: Not specified.
		 * @return int $statusoutoflimits
		 * @category Accessor of $statusoutoflimits
		 */
		public function getStatusoutoflimits(){
			return $this->statusoutoflimits;
		}

		/**
		 * getStatuschangeon gets the class attribute statuschangeon value
		 * The attribute statuschangeon maps the field statusChangeOn defined as tinyint(4).<br>
		 * Comment for field statusChangeOn: Not specified.
		 * @return int $statuschangeon
		 * @category Accessor of $statuschangeon
		 */
		public function getStatuschangeon(){
			return $this->statuschangeon;
		}

		/**
		 * getStatusreset gets the class attribute statusreset value
		 * The attribute statusreset maps the field statusReset defined as tinyint(4).<br>
		 * Comment for field statusReset: Not specified.
		 * @return int $statusreset
		 * @category Accessor of $statusreset
		 */
		public function getStatusreset(){
			return $this->statusreset;
		}

		/**
		 * getStatusnoaddress gets the class attribute statusnoaddress value
		 * The attribute statusnoaddress maps the field statusNoAddress defined as tinyint(4).<br>
		 * Comment for field statusNoAddress: Not specified.
		 * @return int $statusnoaddress
		 * @category Accessor of $statusnoaddress
		 */
		public function getStatusnoaddress(){
			return $this->statusnoaddress;
		}

		/**
		 * getStatuspowerfailure gets the class attribute statuspowerfailure value
		 * The attribute statuspowerfailure maps the field statusPowerFailure defined as tinyint(4).<br>
		 * Comment for field statusPowerFailure: Not specified.
		 * @return int $statuspowerfailure
		 * @category Accessor of $statuspowerfailure
		 */
		public function getStatuspowerfailure(){
			return $this->statuspowerfailure;
		}

		/**
		 * Gets DDL SQL code of the table device
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "device";
		}

		/**
		 * The BeanDevice constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $iddevice is given.
		 *  - with a fetched data row from the table device having idDevice=$iddevice
		 *
		 * @param int $iddevice . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanDevice Object
		 */
		public function __construct($iddevice = null){
			parent::__construct();
			if (!empty($iddevice)){
				$this->select($iddevice);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of device into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $iddevice the primary key idDevice value of table device which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($iddevice){
			$sql = "SELECT * FROM device WHERE idDevice={$this->parseValue($iddevice,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->iddevice = (integer)$rowObject->idDevice;
				@$this->devicestatusIddevicestatus = (integer)$rowObject->DeviceStatus_idDeviceStatus;
				@$this->adv = (integer)$rowObject->adv;
				@$this->advmin = (integer)$rowObject->advMin;
				@$this->advmax = (integer)$rowObject->advMax;
				@$this->response = (integer)$rowObject->response;
				@$this->version = (integer)$rowObject->version;
				@$this->devicetypeIddevicetype = (integer)$rowObject->DeviceType_idDeviceType;
				@$this->statusballastko = (integer)$rowObject->statusBallastKO;
				@$this->statuslampko = (integer)$rowObject->statusLampKO;
				@$this->statuslampon = (integer)$rowObject->statusLampON;
				@$this->statusoutoflimits = (integer)$rowObject->statusOutOfLimits;
				@$this->statuschangeon = (integer)$rowObject->statusChangeOn;
				@$this->statusreset = (integer)$rowObject->statusReset;
				@$this->statusnoaddress = (integer)$rowObject->statusNoAddress;
				@$this->statuspowerfailure = (integer)$rowObject->statusPowerFailure;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table device
		 *
		 * @param int $iddevice the primary key idDevice value of table device which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($iddevice){
			$sql = "DELETE FROM device WHERE idDevice={$this->parseValue($iddevice,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of device
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->iddevice = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO device
            (DeviceStatus_idDeviceStatus,adv,advMin,advMax,response,version,DeviceType_idDeviceType,statusBallastKO,statusLampKO,statusLampON,statusOutOfLimits,statusChangeOn,statusReset,statusNoAddress,statusPowerFailure)
            VALUES(
			{$this->parseValue($this->devicestatusIddevicestatus)},
			{$this->parseValue($this->adv)},
			{$this->parseValue($this->advmin)},
			{$this->parseValue($this->advmax)},
			{$this->parseValue($this->response)},
			{$this->parseValue($this->version)},
			{$this->parseValue($this->devicetypeIddevicetype)},
			{$this->parseValue($this->statusballastko)},
			{$this->parseValue($this->statuslampko)},
			{$this->parseValue($this->statuslampon)},
			{$this->parseValue($this->statusoutoflimits)},
			{$this->parseValue($this->statuschangeon)},
			{$this->parseValue($this->statusreset)},
			{$this->parseValue($this->statusnoaddress)},
			{$this->parseValue($this->statuspowerfailure)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->iddevice = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table device with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $iddevice the primary key idDevice value of table device which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($iddevice){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                device
            SET 
				DeviceStatus_idDeviceStatus={$this->parseValue($this->devicestatusIddevicestatus)},
				adv={$this->parseValue($this->adv)},
				advMin={$this->parseValue($this->advmin)},
				advMax={$this->parseValue($this->advmax)},
				response={$this->parseValue($this->response)},
				version={$this->parseValue($this->version)},
				DeviceType_idDeviceType={$this->parseValue($this->devicetypeIddevicetype)},
				statusBallastKO={$this->parseValue($this->statusballastko)},
				statusLampKO={$this->parseValue($this->statuslampko)},
				statusLampON={$this->parseValue($this->statuslampon)},
				statusOutOfLimits={$this->parseValue($this->statusoutoflimits)},
				statusChangeOn={$this->parseValue($this->statuschangeon)},
				statusReset={$this->parseValue($this->statusreset)},
				statusNoAddress={$this->parseValue($this->statusnoaddress)},
				statusPowerFailure={$this->parseValue($this->statuspowerfailure)}
            WHERE
                idDevice={$this->parseValue($iddevice, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($iddevice);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of device previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->iddevice != ""){
				return $this->update($this->iddevice);
			}else{
				return false;
			}
		}

		/**
		 * Facility for linking groups to device.
		 * @category DML Helper
		 * @return none
		 * @param chr $groupA
		 * @param chr $groupB
		 */
		public function node2groupLink($groupA, $groupB){
			$nodo_6_Ord = ord($groupA);//converto il carattere in decimale
			$groupA_bin = sprintf('%08b', $nodo_6_Ord); //creo una stringa binaria
			$nodo_7_Ord = ord($groupB);//converto il carattere in decimale
			$groupB_bin = sprintf('%08b', $nodo_7_Ord); //creo una stringa binaria

			groupLink($groupA_bin.$groupB_bin);
		}

		/**
		 * Facility for linking groups to device.
		 * @category DML Helper
		 * @return none
		 * @param string $group
		 */
		public function groupLink($group){
			$array = str_split($group);

			foreach ($array as $isInGroup) {
				$beanDeviceHasGroup = new BeanDeviceHasGroup();
				$beanDeviceHasGroup->setDeviceIddevice($this->getIddevice());
				$beanDeviceHasGroup->setGroupIdgroup($isInGroup);
				$beanDeviceHasGroup->insert();
				$beanDeviceHasGroup->close();
			}
		}

		/**
		 * Facility for conversion from node[0] to device status.
		 * @category DML Helper
		 * @return none
		 * @param chr $nodo
		 */
		public function nodoStatus2beanStatus($nodo_0){
			$nodo_0_Ord = ord($nodo_0);//converto il carattere in decimale
			$stato_bin = sprintf('%08b', $nodo_0_Ord); //creo una stringa binaria

			$this->setStatusballastko( $stato_bin[0]);
			$this->setStatuslampko( $stato_bin[1]);
			$this->setStatuslampon( $stato_bin[2]);
			$this->setStatusoutoflimits( $stato_bin[3]);
			$this->setStatuschangeon( $stato_bin[4]);
			$this->setStatusreset( $stato_bin[5]);
			$this->setStatusnoaddress( $stato_bin[6]);
			$this->setStatuspowerfailure( $stato_bin[7]);
		}

		/**
		 * Facility for conversion from node to device.
		 * @category DML Helper
		 * @return none
		 * @param array $nodo
		 */
		public function nodo2bean($nodo){
			$beanDevice = new BeanDevice();
			//n/a      = `idDevice` INT(11) GENERATED ALWAYS AS () VIRTUAL,
			//$nodo[0] = `DeviceStatus_idDeviceStatus` INT(11) NOT NULL,
			//$nodo[1] = `adv` SMALLINT(6) NULL DEFAULT NULL,
			//$nodo[4] = `advMin` SMALLINT(6) NULL DEFAULT NULL,
			//$nodo[5] = `advMax` SMALLINT(6) NULL DEFAULT NULL,
			//$nodo[2] = `response` SMALLINT(6) NULL DEFAULT NULL,
			//$nodo[?] = `version` SMALLINT(6) NULL DEFAULT NULL,
			//$nodo[3] = `DeviceType_idDeviceType` INT(11) NOT NULL,

			$this->nodoStatus2beanStatus($nodo[0]);

			$this->setAdv(ord($nodo[1]));// = chr(254); // 0 = spento, 1 = acceso al minimo, 254 = acceso al massimo, 255 valore non valido
			$this->setResponse(ord($nodo[2])); // response - chr
			$this->setDevicetypeIddevicetype(ord($nodo[3])); //device type - chr
			$this->setAdvmin(ord($nodo[4]));// = chr(0);                                // minimo valore = 0
			$this->setAdvmax(ord($nodo[5]));// = chr(254);                            // massimo valore = 254

			//gruppi
			//$nodo[6] = chr(rand(0, 255));                    // viene generato un numero random
			//$nodo[7] = chr(rand(0, 255));                    // viene generato un numero random

			//creo struttura gruppo associata al nodo
			$this->node2groupLink($nodo[6],$nodo[7]);

			// FUNZIONALITA' ESTESE
			//$nodo[8] = chr(254);                            // Power On Level, 254, no change, 1 – 254 [ description, default value, reset value, range ]
			//$nodo[9] = chr(254);                            // System Failure Level, 254, no change, 0 – 255 (mask)
			//$nodo[10] = chr(7);                                // Fade Rate, 7 (45 steps/sec), no change, 1 – 15
			//$nodo[11] = chr(0);                                // Fade Time, 0 (no fade), no change, 0 – 15
			//$nodo[12] = chr(255);                            // Short Address, 255 (no address), no change, 0 – 63, 255 (mask)
			//$nodo[13] = chr(255);                            // Search Address Byte 1, 0xFF, 0xFF, 0 – 0xFF
			//$nodo[14] = chr(255);                            // Search Address Byte 2, 0xFF, 0xFF, 0 – 0xFF
			//$nodo[15] = chr(255);                            // Search Address Byte 3, 0xFF, 0xFF, 0 – 0xFF
			//$nodo[16] = chr(255);                            // Random Address Byte 1, 0xFF, no change, 0 – 0xFF
			//$nodo[17] = chr(255);                            // Random Address Byte 2, 0xFF, no change, 0 – 0xFF
			//$nodo[18] = chr(255);                            // Random Address Byte 3, 0xFF, no change, 0 – 0xFF
			//$nodo[19] = chr(0);                                // Physical Min Level, Factory burn-in, Factory burn-in, 1 – 254

			return $beanDevice;
		}

		/**
		 * Facility for conversion from device to nodo structure.
		 * @category DML Helper
		 * @return nodo
		 * @param none
		 */
		public function bean2nodo(){
			$nodo = str_repeat(chr(0), 20);

			$nodo[0] = $this->recupera_stato_nodo_DALI();    // Lo stato dovrebbe essere composto da 8 bit del tipo: 0?100???

			$nodo[1] = chr($this->getAdv());
			$nodo[2] = chr($this->getResponse());
			$nodo[3] = chr($this->getDevicetypeIddevicetype());

			$nodo[4] = chr(0);                                // minimo valore = 0
			$nodo[5] = chr(254);                            // massimo valore = 254
			$nodo[6] = chr(rand(0, 255));                    // viene generato un numero random
			$nodo[7] = chr(rand(0, 255));                    // viene generato un numero random
			// FUNZIONALITA' ESTESE
			$nodo[8] = chr(254);                            // Power On Level, 254, no change, 1 – 254 [ description, default value, reset value, range ]
			$nodo[9] = chr(254);                            // System Failure Level, 254, no change, 0 – 255 (mask)
			$nodo[10] = chr(7);                                // Fade Rate, 7 (45 steps/sec), no change, 1 – 15
			$nodo[11] = chr(0);                                // Fade Time, 0 (no fade), no change, 0 – 15
			$nodo[12] = chr(255);                            // Short Address, 255 (no address), no change, 0 – 63, 255 (mask)
			$nodo[13] = chr(255);                            // Search Address Byte 1, 0xFF, 0xFF, 0 – 0xFF
			$nodo[14] = chr(255);                            // Search Address Byte 2, 0xFF, 0xFF, 0 – 0xFF
			$nodo[15] = chr(255);                            // Search Address Byte 3, 0xFF, 0xFF, 0 – 0xFF
			$nodo[16] = chr(255);                            // Random Address Byte 1, 0xFF, no change, 0 – 0xFF
			$nodo[17] = chr(255);                            // Random Address Byte 2, 0xFF, no change, 0 – 0xFF
			$nodo[18] = chr(255);                            // Random Address Byte 3, 0xFF, no change, 0 – 0xFF
			$nodo[19] = chr(0);                                // Physical Min Level, Factory burn-in, Factory burn-in, 1 – 254


			return $nodo;






			$beanDevice = new BeanDevice();

			$nodo_0_Ord = ord($nodo[0]);//converto il carattere in decimale
			$stato_bin = sprintf('%08b', $nodo_0_Ord); //creo una stringa binaria

			$this->setStatusballastko( $stato_bin[0]);
			$this->setStatuslampko( $stato_bin[1]);
			$this->setStatuslampon( $stato_bin[2]);
			$this->setStatusoutoflimits( $stato_bin[3]);
			$this->setStatuschangeon( $stato_bin[4]);
			$this->setStatusreset( $stato_bin[5]);
			$this->setStatusnoaddress( $stato_bin[6]);
			$this->setStatuspowerfailure( $stato_bin[7]);


			$this->setDevicetypeIddevicetype(ord($nodo[3])); //device type - chr
			$this->setAdvmin($nodo[4]);// = chr(0);                                // minimo valore = 0
			$this->setAdvmax($nodo[5]);// = chr(254);                            // massimo valore = 254

			//gruppi
			//$nodo[6] = chr(rand(0, 255));                    // viene generato un numero random
			//$nodo[7] = chr(rand(0, 255));                    // viene generato un numero random

			//creo struttura gruppo associata al nodo
			$this->node2groupLink($nodo[6],$nodo[7]);

			// FUNZIONALITA' ESTESE
			//$nodo[8] = chr(254);                            // Power On Level, 254, no change, 1 – 254 [ description, default value, reset value, range ]
			//$nodo[9] = chr(254);                            // System Failure Level, 254, no change, 0 – 255 (mask)
			//$nodo[10] = chr(7);                                // Fade Rate, 7 (45 steps/sec), no change, 1 – 15
			//$nodo[11] = chr(0);                                // Fade Time, 0 (no fade), no change, 0 – 15
			//$nodo[12] = chr(255);                            // Short Address, 255 (no address), no change, 0 – 63, 255 (mask)
			//$nodo[13] = chr(255);                            // Search Address Byte 1, 0xFF, 0xFF, 0 – 0xFF
			//$nodo[14] = chr(255);                            // Search Address Byte 2, 0xFF, 0xFF, 0 – 0xFF
			//$nodo[15] = chr(255);                            // Search Address Byte 3, 0xFF, 0xFF, 0 – 0xFF
			//$nodo[16] = chr(255);                            // Random Address Byte 1, 0xFF, no change, 0 – 0xFF
			//$nodo[17] = chr(255);                            // Random Address Byte 2, 0xFF, no change, 0 – 0xFF
			//$nodo[18] = chr(255);                            // Random Address Byte 3, 0xFF, no change, 0 – 0xFF
			//$nodo[19] = chr(0);                                // Physical Min Level, Factory burn-in, Factory burn-in, 1 – 254

			return $beanDevice;
		}

	}

?>
