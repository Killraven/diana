<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanMeasure extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idmeasure of table measure
		 * Comment for field idmeasure: Not specified<br>
		 * @var int $idmeasure
		 */
		private $idmeasure;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field timestamp
		 * Comment for field timestamp: Not specified.<br>
		 * Field information:
		 *  - Data type: datetime
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default: CURRENT_TIMESTAMP
		 *  - Extra:
		 * @var string $timestamp
		 */
		private $timestamp;

		/**
		 * Class attribute for mapping table field value
		 * Comment for field value: Not specified.<br>
		 * Field information:
		 *  - Data type: float
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var float $value
		 */
		private $value;

		/**
		 * Class attribute for mapping table field unitofmeasurement_idunitOfMeasurement
		 * Comment for field unitofmeasurement_idunitOfMeasurement: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $unitofmeasurementIdunitofmeasurement
		 */
		private $unitofmeasurementIdunitofmeasurement;

		/**
		 * Class attribute for mapping table field sensorType_idsensorType
		 * Comment for field sensorType_idsensorType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $sensortypeIdsensortype
		 */
		private $sensortypeIdsensortype;

		/**
		 * Class attribute for mapping table field sensor_idsensor
		 * Comment for field sensor_idsensor: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $sensorIdsensor
		 */
		private $sensorIdsensor;

		/**
		 * Class attribute for storing the SQL DDL of table measure
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBtZWFzdXJlYCAoCiAgYGlkbWVhc3VyZWAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgdGltZXN0YW1wYCBkYXRldGltZSBERUZBVUxUIENVUlJFTlRfVElNRVNUQU1QLAogIGB2YWx1ZWAgZmxvYXQgREVGQVVMVCBOVUxMLAogIGB1bml0b2ZtZWFzdXJlbWVudF9pZHVuaXRPZk1lYXN1cmVtZW50YCBpbnQoMTEpIE5PVCBOVUxMLAogIGBzZW5zb3JUeXBlX2lkc2Vuc29yVHlwZWAgaW50KDExKSBOT1QgTlVMTCwKICBgc2Vuc29yX2lkc2Vuc29yYCBpbnQoMTEpIE5PVCBOVUxMLAogIFBSSU1BUlkgS0VZIChgaWRtZWFzdXJlYCksCiAgS0VZIGBma19tZWFzdXJlX3VuaXRvZm1lYXN1cmVtZW50MV9pZHhgIChgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudGApLAogIEtFWSBgZmtfbWVhc3VyZV9zZW5zb3JUeXBlMV9pZHhgIChgc2Vuc29yVHlwZV9pZHNlbnNvclR5cGVgKSwKICBLRVkgYGZrX21lYXN1cmVfc2Vuc29yMV9pZHhgIChgc2Vuc29yX2lkc2Vuc29yYCksCiAgQ09OU1RSQUlOVCBgZmtfbWVhc3VyZV9zZW5zb3IxYCBGT1JFSUdOIEtFWSAoYHNlbnNvcl9pZHNlbnNvcmApIFJFRkVSRU5DRVMgYHNlbnNvcmAgKGBpZHNlbnNvcmApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTiwKICBDT05TVFJBSU5UIGBma19tZWFzdXJlX3NlbnNvclR5cGUxYCBGT1JFSUdOIEtFWSAoYHNlbnNvclR5cGVfaWRzZW5zb3JUeXBlYCkgUkVGRVJFTkNFUyBgc2Vuc29ydHlwZWAgKGBpZHNlbnNvclR5cGVgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04sCiAgQ09OU1RSQUlOVCBgZmtfbWVhc3VyZV91bml0b2ZtZWFzdXJlbWVudDFgIEZPUkVJR04gS0VZIChgdW5pdG9mbWVhc3VyZW1lbnRfaWR1bml0T2ZNZWFzdXJlbWVudGApIFJFRkVSRU5DRVMgYHVuaXRvZm1lYXN1cmVtZW50YCAoYGlkdW5pdE9mTWVhc3VyZW1lbnRgKSBPTiBERUxFVEUgTk8gQUNUSU9OIE9OIFVQREFURSBOTyBBQ1RJT04KKSBFTkdJTkU9SW5ub0RCIEFVVE9fSU5DUkVNRU5UPTMxNDUgREVGQVVMVCBDSEFSU0VUPXV0Zjg=";

		/**
		 * setIdmeasure Sets the class attribute idmeasure with a given value
		 * The attribute idmeasure maps the field idmeasure defined as int(11).<br>
		 * Comment for field idmeasure: Not specified.<br>
		 *
		 * @param int $idmeasure
		 *
		 * @category Modifier
		 */
		public function setIdmeasure($idmeasure){
			$this->idmeasure = (int)$idmeasure;
		}

		/**
		 * setTimestamp Sets the class attribute timestamp with a given value
		 * The attribute timestamp maps the field timestamp defined as datetime.<br>
		 * Comment for field timestamp: Not specified.<br>
		 *
		 * @param string $timestamp
		 *
		 * @category Modifier
		 */
		public function setTimestamp($timestamp){
			$this->timestamp = (string)$timestamp;
		}

		/**
		 * setValue Sets the class attribute value with a given value
		 * The attribute value maps the field value defined as float.<br>
		 * Comment for field value: Not specified.<br>
		 *
		 * @param float $value
		 *
		 * @category Modifier
		 */
		public function setValue($value){
			$this->value = (float)$value;
		}

		/**
		 * setUnitofmeasurementIdunitofmeasurement Sets the class attribute unitofmeasurementIdunitofmeasurement with a given value
		 * The attribute unitofmeasurementIdunitofmeasurement maps the field unitofmeasurement_idunitOfMeasurement defined as int(11).<br>
		 * Comment for field unitofmeasurement_idunitOfMeasurement: Not specified.<br>
		 *
		 * @param int $unitofmeasurementIdunitofmeasurement
		 *
		 * @category Modifier
		 */
		public function setUnitofmeasurementIdunitofmeasurement($unitofmeasurementIdunitofmeasurement){
			$this->unitofmeasurementIdunitofmeasurement = (int)$unitofmeasurementIdunitofmeasurement;
		}

		/**
		 * setSensortypeIdsensortype Sets the class attribute sensortypeIdsensortype with a given value
		 * The attribute sensortypeIdsensortype maps the field sensorType_idsensorType defined as int(11).<br>
		 * Comment for field sensorType_idsensorType: Not specified.<br>
		 *
		 * @param int $sensortypeIdsensortype
		 *
		 * @category Modifier
		 */
		public function setSensortypeIdsensortype($sensortypeIdsensortype){
			$this->sensortypeIdsensortype = (int)$sensortypeIdsensortype;
		}

		/**
		 * setSensorIdsensor Sets the class attribute sensorIdsensor with a given value
		 * The attribute sensorIdsensor maps the field sensor_idsensor defined as int(11).<br>
		 * Comment for field sensor_idsensor: Not specified.<br>
		 *
		 * @param int $sensorIdsensor
		 *
		 * @category Modifier
		 */
		public function setSensorIdsensor($sensorIdsensor){
			$this->sensorIdsensor = (int)$sensorIdsensor;
		}

		/**
		 * getIdmeasure gets the class attribute idmeasure value
		 * The attribute idmeasure maps the field idmeasure defined as int(11).<br>
		 * Comment for field idmeasure: Not specified.
		 * @return int $idmeasure
		 * @category Accessor of $idmeasure
		 */
		public function getIdmeasure(){
			return $this->idmeasure;
		}

		/**
		 * getTimestamp gets the class attribute timestamp value
		 * The attribute timestamp maps the field timestamp defined as datetime.<br>
		 * Comment for field timestamp: Not specified.
		 * @return string $timestamp
		 * @category Accessor of $timestamp
		 */
		public function getTimestamp(){
			return $this->timestamp;
		}

		/**
		 * getValue gets the class attribute value value
		 * The attribute value maps the field value defined as float.<br>
		 * Comment for field value: Not specified.
		 * @return float $value
		 * @category Accessor of $value
		 */
		public function getValue(){
			return $this->value;
		}

		/**
		 * getUnitofmeasurementIdunitofmeasurement gets the class attribute unitofmeasurementIdunitofmeasurement value
		 * The attribute unitofmeasurementIdunitofmeasurement maps the field unitofmeasurement_idunitOfMeasurement defined as int(11).<br>
		 * Comment for field unitofmeasurement_idunitOfMeasurement: Not specified.
		 * @return int $unitofmeasurementIdunitofmeasurement
		 * @category Accessor of $unitofmeasurementIdunitofmeasurement
		 */
		public function getUnitofmeasurementIdunitofmeasurement(){
			return $this->unitofmeasurementIdunitofmeasurement;
		}

		/**
		 * getSensortypeIdsensortype gets the class attribute sensortypeIdsensortype value
		 * The attribute sensortypeIdsensortype maps the field sensorType_idsensorType defined as int(11).<br>
		 * Comment for field sensorType_idsensorType: Not specified.
		 * @return int $sensortypeIdsensortype
		 * @category Accessor of $sensortypeIdsensortype
		 */
		public function getSensortypeIdsensortype(){
			return $this->sensortypeIdsensortype;
		}

		/**
		 * getSensorIdsensor gets the class attribute sensorIdsensor value
		 * The attribute sensorIdsensor maps the field sensor_idsensor defined as int(11).<br>
		 * Comment for field sensor_idsensor: Not specified.
		 * @return int $sensorIdsensor
		 * @category Accessor of $sensorIdsensor
		 */
		public function getSensorIdsensor(){
			return $this->sensorIdsensor;
		}

		/**
		 * Gets DDL SQL code of the table measure
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "measure";
		}

		/**
		 * The BeanMeasure constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idmeasure is given.
		 *  - with a fetched data row from the table measure having idmeasure=$idmeasure
		 *
		 * @param int $idmeasure . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanMeasure Object
		 */
		public function __construct($idmeasure = null){
			parent::__construct();
			if (!empty($idmeasure)){
				$this->select($idmeasure);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of measure into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idmeasure the primary key idmeasure value of table measure which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idmeasure){
			$sql = "SELECT * FROM measure WHERE idmeasure={$this->parseValue($idmeasure,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idmeasure = (integer)$rowObject->idmeasure;
				@$this->timestamp = empty($rowObject->timestamp) ? null : date(FETCHED_DATETIME_FORMAT, strtotime($rowObject->timestamp));
				@$this->value = (float)$rowObject->value;
				@$this->unitofmeasurementIdunitofmeasurement = (integer)$rowObject->unitofmeasurement_idunitOfMeasurement;
				@$this->sensortypeIdsensortype = (integer)$rowObject->sensorType_idsensorType;
				@$this->sensorIdsensor = (integer)$rowObject->sensor_idsensor;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table measure
		 *
		 * @param int $idmeasure the primary key idmeasure value of table measure which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idmeasure){
			$sql = "DELETE FROM measure WHERE idmeasure={$this->parseValue($idmeasure,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of measure
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idmeasure = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO measure
            (timestamp,value,unitofmeasurement_idunitOfMeasurement,sensorType_idsensorType,sensor_idsensor)
            VALUES(
			{$this->parseValue($this->timestamp, 'datetime')},
			{$this->parseValue($this->value)},
			{$this->parseValue($this->unitofmeasurementIdunitofmeasurement)},
			{$this->parseValue($this->sensortypeIdsensortype)},
			{$this->parseValue($this->sensorIdsensor)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idmeasure = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table measure with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idmeasure the primary key idmeasure value of table measure which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idmeasure){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                measure
            SET 
				timestamp={$this->parseValue($this->timestamp, 'datetime')},
				value={$this->parseValue($this->value)},
				unitofmeasurement_idunitOfMeasurement={$this->parseValue($this->unitofmeasurementIdunitofmeasurement)},
				sensorType_idsensorType={$this->parseValue($this->sensortypeIdsensortype)},
				sensor_idsensor={$this->parseValue($this->sensorIdsensor)}
            WHERE
                idmeasure={$this->parseValue($idmeasure, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idmeasure);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of measure previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idmeasure != ""){
				return $this->update($this->idmeasure);
			}else{
				return false;
			}
		}

	}

?>
