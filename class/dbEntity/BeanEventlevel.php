<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanEventlevel extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key id of table eventlevel
		 * Comment for field id: Not specified<br>
		 * @var int $id
		 */
		private $id;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field name
		 * Comment for field name: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $name
		 */
		private $name;

		/**
		 * Class attribute for mapping table field description
		 * Comment for field description: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(200)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $description
		 */
		private $description;

		/**
		 * Class attribute for storing the SQL DDL of table eventlevel
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBldmVudGxldmVsYCAoCiAgYGlkYCBpbnQoMTEpIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULAogIGBuYW1lYCB2YXJjaGFyKDQ1KSBERUZBVUxUIE5VTEwsCiAgYGRlc2NyaXB0aW9uYCB2YXJjaGFyKDIwMCkgREVGQVVMVCBOVUxMLAogIFBSSU1BUlkgS0VZIChgaWRgKSwKICBVTklRVUUgS0VZIGBpZF9VTklRVUVgIChgaWRgKQopIEVOR0lORT1Jbm5vREIgQVVUT19JTkNSRU1FTlQ9NCBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setId Sets the class attribute id with a given value
		 * The attribute id maps the field id defined as int(11).<br>
		 * Comment for field id: Not specified.<br>
		 *
		 * @param int $id
		 *
		 * @category Modifier
		 */
		public function setId($id){
			$this->id = (int)$id;
		}

		/**
		 * setName Sets the class attribute name with a given value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.<br>
		 *
		 * @param string $name
		 *
		 * @category Modifier
		 */
		public function setName($name){
			$this->name = (string)$name;
		}

		/**
		 * setDescription Sets the class attribute description with a given value
		 * The attribute description maps the field description defined as varchar(200).<br>
		 * Comment for field description: Not specified.<br>
		 *
		 * @param string $description
		 *
		 * @category Modifier
		 */
		public function setDescription($description){
			$this->description = (string)$description;
		}

		/**
		 * getId gets the class attribute id value
		 * The attribute id maps the field id defined as int(11).<br>
		 * Comment for field id: Not specified.
		 * @return int $id
		 * @category Accessor of $id
		 */
		public function getId(){
			return $this->id;
		}

		/**
		 * getName gets the class attribute name value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.
		 * @return string $name
		 * @category Accessor of $name
		 */
		public function getName(){
			return $this->name;
		}

		/**
		 * getDescription gets the class attribute description value
		 * The attribute description maps the field description defined as varchar(200).<br>
		 * Comment for field description: Not specified.
		 * @return string $description
		 * @category Accessor of $description
		 */
		public function getDescription(){
			return $this->description;
		}

		/**
		 * Gets DDL SQL code of the table eventlevel
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "eventlevel";
		}

		/**
		 * The BeanEventlevel constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $id is given.
		 *  - with a fetched data row from the table eventlevel having id=$id
		 *
		 * @param int $id . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanEventlevel Object
		 */
		public function __construct($id = null){
			parent::__construct();
			if (!empty($id)){
				$this->select($id);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of eventlevel into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $id the primary key id value of table eventlevel which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($id){
			$sql = "SELECT * FROM eventlevel WHERE id={$this->parseValue($id,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->id = (integer)$rowObject->id;
				@$this->name = $this->replaceAposBackSlash($rowObject->name);
				@$this->description = $this->replaceAposBackSlash($rowObject->description);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table eventlevel
		 *
		 * @param int $id the primary key id value of table eventlevel which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($id){
			$sql = "DELETE FROM eventlevel WHERE id={$this->parseValue($id,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of eventlevel
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->id = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO eventlevel
            (name,description)
            VALUES(
			{$this->parseValue($this->name, 'notNumber')},
			{$this->parseValue($this->description, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->id = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table eventlevel with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $id the primary key id value of table eventlevel which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($id){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                eventlevel
            SET 
				name={$this->parseValue($this->name, 'notNumber')},
				description={$this->parseValue($this->description, 'notNumber')}
            WHERE
                id={$this->parseValue($id, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($id);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of eventlevel previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->id != ""){
				return $this->update($this->id);
			}else{
				return false;
			}
		}

	}

?>
