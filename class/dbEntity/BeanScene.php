<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanScene extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key idScene of table scene
		 * Comment for field idScene: Not specified<br>
		 * @var int $idscene
		 */
		private $idscene;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = false;

		/**
		 * Class attribute for mapping table field name
		 * Comment for field name: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $name
		 */
		private $name;

		/**
		 * Class attribute for storing the SQL DDL of table scene
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBzY2VuZWAgKAogIGBpZFNjZW5lYCBpbnQoMTEpIE5PVCBOVUxMLAogIGBuYW1lYCB2YXJjaGFyKDQ1KSBERUZBVUxUIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZFNjZW5lYCkKKSBFTkdJTkU9SW5ub0RCIERFRkFVTFQgQ0hBUlNFVD11dGY4";

		/**
		 * setIdscene Sets the class attribute idscene with a given value
		 * The attribute idscene maps the field idScene defined as int(11).<br>
		 * Comment for field idScene: Not specified.<br>
		 *
		 * @param int $idscene
		 *
		 * @category Modifier
		 */
		public function setIdscene($idscene){
			$this->idscene = (int)$idscene;
		}

		/**
		 * setName Sets the class attribute name with a given value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.<br>
		 *
		 * @param string $name
		 *
		 * @category Modifier
		 */
		public function setName($name){
			$this->name = (string)$name;
		}

		/**
		 * getIdscene gets the class attribute idscene value
		 * The attribute idscene maps the field idScene defined as int(11).<br>
		 * Comment for field idScene: Not specified.
		 * @return int $idscene
		 * @category Accessor of $idscene
		 */
		public function getIdscene(){
			return $this->idscene;
		}

		/**
		 * getName gets the class attribute name value
		 * The attribute name maps the field name defined as varchar(45).<br>
		 * Comment for field name: Not specified.
		 * @return string $name
		 * @category Accessor of $name
		 */
		public function getName(){
			return $this->name;
		}

		/**
		 * Gets DDL SQL code of the table scene
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "scene";
		}

		/**
		 * The BeanScene constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $idscene is given.
		 *  - with a fetched data row from the table scene having idScene=$idscene
		 *
		 * @param int $idscene . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanScene Object
		 */
		public function __construct($idscene = null){
			parent::__construct();
			if (!empty($idscene)){
				$this->select($idscene);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of scene into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idscene the primary key idScene value of table scene which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idscene){
			$sql = "SELECT * FROM scene WHERE idScene={$this->parseValue($idscene,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idscene = (integer)$rowObject->idScene;
				@$this->name = $this->replaceAposBackSlash($rowObject->name);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table scene
		 *
		 * @param int $idscene the primary key idScene value of table scene which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idscene){
			$sql = "DELETE FROM scene WHERE idScene={$this->parseValue($idscene,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of scene
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->idscene = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO scene
            (idScene,name)
            VALUES({$this->parseValue($this->idscene)},
			{$this->parseValue($this->name, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->idscene = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table scene with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idscene the primary key idScene value of table scene which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idscene){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                scene
            SET 
				name={$this->parseValue($this->name, 'notNumber')}
            WHERE
                idScene={$this->parseValue($idscene, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idscene);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of scene previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->idscene != ""){
				return $this->update($this->idscene);
			}else{
				return false;
			}
		}

	}

?>
