<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanLedmatrix extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping table field idLedMatrix
		 * Comment for field idLedMatrix: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $idledmatrix
		 */
		private $idledmatrix;

		/**
		 * Class attribute for mapping table field on
		 * Comment for field on: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default: 0
		 *  - Extra:
		 * @var string $on
		 */
		private $on;

		/**
		 * Class attribute for mapping table field off
		 * Comment for field off: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default: 0
		 *  - Extra:
		 * @var string $off
		 */
		private $off;

		/**
		 * Class attribute for mapping table field failure
		 * Comment for field failure: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(45)
		 *  - Null : YES
		 *  - DB Index:
		 *  - Default: 0
		 *  - Extra:
		 * @var string $failure
		 */
		private $failure;

		/**
		 * Class attribute for mapping table field DeviceType_idDeviceType
		 * Comment for field DeviceType_idDeviceType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: PRI
		 *  - Default:
		 *  - Extra:
		 * @var int $devicetypeIddevicetype
		 */
		private $devicetypeIddevicetype;

		/**
		 * Class attribute for storing the SQL DDL of table ledmatrix
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBsZWRtYXRyaXhgICgKICBgaWRMZWRNYXRyaXhgIGludCgxMSkgTk9UIE5VTEwsCiAgYG9uYCB2YXJjaGFyKDQ1KSBERUZBVUxUICcwJywKICBgb2ZmYCB2YXJjaGFyKDQ1KSBERUZBVUxUICcwJywKICBgZmFpbHVyZWAgdmFyY2hhcig0NSkgREVGQVVMVCAnMCcsCiAgYERldmljZVR5cGVfaWREZXZpY2VUeXBlYCBpbnQoMTEpIE5PVCBOVUxMLAogIFBSSU1BUlkgS0VZIChgaWRMZWRNYXRyaXhgLGBEZXZpY2VUeXBlX2lkRGV2aWNlVHlwZWApLAogIEtFWSBgZmtfTGVkTWF0cml4X0RldmljZVR5cGUxX2lkeGAgKGBEZXZpY2VUeXBlX2lkRGV2aWNlVHlwZWApLAogIENPTlNUUkFJTlQgYGZrX0xlZE1hdHJpeF9EZXZpY2VUeXBlMWAgRk9SRUlHTiBLRVkgKGBEZXZpY2VUeXBlX2lkRGV2aWNlVHlwZWApIFJFRkVSRU5DRVMgYGRldmljZXR5cGVgIChgaWREZXZpY2VUeXBlYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OCikgRU5HSU5FPUlubm9EQiBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIdledmatrix Sets the class attribute idledmatrix with a given value
		 * The attribute idledmatrix maps the field idLedMatrix defined as int(11).<br>
		 * Comment for field idLedMatrix: Not specified.<br>
		 *
		 * @param int $idledmatrix
		 *
		 * @category Modifier
		 */
		public function setIdledmatrix($idledmatrix){
			$this->idledmatrix = (int)$idledmatrix;
		}

		/**
		 * setOn Sets the class attribute on with a given value
		 * The attribute on maps the field on defined as varchar(45).<br>
		 * Comment for field on: Not specified.<br>
		 *
		 * @param string $on
		 *
		 * @category Modifier
		 */
		public function setOn($on){
			$this->on = (string)$on;
		}

		/**
		 * setOff Sets the class attribute off with a given value
		 * The attribute off maps the field off defined as varchar(45).<br>
		 * Comment for field off: Not specified.<br>
		 *
		 * @param string $off
		 *
		 * @category Modifier
		 */
		public function setOff($off){
			$this->off = (string)$off;
		}

		/**
		 * setFailure Sets the class attribute failure with a given value
		 * The attribute failure maps the field failure defined as varchar(45).<br>
		 * Comment for field failure: Not specified.<br>
		 *
		 * @param string $failure
		 *
		 * @category Modifier
		 */
		public function setFailure($failure){
			$this->failure = (string)$failure;
		}

		/**
		 * setDevicetypeIddevicetype Sets the class attribute devicetypeIddevicetype with a given value
		 * The attribute devicetypeIddevicetype maps the field DeviceType_idDeviceType defined as int(11).<br>
		 * Comment for field DeviceType_idDeviceType: Not specified.<br>
		 *
		 * @param int $devicetypeIddevicetype
		 *
		 * @category Modifier
		 */
		public function setDevicetypeIddevicetype($devicetypeIddevicetype){
			$this->devicetypeIddevicetype = (int)$devicetypeIddevicetype;
		}

		/**
		 * getIdledmatrix gets the class attribute idledmatrix value
		 * The attribute idledmatrix maps the field idLedMatrix defined as int(11).<br>
		 * Comment for field idLedMatrix: Not specified.
		 * @return int $idledmatrix
		 * @category Accessor of $idledmatrix
		 */
		public function getIdledmatrix(){
			return $this->idledmatrix;
		}

		/**
		 * getOn gets the class attribute on value
		 * The attribute on maps the field on defined as varchar(45).<br>
		 * Comment for field on: Not specified.
		 * @return string $on
		 * @category Accessor of $on
		 */
		public function getOn(){
			return $this->on;
		}

		/**
		 * getOff gets the class attribute off value
		 * The attribute off maps the field off defined as varchar(45).<br>
		 * Comment for field off: Not specified.
		 * @return string $off
		 * @category Accessor of $off
		 */
		public function getOff(){
			return $this->off;
		}

		/**
		 * getFailure gets the class attribute failure value
		 * The attribute failure maps the field failure defined as varchar(45).<br>
		 * Comment for field failure: Not specified.
		 * @return string $failure
		 * @category Accessor of $failure
		 */
		public function getFailure(){
			return $this->failure;
		}

		/**
		 * getDevicetypeIddevicetype gets the class attribute devicetypeIddevicetype value
		 * The attribute devicetypeIddevicetype maps the field DeviceType_idDeviceType defined as int(11).<br>
		 * Comment for field DeviceType_idDeviceType: Not specified.
		 * @return int $devicetypeIddevicetype
		 * @category Accessor of $devicetypeIddevicetype
		 */
		public function getDevicetypeIddevicetype(){
			return $this->devicetypeIddevicetype;
		}

		/**
		 * Gets DDL SQL code of the table ledmatrix
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "ledmatrix";
		}

		/**
		 * The BeanLedmatrix constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none ${ClassPkAttributeName} is given.
		 *  - with a fetched data row from the table {TableName} having {TablePkName}=${ClassPkAttributeName}
		 *
		 * @param int $idledmatrix
		 * @param int $devicetypeIddevicetype
		 *
		 * @return BeanLedmatrix Object
		 */
		public function __construct($idledmatrix = NULL, $devicetypeIddevicetype = NULL){
			parent::__construct();
			if (!empty($idledmatrix) && !empty($devicetypeIddevicetype)){
				$this->select($idledmatrix, $devicetypeIddevicetype);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of ledmatrix into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $idledmatrix
		 * @param int $devicetypeIddevicetype
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($idledmatrix, $devicetypeIddevicetype){
			$sql = "SELECT * FROM ledmatrix WHERE idLedMatrix={$this->parseValue($idledmatrix,'int')} AND DeviceType_idDeviceType={$this->parseValue($devicetypeIddevicetype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->idledmatrix = (integer)$rowObject->idLedMatrix;
				@$this->on = $this->replaceAposBackSlash($rowObject->on);
				@$this->off = $this->replaceAposBackSlash($rowObject->off);
				@$this->failure = $this->replaceAposBackSlash($rowObject->failure);
				@$this->devicetypeIddevicetype = (integer)$rowObject->DeviceType_idDeviceType;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table ledmatrix
		 *
		 * @param int $idledmatrix
		 * @param int $devicetypeIddevicetype
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($idledmatrix, $devicetypeIddevicetype){
			$sql = "DELETE FROM ledmatrix WHERE idLedMatrix={$this->parseValue($idledmatrix,'int')} AND DeviceType_idDeviceType={$this->parseValue($devicetypeIddevicetype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of ledmatrix
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			// $constants = get_defined_constants();
			$sql = <<< SQL
        INSERT INTO ledmatrix
        (idLedMatrix,on,off,failure,DeviceType_idDeviceType)
        VALUES({$this->parseValue($this->idledmatrix)},
			{$this->parseValue($this->on, 'notNumber')},
			{$this->parseValue($this->off, 'notNumber')},
			{$this->parseValue($this->failure, 'notNumber')},
			{$this->parseValue($this->devicetypeIddevicetype)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table ledmatrix with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $idledmatrix
		 * @param int $devicetypeIddevicetype
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($idledmatrix, $devicetypeIddevicetype){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                ledmatrix
                SET 
				on={$this->parseValue($this->on, 'notNumber')},
				off={$this->parseValue($this->off, 'notNumber')},
				failure={$this->parseValue($this->failure, 'notNumber')}
            WHERE
                idLedMatrix={$this->parseValue($idledmatrix, 'int')} AND DeviceType_idDeviceType={$this->parseValue($devicetypeIddevicetype, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($idledmatrix, $devicetypeIddevicetype);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of ledmatrix previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if (!empty($this->idledmatrix) && !empty($this->devicetypeIddevicetype)){
				return $this->update($this->idledmatrix, $this->devicetypeIddevicetype);
			}else{
				return false;
			}
		}

	}

?>
