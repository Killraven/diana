<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanDalifaulttype extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key iddaliFaultType of table dalifaulttype
		 * Comment for field iddaliFaultType: Not specified<br>
		 * @var int $iddalifaulttype
		 */
		private $iddalifaulttype;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = true;

		/**
		 * Class attribute for mapping table field errorDesc
		 * Comment for field errorDesc: Not specified.<br>
		 * Field information:
		 *  - Data type: varchar(128)
		 *  - Null : NO
		 *  - DB Index:
		 *  - Default:
		 *  - Extra:
		 * @var string $errordesc
		 */
		private $errordesc;

		/**
		 * Class attribute for storing the SQL DDL of table dalifaulttype
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBkYWxpZmF1bHR0eXBlYCAoCiAgYGlkZGFsaUZhdWx0VHlwZWAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCwKICBgZXJyb3JEZXNjYCB2YXJjaGFyKDEyOCkgTk9UIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZGRhbGlGYXVsdFR5cGVgKQopIEVOR0lORT1Jbm5vREIgQVVUT19JTkNSRU1FTlQ9NCBERUZBVUxUIENIQVJTRVQ9dXRmOA==";

		/**
		 * setIddalifaulttype Sets the class attribute iddalifaulttype with a given value
		 * The attribute iddalifaulttype maps the field iddaliFaultType defined as int(11).<br>
		 * Comment for field iddaliFaultType: Not specified.<br>
		 *
		 * @param int $iddalifaulttype
		 *
		 * @category Modifier
		 */
		public function setIddalifaulttype($iddalifaulttype){
			$this->iddalifaulttype = (int)$iddalifaulttype;
		}

		/**
		 * setErrordesc Sets the class attribute errordesc with a given value
		 * The attribute errordesc maps the field errorDesc defined as varchar(128).<br>
		 * Comment for field errorDesc: Not specified.<br>
		 *
		 * @param string $errordesc
		 *
		 * @category Modifier
		 */
		public function setErrordesc($errordesc){
			$this->errordesc = (string)$errordesc;
		}

		/**
		 * getIddalifaulttype gets the class attribute iddalifaulttype value
		 * The attribute iddalifaulttype maps the field iddaliFaultType defined as int(11).<br>
		 * Comment for field iddaliFaultType: Not specified.
		 * @return int $iddalifaulttype
		 * @category Accessor of $iddalifaulttype
		 */
		public function getIddalifaulttype(){
			return $this->iddalifaulttype;
		}

		/**
		 * getErrordesc gets the class attribute errordesc value
		 * The attribute errordesc maps the field errorDesc defined as varchar(128).<br>
		 * Comment for field errorDesc: Not specified.
		 * @return string $errordesc
		 * @category Accessor of $errordesc
		 */
		public function getErrordesc(){
			return $this->errordesc;
		}

		/**
		 * Gets DDL SQL code of the table dalifaulttype
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "dalifaulttype";
		}

		/**
		 * The BeanDalifaulttype constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $iddalifaulttype is given.
		 *  - with a fetched data row from the table dalifaulttype having iddaliFaultType=$iddalifaulttype
		 *
		 * @param int $iddalifaulttype . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanDalifaulttype Object
		 */
		public function __construct($iddalifaulttype = null){
			parent::__construct();
			if (!empty($iddalifaulttype)){
				$this->select($iddalifaulttype);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of dalifaulttype into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $iddalifaulttype the primary key iddaliFaultType value of table dalifaulttype which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($iddalifaulttype){
			$sql = "SELECT * FROM dalifaulttype WHERE iddaliFaultType={$this->parseValue($iddalifaulttype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->iddalifaulttype = (integer)$rowObject->iddaliFaultType;
				@$this->errordesc = $this->replaceAposBackSlash($rowObject->errorDesc);
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table dalifaulttype
		 *
		 * @param int $iddalifaulttype the primary key iddaliFaultType value of table dalifaulttype which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($iddalifaulttype){
			$sql = "DELETE FROM dalifaulttype WHERE iddaliFaultType={$this->parseValue($iddalifaulttype,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of dalifaulttype
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->iddalifaulttype = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO dalifaulttype
            (errorDesc)
            VALUES(
			{$this->parseValue($this->errordesc, 'notNumber')})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->iddalifaulttype = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table dalifaulttype with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $iddalifaulttype the primary key iddaliFaultType value of table dalifaulttype which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($iddalifaulttype){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                dalifaulttype
            SET 
				errorDesc={$this->parseValue($this->errordesc, 'notNumber')}
            WHERE
                iddaliFaultType={$this->parseValue($iddalifaulttype, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($iddalifaulttype);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of dalifaulttype previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->iddalifaulttype != ""){
				return $this->update($this->iddalifaulttype);
			}else{
				return false;
			}
		}

	}

?>
