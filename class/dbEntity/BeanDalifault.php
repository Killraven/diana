<?php
	include_once("bean.config.php");

	/**
	 * Created by PhpStorm.
	 * User: Paolo
	 */
	// namespace beans;

	class BeanDalifault extends MySqlRecord{
		/**
		 * A control attribute for the update operation.
		 * @note An instance fetched from db is allowed to run the update operation.
		 *       A new instance (not fetched from db) is allowed only to run the insert operation but,
		 *       after running insertion, the instance is automatically allowed to run update operation.
		 * @var bool
		 */
		private $allowUpdate = false;

		/**
		 * Class attribute for mapping the primary key iddaliFault of table dalifault
		 * Comment for field iddaliFault: Not specified<br>
		 * @var int $iddalifault
		 */
		private $iddalifault;

		/**
		 * A class attribute for evaluating if the table has an autoincrement primary key
		 * @var bool $isPkAutoIncrement
		 */
		private $isPkAutoIncrement = false;

		/**
		 * Class attribute for mapping table field device_idDevice
		 * Comment for field device_idDevice: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $deviceIddevice
		 */
		private $deviceIddevice;

		/**
		 * Class attribute for mapping table field daliFaultType_iddaliFaultType
		 * Comment for field daliFaultType_iddaliFaultType: Not specified.<br>
		 * Field information:
		 *  - Data type: int(11)
		 *  - Null : NO
		 *  - DB Index: MUL
		 *  - Default:
		 *  - Extra:
		 * @var int $dalifaulttypeIddalifaulttype
		 */
		private $dalifaulttypeIddalifaulttype;

		/**
		 * Class attribute for storing the SQL DDL of table dalifault
		 * @var string base64 encoded string for DDL
		 */
		private $ddl = "Q1JFQVRFIFRBQkxFIGBkYWxpZmF1bHRgICgKICBgaWRkYWxpRmF1bHRgIGludCgxMSkgTk9UIE5VTEwsCiAgYGRldmljZV9pZERldmljZWAgaW50KDExKSBOT1QgTlVMTCwKICBgZGFsaUZhdWx0VHlwZV9pZGRhbGlGYXVsdFR5cGVgIGludCgxMSkgTk9UIE5VTEwsCiAgUFJJTUFSWSBLRVkgKGBpZGRhbGlGYXVsdGApLAogIEtFWSBgZmtfZGFsaUZhdWx0X2RldmljZTFfaWR4YCAoYGRldmljZV9pZERldmljZWApLAogIEtFWSBgZmtfZGFsaUZhdWx0X2RhbGlGYXVsdFR5cGUxX2lkeGAgKGBkYWxpRmF1bHRUeXBlX2lkZGFsaUZhdWx0VHlwZWApLAogIENPTlNUUkFJTlQgYGZrX2RhbGlGYXVsdF9kYWxpRmF1bHRUeXBlMWAgRk9SRUlHTiBLRVkgKGBkYWxpRmF1bHRUeXBlX2lkZGFsaUZhdWx0VHlwZWApIFJFRkVSRU5DRVMgYGRhbGlmYXVsdHR5cGVgIChgaWRkYWxpRmF1bHRUeXBlYCkgT04gREVMRVRFIE5PIEFDVElPTiBPTiBVUERBVEUgTk8gQUNUSU9OLAogIENPTlNUUkFJTlQgYGZrX2RhbGlGYXVsdF9kZXZpY2UxYCBGT1JFSUdOIEtFWSAoYGRldmljZV9pZERldmljZWApIFJFRkVSRU5DRVMgYGRldmljZWAgKGBpZERldmljZWApIE9OIERFTEVURSBOTyBBQ1RJT04gT04gVVBEQVRFIE5PIEFDVElPTgopIEVOR0lORT1Jbm5vREIgREVGQVVMVCBDSEFSU0VUPXV0Zjg=";

		/**
		 * setIddalifault Sets the class attribute iddalifault with a given value
		 * The attribute iddalifault maps the field iddaliFault defined as int(11).<br>
		 * Comment for field iddaliFault: Not specified.<br>
		 *
		 * @param int $iddalifault
		 *
		 * @category Modifier
		 */
		public function setIddalifault($iddalifault){
			$this->iddalifault = (int)$iddalifault;
		}

		/**
		 * setDeviceIddevice Sets the class attribute deviceIddevice with a given value
		 * The attribute deviceIddevice maps the field device_idDevice defined as int(11).<br>
		 * Comment for field device_idDevice: Not specified.<br>
		 *
		 * @param int $deviceIddevice
		 *
		 * @category Modifier
		 */
		public function setDeviceIddevice($deviceIddevice){
			$this->deviceIddevice = (int)$deviceIddevice;
		}

		/**
		 * setDalifaulttypeIddalifaulttype Sets the class attribute dalifaulttypeIddalifaulttype with a given value
		 * The attribute dalifaulttypeIddalifaulttype maps the field daliFaultType_iddaliFaultType defined as int(11).<br>
		 * Comment for field daliFaultType_iddaliFaultType: Not specified.<br>
		 *
		 * @param int $dalifaulttypeIddalifaulttype
		 *
		 * @category Modifier
		 */
		public function setDalifaulttypeIddalifaulttype($dalifaulttypeIddalifaulttype){
			$this->dalifaulttypeIddalifaulttype = (int)$dalifaulttypeIddalifaulttype;
		}

		/**
		 * getIddalifault gets the class attribute iddalifault value
		 * The attribute iddalifault maps the field iddaliFault defined as int(11).<br>
		 * Comment for field iddaliFault: Not specified.
		 * @return int $iddalifault
		 * @category Accessor of $iddalifault
		 */
		public function getIddalifault(){
			return $this->iddalifault;
		}

		/**
		 * getDeviceIddevice gets the class attribute deviceIddevice value
		 * The attribute deviceIddevice maps the field device_idDevice defined as int(11).<br>
		 * Comment for field device_idDevice: Not specified.
		 * @return int $deviceIddevice
		 * @category Accessor of $deviceIddevice
		 */
		public function getDeviceIddevice(){
			return $this->deviceIddevice;
		}

		/**
		 * getDalifaulttypeIddalifaulttype gets the class attribute dalifaulttypeIddalifaulttype value
		 * The attribute dalifaulttypeIddalifaulttype maps the field daliFaultType_iddaliFaultType defined as int(11).<br>
		 * Comment for field daliFaultType_iddaliFaultType: Not specified.
		 * @return int $dalifaulttypeIddalifaulttype
		 * @category Accessor of $dalifaulttypeIddalifaulttype
		 */
		public function getDalifaulttypeIddalifaulttype(){
			return $this->dalifaulttypeIddalifaulttype;
		}

		/**
		 * Gets DDL SQL code of the table dalifault
		 * @return string
		 * @category Accessor
		 */
		public function getDdl(){
			return base64_decode($this->ddl);
		}

		/**
		 * Gets the name of the managed table
		 * @return string
		 * @category Accessor
		 */
		public function getTableName(){
			return "dalifault";
		}

		/**
		 * The BeanDalifault constructor
		 * It creates and initializes an object in two way:
		 *  - with null (not fetched) data if none $iddalifault is given.
		 *  - with a fetched data row from the table dalifault having iddaliFault=$iddalifault
		 *
		 * @param int $iddalifault . If omitted an empty (not fetched) instance is created.
		 *
		 * @return BeanDalifault Object
		 */
		public function __construct($iddalifault = null){
			parent::__construct();
			if (!empty($iddalifault)){
				$this->select($iddalifault);
			}
		}

		/**
		 * The implicit destructor
		 */
		public function __destruct(){
			$this->close();
		}

		/**
		 * Explicit destructor. It calls the implicit destructor automatically.
		 */
		public function close(){
			unset($this);
		}

		/**
		 * Fetchs a table row of dalifault into the object.
		 * Fetched table fields values are assigned to class attributes and they can be managed by using
		 * the accessors/modifiers methods of the class.
		 *
		 * @param int $iddalifault the primary key iddaliFault value of table dalifault which identifies the row to select.
		 *
		 * @return int affected selected row
		 * @category DML
		 */
		public function select($iddalifault){
			$sql = "SELECT * FROM dalifault WHERE iddaliFault={$this->parseValue($iddalifault,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->resultSet = $result;
			$this->lastSql = $sql;
			if ($result){
				$rowObject = $result->fetch_object();
				@$this->iddalifault = (integer)$rowObject->iddaliFault;
				@$this->deviceIddevice = (integer)$rowObject->device_idDevice;
				@$this->dalifaulttypeIddalifaulttype = (integer)$rowObject->daliFaultType_iddaliFaultType;
				$this->allowUpdate = true;
			}else{
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Deletes a specific row from the table dalifault
		 *
		 * @param int $iddalifault the primary key iddaliFault value of table dalifault which identifies the row to delete.
		 *
		 * @return int affected deleted row
		 * @category DML
		 */
		public function delete($iddalifault){
			$sql = "DELETE FROM dalifault WHERE iddaliFault={$this->parseValue($iddalifault,'int')}";
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}
			return $this->affected_rows;
		}

		/**
		 * Insert the current object into a new table row of dalifault
		 * All class attributes values defined for mapping all table fields are automatically used during inserting
		 * @return mixed MySQL insert result
		 * @category DML
		 */
		public function insert(){
			if ($this->isPkAutoIncrement){
				$this->iddalifault = "";
			}
			// $constants = get_defined_constants();
			$sql = <<< SQL
            INSERT INTO dalifault
            (iddaliFault,device_idDevice,daliFaultType_iddaliFaultType)
            VALUES({$this->parseValue($this->iddalifault)},
			{$this->parseValue($this->deviceIddevice)},
			{$this->parseValue($this->dalifaulttypeIddalifaulttype)})
SQL;
			$this->resetLastSqlError();
			$result = $this->query($sql);
			$this->lastSql = $sql;
			if (!$result){
				$this->lastSqlError = $this->sqlstate . " - " . $this->error;
			}else{
				$this->allowUpdate = true;
				if ($this->isPkAutoIncrement){
					$this->iddalifault = $this->insert_id;
				}
			}
			return $result;
		}

		/**
		 * Updates a specific row from the table dalifault with the values of the current object.
		 * All class attribute values defined for mapping all table fields are automatically used during updating of selected row.<br>
		 * Null values are used for all attributes not previously setted.
		 *
		 * @param int $iddalifault the primary key iddaliFault value of table dalifault which identifies the row to update.
		 *
		 * @return mixed MySQL update result
		 * @category DML
		 */
		public function update($iddalifault){
			// $constants = get_defined_constants();
			if ($this->allowUpdate){
				$sql = <<< SQL
            UPDATE
                dalifault
            SET 
				device_idDevice={$this->parseValue($this->deviceIddevice)},
				daliFaultType_iddaliFaultType={$this->parseValue($this->dalifaulttypeIddalifaulttype)}
            WHERE
                iddaliFault={$this->parseValue($iddalifault, 'int')}
SQL;
				$this->resetLastSqlError();
				$result = $this->query($sql);
				if (!$result){
					$this->lastSqlError = $this->sqlstate . " - " . $this->error;
				}else{
					$this->select($iddalifault);
					$this->lastSql = $sql;
					return $result;
				}
			}else{
				return false;
			}
		}

		/**
		 * Facility for updating a row of dalifault previously loaded.
		 * All class attribute values defined for mapping all table fields are automatically used during updating.
		 * @category DML Helper
		 * @return mixed MySQLi update result
		 */
		public function updateCurrent(){
			if ($this->iddalifault != ""){
				return $this->update($this->iddalifault);
			}else{
				return false;
			}
		}

	}

?>
