<?php

	/*
 * Copyright 2018 Vincenzo Suraci
 */

	require_once __DIR__ . '/dali_ethernet_client.class.php';

	class DALI_ETHERNET_CONFIG extends DALI_ETHERNET{
		protected $config_file;

		function __construct(){
			// file di configurazione
			$this->config_file = $this->recupera_config_dir() . 'config.json';
		}

		protected function recupera_config_dir(){
			$dir = __DIR__ . '/../config';
			if (!is_dir($dir)) mkdir($dir);
			return $dir;
		}

		function __destruct(){
			parent::__destruct();
		}

		public function recupera_html_contenuto_pannello_verifica_lampade(){
			$html = '';

			$lista_moduli = $this->recupera_lista_moduli();

			if (count($lista_moduli) > 0){
				$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th><span class="glyphicon glyphicon-lamp"></span>&nbsp;Lamps</th>
			   <th><span class="glyphicon glyphicon-wrench"></span>&nbsp;ADV</th>
			   <th><span class="glyphicon glyphicon-wrench"></span>&nbsp;PoL</th>
			   <th><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Info</th>
			  </tr>
			 </thead>
			 <tbody>
			  <tr>
			   <td>' . $this->recupera_html_select_lampade($lista_moduli, 'modifica_lampade_scelte();') . '</td>
			   <td>' . $this->recupera_html_select_ADV('', 'modifica_ADV_lampade_scelte();', 254, 'ADV_lampade_scelte', true) . '</td>
			   <td>' . $this->recupera_html_select_ADV('', 'modifica_PoL_lampade_scelte();', 254, 'PoL_lampade_scelte', true) . '</td>
			   <td id="td_info"></td>
			  </tr>
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table> 
			';

			}else{
				$html .= $this->recupera_html_info('Nessuno modulo configurato.');
			}

			return $html;
		}

		/**
		 * Questa funzione accede ai file json presenti nella cartella config
		 */
		public function recupera_lista_moduli(){
			$lista_moduli = array();
			$filenames = scandir($this->recupera_config_dir());
			foreach ($filenames as $filename){
				$file = $this->recupera_config_dir() . '/' . $filename;
				if (!is_dir($file)){
					if (strlen($filename) > 4){
						if (strtolower(substr($filename, -5)) == '.json'){
							// abbiamo un file json
							$basename = substr($filename, 0, -5);
							$arr = explode("_", $basename);
							if (count($arr) == 6 && $arr[0] == 'config'){
								// recuperiamo indirizzo ip e porta
								$addr = $arr[1] . '.' . $arr[2] . '.' . $arr[3] . '.' . $arr[4];
								$port = $arr[5];
								$json = file_get_contents($file);
								$lista_moduli[$addr . ':' . $port] = json_decode($json, true);
							}
						}
					}
				}
			}
			return $lista_moduli;
		}

		protected function recupera_html_select_lampade(&$lista_moduli, $onchange, $selected_ids = array()){
			$elements = array();

			$html = '
		<select 
			multiple 
			id = "select_lamp"
			name = "lampade_da_verificare"
			onchange = "' . $onchange . '"
			class="selectpicker" 
			data-live-search="true"  
			data-width="100%" 
			data-size="10" 
			data-actions-box="true" 
			data-selected-text-format="count > 10">
		';

			foreach ($lista_moduli as $addr_port => $configurazione){
				$addr = $configurazione['addr'];
				$port = $configurazione['port'];
				$cmd_port = $configurazione['cmd_port'];

				$controller = $this->recupera_html_controller($addr, $port, $cmd_port);

				if (key_exists('nodi', $configurazione)){
					foreach ($configurazione['nodi'] as $num_nodo => $nodo){
						// gestiamo solo i nodi attivi
						if ($nodo['presente'] == 1){
							$descrizione = key_exists('descrizione', $nodo) ? $this->recupera_html_descrizione_NODO($nodo['descrizione']) : '';

							$tipo = '';
							$arr = explode('_', $descrizione);
							if (count($arr) >= 3) $tipo = $arr[1];

							$html .= '<option value="' . $addr . '_' . $port . '_' . $cmd_port . '_' . $num_nodo . '"';

							if (in_array($num_nodo, $selected_ids)) $html .= ' selected';

							$html .= ' data-content="' . htmlentities($descrizione) . '"';

							$html .= '>';

							$html .= '</option>';
						}
					}
				}
			}

			$html .= '
		</select>
		';

			return $html;
		}

		public function recupera_html_controller($addr, $port, $cmd_port){
			$html = '';

			$html .= '
		<span class="label label-primary">' . $addr . '</span><br/>
		<h6><span class="label label-default">' . $port . '</span>&nbsp;
		<span class="label label-default">' . $cmd_port . '</span></h6>
		';

			return $html;
		}

		public function recupera_html_descrizione_NODO(&$descrizione){
			return '<span class="label label-warning">' . $descrizione . '</span>&nbsp;';
		}

		protected function recupera_html_select_ADV($ADV = '', $onchange = '', $MAX = 255, $id = 'ADV', $disabled = false, $min = 0, $include_0 = true){
			$elements = array();

			// aggiungiamo i nodi
			if ($include_0 && $min > 0) $elements[0] = '';
			for ($i = $min; $i <= $MAX; $i++) $elements[$i] = '';

			$html = HtmlSelect::getHTMLselectFromArray($elements, $ADV, 'ADV', $id, '', $onchange, $disabled, true);

			if ($include_0 && $min > 0){
				$adv = 0;
				$adv_html = str_replace('"', "'", $this->recupera_html_ADV($adv));
				$html = str_replace('value="' . $adv . '"', 'value="' . $adv . '" data-content="' . $adv_html . '" ', $html);
			}

			for ($adv = $min; $adv <= $MAX; $adv++){
				$adv_html = str_replace('"', "'", $this->recupera_html_ADV($adv));
				$html = str_replace('value="' . $adv . '"', 'value="' . $adv . '" data-content="' . $adv_html . '" ', $html);
			}

			if ($MAX < 255) $html = str_replace('<select', '<select multiple data-max-options="1"', $html);

			$html = str_replace('<select', '<select data-size="10" data-width="fit"', $html);

			$html = str_replace(' data-live-search="true"', '', $html);

			return $html;
		}

		public function recupera_html_contenuto_pannello_lista_lampade(){
			$html = '';

			$lista_moduli = $this->recupera_lista_moduli();

			if (count($lista_moduli) > 0){
				$html .= '
			<table class="table sortable">
			 <thead>
			  <tr class="title">
			   <th>Controller</th>
			   <th>Node</th>
			   <th>Description</th>
			   <th>Type</th>
			   <th>Status</th>
			   <th>Action</th>
			  </tr>
			 </thead>
			 <tbody>
			';

				foreach ($lista_moduli as $addr_port => $configurazione){
					$addr = $configurazione['addr'];
					$port = $configurazione['port'];
					$cmd_port = $configurazione['cmd_port'];

					$controller = $this->recupera_html_controller($addr, $port, $cmd_port);

					if (key_exists('nodi', $configurazione)){
						foreach ($configurazione['nodi'] as $num_nodo => $nodo){
							// gestiamo solo i nodi attivi
							if ($nodo['presente'] == 1){
								$descrizione = key_exists('descrizione', $nodo) ? $this->recupera_html_descrizione_NODO($nodo['descrizione']) : '';

								$tipo = '';
								$arr = explode('_', $descrizione);
								if (count($arr) >= 3) $tipo = $arr[1];


								$html .= '
							<tr>
							 <td>' . $controller . '</td>
							 <td>' . $this->recupera_html_num_NODO($num_nodo) . '</td>
							 <td>' . $descrizione . '</td>
							 <td>' . $tipo . '</td>
							 <td><pre>' . print_r($nodo, true) . '</pre></td>
							 <td>
							  <div class="block"><b>ADV</b><br/>' . $this->recupera_html_select_ADV($nodo['ADV'], '', $nodo['ADV_max'], 'ADV', false, $nodo['ADV_min'], true) . '</div>
							  <div class="block"><b>PoL</b><br/>' . $this->recupera_html_select_ADV($nodo['ADV'], '', $nodo['ADV_max'], 'ADV', false, $nodo['ADV_min'], true) . '</div>
							 </td>
							</tr> 
							';
							}
						}
					}
				}

				$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table> 
			';
			}else{
				$html .= $this->recupera_html_info('Nessuno modulo configurato.');
			}

			return $html;
		}

		public function recupera_html_num_NODO($num_elemento){
			$html = '';

			if ($num_elemento < self::$MAX_NUM_NODI_DALI){
				$html .= '<span class="label label-primary">Node #' . $num_elemento . '</span>&nbsp;';
			}else{
				$num_elemento -= self::$MAX_NUM_NODI_DALI;
				if ($num_elemento < self::$MAX_NUM_GRUPPI_DALI){
					$html .= '<span class="label label-danger">Group #' . $num_elemento . '</span>&nbsp;';
				}else{
					$html .= '<span class="label label-success">All</span>&nbsp;';
				}
			}

			return $html;
		}

		public function recupera_html_contenuto_pannello_lista_moduli(){
			$html = '';

			$lista_moduli = $this->recupera_lista_moduli();

			if (count($lista_moduli) > 0){
				foreach ($lista_moduli as $addr_port => $configurazione){
					$arr = explode(":", $addr_port);
					$html .= $this->recupera_html_div_modulo_HD678DA_B2($arr[0], $arr[1], $configurazione['cmd_port']);
				}

			}else{
				$html .= $this->recupera_html_info('Nessuno modulo configurato.');
			}

			return $html;
		}

		public function recupera_html_div_modulo_HD678DA_B2($addr, $port, $cmd_port){
			$html = '';

			$escaped_addr = $this->addr_encode($addr);

			$html .= '
		<div class="modulo" id="modulo_' . $escaped_addr . '_' . $port . '_' . $cmd_port . '">
		 
		 <div class="mblock">
		  <img src="../img/HD678DA-B2.png" height="100px">
		 </div>
		 
		 <div class="mblock">
		  <div>
		   IP <b>' . $addr . '</b>
		  </div>
		  <div>
		   Ctrl Port <b>' . $port . '</b>
		  </div>
		  <div>
		   Cmd Port <b>' . $cmd_port . '</b>
		  </div>
		  <div>
		   <h6><span class="label label-default stato_modulo" id="span_' . $escaped_addr . '_' . $port . '_' . $cmd_port . '">In connessione...</span></h6>
		  </div>
		  <div>
		   <p>
		   <button type="button" class="btn btn-xs btn-primary" style="display:none;" id="btn_apri_' . $escaped_addr . '_' . $port . '" onclick="visualizza_modulo(\'' . $addr . '\',' . $port . ',' . $cmd_port . ');">
		    <span class="glyphicon glyphicon-search"></span> View</button>
		   <button type="button" class="btn btn-xs btn-danger" id="btn_elimina_' . $escaped_addr . '_' . $port . '" onclick="conferma_elimina_modulo(\'' . $addr . '\',' . $port . ',' . $cmd_port . ');">
		    <span class="glyphicon glyphicon-trash"></span> Remove</button>
		   <button type="button" class="btn btn-xs btn-success" id="btn_scarica_' . $escaped_addr . '_' . $port . '" onclick="window.open(\'' . $this->recupera_url_relativa_al_file_configurazione_da_addr_port($addr, $port) . '\',\'_blank\');">
		    <span class="glyphicon glyphicon-download"></span> Download</button> 
		   </p>
		  </div>
		 </div>
		
		</div>
		';

			return $html;
		}

		protected function recupera_url_relativa_al_file_configurazione_da_addr_port($addr, $port){
			$filename = 'config_' . $this->addr_encode($addr) . '_' . $port . '.json';
			return '../config/' . $filename;
		}

		public function recupera_html_pannelli(){
			$html = '';

			$html .= $this->recupera_html_pannello_aggiungi_modulo();

			$html .= $this->recupera_html_pannello_lista_moduli();

			$html .= $this->recupera_html_pannello_controllo_modulo();

			$html .= $this->recupera_html_pannello_configurazione_modulo();

			$html .= $this->recupera_html_pannello_verifica_lampade();

			$html .= $this->recupera_html_pannello_lista_lampade();

			return $html;
		}

		protected function recupera_html_pannello_aggiungi_modulo(){
			$html = '';

			$html .= '
		<div class="panel panel-primary">
		 <!-- <div class="panel-heading"><span class="glyphicon glyphicon-search"></span>&nbsp;Cerca nuovi moduli HD678DA-B2 </div> -->
		 <div class="panel-heading"><span class="glyphicon glyphicon-search"></span>&nbsp;Search for new HD678DA-B2 modules</div>
		 <div class="panel-body">
		  
		    <div class="mblock">
			 <b>IP Address</b>
			 <br/>
			 <input type="text" class="form-control" placeholder="IP Address" aria-describedby="basic-addon2" value="" id="addr">
			</div>
			
			<div style="float:left; padding-right:10px;">
			 <b>Control Port</b>
			 <br/>
			 <input type="text" class="form-control" placeholder="Control Port" aria-describedby="basic-addon2" value="' . $this->port . '" id="port">
			</div>
		  
		  	<div style="float:left; padding-right:10px;">
			 <b>Command Port</b>
			 <br/>
			 <input type="text" class="form-control" placeholder="Command Port" aria-describedby="basic-addon2" value="' . $this->cmd_port . '" id="cmd_port">
			</div>
			
			<div class="mblock">
			 <br/>
			 <button type="button" class="btn btn-primary" onclick="aggiungi_modulo();"><span class="glyphicon glyphicon-plus"></span> Add</button>
			</div>
			
			<table class="table"><tr></tr></table>
			
			<div id="div_log_aggiungi_modulo"></div>
			
		 </div>
		</div>
		';

			return $html;
		}

		protected function recupera_html_pannello_lista_moduli(){
			$html = '';

			$html .= '
			<div class="panel panel-primary">
			 <div class="panel-heading">
			  <span class="glyphicon glyphicon-th"></span>&nbsp;HD678DA-B2 module list
			  <div style="float:right;"><button type="button" class="btn btn-xs btn-info" onclick="carica_lista_moduli();"><span class="glyphicon glyphicon-refresh"></span> Refresh</button></div>
			 </div>
			 <div class="panel-body" id="div_lista_moduli">
 			  
			 </div>
			</div>
			';


			return $html;
		}

		protected function recupera_html_pannello_controllo_modulo(){
			$html = '';

			$html .= '
			<div class="panel panel-primary" id="panel_controllo_modulo" style="display:none;">
			 <div class="panel-heading">
			  <span class="glyphicon glyphicon-transfer"></span>&nbsp;Check HD678DA-B2 module <b><span id="span_controllo_modulo"></span></b>
			  <div style="float:right;"><button type="button" class="btn btn-xs btn-info" onclick="aggiorna_modulo();"><span class="glyphicon glyphicon-refresh"></span> Refresh</button></div>
			 </div>
			 <div class="panel-body" id="div_controllo_modulo_body">
			 </div>
			</div>
			';

			return $html;
		}

		protected function recupera_html_pannello_configurazione_modulo(){
			$html = '';

			$html .= '
			<div class="panel panel-primary" id="panel_modulo" style="display:none;">
			 <div class="panel-heading">
			  <span class="glyphicon glyphicon-cog"></span>&nbsp; HD678DA-B2 module configuration <b><span id="span_modulo"></span></b>
			  <div style="float:right;"><button type="button" class="btn btn-xs btn-info" onclick="aggiorna_modulo();"><span class="glyphicon glyphicon-refresh"></span> Refresh</button></div>
			 </div>
			 <div class="panel-body" id="div_modulo_body">
			 </div>
			</div>
			';


			return $html;
		}

		protected function recupera_html_pannello_verifica_lampade(){
			$html = '';

			$html .= '
			<div class="panel panel-primary">
			 <div class="panel-heading">
			  <span class="glyphicon glyphicon-wrench"><span class="glyphicon glyphicon-lamp"></span></span>&nbsp;DALI lamps check
			  <div style="float:right;"><button type="button" class="btn btn-xs btn-info" onclick="carica_verifica_lampade();"><span class="glyphicon glyphicon-refresh"></span> Refresh</button></div>
			 </div>
			 <div class="panel-body" id="div_verifica_lampade">
 			  
			 </div>
			</div>
			';

			return $html;
		}

		protected function recupera_html_pannello_lista_lampade(){
			$html = '';

			$html .= '
			<div class="panel panel-primary">
			 <div class="panel-heading">
			  <span class="glyphicon glyphicon-th-list"></span><span class="glyphicon glyphicon-lamp"></span>&nbsp;DALI lamps list
			  <div style="float:right;"><button type="button" class="btn btn-xs btn-info" onclick="carica_lista_lampade();"><span class="glyphicon glyphicon-refresh"></span> Refresh</button></div>
			 </div>
			 <div class="panel-body" id="div_lista_lampade">
 			  <button class="btn btn-success" onclick="carica_lista_lampade();"><span class="glyphicon glyphicon-play"></span>&nbsp;Load lamp list</button>
			 </div>
			</div>
			';

			return $html;
		}

		public function elimina_modulo($addr, $port){
			$file = $this->recupera_file_configurazione_da_addr_port($addr, $port);
			if (file_exists($file)) unlink($file);
		}

		protected function recupera_file_configurazione_da_addr_port($addr, $port){
			$filename = 'config_' . $this->addr_encode($addr) . '_' . $port . '.json';
			return $this->recupera_config_dir() . '/' . $filename;
		}

		public function aggiungi_modulo($addr, $port, $cmd_port){
			$html = '';
			$risultato = 0;

			// verifichiamo che la porta sia un numero intero
			if (is_numeric($port) && is_numeric($cmd_port)){
				if ($port >= 0 && $port <= 65535 && $cmd_port >= 0 && $cmd_port <= 65535){
					// Verifichiamo che l'indirizzo IP sia valido
					$arr = explode(".", $addr);
					if (count($arr) == 4){
						if (is_numeric($arr[0]) && is_numeric($arr[1]) && is_numeric($arr[2]) && is_numeric($arr[3])){
							if ($arr[0] >= 0 && $arr[0] <= 255 && $arr[1] >= 0 && $arr[1] <= 255 && $arr[2] >= 0 && $arr[2] <= 255 && $arr[3] >= 0 && $arr[3] <= 255){
								$file = $this->recupera_file_configurazione_da_addr_port($addr, $port);

								if (!file_exists($file)){
									file_put_contents($file, json_encode(array('addr' => $addr, 'port' => $port, 'cmd_port' => $cmd_port, 'created' => date('Y-m-d H:i:s'))));
									$risultato = 1;
									$html .= $this->recupera_html_successo('Modulo <b>' . $addr . ':' . $port . '</b> aggiunto alla lista dei moduli.');
								}else{
									$configurazione = $this->recupera_configurazione_da_addr_port($addr, $port);
									if ($configurazione['cmd_port'] != $cmd_port){
										$risultato = 1;
										$configurazione['cmd_port'] = $cmd_port;
										$this->salva_configurazione_da_addr_port($addr, $port, $configurazione);
										$html .= $this->recupera_html_successo('Il modulo <b>' . $addr . ':' . $port . '</b> è stato aggiornato.');
									}else{
										$html .= $this->recupera_html_errore('Il modulo <b>' . $addr . ':' . $port . '</b>, con porta di comando <b>' . $cmd_port . '</b> è già presente nella lista dei moduli.');
									}
								}
							}else{
								$html .= $this->recupera_html_errore('L\'indirizzo IP deve essere composto da 4 numeri con valori da 0 a 255.');
							}
						}else{
							$html .= $this->recupera_html_errore('L\'indirizzo IP deve essere composto da 4 numeri con valori da 0 a 255.');
						}
					}else{
						$html .= $this->recupera_html_errore('L\'indirizzo IP deve essere composto da 4 numeri separati da un punto (ad es. 192.168.1.10).');
					}
				}else{
					$html .= $this->recupera_html_errore('I numeri di porta devono essere numeri compresi tra 0 e 65535.');
				}
			}else{
				$html .= $this->recupera_html_errore('I numeri di porta devono essere numeri interi.');
			}

			return array('risultato' => $risultato, 'html' => $html,);
		}

		public function recupera_configurazione_da_addr_port($addr, $port){
			$configurazione = array();
			$file = $this->recupera_file_configurazione_da_addr_port($addr, $port);
			if (file_exists($file)){
				$json = file_get_contents($file);
				$configurazione = json_decode($json, true);
			}
			return $configurazione;
		}

		protected function salva_configurazione_da_addr_port($addr, $port, &$configurazione){
			$file = $this->recupera_file_configurazione_da_addr_port($addr, $port);
			$json = json_encode($configurazione);
			return file_put_contents($file, $json);
		}

		public function recupera_html_div_scena($num_scena, &$scena, $inline = true){
			$html = '';

			$html_inline = ($inline ? '' : '<br/>');

			$html_num_scena = '<span class="label label-primary">Scena #' . $num_scena . '</span>&nbsp;';
			$html_des_scena = ($scena != null && key_exists('descrizione', $scena)) ? '<span class="label label-warning">' . $scena['descrizione'] . '</span>&nbsp;' : '';

			$html_div_tags = ' class="scena clickable"';

			$html .= '<div' . $html_div_tags . '>' . $html_num_scena . $html_inline . $html_des_scena . '</div>';

			return $html;
		}

		public function recupera_html_div_elemento($num_elemento, &$configurazione, $inline = true){
			$html = '';

			$html_inline = ($inline ? '' : '<br/>');

			$html .= $this->recupera_html_num_NODO($num_elemento);

			if ($num_elemento < self::$MAX_NUM_NODI_DALI){
				if (key_exists($num_elemento, $configurazione['nodi'])) if (key_exists('descrizione', $configurazione['nodi'][$num_elemento])) $html .= $html_inline . $this->recupera_html_descrizione_NODO($configurazione['nodi'][$num_elemento]['descrizione']);

			}else{
				$num_elemento -= self::$MAX_NUM_NODI_DALI;
				if (key_exists($num_elemento, $configurazione['gruppi'])) if (key_exists('descrizione', $configurazione['gruppi'][$num_elemento])) $html .= $html_inline . $this->recupera_html_descrizione_NODO($configurazione['gruppi'][$num_elemento]['descrizione']);
			}

			return $html;
		}

		/**
		 * Restituisce -1 in caso di fallimento
		 */
		public function aggiorna_power_on_level($addr, $port, $cmd_port, $nodo_id, $valore){
			$risultato = 1;
			$html = '';
			$PoL_dec = -1;

			$DEC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);

			// Invio comandi per "resettare" il converter
			$DEC->invia_comando(self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST);
			$DEC->invia_comando(self::$COMANDO_DALI_SCENE_SCANNING_REQUEST);

			// Invio del comando di CONFIGURAZIONE del Power up Level
			$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
			$PARAMETRI = array('DTR' => $valore, 'DALI_COMMAND' => 45, // Store the value in the DTR as the power on level
				'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
			$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

			if ($r['risultato'] == 1){
				// Invio del comando di RICHIESTA Power up Level (PuL)
				$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
				$PARAMETRI = array('DALI_COMMAND' => 163, // Return the power up level as XX
					'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
				$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

				if ($r['risultato'] == 1){
					$PoL_dec = hexdec(substr($r['risposta'], -2));
				}else{
					$risultato = 0;
					$html .= $r['html'];
				}
			}else{
				$risultato = 0;
				$html .= $r['html'];
			}

			return array('risultato' => $risultato, 'html' => $html, 'PoL_dec' => $PoL_dec,);
		}

		public function aggiorna_fade_rate($addr, $port, $cmd_port, $nodo_id, $valore){
			$DEC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);

			// Invio comandi per "resettare" il converter
			$DEC->invia_comando(self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST);
			$DEC->invia_comando(self::$COMANDO_DALI_SCENE_SCANNING_REQUEST);

			// Invio del comando di CONFIGURAZIONE del Power up Level
			$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
			$PARAMETRI = array('DTR' => $valore, 'DALI_COMMAND' => 47, // Store the value in the DTR as the fade rate
				'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
			$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

			if ($r['risultato'] == 1){
				// Invio del comando di RICHIESTA Power up Level (PuL)
				$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
				$PARAMETRI = array('DALI_COMMAND' => 165, // Returns the fade time as X and the fade rate as Y
					'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
				$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

				if ($r['risultato'] == 1){
					return hexdec(substr($r['risposta'], -1));
				}
			}

			return -1;
		}

		public function aggiorna_fade_time($addr, $port, $cmd_port, $nodo_id, $valore){
			$DEC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);

			// Invio comandi per "resettare" il converter
			$DEC->invia_comando(self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST);
			$DEC->invia_comando(self::$COMANDO_DALI_SCENE_SCANNING_REQUEST);

			// Invio del comando di CONFIGURAZIONE del Power up Level
			$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
			$PARAMETRI = array('DTR' => $valore, 'DALI_COMMAND' => 46, // Store the value in the DTR as the fade time
				'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
			$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

			if ($r['risultato'] == 1){
				// Invio del comando di RICHIESTA Power up Level (PuL)
				$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
				$PARAMETRI = array('DALI_COMMAND' => 165, // Returns the fade time as X and the fade rate as Y
					'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
				$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

				if ($r['risultato'] == 1){
					return hexdec(substr($r['risposta'], -2, 1));
				}
			}

			return -1;
		}

		public function recupera_html_azioni_nodo($addr, $port, $cmd_port, $nodo_id){
			$html = '';

			$PuL_dec = 0;
			$FR = 0;
			$FT = 0;

			$DEC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);

			// Invio comandi per "resettare" il converter
			$r = $DEC->invia_comando(self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST);
			$stato = $r['risposta'];

			$DEC->invia_comando(self::$COMANDO_DALI_SCENE_SCANNING_REQUEST);

			// Invio del comando Power on Level
			$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
			$PARAMETRI = array('DALI_COMMAND' => 163, // Return the power up level as XX
				'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
			$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

			if ($r['risultato'] == 1){
				$PuL_dec = hexdec(substr($r['risposta'], -2));
				$html .= '
			 <div class="mblock">
			  <b>Power on Level</b>
			  <br/>
			  ' . $this->recupera_html_select_ADV($PuL_dec, 'cambia_power_on_level(this,' . $nodo_id . ');', ord($stato[($nodo_id * 8) + 5]), 'ADV', false, ord($stato[($nodo_id * 8) + 4]), true) . '
			 </div>
			 ';
			}else{
				$html .= $r['html'];
			}

			// Invio del comando Returns the fade time as X and the fade rate as Y
			$COMANDO = self::$COMANDO_GENERIC_DALI_COMMAND_REQUEST;
			$PARAMETRI = array('DALI_COMMAND' => 165, // Returns the fade time as X and the fade rate as Y
				'COUNTER' => $DEC->COUNTER, 'DALI_ID' => $nodo_id,);
			$r = $DEC->invia_comando($COMANDO, $PARAMETRI);

			if ($r['risultato'] == 1){
				$hex = substr($r['risposta'], -2);
				$FT_dec = hexdec(substr($hex, 0, 1));
				$FR_dec = hexdec(substr($hex, 1, 1));

				$html .= '
			 <div class="mblock">
			  <b>Fade Time</b> <small><i>(secs)</i></small>
			  <br/>
			  ' . $this->recupera_html_select_FADE_TIME($nodo_id, $FT_dec) . '
			 </div>
			 ';

				$html .= '
			 <div class="mblock">
			  <b>Fade Rate</b> <small><i>(steps/sec)</i></small>
			  <br/>
			  ' . $this->recupera_html_select_FADE_RATE($nodo_id, $FR_dec) . '
			 </div>
			 ';
			}else{
				$html .= $r['html'];
			}

			return $html;
		}

		protected function recupera_html_select_FADE_TIME($nodo_id, $FR_dec){
			$elements = array();

			$onchange = 'cambia_fade_time(this,' . $nodo_id . ');';

			$html = '
		<select 
			id = "' . $nodo_id . '"
			name = "FT"
			onchange = "' . $onchange . '"
			class="selectpicker" 
			data-live-search="true" 
			data-width="auto" 
			data-size="10"  
			data-selected-text-format="count > 10">
		';

			// aggiungiamo i nodi
			foreach (self::$FADE_TIME_TABLE as $val => $time){
				$html .= '<option value="' . $val . '"';

				if ($FR_dec == $val) $html .= ' selected';

				$html .= '>';

				$html .= number_format(doubleval($time), 1);

				$html .= '</option>';
			}

			$html .= '
		</select>
		';

			return $html;
		}

		protected function recupera_html_select_FADE_RATE($nodo_id, $FR_dec){
			$elements = array();

			$onchange = 'cambia_fade_rate(this,' . $nodo_id . ');';

			$html = '
		<select 
			id = "' . $nodo_id . '"
			name = "FR"
			onchange = "' . $onchange . '"
			class="selectpicker" 
			data-live-search="true" 
			data-width="auto" 
			data-size="10"  
			data-selected-text-format="count > 10">
		';

			// aggiungiamo i nodi
			foreach (self::$FADE_RATE_TABLE as $val => $rate){
				$html .= '<option value="' . $val . '"';

				if ($FR_dec == $val) $html .= ' selected';

				$html .= '>';

				if ($val == 0) $html .= $rate;else
					$html .= number_format(doubleval($rate), 0);


				$html .= '</option>';
			}

			$html .= '
		</select>
		';

			return $html;
		}

		/**
		 * Questa funzione recupera le informazioni di configurazione salvate su file.
		 * Confronta quindi le informazioni con quelle del modulo on-line.
		 * Infine restituisce due HTML, uno per il pannello di configurazione del modulo,
		 * l'altro per il suo controllo
		 */
		public function recupera_html_contenuto_pannello_modulo($addr, $port){
			$html = '';

			// recuperiamo la configurazione da file
			$configurazione = $this->recupera_configurazione_da_addr_port($addr, $port);

			// aggiorniamo i dati della configurazione con lo stato attuale dei moduli
			$this->aggiorna_configurazione_nodi_gruppi_da_modulo($addr, $port, $configurazione);
			$this->aggiorna_configurazione_scene_da_modulo($addr, $port, $configurazione);
			$this->salva_configurazione_da_addr_port($addr, $port, $configurazione);


			//-----------------------------------------------------------------------------------------
			// STAMPIAMO IL CONTROLLO DEI NODI

			$html .= $this->recupera_html_controllo_ADV($configurazione);

			//-----------------------------------------------------------------------------------------
			// STAMPIAMO IL CONTROLLO DELLE SCENE

			$html .= $this->recupera_html_controllo_scene($configurazione);

			$html_controllo = $html;

			$html = '';


			//-----------------------------------------------------------------------------------------
			// STAMPIAMO LA CONFIGURAZIONE DEI NODI

			$html .= $this->recupera_html_lista_descrizione_elementi($configurazione['nodi'], 'nodo', 'nodi');

			//-----------------------------------------------------------------------------------------
			// STAMPIAMO LA CONFIGURAZIONE DEI GRUPPI

			$html .= $this->recupera_html_lista_descrizione_elementi($configurazione['gruppi'], 'gruppo', 'gruppi');

			//-----------------------------------------------------------------------------------------
			// STAMPIAMO LA CONFIGURAZIONE DELLE SCENE

			$html .= $this->recupera_html_lista_descrizione_elementi($configurazione['scene'], 'scena', 'scene');

			//-----------------------------------------------------------------------------------------
			// STAMPIAMO LA CONFIGURAZIONE NODI - GRUPPI

			$html .= $this->recupera_html_matrice_nodi_gruppi($configurazione);

			//-----------------------------------------------------------------------------------------
			// STAMPIAMO LA CONFIGURAZIONE SCENE - NODI

			$html .= $this->recupera_html_matrice_nodi_scene($configurazione);

			$html_configurazione = $html;

			return array('html_configurazione' => $html_configurazione, 'html_controllo' => $html_controllo,);
		}

		/**
		 * Questa funzione aggiorna la configurazione salvata su file con i dati
		 * provenienti dal modulo
		 */
		public function aggiorna_configurazione_nodi_gruppi_da_modulo($addr, $port, &$configurazione){
			$html = '';
			$risultato = 0;

			$DC = new DALI_ETHERNET_CLIENT($addr, $port, $configurazione['cmd_port']);
			$COMANDO = self::$COMANDO_DALI_NETWORK_SCANNING_REQUEST;
			$r = $DC->invia_comando($COMANDO);
			$html .= $r['html'];
			if ($r['risultato'] == 1){
				$risultato = 1;

				$risposta = $r['risposta'];
				$r = $DC->recupera_dati_risposta_COMANDO_DALI_NETWORK_SCANNING($risposta);
				$html .= $r['html'];
				if ($r['risultato']){
					if (!key_exists('nodi', $configurazione)) $configurazione['nodi'] = array();

					foreach ($r['nodi'] as $num_nodo => $nodo) if (!key_exists($num_nodo, $configurazione['nodi'])) $configurazione['nodi'][$num_nodo] = $nodo;else
						foreach ($nodo as $key => $value) $configurazione['nodi'][$num_nodo][$key] = $value;

					if (!key_exists('gruppi', $configurazione)) $configurazione['gruppi'] = array_fill(0, self::$MAX_NUM_GRUPPI_DALI, array('nodi' => array()));

					foreach ($configurazione['nodi'] as $num_nodo => $nodo) for ($num_gruppo = 0; $num_gruppo < self::$MAX_NUM_GRUPPI_DALI; $num_gruppo++) $configurazione['gruppi'][$num_gruppo]['nodi'][$num_nodo] = substr($nodo['gruppi'], $num_gruppo, 1);
				}
			}

			return array('html' => $html, 'risultato' => $risultato,);
		}

		public function aggiorna_configurazione_scene_da_modulo($addr, $port, &$configurazione){
			$risultato = 0;
			$html = '';

			$DC = new DALI_ETHERNET_CLIENT($addr, $port, $configurazione['cmd_port']);
			$COMANDO = self::$COMANDO_DALI_SCENE_SCANNING_REQUEST;
			$r = $DC->invia_comando($COMANDO);
			$html .= $r['html'];
			if ($r['risultato'] == 1){
				$risultato = 1;
				$risposta = $r['risposta'];
				$r = $DC->recupera_dati_risposta_COMANDO_DALI_SCENE_SCANNING($risposta);
				$html .= $r['html'];
				if ($r['risultato']){
					$scene_nodi = $r['scene'];

					if (!key_exists('scene', $configurazione)) $configurazione['scene'] = array();

					foreach ($scene_nodi as $num_nodo => $scene) foreach ($scene as $num_scena => $ADV){
						if (!key_exists($num_scena, $configurazione['scene'])) $configurazione['scene'][$num_scena] = array('nodi' => array());

						$configurazione['scene'][$num_scena]['nodi'][$num_nodo] = $ADV;
					}
				}
			}

			return array('html' => $html, 'risultato' => $risultato,);
		}

		protected function recupera_html_controllo_ADV($configurazione){
			$html = '';

			$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th colspan="2"><span class="glyphicon glyphicon-transfer"></span> Nodes and groups control<div style="float:right;"><button type="button" class="btn btn-sm btn-default" onclick="apri_o_chiudi(this);"><span class="glyphicon glyphicon-chevron-down"></span></button></div></th>
			  </tr>
			 </thead>
			 <tbody id="tbody_controllo_ADV">
			';

			$html .= $this->recupera_html_tbody_controllo_ADV($configurazione);

			$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';

			return $html;
		}

		public function recupera_html_tbody_controllo_ADV(&$configurazione){
			$html = '';

			$html .= '
		<tr>
		 <td>
		   
		   ' . $this->recupera_html_div_controllo_ADV($configurazione) . ' 
		   
		   <div class="block">
		    <br/>
		    <button id="btn_salvato" type="button" class="btn btn-success" style="display:none;">&nbsp;Saved</button> 
		    <button id="btn_salvando" type="button" class="btn btn-warning" style="display:none;">&nbsp;Saving...</button>
		   </div> 
		 
		 </td>
		</tr>
		';

			return $html;
		}

		public function recupera_html_div_controllo_ADV(&$configurazione, $DALI_ID = '', $ADV = ''){
			return '
		<div class="block">
		    <b>Item</b>
		    <br/>
		    ' . $this->recupera_html_select_nodo_gruppo_broadcast($configurazione['nodi'], $configurazione['gruppi'], 'elemento_da_controllare', 'nodo_gruppo_broadcast_scelto(this);', false, 80, $DALI_ID) . '
		   </div>
		   <div class="block">
		    <b>ADV</b>
		    <br/>
		    ' . $this->recupera_html_select_ADV($ADV, 'valore_ADV_scelto(this);', 254, 'controllo_ADV', $ADV === '') . '
		   </div>
		';
		}

		protected function recupera_html_select_nodo_gruppo_broadcast(&$nodi, &$gruppi, $id = 'elemento_da_controllare', $onchange = 'nodo_gruppo_broadcast_scelto(this);', $broadcast = false, $broadcast_value = 80, $selected_id = ''){
			$elements = array();

			$html = '
		<select 
			multiple 
			data-max-options="1"
			id = "' . $id . '"
			name = "elemento_da_controllare"
			onchange = "' . $onchange . '"
			class="selectpicker" 
			data-live-search="true" 
			data-width="fit" 
			data-size="10"  
			data-selected-text-format="count > 10">
		';

			// aggiungiamo i nodi
			$html .= '<optgroup label="Nodes">';
			for ($num_nodo = 0; $num_nodo < self::$MAX_NUM_NODI_DALI; $num_nodo++){
				if ($nodi[$num_nodo]['presente'] == 1){
					$html .= '<option value="' . $num_nodo . '"';

					if ($selected_id !== '' && $selected_id == $num_nodo) $html .= ' selected';

					$html .= ' data-content="<span class=\'label label-primary\'>Node #' . $num_nodo;
					if (is_array($nodi)) if (key_exists($num_nodo, $nodi)) if (key_exists('descrizione', $nodi[$num_nodo])) $html .= '</span>&nbsp;<span class=\'label label-warning\'>' . $nodi[$num_nodo]['descrizione'];
					$html .= '</span>&nbsp;"';

					$html .= '>';

					$html .= '</option>';
				}
			}
			$html .= '</optgroup>';

			$html .= '<optgroup label="Groups">';
			// aggiungiamo i gruppi
			for ($num_gruppo = 0; $num_gruppo < self::$MAX_NUM_GRUPPI_DALI; $num_gruppo++){
				$html .= '<option value="' . (self::$MAX_NUM_NODI_DALI + $num_gruppo) . '"';

				if ($selected_id == self::$MAX_NUM_NODI_DALI + $num_gruppo) $html .= ' selected';

				$html .= ' data-content="<span class=\'label label-danger\'>Group #' . $num_gruppo;
				if (is_array($gruppi)) if (key_exists($num_gruppo, $gruppi)) if (key_exists('descrizione', $gruppi[$num_gruppo])) $html .= '</span>&nbsp;<span class=\'label label-warning\'>' . $gruppi[$num_gruppo]['descrizione'];
				$html .= '</span>&nbsp;"';

				$html .= '>';

				$html .= '</option>';
			}
			$html .= '</optgroup>';

			$html .= '
		<optgroup label="Broadcast">
		 <option ' . (($broadcast || ($selected_id == $broadcast_value)) ? 'selected ' : '') . 'value="' . $broadcast_value . '" data-content="<span class=\'label label-success\'>Tutti</span>&nbsp;"></option>
		</optgroup>
		';

			$html .= '
		</select>
		';

			return $html;
		}

		protected function recupera_html_controllo_scene($configurazione){
			$html = '';

			$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th colspan="2"><span class="glyphicon glyphicon-transfer"></span> Scene control<div style="float:right;">
			   <button type="button" class="btn btn-sm btn-default" onclick="apri_o_chiudi(this);">
			   <span class="glyphicon glyphicon-chevron-down"></span></button></div></th>
			  </tr>
			 </thead>
			 <tbody id="tbody_controllo_scene">
			';

			$html .= $this->recupera_html_tbody_controllo_scene($configurazione);

			$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';

			return $html;
		}

		public function recupera_html_tbody_controllo_scene(&$configurazione){
			$html = '';

			$scene = $configurazione['scene'];

			$html .= '
		<tr>
		 <td>  
		
		   ' . $this->recupera_html_div_controllo_scene($configurazione) . ' 
		   
		   <div class="block">
		    <br/>
		    <button id="btn_scena_salvata" type="button" class="btn btn-success" style="display:none;">&nbsp;Saved</button> 
		    <button id="btn_salvando_scena" type="button" class="btn btn-warning" style="display:none;">&nbsp;Saving...</button>
		   </div> 
			
		 </td>
		</tr>
		';

			return $html;
		}

		public function recupera_html_div_controllo_scene(&$configurazione, $DALI_ID = '', $SCENE = ''){
			return '
		<div class="block">
		    <b>Item</b>
		    <br/>
		    ' . $this->recupera_html_select_nodo_gruppo_broadcast($configurazione['nodi'], $configurazione['gruppi'], 'elemento_scena', 'elemento_scena_scelto(this);', $DALI_ID == '', 127, $DALI_ID) . '
		   </div>
		   <div class="block">
		    <b>Scene</b>
		    <br/>
		    ' . $this->recupera_html_select_scena($configurazione['scene'], 'valore_scena_scelto(this);', $SCENE) . '
		   </div>
		';
		}

		protected function recupera_html_select_scena(&$scene, $onchange, $selected_id = ''){
			$elements = array();

			$html = '
		<select 
			multiple 
			data-max-options="1"
			id = "select_scena"
			name = "scena_da_controllare"
			onchange = "' . $onchange . '"
			class="selectpicker" 
			data-live-search="true" 
			data-width="fit" 
			data-size="10"  
			data-selected-text-format="count > 10">
		';

			// aggiungiamo i nodi
			for ($num_scena = 0; $num_scena < self::$MAX_NUM_SCENE_DALI; $num_scena++){
				$html .= '<option value="' . $num_scena . '"';

				if ($selected_id !== '' && $num_scena == $selected_id) $html .= ' selected';

				$html .= ' data-content="<span class=\'label label-primary\'>Scena #' . $num_scena;
				if (null != $scene) if (key_exists($num_scena, $scene)) if (key_exists('descrizione', $scene[$num_scena])) $html .= '</span>&nbsp;<span class=\'label label-warning\'>' . $scene[$num_scena]['descrizione'];
				$html .= '</span>&nbsp;"';

				$html .= '>';

				$html .= '</option>';
			}

			$html .= '
		</select>
		';

			return $html;
		}

		protected function recupera_html_lista_descrizione_elementi(&$elementi, $nome_elemento_singolare, $nome_elemento_plurale){
			$html = '';

			$El = ucfirst($nome_elemento_singolare);
			$el = strtolower($nome_elemento_singolare);
			$Els = ucfirst($nome_elemento_plurale);
			$els = strtolower($nome_elemento_plurale);

			if (count($elementi) > 0){
				$html_span_salvata = '<span class="label label-success">Saved</span>';
				$html_span_mancante = '<span class="label label-default">Missing</span>';

				$html .= '
			<div id="div_span_salvata" style="display:none;">' . $html_span_salvata . '</div>
			<div id="div_span_salvando" style="display:none;"><span class="label label-warning">Saving...</span></div>
			';

				$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th colspan="2"><span class="glyphicon glyphicon-cog"></span> ' . $Els . '<div style="float:right;"><button type="button" class="btn btn-sm btn-default" onclick="apri_o_chiudi(this);"><span class="glyphicon glyphicon-chevron-down"></span></button></div></th>
			  </tr>
			 </thead>
			 <tbody style="display:none;">
			';

				foreach ($elementi as $i => $elem){
					$html_elem = '<span class="label label-primary">' . $El . ' #' . $i . '</span>';

					$carica_azioni = false;

					if (key_exists('presente', $elem)){
						if ($elem['presente'] == 1){
							$carica_azioni = true;
							$html_elem .= ' <span class="label label-success">Connesso</span>';
						}else{
							$html_elem .= ' <span class="label label-danger">Non conesso</span>';
						}
					}

					$div_id = 'div_azioni_' . $el . '_' . $i;

					$html .= '
				<tr>
				 <td>
				  ' . ($carica_azioni ? '<br/>' : '') . '
				  <div class="input-group" style="width:100%">
  				   <span class="input-group-addon">
                    ' . $html_elem . '
                   </span>
  				   <input 
  				    value="' . (key_exists('descrizione', $elem) ? $elem['descrizione'] : '') . '"
  				   	id="descrizione_' . $el . '_' . $i . '" 
  				   	name="descrizione_' . $el . '" 
  				   	type="text" 
  				   	class="form-control keypress" 
  				   	placeholder="Descrizione ' . $El . ' #' . $i . '" 
  				   	aria-describedby="basic-addon2"> 
  				   <span class="input-group-addon" id="stato_' . $el . '_' . $i . '">
                    ' . (key_exists('descrizione', $elem) ? $html_span_salvata : $html_span_mancante) . '
                   </span>	
				  </div>
				 </td>
				 <td class="clickable" onclick="aggiorna_azione($(\'#' . $div_id . '\'),\'' . $this->addr . '\', ' . $this->port . ', ' . $this->cmd_port . ');">
				  ' . ($carica_azioni ? '
				  <div class="input-group carica azioni" id="' . $div_id . '">
				   <span class="glyphicon glyphicon-hourglass"></span>
				  </div>
				  ' : '') . '
				 </td>
				</tr>
				';
				}

				$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';
			}else{
				$html .= $this->recupera_html_errore('Il modulo <b>' . $addr . ':' . $port . '</b> non ha ' . $els . '.');
			}

			return $html;
		}

		protected function recupera_html_matrice_nodi_gruppi(&$configurazione){
			$html = '';

			$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th><span class="glyphicon glyphicon-th"></span> Nodes & Groups Matrix<div style="float:right;"><button type="button" class="btn btn-sm btn-default" onclick="apri_o_chiudi(this);"><span class="glyphicon glyphicon-chevron-down"></span></button></div></th>
			  </tr>
			 </thead>
			 <tbody id="matrice_nodi_gruppi" style="display:none;">
			';

			$html .= $this->recupera_html_tbody_matrice_nodi_gruppi($configurazione);

			$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';

			return $html;
		}

		public function recupera_html_tbody_matrice_nodi_gruppi(&$configurazione){
			$html = '';

			$gruppi = $configurazione['gruppi'];

			$html_span_salvato = '<span class="label label-success">Saved</span>';

			$html .= '
		<div id="div_span_salvato" style="display:none;">' . $html_span_salvato . '</div>
		';

			foreach ($gruppi as $num_gruppo => $dati_gruppo){
				$nodi_selezionati = array();
				foreach ($dati_gruppo['nodi'] as $num_nodo => $value) if ($value == 1) $nodi_selezionati[] = $num_nodo;

				$html .= '
			<tr>
			 <td>
			  <div style="display: flex;">
			   <div class="mblock">
			    <div class="input-group">
  				   <span class="input-group-addon">
                    <span class="label label-primary">Gruppo #' . $num_gruppo . '</span>' . (key_exists('descrizione', $dati_gruppo) ? ' <span class="label label-warning">' . $dati_gruppo['descrizione'] . '</span>' : '') . '
                   </span>
  				   <span class="input-group-addon" id="stato_nodi_gruppo_' . $num_gruppo . '">
                    ' . $html_span_salvato . '
                   </span>	
				  </div>
			    
			   </div>
			   <div style="flex:1;">
			    ' . $this->recupera_html_select_nodi_gruppo($configurazione['nodi'], $nodi_selezionati, $num_gruppo) . '
			   </div>
			  </div>
			 </td>
			</tr> 
			';
			}

			return $html;
		}

		protected function recupera_html_select_nodi_gruppo(&$nodi, &$nodi_selezionati, $num_gruppo){
			$elements = array();

			$html = '
		<select 
			multiple 
			id = "nodi_gruppo_' . $num_gruppo . '",
			name = "nodi_gruppo",
			onchange="aggiorna_nodi_gruppo(' . $num_gruppo . ');" 
			class="selectpicker" 
			data-live-search="true" 
			data-width="100%" 
			data-size="10" 
			data-actions-box="true" 
			data-selected-text-format="count > 10">
		';

			// aggiungiamo i nodi
			for ($num_nodo = 0; $num_nodo < self::$MAX_NUM_NODI_DALI; $num_nodo++){
				if ($nodi[$num_nodo]['presente'] == 1){
					$html .= '<option value="' . $num_nodo . '"';

					if (in_array($num_nodo, $nodi_selezionati)) $html .= ' selected';

					$html .= ' data-content="<span class=\'label label-primary\'>Node #' . $num_nodo;
					if (key_exists('descrizione', $nodi[$num_nodo])) $html .= '</span>&nbsp;<span class=\'label label-warning\'>' . $nodi[$num_nodo]['descrizione'];
					$html .= '</span>&nbsp;"';

					$html .= '>';

					$html .= '</option>';
				}
			}

			$html .= '
		</select>
		';

			return $html;
		}

		protected function recupera_html_matrice_nodi_scene(&$configurazione){
			$html = '';

			$html .= '
			<table class="table">
			 <thead>
			  <tr class="title">
			   <th colspan="2">
			    <span class="glyphicon glyphicon-th"></span> 
			    Scenes & Nodes Matrix
			    <div style="float:right;">
			     <button type="button" class="btn btn-sm btn-default" onclick="apri_o_chiudi(this);">
			      <span class="glyphicon glyphicon-chevron-down"></span>
			      </button>
			    </div>
			   </th>
			  </tr>
			 </thead>
			 <tbody id="matrice_nodi_scene" style="display:none;">
			';

			$html .= $this->recupera_html_tbody_matrice_nodi_scene($configurazione);

			$html .= '
			 </tbody>
			 <tfoot>
			 </tfoot>
			</table>
			';

			return $html;
		}

		public function recupera_html_tbody_matrice_nodi_scene(&$configurazione){
			$html = '';

			// questo array contine le 16 scene.
			// per ogni scena ha un campo 'descrizione' e un campo 'nodi'
			// nel campo 'nodi' ci sono i 64 nodi col valore da 0 a 255 impostato
			$scene = $configurazione['scene'];
			$nodi = $configurazione['nodi'];

			//DEBUG
			//$html .= '<tr><td>'.$this->recupera_html_info(print_r($scene,true)).'</td></tr>';

			foreach ($scene as $num_scena => $scena){
				$html .= '
			<tr id="tr_scena_' . $num_scena . '">
			 <td><b>Scena #' . $num_scena . (key_exists('descrizione', $scena) ? '</b><br/><small><i>' . $scena['descrizione'] . '</i></small>' : '') . '</td>
			 <td>
			';

				foreach ($scena['nodi'] as $num_nodo => $ADV) if ($nodi[$num_nodo]['presente'] == 1) $html .= '<div class="block nodo-scena">' . $this->recupera_html_div_nodo_scena($num_nodo, $ADV, false, (key_exists('descrizione', $nodi[$num_nodo]) ? $nodi[$num_nodo]['descrizione'] : '')) . '</div>';

				$html .= '
			 </td>
			</tr>
			';
			}

			return $html;
		}

		public function recupera_html_div_nodo_scena($num_nodo, $ADV, $modifica = false, $descrizione = ''){
			$html = '';

			$html_num_nodo = '<span class="label label-primary">Nodo #' . $num_nodo . '</span>&nbsp;';
			if (strlen($descrizione) > 0) $html_num_nodo .= '<span class="label label-warning">' . $descrizione . '</span>&nbsp;';
			$html_des_nodo = '';
			$html_adv_nodo = $modifica ? $this->recupera_html_select_ADV($ADV, 'esegui_modifica_nodo_scena(this,' . $num_nodo . ',' . $ADV . ');') : $this->recupera_html_ADV($ADV);
			$html_but_nodo = $modifica ? '
			<button type="button" class="btn btn-xs btn-danger" onclick="annulla_modifica_nodo_scena(this,' . $num_nodo . ',' . $ADV . ');">
	         <span class="glyphicon glyphicon-remove"></span> Annulla
	        </button>' : '';

			$html_div_tags = !$modifica ? ' onclick="mostra_modifica_nodo_scena(this,' . $num_nodo . ',' . $ADV . ');" class="nodo clickable' . ($ADV == 255 ? ' off' : '') . '"' : ' class="nodo highlight"';

			$html .= '<div' . $html_div_tags . '>' . $html_num_nodo . $html_des_nodo . $html_adv_nodo . $html_but_nodo . '</div>';

			return $html;
		}

		public function salva_descrizione($addr, $port, $tipo, $num, $testo){
			// recuperiamo la configurazione
			$configurazione = $this->recupera_configurazione_da_addr_port($addr, $port);

			switch ($tipo){
				case 'nodo':
					if (key_exists('nodi', $configurazione)) if (key_exists($num, $configurazione['nodi'])) $configurazione['nodi'][$num]['descrizione'] = $testo;
					break;
				case 'gruppo':
					if (key_exists('gruppi', $configurazione)) if (key_exists($num, $configurazione['gruppi'])) $configurazione['gruppi'][$num]['descrizione'] = $testo;
					break;
				case 'scena':
					if (key_exists('scene', $configurazione)) if (key_exists($num, $configurazione['scene'])) $configurazione['scene'][$num]['descrizione'] = $testo;
					break;
			}

			$this->salva_configurazione_da_addr_port($addr, $port, $configurazione);
		}

		public function esegui_controllo_scena($addr, $port, $SCENA, $DALI_ID){
			$risultato = 0;
			$html = '';

			$DC = new DALI_ETHERNET_CLIENT($addr, $port);
			$COMANDO = self::$COMANDO_SCENE_SETTING_REQUEST;
			$PARAMETRI = array('SCENE' => $SCENA, 'COUNTER' => $DC->COUNTER, 'DALI_ID' => $DALI_ID,);
			$r = $DC->invia_comando($COMANDO, $PARAMETRI);
			$html .= $r['html'];
			if ($r['risultato'] == 1) $risultato = 1;

			return array('html' => $html, 'risultato' => $risultato,);
		}

		public function esegui_controllo_ADV($addr, $port, $ADV, $DALI_ID){
			$risultato = 0;
			$html = '';

			$DC = new DALI_ETHERNET_CLIENT($addr, $port);
			$COMANDO = self::$COMANDO_ADV_SETTING_REQUEST;
			$PARAMETRI = array('ADV' => $ADV, 'COUNTER' => $DC->COUNTER, 'DALI_ID' => $DALI_ID,);
			$r = $DC->invia_comando($COMANDO, $PARAMETRI);
			$html .= $r['html'];

			if ($r['risultato'] == 1) $risultato = 1;

			return array('html' => $html, 'risultato' => $risultato,);
		}

		public function esegui_modifica_nodo_scena($addr, $port, $cmd_port, $num_scena, $num_nodo, $ADV){
			$risultato = 0;
			$html = '';

			$DC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);
			$COMANDO = self::$COMANDO_SCENE_PROGRAMMING_REQUEST;
			$PARAMETRI = array('ADV' => $ADV, 'SCENE' => $num_scena, 'COUNTER' => $DC->COUNTER, 'DALI_ID' => $num_nodo,);
			$r = $DC->invia_comando($COMANDO, $PARAMETRI);
			$html .= $r['html'];
			if ($r['risultato'] == 1) $risultato = 1;

			return array('html' => $html, 'risultato' => $risultato,);
		}

		public function aggiorna_nodi_gruppo($addr, $port, $cmd_port, $num_gruppo, $nodi_gruppo){
			$risultato = 1;
			$html = '';

			$DC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);

			// carichiamo la configurazione dal modulo
			$configurazione = array();
			$this->aggiorna_configurazione_nodi_gruppi_da_modulo($addr, $port, $configurazione);

			// controlliamo i 64 nodi
			for ($num_nodo = 0; $num_nodo < self::$MAX_NUM_NODI_DALI; $num_nodo++){
				// mandiamo un messaggio solo se discorde con lo stato attuale del modulo
				$ENABLE = in_array($num_nodo, $nodi_gruppo) ? 1 : 0;
				if ($ENABLE != $configurazione['gruppi'][$num_gruppo]['nodi'][$num_nodo]){
					$COMANDO = self::$COMANDO_GROUP_SETTING_REQUEST;
					$PARAMETRI = array('GROUP' => $num_gruppo, 'COUNTER' => $DC->COUNTER, 'DALI_ID' => $num_nodo, 'AZIONE' => $ENABLE,);
					$r = $DC->invia_comando($COMANDO, $PARAMETRI);
					if ($r['risultato'] == 0) $risultato = 0;
					$html .= $r['html'];
				}
			}

			return array('risultato' => $risultato, 'html' => $html,);
		}
	}
	