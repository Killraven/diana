<?php

//If the HTTPS is not found to be "on"
	if (!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
		if ($_SERVER["HTTP_HOST"] != '127.0.0.1' && $_SERVER["HTTP_HOST"] != 'localhost'){
			//Tell the browser to redirect to the HTTPS URL.
			header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
			//Prevent the rest of the script from executing.
			exit;
		}
	}

	header("location: forms/EventList/eventlist.php");
	exit;

?>