<html xmlns="http://www.w3.org/1999/xhtml"><head>
  <title>YUN Config v1.0</title>
  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <!-- This one is usefull to activate the RESPONSIVENESS of bootstrap-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
           
  <!-- Favicon-->
  <link rel="shortcut icon" type="image/png" href="../img/favicon.ico"/>
           
  <!-- Context-Dependent CSS -->                             
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-select.min.css"> 
  <link rel="stylesheet" type="text/css" href="../css/yun.css?<?php echo rand(0,65535); ?>"> 
  
  <!-- JS functions needed while page is loading (e.g. progress bar)... -->
  <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>   
  <script type="text/javascript" src="../js/bootstrap.min.js"></script> 
  <script type="text/javascript" src="../js/bootstrap-select.min.js"></script>  
  <script type="text/javascript" src="../js/yun.js?<?php echo rand(0,65535); ?>"></script>                                   
   </head>
   <body>    
       
<?php

error_reporting(E_ALL);

require_once __DIR__.'/../class/arduino_yun_config.class.php';

$YUN = new ARDUINO_YUN_CONFIG();

$html = '';

require_once __DIR__.'/../class/html.class.php';

$html .= HTML_Dali::recupera_html_barra_navigazione('yun');

$html .= '

<div class="block">
 <img src="../img/arduino-yun.jpg" height="100px">
</div>

<div class="block">
 <h1><span class="label label-default">Arduino Yun</span></h1>
 <h4><br/><span class="label label-primary">Configurazione</span> '.$YUN->recupera_html_button_download_configurazione().'</h4>
</div>

<table class="table"><tr></tr></table>

<div id="div_loading" style="display:none;"><img src="../img/loading.gif"></div>
';

$html .= $YUN->recupera_html_pannelli();

echo $html;

?>
 
	</body>
	</html>
