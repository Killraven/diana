<html xmlns="http://www.w3.org/1999/xhtml"><head>
  <title>DALI Client v1.0</title>
  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <!-- This one is usefull to activate the RESPONSIVENESS of bootstrap-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
           
  <!-- Favicon-->
  <link rel="shortcut icon" type="image/png" href="../img/favicon.ico"/>
           
  <!-- Context-Dependent CSS -->                             
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-select.min.css"> 
  <link rel="stylesheet" type="text/css" href="../css/client.css?<?php echo rand(0,65535); ?>"> 
  
  <!-- JS functions needed while page is loading (e.g. progress bar)... -->
  <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>   
  <script type="text/javascript" src="../js/bootstrap.min.js"></script> 
  <script type="text/javascript" src="../js/bootstrap-select.min.js"></script>  
  <script type="text/javascript" src="../js/client.js?<?php echo rand(0,65535); ?>"></script>                                   
<link rel="stylesheet" type="text/css" href="../js/ui-lightness/jquery.ui.all.css"/>
    <link rel="stylesheet" type="text/css" href="../js/ui-lightness/jquery.ui.datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/bootstrap/css/bootstrap.css"/>


    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="../assets/css/diana.css">
    <link rel="stylesheet" href="../assets/css/Profile-Card.css">
    <link rel="stylesheet" href="../assets/css/styles.css">  
<style>
        .error_border { border: 2px solid red !important; }

        /* css for timepicker */
        .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }

        .ui-timepicker-div dl { text-align: left; }

        .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }

        .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }

        .ui-timepicker-div td { font-size: 90%; }

        .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

        .ui-slider { position: relative; text-align: left; }

        .ui-slider .ui-slider-handle { position: absolute; z-index: 2; width: 1.2em; height: 1.2em; cursor: default; }

        .ui-slider .ui-slider-range { position: absolute; z-index: 1; font-size: .7em; display: block; border: 0; background-position: 0 0; }

        .ui-slider-horizontal { height: .8em; }

        .ui-slider-horizontal .ui-slider-handle { top: -.3em; margin-left: -.6em; }

        .ui-slider-horizontal .ui-slider-range { top: 0; height: 100%; }

        .ui-slider-horizontal .ui-slider-range-min { left: 0; }

        .ui-slider-horizontal .ui-slider-range-max { right: 0; }

        .ui-slider-vertical { width: .8em; height: 100px; }

        .ui-slider-vertical .ui-slider-handle { left: -.3em; margin-left: 0; margin-bottom: -.6em; }

        .ui-slider-vertical .ui-slider-range { left: 0; width: 100%; }

        .ui-slider-vertical .ui-slider-range-min { bottom: 0; }

        .ui-slider-vertical .ui-slider-range-max { top: 0; }

        #logout { text-decoration: none; color: inherit; }

        #logout:hover { text-decoration: underline; }

        .form-control {
            display: block;
            width: 100%;
            height: 34px;
            padding: 2px 12px;
            font-size: 12px;
            line-height: 1.42857143;
            color: #555555;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            }
    </style>
 </head>
   <body>    
<div class="profile-card" style="height:950px; text-align: left;">

    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->

    <p class="profile-bio" style="/*padding:0px;*/color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;text-align: center;">
        <strong>Dali Client</strong></p>

<?php

error_reporting(E_ALL);

require_once __DIR__.'/../class/dali_ethernet_client.class.php';

$DC = new DALI_ETHERNET_CLIENT();

$html = '';

require_once __DIR__.'/../class/html.class.php';

//$html .= HTML_Dali::recupera_html_barra_navigazione('client');

$html .= '


<table class="table"><tr></tr></table>

<div class="panel panel-primary">
 <div class="panel-heading"><span class="glyphicon glyphicon-wrench"></span>&nbsp;Lan configuration
 </div>
 <div class="panel-body">
  
    <div style="float:left; padding-right:10px;">
	 <b>IP Address</b>
	 <br/>
	 <input type="text" class="form-control" placeholder="IP Address" aria-describedby="basic-addon2" value="'.$DC->addr.'" id="addr">
	</div>
	
	<div style="float:left; padding-right:10px;">
	 <b>Control Port</b>
	 <br/>
	 <input type="text" class="form-control" placeholder="Control Port" aria-describedby="basic-addon2" value="'.$DC->port.'" id="port">
	</div>
  
  	<div style="float:left; padding-right:10px;">
	 <b>Command Port</b>
	 <br/>
	 <input type="text" class="form-control" placeholder="Command Port" aria-describedby="basic-addon2" value="'.$DC->cmd_port.'" id="cmd_port">
	</div>
	
 </div>
</div>

<div class="panel panel-primary">
 <div class="panel-heading"><span class="glyphicon glyphicon-transfer"></span>&nbsp;Single command execution
 </div>
 <div class="panel-body">
  	
	<div id="loading" style="display:none;"><img src="../img/loading.gif"></div>
  
    <div class="btn-group" role="group" id="btn_grp_comandi">
	  
	  <button type="button" class="btn btn-default" onclick="comando($(this),'.DALI_ETHERNET::$COMANDO_DALI_NETWORK_SCANNING_REQUEST.');" title="This command is used to get the informations from the DALI nodes configured in the network.">
	   <span class="glyphicon glyphicon-search"></span>&nbsp;Network scanning
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="comando($(this),'.DALI_ETHERNET::$COMANDO_DALI_SCENE_SCANNING_REQUEST.');" title="This command is used to get the scenes configured into the DALI nodes configured in the network.">
	   <span class="glyphicon glyphicon-search"></span>&nbsp;Scene scanning
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="parametri_comando($(this),'.DALI_ETHERNET::$COMANDO_ADV_SETTING_REQUEST.');" title="This command is used to set the ADV for a specific DALI node, for a Group or for the entire DALI network.">
	   <span class="glyphicon glyphicon-lamp"></span>&nbsp;ADV Setting
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="parametri_comando($(this),'.DALI_ETHERNET::$COMANDO_SCENE_SETTING_REQUEST.');" title="This command is used to activate a programmed scene in a single DALI node, in a group or for the entire DALI network.">
	   <span class="glyphicon glyphicon-lamp"></span>&nbsp;Scene Setting
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="parametri_comando($(this),'.DALI_ETHERNET::$COMANDO_GENERIC_DALI_COMMAND_REQUEST.');" title="This command is used to send a generic DALI command to a DALI node, to a group or to the entire network.">
	   <span class="glyphicon glyphicon-console"></span>&nbsp;Generic DALI command
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="parametri_comando($(this),'.DALI_ETHERNET::$COMANDO_SCENE_PROGRAMMING_REQUEST.');" title="This commands are used to program the scenes inside the DALI nodes. It is necessary to send two consecutive commands for the programming.">
	   <span class="glyphicon glyphicon-cog"></span>&nbsp;Scene programming
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="parametri_comando($(this),'.DALI_ETHERNET::$COMANDO_GROUP_SETTING_REQUEST.');" title="This command is used to enable/disable the membership of the DALI nodes inside the group.">
	   <span class="glyphicon glyphicon-cog"></span>&nbsp;Group Setting
	  </button>
	  
	  <button type="button" class="btn btn-default" onclick="parametri_comando($(this),'.DALI_ETHERNET::$COMANDO_NODE_ID_SETTING_REQUEST.');" title="These commands are used to program the ID Address on the DALI nodes. It is necessary to send them consecutively.">
	   <span class="glyphicon glyphicon-cog"></span>&nbsp;Node ID Setting
	  </button>
	  
	</div>
	
	<br/><br/>

	<div class="panel panel-default">
	 <div class="panel-heading"><span class="glyphicon glyphicon-wrench"></span>&nbsp;Parameters
	 </div>
	 <div class="panel-body"> 	
		<div id="parametri">
		</div>
	 </div>
	</div>
	
	<div class="panel panel-default">
	 <div class="panel-heading"><span class="glyphicon glyphicon-circle-arrow-left"></span>&nbsp;Request
	 </div>
	 <div class="panel-body"> 	
		<div id="richiesta">
		</div>
	 </div>
	</div>
	
	<div class="panel panel-default">
	 <div class="panel-heading"><span class="glyphicon glyphicon-circle-arrow-right"></span>&nbsp;Response
	 </div>
	 	<div class="panel-body">	
		<div id="risposta">
		</div>
	 </div>
	</div>
	
	<div class="panel panel-default">
	 <div class="panel-heading"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Log
	 </div>
	 	<div class="panel-body">	
		<div id="log">
		</div>
	 </div>
	 
</div>
<div id="divFooter"></div>
<nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
    <div class="container-fluid" style="text-align: center;">
        <a class="navbar-brand bg-primary m-auto fixed-bottom" style="height:30px;" href="#">Copyright Paolo Maruotti 2018</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
        <div class="collapse navbar-collapse" id="navcol-2">
	
 </div>
</div>	
</nav>

';

echo $html;

?>
 
	</body>
	</html>
