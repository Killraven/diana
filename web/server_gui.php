<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Diana DALI Server</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <!-- This one is usefull to activate the RESPONSIVENESS of bootstrap-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <!-- Favicon-->
  <link rel="shortcut icon" type="image/png" href="../img/favicon.ico"/>
           
  <!-- Context-Dependent CSS -->                             
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/server.css?<?php echo rand(0,65535); ?>"> 
  
  <!-- JS functions needed while page is loading (e.g. progress bar)... -->
    <!--  <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>-->
    <script type="text/javascript" src="../js/jquery-1.9.1.js"></script>

  <script type="text/javascript" src="../js/bootstrap.min.js"></script>   
  <script type="text/javascript" src="../js/server.js?<?php echo rand(0,65535); ?>"></script>                                   


    <link rel="stylesheet" type="text/css" href="../js/ui-lightness/jquery.ui.all.css"/>
    <link rel="stylesheet" type="text/css" href="../js/ui-lightness/jquery.ui.datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="../assets/css/diana.css">
    <link rel="stylesheet" href="../assets/css/Profile-Card.css">
    <link rel="stylesheet" href="../assets/css/styles.css">

<style>
        .error_border { border: 2px solid red !important; }

        /* css for timepicker */
        .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }

        .ui-timepicker-div dl { text-align: left; }

        .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }

        .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }

        .ui-timepicker-div td { font-size: 90%; }

        .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

        .ui-slider { position: relative; text-align: left; }

        .ui-slider .ui-slider-handle { position: absolute; z-index: 2; width: 1.2em; height: 1.2em; cursor: default; }

        .ui-slider .ui-slider-range { position: absolute; z-index: 1; font-size: .7em; display: block; border: 0; background-position: 0 0; }

        .ui-slider-horizontal { height: .8em; }

        .ui-slider-horizontal .ui-slider-handle { top: -.3em; margin-left: -.6em; }

        .ui-slider-horizontal .ui-slider-range { top: 0; height: 100%; }

        .ui-slider-horizontal .ui-slider-range-min { left: 0; }

        .ui-slider-horizontal .ui-slider-range-max { right: 0; }

        .ui-slider-vertical { width: .8em; height: 100px; }

        .ui-slider-vertical .ui-slider-handle { left: -.3em; margin-left: 0; margin-bottom: -.6em; }

        .ui-slider-vertical .ui-slider-range { left: 0; width: 100%; }

        .ui-slider-vertical .ui-slider-range-min { bottom: 0; }

        .ui-slider-vertical .ui-slider-range-max { top: 0; }

        #logout { text-decoration: none; color: inherit; }

        #logout:hover { text-decoration: underline; }

        .form-control {
            display: block;
            width: 100%;
            height: 22px;
            padding: 2px 12px;
            font-size: 12px;
            line-height: 1.42857143;
            color: #555555;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            }
    </style>   
</head>
   <body>
   	  	
<div class="profile-card" style="height:950px; text-align: left;">

    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->

    <p class="profile-bio" style="/*padding:0px;*/color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;text-align: center;">
        <strong>Dali Server Administration</strong></p>

<?php

require_once __DIR__.'/../class/dali_ethernet.class.php';

error_reporting(E_ALL);

$html = '';

$html .= '

<div id="sessione" style="display:none;"></div>

<div class="block">
 <br/>
 <img src="../img/HD678DA-B2.png" width="50px">
</div>

<div class="block">
 <h1><span class="label label-default">HD678DA-B2</span></h1>
 <h4><br/><span class="label label-primary">Emulator</span></h4>
</div>

<table class="table"><tr></tr></table>

<div class="panel panel-primary">
 
 <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span>&nbsp;Comandi
 </div>
 
 <div class="panel-body">
  
  <div class="block">
   <button type="button" onclick="avvia_server();" class="btn btn-success" id="START"><span class="glyphicon glyphicon-play"></span>&nbsp;Start</button>
   <button type="button" onclick="ferma_server();" class="btn btn-danger" id="STOP" style="display:none;"><span class="glyphicon glyphicon-stop"></span>&nbsp;Stop</button>
  </div>
  
  <div class="block">
   <button class="btn btn-default" type="button" id="btn_addr">
    <div class="block"><span class="badge">IP</span></div> <div class="block" id="addr">'.DALI_ETHERNET::$ADDR_NULL.'</div> 
   </button>
  </div>
  
  <div class="block">
   <button class="btn btn-default" type="button" id="btn_port">
    <div class="block"><span class="badge">Port</span></div> <div class="block" id="port">'.DALI_ETHERNET::$PORT_NULL.'</div> 
   </button>
  </div>
  
  <div class="block">
   <button class="btn btn-default" type="button" id="btn_port_cmd">
    <div class="block"><span class="badge">Command Port</span></div> <div class="block" id="port_cmd">'.DALI_ETHERNET::$PORT_NULL.'</div> 
   </button>
  </div>
  
 </div>
</div>

<div class="panel panel-primary">
 <div class="panel-heading"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Log
  <div style="float:right;">
   <button type="button" class="btn btn-xs btn-danger" onclick="svuota_log();">
    <span class="glyphicon glyphicon-trash"></span>&nbsp;Svuota
    </button>
  </div>
 </div>
 <div class="panel-body">
  <div class="log" id="log"></div>
  <input id="len" style="display:none;" value="0"/>
 </div>
</div>

<div class="panel panel-primary">
 <div class="panel-heading"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Stato
 </div>
 <div class="panel-body">
  <div id="stato"></div>
 </div>
</div>
';

echo $html;

?>
</div>
<div id="divFooter"></div>
<nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
    <div class="container-fluid" style="text-align: center;">
        <a class="navbar-brand bg-primary m-auto fixed-bottom" style="height:30px;" href="#">Copyright Paolo Maruotti 2018</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
        <div class="collapse navbar-collapse" id="navcol-2">

        </div>
    </div>
</nav>

	</body>
	</html>