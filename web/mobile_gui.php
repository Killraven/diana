<html xmlns="http://www.w3.org/1999/xhtml"><head>
  <title>DALI Mobile v1.0</title>
  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <!-- This one is usefull to activate the RESPONSIVENESS of bootstrap-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
           
  <!-- Favicon-->
  <link rel="shortcut icon" type="image/png" href="../img/favicon.ico"/>
           
  <!-- Context-Dependent CSS -->                             
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-select.min.css"> 
  <link rel="stylesheet" type="text/css" href="../css/mobile.css?<?php echo rand(0,65535); ?>">
  
  <!-- JS functions needed while page is loading (e.g. progress bar)... -->
  <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>   
  <script type="text/javascript" src="../js/bootstrap.min.js"></script> 
  <script type="text/javascript" src="../js/bootstrap-select.min.js"></script>  
  <script type="text/javascript" src="../js/mobile.js?<?php echo rand(0,65535); ?>"></script>                                   
   </head>
   <body background="../img/clr.jpg">    
       
<?php

require_once __DIR__.'/../class/arduino_yun_config.class.php';

$AYC = new ARDUINO_YUN_CONFIG();

$lista_pulsanti = $AYC->recupera_lista_pulsanti();

$html = '';

if (count($lista_pulsanti) > 0)
{
	foreach($lista_pulsanti as $num_pulsante => $pulsante)
	{
		if (count($pulsante['comandi']) > 0)
		{
			$descrizione = $pulsante['descrizione'];
			if (strlen($descrizione) == 0)
			 $descrizione = 'Pulsante #'.($num_pulsante+1);
			
			$html .= '
			<br/>
			<button type="button" class="btn btn-block btn-lg btn-primary" onclick="premi_pulsante('.$num_pulsante.');"><span class="glyphicon glyphicon-cog"></span> '.$descrizione.'</button>
			<br/>
			';
		}	
	}	
}

echo $html;

?>
 
	</body>
	</html>