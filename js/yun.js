//-----------------------------------------------------------------------------
// FUNZIONI DI PRIMO CARICAMENTO DELLA PAGINA
//-----------------------------------------------------------------------------

window.onload = onLoad;

function onLoad() {
    aggiorna_pulsanti();
}

function aggiorna_pulsanti() {
    var comando = 'carica_configurazione_pulsanti';
    var url = '../ajax/ajax.php';
    var div_loading = $('#div_loading');
    var div_landing = $('#panel_body_pulsanti');

    $.ajax({
        type: "POST", url: url, data: {comando: comando}, beforeSend: function () {
            div_landing.html(div_loading.html());
        }
    })
        .done(function (html) {
            div_landing.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function apri_o_chiudi(button) {
    var span = $(button).find('span');
    var chiudi = (span.hasClass('glyphicon-chevron-up'));
    var tbody = $(button).closest('table').find('tbody').first();
    if (chiudi) {
        span.addClass('glyphicon-chevron-down');
        span.removeClass('glyphicon-chevron-up');
        tbody.hide(200);
    } else {
        span.addClass('glyphicon-chevron-up');
        span.removeClass('glyphicon-chevron-down');
        tbody.show(200);
    }
}

function salva_id(dom, num) {
    var td = $(dom).closest('td');
    td.removeClass('modifying');

    var comando = 'salva_id_pulsante';

    var url = '../ajax/ajax.php';

    var desc = $('#input_id_pulsante_' + num).val();

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num: num, desc: desc}, beforeSend: function () {

        }
    })
        .done(function (html) {
            td.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function annulla_salva_id(dom, num) {
    var td = $(dom).closest('td');
    td.removeClass('modifying');

    var comando = 'annulla_salva_id_pulsante';

    var desc = $('#input_old_id_pulsante_' + num).val();

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num: num, desc: desc}, beforeSend: function () {

        }
    })
        .done(function (html) {
            td.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function click_id_pulsante(dom, num, desc) {
    var td = $(dom).closest('td');

    td.addClass('modifying');
    var comando = 'carica_modifica_id_pulsante';

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num: num, desc: desc}, beforeSend: function () {

        }
    })
        .done(function (html) {
            td.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function premi_pulsante(btn) {
    $.ajax({
        type: "POST", url: './../api/button.php', data: {btn: btn}
    });
}

function aggiungi_comando(dom, num_pulsante, num_comando) {
    var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);

    var comando = 'mostra_aggiungi_comando';

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
            $('select').selectpicker();
            aggiorna_stato_moduli();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_stato_moduli() {
    /*
    var comando = 'aggiorna_stato_modulo';
    var url = '../ajax/ajax.php';

    $('span.stato_modulo').each( function () {

        var span = $(this);

        var arr = span.attr('id').split('_');
        var addr = arr[1]+'.'+arr[2]+'.'+arr[3]+'.'+arr[4];
        var escaped_addr = arr[1]+'_'+arr[2]+'_'+arr[3]+'_'+arr[4];
        var port = arr[5];

        var modulo = $('#modulo_'+escaped_addr+'_'+port);
        var button_apri = $('#btn_apri_'+escaped_addr+'_'+port);
        var button_elimina = $('#btn_elimina_'+escaped_addr+'_'+port);

        $.ajax(
        {
         timeout: 2000, // sets timeout to 2 seconds
         type: "POST",
         url: url,
         data: { comando:comando, addr:addr, port:port },
         beforeSend: function()
             {
                  span.html('In connessione...');
             }
        })
        .done
         ( function( json )
            {
                var arr = JSON.parse(json);
                if (arr['risultato'] == 1)
                {
                    span.addClass('label-success');
                    span.removeClass('label-default');
                    span.removeClass('label-danger');
                    modulo.addClass('ok');
                    modulo.removeClass('ko');
                    button_apri.show();
                    button_elimina.hide();
                }
                else
                {
                    span.addClass('label-danger');
                    span.removeClass('label-default');
                    span.removeClass('label-success');
                    modulo.addClass('ko');
                    modulo.removeClass('ok');
                    button_apri.hide();
                    button_elimina.show();
                }
                span.html(arr['stato']);
                $('select').selectpicker();
            }
         )
        .fail
         ( function( jqXHR, textStatus )
            {
                if(textStatus === 'timeout')
                {
                    span.addClass('label-danger');
                    span.removeClass('label-default');
                    span.removeClass('label-success');
                    modulo.addClass('ko');
                    modulo.removeClass('ok');
                    button_apri.hide();
                    button_elimina.show();
                    span.html('Offline');
                }
            }
         );
    });
    */
}

function converter_scelto(dom, num_pulsante, num_comando) {
    carica_tipo_comando_scelto(dom, num_pulsante, num_comando);
}

function comando_scelto(dom, num_pulsante, num_comando) {
    carica_tipo_comando_scelto(dom, num_pulsante, num_comando);
}

function carica_tipo_comando_scelto(dom, num_pulsante, num_comando) {
    var arr_tipo = $('#tipo_' + num_pulsante + '_' + num_comando).val();
    var arr_converter = $('#converter_' + num_pulsante + '_' + num_comando).val();
    var landing = $('#div_comando_' + num_pulsante + '_' + num_comando);
    if (arr_tipo.length == 1 && arr_converter.length == 1) {
        var comando = 'carica_tipo_comando_scelto';
        var tipo = arr_tipo[0];
        var converter = arr_converter[0].split(':');
        var addr = converter[0];
        var port = converter[1];

        var url = '../ajax/ajax.php';

        $.ajax({
            type: "POST", url: url, data: {comando: comando, tipo: tipo, num_pulsante: num_pulsante, num_comando: num_comando, addr: addr, port: port}, beforeSend: function () {

            }
        })
            .done(function (html) {
                landing.html(html);
                $('select').selectpicker();
                aggiorna_btn_salva_comando(dom);
            })
            .fail(function (jqXHR, textStatus) {

            });
    } else {
        landing.html('');
        aggiorna_btn_salva_comando(dom);
    }
}

function nodo_gruppo_broadcast_scelto(dom) {
    var id = $(dom).closest('td').attr('id');
    var select = $('#select_valore_' + id);
    select.val('');
    if ($(dom).val().length == 0) select.prop('disabled', 'disabled'); else select.prop('disabled', false);
    select.selectpicker('refresh');
    aggiorna_btn_salva_comando(dom);
}

function aggiorna_btn_salva_comando(dom) {
    var id = $(dom).closest('td').attr('id');
    var btn = $('#btn_salva_comando_' + id);
    var nascondi = true;

    if ($('#converter_' + id).val().length == 1 && $('#tipo_' + id).val().length == 1) {
        if ($('#select_elemento_' + id).length) {
            if ($('#select_elemento_' + id).val().length == 1) {
                $('#select_valore_' + id).prop('disabled', false);
                if ($('#select_valore_' + id).val().length == 1) {
                    nascondi = false;
                }
            } else {
                $('#select_valore_' + id).prop('disabled', 'disabled');
            }
        }
    }

    if (nascondi) btn.hide(); else btn.show();
}

function elemento_scena_scelto(dom) {
    aggiorna_btn_salva_comando(dom);
}

function valore_scena_scelto(dom) {
    aggiorna_btn_salva_comando(dom);
}

function valore_ADV_scelto(dom) {
    aggiorna_btn_salva_comando(dom);
}

function annulla_comando(dom, num_pulsante, num_comando) {
    var comando = 'annulla_comando';
    var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function elimina_comando(dom, num_pulsante, num_comando) {
    if (confirm('Sicuro di eliminare questo comando?')) {
        var comando = 'elimina_comando';
        var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);
        var url = '../ajax/ajax.php';

        $.ajax({
            type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando}, beforeSend: function () {

            }
        })
            .done(function (html) {
                landing.html(html);
            })
            .fail(function (jqXHR, textStatus) {

            });
    }
}

function salva_comando(dom, num_pulsante, num_comando) {
    var comando = 'salva_comando';
    var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);

    var id = num_pulsante + '_' + num_comando;

    var converter = $('#converter_' + num_pulsante + '_' + num_comando).val()[0];
    var tipo = $('#tipo_' + num_pulsante + '_' + num_comando).val()[0];

    var url = '../ajax/ajax.php';

    var elemento = $('#select_elemento_' + id).val()[0];
    var valore = $('#select_valore_' + id).val()[0];

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando, converter: converter, tipo: tipo, elemento: elemento, valore: valore}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function modifica_comando(dom, num_pulsante, num_comando) {
    var comando = 'modifica_comando';
    var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
            $('select').selectpicker();
            aggiorna_stato_moduli();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aumenta_comando(dom, num_pulsante, num_comando) {
    var comando = 'aumenta_comando';
    var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function diminuisci_comando(dom, num_pulsante, num_comando) {
    var comando = 'diminuisci_comando';
    var landing = $('#td_lista_comandi_pulsante_' + num_pulsante);

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante, num_comando: num_comando}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function elimina_pulsante(num_pulsante) {
    if (confirm('Sicuro di eliminare il pulsante #' + num_pulsante + '?')) {
        var comando = 'elimina_pulsante';
        var url = '../ajax/ajax.php';

        $.ajax({
            type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante}, beforeSend: function () {

            }
        })
            .done(function (html) {
                aggiorna_pulsanti();
            })
            .fail(function (jqXHR, textStatus) {

            });
    }
}

function clona_pulsante(num_pulsante) {
    var comando = 'clona_pulsante';
    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante}, beforeSend: function () {

        }
    })
        .done(function (html) {
            aggiorna_pulsanti();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aumenta_pulsante(num_pulsante) {
    var comando = 'aumenta_pulsante';
    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante}, beforeSend: function () {

        }
    })
        .done(function (html) {
            aggiorna_pulsanti();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function diminuisci_pulsante(num_pulsante) {
    var comando = 'diminuisci_pulsante';
    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_pulsante: num_pulsante}, beforeSend: function () {

        }
    })
        .done(function (html) {
            aggiorna_pulsanti();
        })
        .fail(function (jqXHR, textStatus) {

        });
}