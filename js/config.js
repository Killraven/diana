//-----------------------------------------------------------------------------
// FUNZIONI DI PRIMO CARICAMENTO DELLA PAGINA
//-----------------------------------------------------------------------------

window.onload = onLoad;

function onLoad() {
    carica_lista_moduli();
}

function carica_verifica_lampade() {
    var comando = 'carica_verifica_lampade';
    var url = '../ajax/ajax.php';
    var div_loading = $('#div_loading');
    var div_landing = $('#div_verifica_lampade');

    $.ajax({
        type: "POST", url: url, data: {comando: comando}, beforeSend: function () {
            div_landing.html(div_loading.html());
        }
    })
        .done(function (html) {
            div_landing.html(html);
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function carica_lista_lampade() {
    var comando = 'carica_lista_lampade';
    var url = '../ajax/ajax.php';
    var div_loading = $('#div_loading');
    var div_landing = $('#div_lista_lampade');

    $.ajax({
        type: "POST", url: url, data: {comando: comando}, beforeSend: function () {
            div_landing.html(div_loading.html());
        }
    })
        .done(function (html) {
            div_landing.html(html);
            $("table.sortable").tablesorter();
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function carica_lista_moduli() {
    var comando = 'carica_lista_moduli';
    var url = '../ajax/ajax.php';
    var div_loading = $('#div_loading');
    var div_landing = $('#div_lista_moduli');

    $.ajax({
        type: "POST", url: url, data: {comando: comando}, beforeSend: function () {
            div_landing.html(div_loading.html());
        }
    })
        .done(function (html) {
            div_landing.html(html);
            aggiorna_stato_moduli();
            $('#panel_modulo').hide();
            $('#panel_controllo_modulo').hide();
            carica_verifica_lampade();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_modulo() {
    var div = $('div.highlight').first();
    if (div.length) {
        var id = div.attr('id');
        var arr = id.split('_');
        var addr = arr[1] + '.' + arr[2] + '.' + arr[3] + '.' + arr[4];
        var escaped_addr = arr[1] + '_' + arr[2] + '_' + arr[3] + '_' + arr[4];
        var port = arr[5];
        var cmd_port = arr[6];
        visualizza_modulo(addr, port, cmd_port);
    }
}

function aggiungi_modulo() {
    var comando = 'aggiungi_modulo';
    var url = '../ajax/ajax.php';
    var div_loading = $('#div_loading');
    var div_landing = $('#div_log_aggiungi_modulo');
    var addr = $('#addr').val();
    var port = $('#port').val();
    var cmd_port = $('#cmd_port').val();

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port, cmd_port: cmd_port}, beforeSend: function () {
            div_landing.html(div_loading.html());
        }
    })
        .done(function (json) {
            var arr = JSON.parse(json);
            div_landing.html(arr['html']);
            if (arr['risultato'] == 1) carica_lista_moduli();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_stato_moduli() {
    var comando = 'aggiorna_stato_modulo';
    var url = '../ajax/ajax.php';

    var span = $('span.stato_modulo.label-default').first();
    if (span.length > 0) {
        var arr = span.attr('id').split('_');
        var addr = arr[1] + '.' + arr[2] + '.' + arr[3] + '.' + arr[4];
        var escaped_addr = arr[1] + '_' + arr[2] + '_' + arr[3] + '_' + arr[4];
        var port = arr[5];
        var cmd_port = arr[6];

        var modulo = $('#modulo_' + escaped_addr + '_' + port + '_' + cmd_port);
        var button_apri = $('#btn_apri_' + escaped_addr + '_' + port);
        var button_elimina = $('#btn_elimina_' + escaped_addr + '_' + port);

        $.ajax({
            timeout: 2000, // sets timeout in ms
            type: "POST", url: url, data: {comando: comando, addr: addr, port: port, cmd_port: cmd_port}, beforeSend: function () {

            }
        })
            .done(function (json) {
                var arr = JSON.parse(json);
                if (arr['risultato'] == 1) {
                    span.addClass('label-success');
                    span.removeClass('label-default');
                    span.removeClass('label-danger');
                    modulo.addClass('ok');
                    modulo.removeClass('ko');
                    button_apri.show();
                    button_elimina.hide();
                } else {
                    span.addClass('label-danger');
                    span.removeClass('label-default');
                    span.removeClass('label-success');
                    modulo.addClass('ko');
                    modulo.removeClass('ok');
                    button_apri.hide();
                    button_elimina.show();
                }
                span.html(arr['stato']);
                aggiorna_stato_moduli();
            })
            .fail(function (jqXHR, textStatus) {
                if (textStatus === 'timeout') {
                    span.addClass('label-danger');
                    span.removeClass('label-default');
                    span.removeClass('label-success');
                    modulo.addClass('ko');
                    modulo.removeClass('ok');
                    button_apri.hide();
                    button_elimina.show();
                    span.html('Offline');
                    aggiorna_stato_moduli();
                }
            });
    }
}

function conferma_elimina_modulo(addr, port, cmd_port) {
    if (confirm('Sei sicuro di voler eliminare il modulo ' + addr + ':' + port + ' ?')) elimina_modulo(addr, port, cmd_port);
}

function elimina_modulo(addr, port, cmd_port) {
    var comando = 'elimina_modulo';
    var url = '../ajax/ajax.php';
    var modulo = $('#modulo_' + addr.replace(new RegExp('\\.', 'g'), '_') + '_' + port + '_' + cmd_port);
    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port}, beforeSend: function () {

        }
    })
        .done(function () {
            modulo.remove();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function visualizza_modulo(addr, port, cmd_port) {
    var id = 'modulo_' + addr.replace(new RegExp('\\.', 'g'), '_') + '_' + port + '_' + cmd_port;
    var modulo = $('#' + id);
    $('div.ok').each(function (index) {
        if ($(this).attr('id') != id) $(this).removeClass('highlight');
    });
    modulo.addClass('highlight');

    $('#panel_modulo').show();
    $('#span_modulo').html(addr + ':' + port);

    $('#panel_controllo_modulo').show();
    $('#span_controllo_modulo').html(addr + ':' + port);

    var comando = 'carica_modulo';
    var url = '../ajax/ajax.php';

    var body_configurazione = $('#div_modulo_body');
    var body_controllo = $('#div_controllo_modulo_body');

    var loading = $('#div_loading');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port}, beforeSend: function () {
            body_configurazione.html(loading.html());
            body_controllo.html(loading.html());
        }
    })
        .done(function (json) {
            var arr = JSON.parse(json);

            body_configurazione.html(arr['html_configurazione']);
            body_controllo.html(arr['html_controllo']);

            $("input.keypress").keypress(function (event) {
                $(this).addClass('da-salvare');
            });

            setInterval(salva_descrizioni, 2000);

            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_azione(div, addr, port, cmd_port) {
    if (div.length > 0) {
        var td = div.closest('td.clickable');
        td.removeClass('clickable');
        td.attr('onclick', '');

        div.removeClass('carica');

        var id = div.attr('id').split('_');
        var nodo_id = id[3];

        var comando = 'carica_azioni_nodo';
        var url = '../ajax/ajax.php';

        var loading = $('#div_loading');

        $.ajax({
            type: "POST", url: url, data: {comando: comando, addr: addr, port: port, cmd_port: cmd_port, nodo_id: nodo_id}, beforeSend: function () {
                div.html(loading.html());
            }
        })
            .done(function (html) {
                div.html(html);

                $('select').selectpicker();

                aggiorna_azioni(addr, port, cmd_port);
            })
            .fail(function (jqXHR, textStatus) {

            });
    }
}

function aggiorna_azioni(addr, port, cmd_port) {
    var div = $('div.carica.azioni').first();
    aggiorna_azione(div, addr, port, cmd_port);
}

function salva_descrizione_tipo(addr, port, tipo, num, testo) {
    var comando = 'salva_descrizione';
    var url = '../ajax/ajax.php';

    var landing = $('#stato_' + tipo + '_' + num);
    var saving = $('#div_span_salvando');
    var saved = $('#div_span_salvata');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port, tipo: tipo, num: num, testo: testo}, beforeSend: function () {
            landing.html(saving.html());
        }
    })
        .done(function (html) {
            landing.html(saved.html());
            salva_descrizioni();
            aggiorna_matrici();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function salva_descrizione(obj) {
    var testo = obj.val();
    var arr = obj.attr('id').split('_');
    var tipo = arr[1];
    var num = arr[2];
    var div = $('div.highlight').first();
    if (div.length) {
        var id = div.attr('id');
        var arr = id.split('_');
        var addr = arr[1] + '.' + arr[2] + '.' + arr[3] + '.' + arr[4];
        var escaped_addr = arr[1] + '_' + arr[2] + '_' + arr[3] + '_' + arr[4];
        var port = arr[5];
        salva_descrizione_tipo(addr, port, tipo, num, testo);
    }
}

function salva_descrizioni() {
    var input = $('input.da-salvare').first();
    if (input.length) {
        input.removeClass('da-salvare');
        salva_descrizione(input);
    }
}

function apri_o_chiudi(button) {
    var span = $(button).find('span');
    var chiudi = (span.hasClass('glyphicon-chevron-up'));
    var tbody = $(button).closest('table').find('tbody').first();
    if (chiudi) {
        span.addClass('glyphicon-chevron-down');
        span.removeClass('glyphicon-chevron-up');
        tbody.hide(200);
    } else {
        span.addClass('glyphicon-chevron-up');
        span.removeClass('glyphicon-chevron-down');
        tbody.show(200);
    }
}

function recupera_addr_port_modulo_selezionato() {
    var div = $('div.highlight').first();
    if (div.length) {
        var id = div.attr('id');
        var arr = id.split('_');
        var addr = arr[1] + '.' + arr[2] + '.' + arr[3] + '.' + arr[4];
        var escaped_addr = arr[1] + '_' + arr[2] + '_' + arr[3] + '_' + arr[4];
        var port = arr[5];
        var cmd_port = arr[6];
        return {'addr': addr, 'port': port, 'cmd_port': cmd_port};
    }
    return false;
}

function aggiorna_matrici() {
    aggiorna_matrice_nodi_gruppi();
    aggiorna_matrice_nodi_scene();
    aggiorna_controllo();
}

function aggiorna_nodi_gruppo(num_gruppo) {
    var comando = 'aggiorna_nodi_gruppo';
    var url = '../ajax/ajax.php';

    var landing = $('#stato_nodi_gruppo_' + num_gruppo);
    var saving = $('#div_span_salvando');
    var saved = $('#div_span_salvato');
    var select = $('#nodi_gruppo_' + num_gruppo);

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];
    var cmd_port = arr['cmd_port'];

    var nodi_gruppo = select.val();

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port, cmd_port: cmd_port, num_gruppo: num_gruppo, nodi_gruppo: nodi_gruppo}, beforeSend: function () {
            landing.html(saving.html());
            select.prop("disabled", true);
        }
    })
        .done(function (json) {
            landing.html(saved.html());
            select.prop("disabled", false);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_controllo() {
    var comando = 'aggiorna_controllo';
    var url = '../ajax/ajax.php';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];

    landing_ADV = $('#tbody_controllo_ADV');
    landing_scene = $('#tbody_controllo_scene');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port}, beforeSend: function () {

        }
    })
        .done(function (json) {
            var arr = JSON.parse(json);
            landing_ADV.html(arr['ADV']);
            landing_scene.html(arr['scene']);
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_matrice_nodi_gruppi() {
    var comando = 'aggiorna_matrice_nodi_gruppi';
    var url = '../ajax/ajax.php';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];

    var landing = $('#matrice_nodi_gruppi');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function aggiorna_matrice_nodi_scene() {
    var comando = 'aggiorna_matrice_nodi_scene';
    var url = '../ajax/ajax.php';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];

    var landing = $('#matrice_nodi_scene');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function modifica_nodo_scena(obj, num_nodo, ADV, comando) {
    var landing = $(obj).closest('div.nodo-scena');
    var url = '../ajax/ajax.php';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_nodo: num_nodo, ADV: ADV, addr: addr, port: port}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function esegui_modifica_nodo_scena(obj, num_nodo) {
    var comando = 'esegui_modifica_nodo_scena';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];
    var cmd_port = arr['cmd_port'];

    var ADV = $(obj).val();

    var num_scena = $(obj).closest('tr').attr('id').split('_')[2];

    console.log(num_scena);

    var landing = $(obj).closest('div.nodo-scena');
    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, num_nodo: num_nodo, ADV: ADV, addr: addr, cmd_port: cmd_port, port: port, num_scena: num_scena}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function annulla_modifica_nodo_scena(obj, num_nodo, ADV) {
    var comando = 'annulla_modifica_nodo_scena';
    modifica_nodo_scena(obj, num_nodo, ADV, comando);
}

function mostra_modifica_nodo_scena(obj, num_nodo, ADV) {
    var comando = 'mostra_modifica_nodo_scena';
    modifica_nodo_scena(obj, num_nodo, ADV, comando);
}

function nodo_gruppo_broadcast_scelto(dom) {
    var select = $('#controllo_ADV');
    select.val('');
    $('#btn_salvato').hide();
    $('#btn_salvando').hide();
    if ($(dom).val().length == 0) select.prop('disabled', 'disabled'); else select.prop('disabled', false);
    select.selectpicker('refresh');
}

function valore_ADV_scelto(dom) {
    var comando = 'esegui_controllo_ADV';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];
    var cmd_port = arr['cmd_port'];

    var ADV = $(dom).val();
    var DALI_ID = $('#elemento_da_controllare').val();

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, cmd_port: cmd_port, port: port, ADV: ADV, DALI_ID: DALI_ID}, beforeSend: function () {
            $('#btn_salvando').show();
            $('#btn_salvato').hide();
        }
    })
        .done(function (html) {
            $('#btn_salvando').hide();
            $('#btn_salvato').show();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function elemento_scena_scelto(dom) {
    var select_scena = $('#select_scena');
    $('#btn_scena_salvata').hide();
    $('#btn_salvando_scena').hide();
    if ($(dom).val().length == 0) {
        select_scena.val('');
        select_scena.prop('disabled', 'disabled');
    } else {
        select_scena.prop('disabled', false);
    }
    select_scena.selectpicker('refresh');
}

function valore_scena_scelto(dom) {
    var comando = 'esegui_controllo_scena';

    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];
    var cmd_port = arr['cmd_port'];

    var SCENA = $(dom).val();
    var DALI_ID = $('#elemento_scena').val();

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, cmd_port: cmd_port, port: port, SCENA: SCENA, DALI_ID: DALI_ID}, beforeSend: function () {
            $('#btn_salvando_scena').show();
            $('#btn_scena_salvata').hide();
        }
    })
        .done(function (html) {
            $('#btn_salvando_scena').hide();
            $('#btn_scena_salvata').show();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function cambia_fade_rate(dom, nodo_id) {
    var select = $(dom);
    var valore = select.val();
    var comando = 'cambia_fade_rate';
    cambia_e_aggiorna_azioni_nodo(nodo_id, comando, valore, select);
}

function cambia_fade_time(dom, nodo_id) {
    var select = $(dom);
    var valore = select.val();
    var comando = 'cambia_fade_time';
    cambia_e_aggiorna_azioni_nodo(nodo_id, comando, valore, select);
}

function cambia_power_on_level(dom, nodo_id) {
    var select = $(dom);
    var valori = select.val();
    var comando = 'cambia_power_on_level';
    cambia_e_aggiorna_azioni_nodo(nodo_id, comando, valori[0], select);
}

function cambia_e_aggiorna_azioni_nodo(nodo_id, comando, valore, select) {
    var arr = recupera_addr_port_modulo_selezionato();
    var addr = arr['addr'];
    var port = arr['port'];
    var cmd_port = arr['cmd_port'];

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, cmd_port: cmd_port, port: port, valore: valore, nodo_id: nodo_id}, beforeSend: function () {
            select.attr('disabled', true);
        }
    })
        .done(function (val) {
            select.selectpicker('val', val);
            select.attr('disabled', false);
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function modifica_lampade_scelte() {
    var select_ADV = $('#ADV_lampade_scelte');
    var select_PoL = $('#PoL_lampade_scelte');

    if ($('#select_lamp').val().length > 0) {
        select_ADV.prop("disabled", false);
        select_PoL.prop("disabled", false);
    } else {
        select_ADV.prop("disabled", true);
        select_PoL.prop("disabled", true);
    }
    select_ADV.val("");
    select_PoL.val("");
    select_ADV.selectpicker('refresh');
    select_PoL.selectpicker('refresh');
}

function modifica_ADV_lampade_scelte() {
    var select_ADV = $('#ADV_lampade_scelte');
    var select_PoL = $('#PoL_lampade_scelte');
    var select_Lamp = $('#select_lamp');

    var comando = 'modifica_ADV_lampade_scelte';

    var div_loading = $('#div_loading');

    var url = '../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, lampade: select_Lamp.val(), ADV: select_ADV.val()}, beforeSend: function () {
            $('#td_info').html(div_loading.html());

            select_ADV.prop("disabled", true);
            select_PoL.prop("disabled", true);
            select_Lamp.prop("disabled", true);
            select_ADV.selectpicker('refresh');
            select_PoL.selectpicker('refresh');
            select_Lamp.selectpicker('refresh');
        }
    })
        .done(function (html) {
            $('#td_info').html(html);

            select_ADV.prop("disabled", false);
            select_PoL.prop("disabled", false);
            select_Lamp.prop("disabled", false);
            select_ADV.selectpicker('refresh');
            select_PoL.selectpicker('refresh');
            select_Lamp.selectpicker('refresh');
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function modifica_PoL_lampade_scelte() {
    var select_ADV = $('#ADV_lampade_scelte');
    var select_PoL = $('#PoL_lampade_scelte');
    var select_Lamp = $('#select_lamp');

    var comando = 'modifica_PoL_lampade_scelte';

    var url = '../ajax/ajax.php';

    var div_loading = $('#div_loading');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, lampade: select_Lamp.val(), PoL: select_PoL.val()}, beforeSend: function () {
            $('#td_info').html(div_loading.html());

            select_ADV.prop("disabled", true);
            select_PoL.prop("disabled", true);
            select_Lamp.prop("disabled", true);
            select_ADV.selectpicker('refresh');
            select_PoL.selectpicker('refresh');
            select_Lamp.selectpicker('refresh');
        }
    })
        .done(function (html) {
            $('#td_info').html(html);

            select_ADV.prop("disabled", false);
            select_PoL.prop("disabled", false);
            select_Lamp.prop("disabled", false);
            select_ADV.selectpicker('refresh');
            select_PoL.selectpicker('refresh');
            select_Lamp.selectpicker('refresh');
        })
        .fail(function (jqXHR, textStatus) {

        });
}
