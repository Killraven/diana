//-----------------------------------------------------------------------------
// FUNZIONI DI PRIMO CARICAMENTO DELLA PAGINA
//-----------------------------------------------------------------------------

window.onload = onLoad;

function onLoad() {

}

function illumina_comando(obj) {
    obj.removeClass('btn-default');
    obj.addClass('btn-primary');
    obj.parent().find('button').each(function () {
        if ($(this).html() != obj.html()) {
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-default');
        }
    });
}

function parametri_comando(obj, code) {
    $('#parametri').html($('#loading').html());
    comando(obj, code);
}

function comando(obj, code) {
    illumina_comando(obj);
    var comando = 'invia_comando';
    var url = '../ajax/ajax.php';
    var div_parametri = $('#parametri');
    var div_richiesta = $('#richiesta');
    var div_risposta = $('#risposta');
    var div_log = $('#log');
    var addr = $('#addr').val();
    var port = $('#port').val();
    var cmd_port = $('#cmd_port').val();
    var data = {};
    var loading = $('#loading').html();

    if ($('#DALI_ID').length) data['DALI_ID'] = $('#DALI_ID').val();

    if ($('#ADV').length) data['ADV'] = $('#ADV').val();

    if ($('#COUNTER').length) data['COUNTER'] = $('#COUNTER').val();

    if ($('#SCENE').length) data['SCENE'] = $('#SCENE').val();

    if ($('#GROUP').length) data['GROUP'] = $('#GROUP').val();

    if ($('#AZIONE').length) data['AZIONE'] = $('#AZIONE').val();

    if ($('#DALI_COMMAND').length) data['DALI_COMMAND'] = $('#DALI_COMMAND').val();

    $.ajax({
        type: "POST", url: url, data: {comando: comando, code: code, addr: addr, port: port, cmd_port: cmd_port, data: data}, beforeSend: function () {
            div_richiesta.html(loading);
            div_risposta.html(loading);
            div_log.html(loading);
        }
    })
        .done(function (json) {
            var arr = JSON.parse(json);
            div_parametri.html(arr['parametri_html']);
            div_richiesta.html(arr['richiesta_html']);
            div_risposta.html(arr['risposta_html']);
            div_log.html(arr['html']);
            $('select').selectpicker();
        })
        .fail(function (jqXHR, textStatus) {

        });
}
