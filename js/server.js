//-----------------------------------------------------------------------------
// FUNZIONI DI PRIMO CARICAMENTO DELLA PAGINA
//-----------------------------------------------------------------------------

window.onload = onLoad;
var intervallo_aggiorna_server = null;
var server_comandi_in_avvio = 0;

function onLoad() {
    recupera_sessione_e_avvia_server();
}

$(window).on("unload", function (e) {
    ferma_server();
});

function aggiorna_server() {
    var sessione = $('#sessione').html();
    var comando = 'aggiorna_server';
    var url = './../ajax/ajax.php';
    var div_log = $('#log');
    var input_len = $('#len');
    var len = input_len.val();
    var addr = $('#addr');
    var port = $('#port');
    var cmd_port = $('#port_cmd');

    $.ajax({
        type: "POST", url: url, data: {comando: comando, len: len, sessione: sessione, addr: addr.html(), port: port.html()}, beforeSend: function () { }
    })
        .done(function (json) {

            var arr = JSON.parse(json);

            input_len.val(arr['len']);
            div_log.append(arr['log']);

            $('#stato').html(arr['html_stato']);

            var stato_connessione = arr['stato_connessione'];
            addr.html(stato_connessione['addr']);
            port.html(stato_connessione['port']);
            cmd_port.html(stato_connessione['cmd_port']);
            if (stato_connessione['attivo'] == 1) {
                $('#btn_addr').addClass('btn-success');
                $('#btn_addr').removeClass('btn-default');
                $('#btn_port').addClass('btn-success');
                $('#btn_port').removeClass('btn-default');
            } else {
                $('#btn_addr').addClass('btn-default');
                $('#btn_addr').removeClass('btn-success');
                $('#btn_port').addClass('btn-default');
                $('#btn_port').removeClass('btn-success');
            }

            if (stato_connessione['cmd_attivo'] == 1) {
                server_comandi_in_avvio = 0;
                $('#btn_port_cmd').addClass('btn-success');
                $('#btn_port_cmd').removeClass('btn-default');
            } else {
                if (stato_connessione['attivo'] == 1 && server_comandi_in_avvio == 0) {
                    server_comandi_in_avvio = 1;
                    setTimeout(avvia_server_comandi, 100);
                }
                $('#btn_port_cmd').addClass('btn-default');
                $('#btn_port_cmd').removeClass('btn-success');
            }
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function avvia_log() {
    if (intervallo_aggiorna_server == null) intervallo_aggiorna_server = setInterval(aggiorna_server, 2000);
}

function recupera_sessione_e_avvia_server() {
    var landing = $('#sessione');
    var comando = 'recupera_sessione';
    var url = './../ajax/ajax.php';
    $.ajax({
        type: "POST", url: url, data: {comando: comando}, beforeSend: function () {

        }
    })
        .done(function (html) {
            landing.html(html);
            avvia_server();
        })
        .fail(function (jqXHR, textStatus) {

        });
}

function avvia_server() {
    $('#log').html('');
    $('#len').val(0);
    var sessione = $('#sessione').html();

    avvia_log();

    $('#START').hide();
    $('#STOP').show();

    var comando = 'avvia_server';
    var url = './../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, sessione: sessione}, beforeSend: function () {

        }
    })
        .done(function (html) {

        })
        .fail(function (jqXHR, textStatus) {

        });
}

function avvia_server_comandi() {
    var sessione = $('#sessione').html();
    var comando = 'avvia_server_comandi';
    var url = './../ajax/ajax.php';

    $.ajax({
        type: "POST", url: url, data: {comando: comando, sessione: sessione}, beforeSend: function () {

        }
    })
        .done(function (html) {

        })
        .fail(function (jqXHR, textStatus) {

        });
}

function ferma_server() {
    var url = '../ajax/ajax.php';
    var comando = 'ferma_server';
    var addr = $('#addr').html();
    var port = $('#port').html();
    var cmd_port = $('#cmd_port').html();

    $('#START').show();
    $('#STOP').hide();

    $.ajax({
        type: "POST", url: url, data: {comando: comando, addr: addr, port: port, cmd_port: cmd_port}, beforeSend: function () {

        }
    })
        .done(function (json) {

        })
        .fail(function (jqXHR, textStatus) {

        });
}

function svuota_log() {
    $('#log').html('');
}
