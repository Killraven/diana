<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diana24</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="assets/css/diana.css">
    <link rel="stylesheet" href="assets/css/Profile-Card.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
<div class="profile-card height:500">
    <!-- START Menu-->
	<?php
		require_once($_SERVER['DOCUMENT_ROOT'] . "/diana/assets/menuBar.php");
	?>
    <!-- END Menu-->
    <p class="profile-bio" style="/*padding:0px;*/color:white;text-shadow:4px 3px rgb(27,41,80);letter-spacing:3px;font-size:36px;"><strong>Yun Controller Sensor Scenario Setup</strong></p>
    <div>
        <div class="row rowGenClass" id="sceneRowSensor" style=";max-width:800px; margin: 2px auto;border-radius:15px;margin-top:40px;">
            <div class="col sceneRowClass" style="margin:5px auto;max-width:800px;border-radius:15px;background-color:rgb(140,201,237);opacity:.9;color:white;margin-bottom:5px;"><label class="col-form-label" for="sceneSelector" id="idSceneSelLbl"
                                                                                                                                                                                         style="; opacity:1; color: black;background-color:rgb(140,201,237)">Users Filter</label></div>
        </div>
    </div>
    <div class="table-responsive" style="margin-top:10px;margin-bottom:0px;max-width:800px;margin: auto;">
        <table class="table sensorTableClass">
            <thead>
            <tr id="idSensorTableHeader" class="tableHeader">
                <th>Id</th>
                <th>Name</th>
                <th>Scene</th>
                <th>Time</th>
                <th>Pres</th>
                <th>Light</th>
                <th>T&amp;H</th>
                <th>Dist</th>
                <th>Soil</th>
                <th>GAS</th>
                <th>Buzzer</th>
                <th>Stepper</th>
                <th>Led</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>0</td>
                <td>Meeting</td>
                <td>2</td>
                <td>N</td>
                <td>Y</td>
                <td>NN</td>
                <td>NN</td>
                <td>N</td>
                <td>N</td>
                <td>N</td>
                <td>N</td>
                <td>Y</td>
                <td>N</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Night</td>
                <td>4</td>
                <td>Y</td>
                <td>Y</td>
                <td>YY</td>
                <td>NN</td>
                <td>N</td>
                <td>N</td>
                <td>N</td>
                <td>N</td>
                <td>Y</td>
                <td>N</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Rain</td>
                <td>7</td>
                <td>N</td>
                <td>N</td>
                <td>NN</td>
                <td>NN</td>
                <td>N</td>
                <td>Y</td>
                <td>N</td>
                <td>Y</td>
                <td>Y</td>
                <td>Y</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Gas Emercency</td>
                <td>12</td>
                <td>N</td>
                <td>N</td>
                <td>NN</td>
                <td>NN</td>
                <td>N</td>
                <td>N</td>
                <td>Y</td>
                <td>Y</td>
                <td>Y</td>
                <td>Y</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div></div>
</div>
<div id="divFooter"></div>
<nav class="navbar navbar-light navbar-expand-md fixed-bottom" id="footerCopyright">
    <div class="container-fluid"><a class="navbar-brand bg-primary m-auto fixed-bottom" href="#" style="text-align:center;">Copyright Paolo Maruotti 2018</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-2"></button>
        <div class="collapse navbar-collapse"
             id="navcol-2"></div>
    </div>
</nav>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-checkbox.js"></script>
</body>

</html>