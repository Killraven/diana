<?php

	require_once __DIR__ . '/../class/dali_ethernet_server.class.php';
	require_once __DIR__ . '/../class/dali_ethernet_client.class.php';
	require_once __DIR__ . '/../class/dali_ethernet_config.class.php';
	require_once __DIR__ . '/../class/arduino_yun_config.class.php';

	if (key_exists('comando', $_REQUEST)){
		$comando = $_REQUEST['comando'];

		switch ($comando){
			case 'aumenta_pulsante':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->aumenta_pulsante($num_pulsante);
				break;
			case 'diminuisci_pulsante':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->diminuisci_pulsante($num_pulsante);
				break;
			case 'elimina_pulsante':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->elimina_pulsante($num_pulsante);
				break;
			case 'clona_pulsante':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->clona_pulsante($num_pulsante);
				break;
			case 'modifica_ADV_lampade_scelte':
				$comandi_totali = 0;
				$comandi_eseguiti_con_successo = 0;
				$risultato = 1;
				$html = '';
				$DEC = new DALI_ETHERNET_CONFIG();
				$ADV = $_REQUEST['ADV'][0];
				foreach ($_REQUEST['lampade'] as $str_lamp){
					$comandi_totali++;
					$lampada = explode('_', $str_lamp); // addr, port, cmd_port, num_node
					$addr = $lampada[0];
					$port = $lampada[1];
					$DALI_ID = $lampada[3];
					$r = $DEC->esegui_controllo_ADV($addr, $port, $ADV, $DALI_ID);
					if ($r['risultato'] == 0) $risultato = 0;else
						$comandi_eseguiti_con_successo++;

					$html .= $r['html'];
				}
				if ($risultato == 0) echo $DEC->recupera_html_errore('<b>' . ($comandi_totali - $comandi_eseguiti_con_successo) . '</b> comandi su ' . $comandi_totali . ' falliti!') . $html;else
					echo $DEC->recupera_html_successo('<b>' . $comandi_totali . '</b> comandi eseguiti con successo!') . $html;
				break;
			case 'modifica_PoL_lampade_scelte':
				$comandi_totali = 0;
				$comandi_eseguiti_con_successo = 0;
				$risultato = 1;
				$html = '';
				$DEC = new DALI_ETHERNET_CONFIG();
				$PoL = $_REQUEST['PoL'][0];
				foreach ($_REQUEST['lampade'] as $str_lamp){
					$comandi_totali++;
					$lampada = explode('_', $str_lamp); // addr, port, cmd_port, num_node
					$addr = $lampada[0];
					$port = $lampada[1];
					$cmd_port = $lampada[2];
					$nodo_id = $lampada[3];
					// restituisce -1 in caso di problemi...
					$r = $DEC->aggiorna_power_on_level($addr, $port, $cmd_port, $nodo_id, $PoL);
					if ($r['risultato'] == 0){
						$risultato = 0;
						$html .= $r['html'];
					}else{
						$comandi_eseguiti_con_successo++;
					}
				}
				if ($risultato == 0) echo $DEC->recupera_html_errore('<b>' . ($comandi_totali - $comandi_eseguiti_con_successo) . '</b> comandi su ' . $comandi_totali . ' falliti!') . $html;else
					echo $DEC->recupera_html_successo('<b>' . $comandi_totali . '</b> comandi eseguiti con successo!') . $html;
				break;
			case 'cambia_fade_rate':
				$nodo_id = $_REQUEST['nodo_id'];
				$valore = $_REQUEST['valore'];
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				echo $DEC->aggiorna_fade_rate($addr, $port, $cmd_port, $nodo_id, $valore);
				break;
			case 'cambia_fade_time':
				$nodo_id = $_REQUEST['nodo_id'];
				$valore = $_REQUEST['valore'];
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				echo $DEC->aggiorna_fade_time($addr, $port, $cmd_port, $nodo_id, $valore);
				break;
			case 'cambia_power_on_level':
				$nodo_id = $_REQUEST['nodo_id'];
				$valore = $_REQUEST['valore'];
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$r = $DEC->aggiorna_power_on_level($addr, $port, $cmd_port, $nodo_id, $valore);
				echo $r['PoL_dec'];
				break;
			case 'carica_azioni_nodo':
				$nodo_id = $_REQUEST['nodo_id'];
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				echo $DEC->recupera_html_azioni_nodo($addr, $port, $cmd_port, $nodo_id);
				break;
			case 'diminuisci_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->diminuisci_comando($num_pulsante, $num_comando);
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante);
				break;
			case 'aumenta_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->aumenta_comando($num_pulsante, $num_comando);
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante);
				break;
			case 'modifica_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante, 'modifica_comando', $num_comando);
				break;
			case 'elimina_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->elimina_comando($num_pulsante, $num_comando);
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante);
				break;
			case 'salva_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$converter = $_REQUEST['converter'];
				$tipo = $_REQUEST['tipo'];
				$elemento = $_REQUEST['elemento'];
				$valore = $_REQUEST['valore'];
				$YUN = new ARDUINO_YUN_CONFIG();
				$YUN->salva_comando($num_pulsante, $num_comando, $converter, $tipo, $elemento, $valore);
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante);
				break;
			case 'annulla_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante);
				break;
			case 'carica_tipo_comando_scelto':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$tipo = $_REQUEST['tipo'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_parametri_comando($num_pulsante, $num_comando, $addr, $port, $tipo);
				break;
			case 'recupera_sessione':
				echo 'sessione_' . date('Ymd_His');
				break;
			case 'mostra_aggiungi_comando':
				$num_pulsante = $_REQUEST['num_pulsante'];
				$num_comando = $_REQUEST['num_comando'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_lista_comandi_pulsante($num_pulsante, 'aggiungi_comando');
				break;
			case 'annulla_salva_id_pulsante':
				$num = $_REQUEST['num'];
				$desc = $_REQUEST['desc'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_identificativo_pulsante($num, $desc);
				break;
			case 'salva_id_pulsante':
				$num = $_REQUEST['num'];
				$desc = $_REQUEST['desc'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_identificativo_pulsante($num, $desc, false, true);
				break;
			case 'carica_modifica_id_pulsante':
				$num = $_REQUEST['num'];
				$desc = $_REQUEST['desc'];
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_identificativo_pulsante($num, $desc, true);
				break;
			case 'carica_configurazione_pulsanti':
				$YUN = new ARDUINO_YUN_CONFIG();
				echo $YUN->recupera_html_panel_body_pulsanti();
				break;
			case 'aggiorna_controllo':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$configurazione = $DEC->recupera_configurazione_da_addr_port($addr, $port);
				$arr = array('ADV' => $DEC->recupera_html_tbody_controllo_ADV($configurazione), 'scene' => $DEC->recupera_html_tbody_controllo_scene($configurazione),);
				echo json_encode($arr);
				break;
			case 'esegui_controllo_ADV':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$ADV = $_REQUEST['ADV'][0];            // sono delle select multiple...
				$DALI_ID = $_REQUEST['DALI_ID'][0]; // sono delle select multiple...
				$DEC = new DALI_ETHERNET_CONFIG();
				$DEC->esegui_controllo_ADV($addr, $port, $ADV, $DALI_ID);
				break;
			case 'esegui_controllo_scena':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$SCENA = $_REQUEST['SCENA'][0];        // sono delle select multiple...
				$DALI_ID = $_REQUEST['DALI_ID'][0]; // sono delle select multiple...
				$DEC = new DALI_ETHERNET_CONFIG();
				$DEC->esegui_controllo_scena($addr, $port, $SCENA, $DALI_ID);
				break;
			case 'esegui_modifica_nodo_scena':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$num_nodo = $_REQUEST['num_nodo'];
				$num_scena = $_REQUEST['num_scena'];
				$ADV = $_REQUEST['ADV'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$DEC->esegui_modifica_nodo_scena($addr, $port, $cmd_port, $num_scena, $num_nodo, $ADV);
				$configurazione = $DEC->recupera_configurazione_da_addr_port($addr, $port);
				$descrizione = '';
				if (key_exists('nodi', $configurazione)) if (key_exists($num_nodo, $configurazione['nodi'])) if (key_exists('descrizione', $configurazione['nodi'][$num_nodo])) $descrizione = $configurazione['nodi'][$num_nodo]['descrizione'];
				echo $DEC->recupera_html_div_nodo_scena($num_nodo, $ADV, false, $descrizione);
				break;
			case 'annulla_modifica_nodo_scena':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$num_nodo = $_REQUEST['num_nodo'];
				$ADV = $_REQUEST['ADV'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$configurazione = $DEC->recupera_configurazione_da_addr_port($addr, $port);
				$descrizione = '';
				if (key_exists('nodi', $configurazione)) if (key_exists($num_nodo, $configurazione['nodi'])) if (key_exists('descrizione', $configurazione['nodi'][$num_nodo])) $descrizione = $configurazione['nodi'][$num_nodo]['descrizione'];
				echo $DEC->recupera_html_div_nodo_scena($num_nodo, $ADV, false, $descrizione);
				break;
			case 'mostra_modifica_nodo_scena':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$num_nodo = $_REQUEST['num_nodo'];
				$ADV = $_REQUEST['ADV'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$configurazione = $DEC->recupera_configurazione_da_addr_port($addr, $port);
				$descrizione = '';
				if (key_exists('nodi', $configurazione)) if (key_exists($num_nodo, $configurazione['nodi'])) if (key_exists('descrizione', $configurazione['nodi'][$num_nodo])) $descrizione = $configurazione['nodi'][$num_nodo]['descrizione'];
				echo $DEC->recupera_html_div_nodo_scena($num_nodo, $ADV, true, $descrizione);
				break;
			case 'aggiorna_matrice_nodi_gruppi':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$configurazione = $DEC->recupera_configurazione_da_addr_port($addr, $port);
				$DEC->aggiorna_configurazione_nodi_gruppi_da_modulo($addr, $port, $configurazione);
				echo $DEC->recupera_html_tbody_matrice_nodi_gruppi($configurazione);
				break;
			case 'aggiorna_matrice_nodi_scene':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$configurazione = $DEC->recupera_configurazione_da_addr_port($addr, $port);
				$DEC->aggiorna_configurazione_scene_da_modulo($addr, $port, $configurazione);
				echo $DEC->recupera_html_tbody_matrice_nodi_scene($configurazione);
				break;
			case 'aggiorna_nodi_gruppo':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$nodi_gruppo = key_exists('nodi_gruppo', $_REQUEST) ? $_REQUEST['nodi_gruppo'] : array();
				$num_gruppo = $_REQUEST['num_gruppo'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$r = $DEC->aggiorna_nodi_gruppo($addr, $port, $cmd_port, $num_gruppo, $nodi_gruppo);
				echo json_encode($r);
				break;
			case 'salva_descrizione':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$tipo = $_REQUEST['tipo'];
				$num = $_REQUEST['num'];
				$testo = $_REQUEST['testo'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$DEC->salva_descrizione($addr, $port, $tipo, $num, $testo);
				break;
			case 'carica_modulo':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$arr = $DEC->recupera_html_contenuto_pannello_modulo($addr, $port);
				echo json_encode($arr);
				break;
			case 'elimina_modulo':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$DEC->elimina_modulo($addr, $port);
				break;
			case 'aggiungi_modulo':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$DEC = new DALI_ETHERNET_CONFIG();
				$r = $DEC->aggiungi_modulo($addr, $port, $cmd_port);
				echo json_encode($r);
				break;
			case 'carica_lista_moduli':
				$DEC = new DALI_ETHERNET_CONFIG();
				echo $DEC->recupera_html_contenuto_pannello_lista_moduli();
				break;
			case 'carica_lista_lampade':
				$DEC = new DALI_ETHERNET_CONFIG();
				echo $DEC->recupera_html_contenuto_pannello_lista_lampade();
				break;
			case 'carica_verifica_lampade':
				$DEC = new DALI_ETHERNET_CONFIG();
				echo $DEC->recupera_html_contenuto_pannello_verifica_lampade();
				break;
			case 'aggiorna_stato_modulo':
				$addr = $_REQUEST['addr'];
				$DC = new DALI_ETHERNET_CONFIG();
				$r = $DC->recupera_stato_modulo($addr);
				echo json_encode($r);
				break;
			case 'avvia_server':
				$sessione = $_REQUEST['sessione'];
				$DALI_SERVER = new DALI_ETHERNET_SERVER(null, null, null, $sessione);
				break;
			case 'avvia_server_comandi':
				$sessione = $_REQUEST['sessione'];
				$DALI_SERVER_COMANDI = new DALI_ETHERNET_SERVER(null, null, null, $sessione, true);
				break;
			case 'ferma_server':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$DC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);
				$code = DALI_ETHERNET::$COMANDO_CLOSE_SERVER_SOCKET;
				$r = $DC->invia_comando($code);
				break;
			case 'aggiorna_server':
				$sessione = $_REQUEST['sessione'];
				$len = intval($_REQUEST['len']);
				$DALI_SERVER = new DALI_ETHERNET_SERVER(null, null, false);

				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$html_stato = is_numeric($port) ? $DALI_SERVER->recupera_html_stato($addr, $port) : '';

				$log = $DALI_SERVER->recupera_log($len, $sessione);
				$len += strlen($log);

				$stato_connessione = $DALI_SERVER->recupera_sessione($sessione);
				if (false !== $stato_connessione){
					$stato_connessione['attivo'] = 1;
					$stato_connessione['cmd_attivo'] = 0;
					if ($stato_connessione['cmd_port'] != DALI_ETHERNET::$PORT_NULL) $stato_connessione['cmd_attivo'] = 1;
				}else{
					$stato_connessione = array('attivo' => 0, 'cmd_attivo' => 0, 'addr' => DALI_ETHERNET::$ADDR_NULL, 'port' => DALI_ETHERNET::$PORT_NULL, 'cmd_port' => DALI_ETHERNET::$PORT_NULL,);
				}

				$arr = array('log' => utf8_encode($log),    // non togliere l'utf8 encoding...
					'len' => $len, 'stato_connessione' => $stato_connessione, 'html_stato' => $html_stato,);

				echo json_encode($arr);
				break;
			case 'carica_parametri':
				$code = $_REQUEST['code'];
				break;
			case 'invia_comando':
				$addr = $_REQUEST['addr'];
				$port = $_REQUEST['port'];
				$cmd_port = $_REQUEST['cmd_port'];
				$code = $_REQUEST['code'];
				$data = key_exists('data', $_REQUEST) ? $_REQUEST['data'] : array();
				$DC = new DALI_ETHERNET_CLIENT($addr, $port, $cmd_port);
				$r = $DC->invia_comando($code, $data);
				$arr = array('parametri_html' => utf8_encode($r['parametri_html']), 'richiesta_html' => utf8_encode($r['richiesta_html']), 'risposta_html' => utf8_encode($r['risposta_html']), 'html' => utf8_encode($r['html']),);
				echo json_encode($arr);
				break;
		}
	}

?>