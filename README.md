Hungarian notation used

_ = a private class variable
a+ = array (often combined with the data type used inside the array)
c+ = character
s+ = string
o+ = object
d+ = date object -- as in what's returned from a date() or gmdate()
v+ = variant -- used very infrequently to mean any kind of possible variable type
i+ = integer -- an integer
f+ = float -- a floating point number, e.g. an integer with a fractional part
n+ = numeric (unknown if it's float, integer, etc. Use infrequently)
x+ = to let other programmers know that this is a variable intended to be used by reference rather than value
rs+ = db recordset (set of rows)
rw+ = db row
h+ = handle, as in db handle, file handle, connection handle, curl handle, socket handle, etc.
hf = handle to function, as in setRetrievalStrategy(callable $hfStrategy)
t+ = a threaded object, use to indicate that an object may be safe to call\pass between threads
g+ = global var (and used sparingly, and often combined with the datatype used for the variable)
b+ = boolean

examples:
	$oMember -- a Member object
	$hFile -- a handle to a file, for instance as passed from the fopen() statement
	$cFirst -- first character retrieved from a string
	$rsMembers -- records of Members, as returned from a database table
	$rwMember -- a single Member record from the database
	$bUseNow -- a boolean flag
	$sxMemberName -- a byref string variable for a name of a member.
	$nCounter -- a numeric counter
	$dBegin -- a beginning date
	$sFirstName -- a string to represent someone's first name
	$_hDB -- a private class variable to store a database connection handle (often addressed by $this->_hDB)