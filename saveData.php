<?php
	// Prepare variables for database connection
	$dbusername = "root";
	$dbpassword = "";
	$server = "localhost";

	// Connect to your database
	$dbconnect = mysqli_connect($server, $dbusername, $dbpassword);
	$dbselect = mysqli_select_db($dbconnect,"diana");

	//print_r($_GET);
	//	id  idUnit      divider desc
	//	1   30	        1       Photoresistor	Light intensity
	//	INSERT INTO `diana`.`measure` (`idmeasure`, `timestamp`, `value`, `unitofmeasurement_idunitOfMeasurement`, `sensorType_idsensorType`)
	$sqlBase = "INSERT INTO diana.measure(idmeasure,timestamp,value,unitofmeasurement_idunitOfMeasurement,sensorType_idsensorType,sensor_idsensor) ";

	//	VALUES (NULL, NULL, $photoR1, 30, 1);
	if(isset($_GET["PR1"])){
		$value = $_GET["PR1"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,1,1)";
		if (mysqli_query($dbconnect,$sql) or die(mysqli_error($dbconnect))) ;
	}

	//	VALUES (NULL, NULL, $photoR2, 30, 1);
	if(isset($_GET["PR2"])){
		$value = $_GET["PR2"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,1,1)";
		mysqli_query($dbconnect,$sql);
	}

	//	VALUES (NULL, NULL, $temp, 23, 2);
	//	VALUES (NULL, NULL, $hum, 23, 0);
	if(isset($_GET["temp"])){
		$value = $_GET["temp"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',23,2,3)";
		mysqli_query($dbconnect,$sql);
	}
	if(isset($_GET["hum"])){
		$value = $_GET["hum"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',23,0,3)";
		mysqli_query($dbconnect,$sql);
	}

	//  5   12(pa)      1   	BMP280	Pressure & Temperature
	//	VALUES (NULL, NULL, $press, 12, 5);
	if(isset($_GET["press"])){
		$value = $_GET["press"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',12,5,6)";
		mysqli_query($dbconnect,$sql);
	}
	if(isset($_GET["alt"])){
		$value = $_GET["alt"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',12,4,6)";
		mysqli_query($dbconnect,$sql);
	}

	//	4   4(cm)       100 	HC-SR04	Ultrasonic distance
	//	VALUES (NULL, NULL, $dist/100, 4, 4);
	if(isset($_GET["dist"])){
		$value = $_GET["dist"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value/100 . "',4,4,5)";
		mysqli_query($dbconnect,$sql);
	}

	//	6	31          1       YL-69	Soil Humidity
	//	VALUES (NULL, NULL, $hyg, 31, 6);
	if(isset($_GET["hyg"])){
		$value = $_GET["hyg"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',31,6,7)";
		mysqli_query($dbconnect,$sql);
	}

	//	3   30  	    1       HC-SR501	Passive Infra Red
	//	VALUES (NULL, NULL, $hum, 30, 3);
	if(isset($_GET["pir"])){
		$value = $_GET["pir"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,3,4)";
		mysqli_query($dbconnect,$sql);
	}

	//	7	16(V),1(A)  1       MAX471	Voltage & Current
	//	VALUES (NULL, NULL, $volt, 16, 7);
	//	VALUES (NULL, NULL, $curr, 1, 7);
	if(isset($_GET["volt"])){
		$value = $_GET["volt"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',16,7,8)";
		mysqli_query($dbconnect,$sql);
	}
	if(isset($_GET["curr"])){
		$value = $_GET["curr"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',1,7,8)";
		mysqli_query($dbconnect,$sql);
	}

	//	8	30          1           MQ2	Methane, Butane, LPG, smoke
	//	VALUES (NULL, NULL, $mq2, 30, 8);
	if(isset($_GET["mq2"])){
		$value = $_GET["mq2"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,8,9)";
		mysqli_query($dbconnect,$sql);
	}

	//	9	30          1       	MQ3	Alcohol, Ethanol, smoke
	//	VALUES (NULL, NULL, $mq3, 30, 9);
	if(isset($_GET["mq3"])){
		$value = $_GET["mq3"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,9,10)";
		mysqli_query($dbconnect,$sql);
	}

	//	10	30          1       	MQ4	Methane, CNG Gas
	//	VALUES (NULL, NULL, $mq4, 30, 10);
	if(isset($_GET["mq4"])){
		$value = $_GET["mq4"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,10,11)";
		mysqli_query($dbconnect,$sql);
	}

	//	11	30          1       	MQ5	Natural gas, LPG
	//	VALUES (NULL, NULL, $mq5, 30, 11);
	if(isset($_GET["mq5"])){
		$value = $_GET["mq5"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,11,12)";
		mysqli_query($dbconnect,$sql);
	}

	//	12	30          1       	MQ6	LPG, butane gas
	//	VALUES (NULL, NULL, $mq6, 30, 12);
	if(isset($_GET["mq6"])){
		$value = $_GET["mq6"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,12,13)";
		mysqli_query($dbconnect,$sql);
	}

	//	13	30          1       	MQ7	Carbon Monoxide
	//	VALUES (NULL, NULL, $mq7, 30, 13);
	if(isset($_GET["mq7"])){
		$value = $_GET["mq7"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,13,14)";
		mysqli_query($dbconnect,$sql);
	}

	//	14	30          1       	MQ8	Hydrogen Gas
	//	VALUES (NULL, NULL, $mq8, 30, 14);
	if(isset($_GET["mq8"])){
		$value = $_GET["mq8"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,14,15)";
		mysqli_query($dbconnect,$sql);
	}

	//	15	30          1       	MQ9	Carbon Monoxide, flammable gasses
	//	VALUES (NULL, NULL, $mq9, 30, 15);
	if(isset($_GET["mq9"])){
		$value = $_GET["mq9"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,15,16)";
		mysqli_query($dbconnect,$sql);
	}

	//	16	30          1       	MQ131	Ozone
	//	VALUES (NULL, NULL, $mq131, 30, 16);
	if(isset($_GET["mq131"])){
		$value = $_GET["mq131"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,16,17)";
		mysqli_query($dbconnect,$sql);
	}

	//	17	30          1       	MQ135	Benzene, Alcohol, smoke
	//	VALUES (NULL, NULL, $mq135, 30, 17);
	if(isset($_GET["mq135"])){
		$value = $_GET["mq135"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,17,18)";
		mysqli_query($dbconnect,$sql);
	}

	//	18	30          1       	MQ136	Hydrogen Sulfide gas
	//	VALUES (NULL, NULL, $mq136, 30, 18);
	if(isset($_GET["mq136"])){
		$value = $_GET["mq136"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,18,19)";
		mysqli_query($dbconnect,$sql);
	}

	//	19	30          1       	MQ137 Ammonia
	//	VALUES (NULL, NULL, $mq136, 30, 19);
	if(isset($_GET["mq137"])){
		$value = $_GET["mq137"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,19,20)";
		mysqli_query($dbconnect,$sql);
	}

	//	20	30          1       	MQ138	Benzene, Toluene, Alcohol, Acetone, Propane, Formaldehyde gas, Hydrogen gas
	//	VALUES (NULL, NULL, $mq138, 30, 20);
	if(isset($_GET["mq138"])){
		$value = $_GET["mq138"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,20,21)";
		mysqli_query($dbconnect,$sql);
	}

	//	21	30          1       	MQ214	Methane, Natural gas
	//	VALUES (NULL, NULL, $mq214, 30, 21);
	if(isset($_GET["mq214"])){
		$value = $_GET["mq214"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,21,22)";
		mysqli_query($dbconnect,$sql);
	}

	//	22	30          1       	MQ216	Natural gas, Coal gas
	//	VALUES (NULL, NULL, $mq216, 30, 22);
	if(isset($_GET["mq216"])){
		$value = $_GET["mq216"];
		$sql = $sqlBase . "VALUES (null,CURRENT_TIMESTAMP,'" . $value . "',30,22,23)";
		mysqli_query($dbconnect,$sql);
	}

?>
